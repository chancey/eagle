#ifndef eagle_test_h
#define eagle_test_h

#include "eagle/EagleLogger.h"

#define ASSERT_LAST_ERROR(msg) { \
    EagleLogger::Event *event = EagleLogger::LastEvent(); \
    ASSERT_FALSE(NULL == event); \
    if(NULL != event) { \
        ASSERT_STREQ(msg, event->getMessage().c_str()); \
    } \
}

#endif
