#include <sys/time.h>
#include "gtest/gtest.h"
#include "eagle/EaglePage.h"

namespace eagle
{
    
    TEST(EaglePageTest, EaglePage) {
        EagleDataTypeFloat *testData = new EagleDataTypeFloat[1];
        ::EaglePage p(EagleData::Type::Float, testData, 2, 1, 500, true);
        EXPECT_EQ(EagleData::Type::Float, p.type);
        EXPECT_EQ(500, p.recordOffset);
        EXPECT_EQ(2, p.totalSize);
        EXPECT_EQ(1, p.count);
        EXPECT_EQ(testData, p.data);
        EXPECT_TRUE(p.freeData);
        EXPECT_EQ(1, p.usageCount);
    }
    
    class EaglePagePublic : public ::EaglePage
    {
    public:
        EaglePagePublic(EagleData::Type theType, void *theData, int theTotalSize, int theCount, int theRecordOffset,
                        bool theFreeData) : EaglePage(theType, theData, theTotalSize, theCount, theRecordOffset,
                                                      theFreeData)
        {
        }
        
        void init_(EagleData::Type theType, void *theData, int theTotalSize, int theCount, int theRecordOffset,
                  bool theFreeData, int theUsageCount)
        {
            init(theType, theData, theTotalSize, theCount, theRecordOffset, theFreeData, theUsageCount);
        }
    };
    
    TEST(EaglePageTest, init) {
        EagleDataTypeFloat *testData = new EagleDataTypeFloat[1];
        EagleDataTypeFloat *otherTestData = new EagleDataTypeFloat[1];
        EaglePagePublic p(EagleData::Type::Integer, otherTestData, 20, 10, 600, false);
        p.init_(EagleData::Type::Float, testData, 2, 1, 500, true, 1);
        EXPECT_EQ(EagleData::Type::Float, p.type);
        EXPECT_EQ(500, p.recordOffset);
        EXPECT_EQ(2, p.totalSize);
        EXPECT_EQ(1, p.count);
        EXPECT_EQ(testData, p.data);
        EXPECT_TRUE(p.freeData);
        EXPECT_EQ(1, p.usageCount);
        delete otherTestData;
    }
    
}
