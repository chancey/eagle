#include "gtest/gtest.h"
#include "eagle/Worker.h"

namespace eagle
{
    
    TEST(WorkerTest, SetForCurrentThread) {
        eagle::Worker worker(0, NULL);
        eagle::Worker::SetForCurrentThread(&worker);
        EXPECT_EQ(&worker, eagle::Worker::GetForCurrentThread());
    }
    
}
