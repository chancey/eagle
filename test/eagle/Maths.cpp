#include "gtest/gtest.h"
#include "eagle/Maths.h"

namespace eagle
{
    
    TEST(MathsTest, Min) {
        EXPECT_EQ(Maths::Min(123, 456), 123);
        EXPECT_EQ(Maths::Min(456, 123), 123);
    }
    
}
