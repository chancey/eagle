#include "gtest/gtest.h"
#include "eagle/PageProvider.h"

namespace eagle
{
    
    TEST(PageProviderTest, TotalPages) {
        EXPECT_EQ(PageProvider::TotalPages(1000, 1000), 1);
        EXPECT_EQ(PageProvider::TotalPages(1001, 1000), 2);
        EXPECT_EQ(PageProvider::TotalPages(1000, 10), 100);
    }
    
}
