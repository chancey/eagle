#include <sys/time.h>
#include "gtest/gtest.h"
#include "eagle/EagleLogger.h"

namespace eagle
{
    
    /**
     * Test the default values created by the constructor for eagle::EagleLogger::Event.
     */
    TEST(EagleLoggerTest, Event) {
        EagleLogger::Event event(EagleLogger::Severity::Debug, "some message");
        EXPECT_NEAR(event.getWhen(), time(NULL), 1.0);
        EXPECT_STREQ(event.getMessage().c_str(), "some message");
        EXPECT_EQ(event.getSeverity(), EagleLogger::Severity::Debug);
        EXPECT_EQ(NULL, event.getObject());
    }
    
    TEST(EagleLoggerTest, setWhen) {
        EagleLogger::Event event(EagleLogger::Severity::Debug, "some message");
        event.setWhen(123);
        EXPECT_EQ(123, event.getWhen());
    }
    
}
