#include "gtest/gtest.h"
#include "eagle/db/sql/Insert.h"

namespace eagle
{
    namespace db
    {
        namespace sql
        {
            
            TEST(InsertTest, Instantiate) {
                eagle::db::sql::Insert insert("abc");
                EXPECT_EQ("abc", insert.tableName);
                EXPECT_EQ(0, insert.names.length());
                EXPECT_EQ(0, insert.values.length());
            }
            
        }
    }
}
