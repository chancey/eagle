#include "gtest/gtest.h"
#include "eagle/db/Table.h"
#include "eagle/EagleData.h"

namespace eagle
{
    
    namespace db
    {
        
        TEST(TableTest, Instantiate)
        {
            eagle::db::Table *table = new eagle::db::Table("mytable");
            table->addColumn(new eagle::db::Column("col1", EagleData::Type::Integer));
            table->addColumn(new eagle::db::Column("col2", EagleData::Type::Integer));
            
            EXPECT_STREQ("mytable", table->name);
            EXPECT_EQ(2, table->countColumns());
            EXPECT_STREQ("col1", table->getColumn(0).name.c_str());
            EXPECT_EQ(EagleData::Type::Integer, table->getColumn(0).type);
            EXPECT_STREQ("col2", table->getColumn(1).name.c_str());
            EXPECT_EQ(EagleData::Type::Integer, table->getColumn(1).type);
            
            eagle_db_Table_DeleteWithColumns(table);
        }
        
        TEST(TableTest, eagle_db_Table_DeleteWithColumns)
        {
            eagle_db_Table_DeleteWithColumns(NULL);
        }
        
        TEST(TableTest, getColumnIndex)
        {
            eagle::db::Table *table = new eagle::db::Table("bla");
            table->addColumn(new eagle::db::Column("bla", EagleData::Type::Integer));
            
            EXPECT_EQ(-1, table->getColumnIndex((char*) "nope"));
            
            eagle_db_Table_DeleteWithColumns(table);
        }
        
    }
    
}
