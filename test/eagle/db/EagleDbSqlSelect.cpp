#include "gtest/gtest.h"
#include "eagledb/EagleDbSqlSelect.h"

namespace eagle
{
    
    class EagleDbSqlSelectTest : public ::testing::Test
    {
        // definitions for doxygen
        void getExpressionsCountWithoutWhere();
        void getExpressionsCountWithWhere();
        
    public:
        EagleDbSqlSelect* getSelectForExpressionsCount();
    };
    
    EagleDbSqlSelect* EagleDbSqlSelectTest::getSelectForExpressionsCount()
    {
        EagleDbSqlSelect *select = EagleDbSqlSelect_New();
        select->selectExpressions = new EagleSynchronizedLinkedList<EagleDbSqlExpression*>();
        return select;
    }
    
    TEST_F(EagleDbSqlSelectTest, getExpressionsCountWithoutWhere)
    {
        EagleDbSqlSelect *select = getSelectForExpressionsCount();
        int count = EagleDbSqlSelect_getExpressionsCount(select);
        EXPECT_EQ(0, count);
        delete select->selectExpressions;
        delete select;
    }
    
    TEST_F(EagleDbSqlSelectTest, getExpressionsCountWithWhere)
    {
        EagleDbSqlSelect *select = getSelectForExpressionsCount();
        select->whereExpression = (EagleDbSqlExpression*) EagleDbSqlValue_NewWithInteger(123);
        int count = EagleDbSqlSelect_getExpressionsCount(select);
        EXPECT_EQ(1, count);
        delete select->whereExpression;
        delete select->selectExpressions;
        delete select;
    }
    
}
