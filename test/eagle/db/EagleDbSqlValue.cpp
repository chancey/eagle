#include "gtest/gtest.h"
#include "eagledb/EagleDbSqlValue.h"

namespace eagle
{
    
    class EagleDbSqlValueTest : public ::testing::Test
    {
        // definitions for doxygen
        void getIntegerWithString();
    };
    
    TEST_F(EagleDbSqlValueTest, getIntegerWithString)
    {
        bool success;
        EagleDbSqlValue *value = EagleDbSqlValue_NewWithString((char*) "abc", false);
        EXPECT_EQ(0, EagleDbSqlValue_getInteger(value, &success));
        EXPECT_FALSE(success);
        delete value;
    }
    
}
