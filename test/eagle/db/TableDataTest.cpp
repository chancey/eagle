#include "gtest/gtest.h"
#include "eagle/db/TableData.h"
#include "eagle/EagleData.h"

namespace eagle
{
    
    namespace db
    {
            
        TEST(TableDataTest, Delete)
        {
            eagle::db::Table *table = new eagle::db::Table("mytable");
            table->addColumn(new eagle::db::Column("a", EagleData::Type::Integer));
            
            eagle::db::TableData td(table, 1);
            eagle_db_Table_DeleteWithColumns(table);
        }
        
        TEST(TableDataTest, insert)
        {
            eagle::db::Table *table = new eagle::db::Table("mytable");
            table->addColumn(new eagle::db::Column("a", EagleData::Type::Integer));
            
            eagle::db::TableData td(table, 1);
            
            eagle::db::Tuple *tuple = new eagle::db::Tuple(table);
            tuple->setInteger(0, 123);
            
            td.providers[0]->type = EagleData::Type::Unknown;
            td.insert(tuple);
            delete tuple;
            
            eagle_db_Table_DeleteWithColumns(table);
        }
        
    }
    
}
