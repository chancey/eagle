#include "gtest/gtest.h"
#include "eagle/db/Tuple.h"
#include "eagle/EagleData.h"
#include "eagle/EagleLogger.h"
#include "test.h"

namespace eagle
{
    
    namespace db
    {
        
        class TupleTestFixture : public ::testing::Test
        {
            
        public:
            
            virtual void SetUp()
            {
            }
            
            virtual void TearDown()
            {
            }
            
            eagle::db::Table* _getTable()
            {
                /*
                 CREATE TABLE mytable (
                 col1 INT,
                 col2 INT
                 );
                 */
                eagle::db::Table *table = new eagle::db::Table("mytable");
                table->addColumn(new eagle::db::Column("col1", EagleData::Type::Integer));
                table->addColumn(new eagle::db::Column("col2", EagleData::Type::Varchar));
                table->addColumn(new eagle::db::Column("col3", EagleData::Type::Float));
                
                return table;
            }
            
        };
        
        TEST_F(TupleTestFixture, Instantiate)
        {
            eagle::db::Table *table = _getTable();
            
            eagle::db::Tuple *tuple = new eagle::db::Tuple(table);
            tuple->setInteger(0, 123);
            tuple->setVarchar(1, (char*) "hello");
            tuple->setFloat(2, 123.456);
            
            char *desc = tuple->toString();
            EXPECT_STREQ("(col1=123,col2=\"hello\",col3=123.456)", desc);
            delete desc;
            
            delete tuple;
            eagle_db_Table_DeleteWithColumns(table);
        }
        
        TEST(TupleTest, toString)
        {
            eagle::db::Table *table = new eagle::db::Table("mytable");
            table->addColumn(new eagle::db::Column("a", EagleData::Type::Unknown));
            
            eagle::db::Tuple *tuple = new eagle::db::Tuple(table);
            char *desc = tuple->toString();
            EXPECT_STREQ("(a=?)", desc);
            free(desc);
            
            delete tuple;
            eagle_db_Table_DeleteWithColumns(table);
        }
        
        class TupleTestFixture2 : public ::testing::Test
        {
            
        protected:
            
            eagle::db::Table *table;
            
        public:
            
            virtual void SetUp()
            {
                EagleLogger::Get()->disableOutHandle();
                table = new eagle::db::Table("mytable");
            }
            
            virtual void TearDown()
            {
                eagle_db_Table_DeleteWithColumns(table);
            }
            
        };
        
        TEST_F(TupleTestFixture2, setInteger)
        {
            table->addColumn(new eagle::db::Column("a", EagleData::Type::Varchar));
            
            eagle::db::Tuple tuple(table);
            tuple.setInteger(0, 123);
            ASSERT_LAST_ERROR("Wrong type.");
        }
        
        TEST_F(TupleTestFixture2, setVarchar)
        {
            table->addColumn(new eagle::db::Column("a", EagleData::Type::Integer));
            
            eagle::db::Tuple tuple(table);
            tuple.setVarchar(0, (char*) "123");
            ASSERT_LAST_ERROR("Wrong type.");
        }
        
        TEST_F(TupleTestFixture2, setFloat)
        {
            table->addColumn(new eagle::db::Column("a", EagleData::Type::Varchar));
            
            eagle::db::Tuple tuple(table);
            tuple.setFloat(0, 123.0);
            ASSERT_LAST_ERROR("Wrong type.");
        }
        
        TEST_F(TupleTestFixture2, set)
        {
            table->addColumn(new eagle::db::Column("col1", EagleData::Type::Unknown));
            
            eagle::db::Tuple tuple(table);
            EXPECT_FALSE(tuple.set(0, NULL, EagleData::Type::Unknown));
        }
        
    }
    
}
