#include "gtest/gtest.h"
#include "eagle/db/Column.h"
#include "eagle/EagleData.h"

namespace eagle
{
    
    namespace db
    {
            
        TEST(ColumnTest, Instantiate)
        {
            Column col("mycol", EagleData::Type::Integer);
            EXPECT_STREQ(col.name.c_str(), "mycol");
            EXPECT_EQ(col.type, EagleData::Type::Integer);
        }
        
    }
    
}
