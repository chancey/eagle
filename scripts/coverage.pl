#!/bin/perl

use List::Util qw(first);
open(DOC, "coverage/index.html");
my $file = join("", <DOC>);
my @files = ($file =~ m/<td class="coverFile"><a href=".*">(.*)<\/a><\/td>/g);
my @covs = ($file =~ m/<td class=".*">(\d+ \/ \d+)<\/td>/g);

# count up all the totals
my ($linesCovered, $linesTotal);
for(my $i = 0; $i < @files; ++$i) {
	next if(substr($files[$i], 0, 6) ne 'eagle/');
	my @parts = split(" / ", $covs[$i * 2]);
	$linesCovered += $parts[0];
	$linesTotal += $parts[1];
}
print STDERR "Coverage (lines): $linesCovered / $linesTotal\n";
if($linesCovered < $linesTotal) {
	printf STDERR "Error: Coverage (%.3g%%) is below minimum the coverage (100%).\n", $linesCovered / $linesTotal * 100;
	exit(1);
}
