#!/bin/bash
set -e

BAMBOO="`pwd`"
NAME="eagle"
[ -z "$BRANCH" ] && BRANCH="`git branch | grep "* " | grep -v master | cut -c3- | tr '-' '_'`"
[[ "$BRANCH" == "master" ]] && BRANCH=""
[ ! -z "$BRANCH" ] && NAME="$NAME-$BRANCH"
VERSION="`git describe --tags --match v* --abbrev=0 | cut -c2-`"
UNIQUE="`git describe --tags --match v* --abbrev=1 | cut -d- -f2 -s`"
RELEASE=${UNIQUE:-1}
FOLDER=$NAME-$VERSION

echo "NAME=$NAME"
echo "VERSION=$VERSION"
echo "RELEASE=$RELEASE"
echo "FOLDER=$FOLDER"

# prepare the folders for the RPM
mkdir -p ~/rpmbuild
mkdir -p ~/rpmbuild/{RPMS,SRPMS,BUILD,SOURCES,SPECS,tmp}

# install macros if we need to
cat <<EOF >~/.rpmmacros
%_topdir   %(echo $HOME)/rpmbuild
%_tmppath  %{_topdir}/tmp
EOF

# begin
cd ~/rpmbuild

# create tarball
rm -rf $FOLDER
mkdir $FOLDER
mkdir -p $FOLDER/usr/bin
install -m 755 $BAMBOO/bin/eagle $FOLDER/usr/bin

tar -zcvf $FOLDER.tar.gz $FOLDER/

# copy to the sources dir
cp $FOLDER.tar.gz SOURCES/

cat <<EOF > SPECS/eagle.spec
%define        __spec_install_post %{nil}
%define          debug_package %{nil}
%define        __os_install_post %{_dbpath}/brp-compress

Summary: eagle is a highly parallel column-oriented embedded SQL database.
Name: $NAME
Version: $VERSION
Release: $RELEASE
License: WTFPL
Group: Applications/Databases
SOURCE0 : %{name}-%{version}.tar.gz
URL: https://elliot.atlassian.net/browse/EDB

BuildRoot: %{_tmppath}/%{name}-%{version}-root

%description
%{summary}

%prep
%setup -q

%build
# Empty section.

%install
rm -rf %{buildroot}
mkdir -p  %{buildroot}

# in builddir
cp -a * %{buildroot}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_bindir}/*
EOF

cat SPECS/eagle.spec

# build the RPM
rpmbuild -ba SPECS/eagle.spec

# copy back to bamboo to be collected as an artifact
mv -f ~/rpmbuild/RPMS/x86_64/`ls -1 ~/rpmbuild/RPMS/x86_64/ | head -n 1` $BAMBOO/eagle.rpm
