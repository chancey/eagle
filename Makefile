SHELL := /bin/bash
LEX = lex
BISON = bison
CXX = g++
INCLUDE_PATH = -I. -Ieagle -Ieagle_test -Itest
OS ?= $(shell uname | tr '[A-Z]' '[a-z]')
LDFLAGS =
CXXFLAGS = -std=c++0x
LIB_PATH =

eagle_SOURCES = $(shell find eagle/eagle eagle/eagledb -name "*.cpp")
eagle_OBJECTS = $(eagle_SOURCES:.cpp=.o)

eagle_test_SOURCES = $(shell find eagle_test/suites -name "*.cpp") eagle_test/TestSuite.cpp
eagle_test_OBJECTS = $(eagle_test_SOURCES:.cpp=.o)

sqlparser_SOURCES = eagle/parser.cpp eagle/tokens.cpp
sqlparser_OBJECTS = $(sqlparser_SOURCES:.cpp=.o)

test_SOURCES = $(shell find test -name "*.cpp")
test_OBJECTS = $(test_SOURCES:.cpp=.o)

include makefiles/Makefile.$(OS)

all: bin/eagle

.PHONY: test
test: bin/test bin/eagle_test

mkdirs:
	mkdir -p bin

bin/eagle : CXXFLAGS += -Werror -pedantic -pedantic-errors -Wall -Wextra -Wformat=2 -Winit-self -Wuninitialized \
	-Wmissing-include-dirs -Wswitch-enum -Wunused-parameter -Wshadow -Wcast-qual -Wconversion \
	-Wmissing-declarations -Wpacked -Wredundant-decls -Winvalid-pch -O3
bin/eagle: mkdirs $(sqlparser_OBJECTS) $(eagle_OBJECTS) eagle/main.o
	$(CXX) $(CXXFLAGS) $(INCLUDE_PATH) $(LIB_PATH) $(LDFLAGS) -o $@ eagle/main.o $(sqlparser_OBJECTS) $(eagle_OBJECTS)
    
bin/eagle.debug : CXXFLAGS += -D CUNIT -O0 -fprofile-arcs -ftest-coverage -Wno-write-strings -g
bin/eagle.debug: mkdirs $(sqlparser_OBJECTS) $(eagle_OBJECTS) eagle/main.o
	$(CXX) $(CXXFLAGS) $(INCLUDE_PATH) $(LIB_PATH) $(LDFLAGS) -o $@ eagle/main.o $(sqlparser_OBJECTS) $(eagle_OBJECTS)

bin/test : CXXFLAGS += -O0 -D CUNIT -fprofile-arcs -ftest-coverage -Wno-write-strings -g
bin/test: mkdirs $(test_OBJECTS) $(eagle_OBJECTS) $(sqlparser_OBJECTS)
	$(CXX) $(CXXFLAGS) $(INCLUDE_PATH) $(LIB_PATH) $(LDFLAGS) -o $@ test/gtest/gtest-all.cc $(test_OBJECTS) $(eagle_OBJECTS) $(sqlparser_OBJECTS)

bin/eagle_test : CXXFLAGS += -D CUNIT -O0 -fprofile-arcs -ftest-coverage -Wno-write-strings -g
bin/eagle_test: mkdirs $(eagle_test_OBJECTS) $(eagle_OBJECTS) $(sqlparser_OBJECTS) eagle_test/main.o
	$(CXX) $(CXXFLAGS) $(INCLUDE_PATH) $(LIB_PATH) $(LDFLAGS) $(eagle_test_OBJECTS) $(eagle_OBJECTS) $(sqlparser_OBJECTS) eagle_test/main.o -lcunit -o $@

install:
	install bin/eagle /usr/bin/eagle

clean:
	- rm -rf eagle/main.o
	- rm -rf bin
	- find . -name "*.o" -print0 | xargs -0 rm -f
	- find . -name "*.gcno" -print0 | xargs -0 rm -f
	- find . -name "*.gcda" -print0 | xargs -0 rm -f
	- find . -name "*.gcov" -print0 | xargs -0 rm -f

eagle/parser.cpp: eagle/sql.ypp
	$(BISON) -y -d eagle/sql.ypp
	$(BISON) -d -o eagle/parser.cpp eagle/sql.ypp

eagle/tokens.cpp: eagle/sql.lpp
	$(LEX) -o $@ eagle/sql.lpp

eagle/parser.o: eagle/parser.cpp
	$(CXX) -std=c++0x -Wno-write-strings -o $@ -c eagle/parser.cpp $(INCLUDE_PATH)

eagle/tokens.o: eagle/tokens.cpp
	$(CXX) -std=c++0x -o $@ -c eagle/tokens.cpp $(INCLUDE_PATH)

%.o: %.cpp
	$(CXX) $(CXXFLAGS) $(INCLUDE_PATH) -o $@ -c $<
