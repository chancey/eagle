include makefiles/Makefile.linux

eagle/parser.o : INCLUDE_PATH += -g
eagle/tokens.o : INCLUDE_PATH += -g

bamboo-cli-tests: bin/eagle
	chmod +x bin/eagle scripts/cli.sh
	./scripts/cli.sh

bamboo-cli-tests-post:
	# Do nothing

bamboo-code-coverage:
	#=== Compile. We have to compile here to get all the object files
	make bin/eagle_test bin/test

	#=== Run CUNIT
	chmod +x bin/eagle_test
	bin/eagle_test --all-suites --exclude-sqlfuzz --exclude-sql

	#=== Run gtest
	chmod +x bin/test
	bin/test

	#=== Generate code coverage report
	scripts/geninfo -q --no-checksum --base-directory . --output-filename coverage.info .
	scripts/genhtml --sort --no-branch-coverage -q -s -t eagle --legend -o coverage coverage.info
	perl scripts/coverage.pl

bamboo-code-coverage-post:
	#=== Clover
	find eagle eagle_test test/eagle -name "*.cpp" | xargs -I file gcov file -o file | rev | cut -d/ -f2- | rev
	python scripts/gcov_to_clover.py -o coverage/clover.xml *.gcov

bamboo-documentation:
	#=== Generate docs
	rm -rf doc/*
	if [ `doxygen 2>&1 | grep Warning | wc -l` -gt 0 ]; then \
        doxygen 1>&2; \
        exit 1; \
    fi

bamboo-documentation-post:
    #=== Validate docs
	php scripts/doxygen.php

bamboo-memory-leaks: bin/test bin/eagle_test
	chmod +x bin/test bin/eagle_test
	valgrind --leak-check=full bin/eagle_test --all-suites
	valgrind --leak-check=full bin/test

bamboo-memory-leaks-post:
	# Do nothing

bamboo-unit-tests-cunit:
	chmod +x bin/eagle_test
	bin/eagle_test --all-suites --mode=automated

bamboo-unit-tests-gtest:
	chmod +x bin/test
	bin/test --gtest_output=xml:build/gtest.xml

bamboo-unit-tests:
	mkdir -p build
	- make bamboo-unit-tests-cunit
	- make bamboo-unit-tests-gtest

bamboo-unit-tests-post:
	#=== Convert cunit to junit
	xsltproc --novalid scripts/cunit-to-junit.xsl build/CUnit-Results.xml > build/JUnit.xml

bamboo-build-rpm:
	chmod +x scripts/build-rpm.sh
	./scripts/build-rpm.sh

bamboo-build-rpm-post:
	# Do nothing
