# install default packages
sudo yum install -y bison flex doxygen expect elfutils patch gdb
sudo rpm --nodeps -i ftp://ftp.sunet.se/pub/Linux/distributions/scientific/6rolling/x86_64/os/Packages/rpm-build-4.8.0-32.el6.x86_64.rpm

# the default valgrind in Amazon Linux is only 3.6
# use the latest from the website
sudo yum install -y ftp://rpmfind.net/linux/centos/6.4/os/x86_64/Packages/valgrind-3.8.1-3.2.el6.x86_64.rpm

# install s3cmd
sudo curl http://s3tools.org/repo/CentOS_5/s3tools.repo > /etc/yum.repos.d/s3tools.repo
yum install -y s3cmd

# install CUnit
cd /tmp
wget http://downloads.sourceforge.net/project/cunit/CUnit/2.1-2/CUnit-2.1-2-src.tar.bz2
tar jxf CUnit-2.1-2-src.tar.bz2
cd CUnit-2.1-2
./configure
make
sudo make install
sudo cp /usr/local/lib/libcunit* /lib64

# fix up git
git config --global user.name "Elliot Chance"
git config --global user.email elliotchance@gmail.com

# install argparse (needed for gcov_to_clover.py)
sudo easy_install argparse
