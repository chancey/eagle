#include <stdio.h>
#include <stdlib.h>

#include "eagledb/EagleDbConsole.h"

/**
 * Entry point.
 * @return Exit status.
 */
int main(int, const char**)
{
    EagleDbConsole *console = EagleDbConsole_New();
    EagleDbConsole_run();
    EagleDbConsole_Delete(console);
    
    return 0;
}
