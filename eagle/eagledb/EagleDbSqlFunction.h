#ifndef eagle_EagleDbSqlFunction_h
#define eagle_EagleDbSqlFunction_h

#include "eagle/EaglePageOperations.h"

/**
 * Construct a function definition with a single argument.
 */
#define EagleDbSqlFunction_Make1(__f, __name, __return, __type1) EagleDbSqlFunction_NewWithSingleArgument(EaglePageOperations_##__f, __name, EagleData::Type::__return, EagleData::Type::__type1)

/**
 * Construct a function definition with two arguments.
 */
#define EagleDbSqlFunction_Make2(__f, __name, __return, __type1, __type2) EagleDbSqlFunction_NewWithTwoArguments(EaglePageOperations_##__f, __name, EagleData::Type::__return, EagleData::Type::__type1, EagleData::Type::__type2)

/**
 * Construct a function definition with four arguments.
 */
#define EagleDbSqlFunction_Make4(__f, __name, __return, __type1, __type2, __type3, __type4) EagleDbSqlFunction_NewWithFourArguments(EaglePageOperations_##__f, __name, EagleData::Type::__return, EagleData::Type::__type1, EagleData::Type::__type2, EagleData::Type::__type3, EagleData::Type::__type4)

/**
 * An expression function.
 */
typedef struct EagleDbSqlFunction {
    
    /**
     * A pointer to the internal function.
     */
    EaglePageOperationFunction(function);
    
    /**
     * NULL-terminated name of the function (eg. "sqrt")
     */
    char *name;
    
    /**
     * The number of fixed arguments this function will accept.
     */
    int argCount;
    
    /**
     * The data type for each argument, as per \c argCount
     */
    EagleData::Type *argTypes;
    
    /**
     * The return type given out from the function.
     */
    EagleData::Type returnType;
    
} EagleDbSqlFunction;

/**
 * Create a new function definition.
 * @param [in] function Function pointer.
 * @param [in] name The name of the function.
 * @param [in] returnType The return type given out from the function.
 * @param [in] argCount The number of fixed arguments this function will accept.
 * @param [in] argTypes The argument types as per \p argCount
 * @return A new function definition.
 */
EagleDbSqlFunction* EagleDbSqlFunction_New(void (*function)(EaglePage *destination, EaglePage *source1, EaglePage *source2, void *obj),
                                           const char *name,
                                           EagleData::Type returnType,
                                           int argCount,
                                           EagleData::Type *argTypes);

/**
 * Shortcut for EagleDbSqlFunction_New() with one argument.
 * @param [in] function Function pointer.
 * @param [in] name The name of the function.
 * @param [in] returnType The return type given out from the function.
 * @param [in] argType The type for the first and only argument.
 * @return A new function definition.
 */
EagleDbSqlFunction* EagleDbSqlFunction_NewWithSingleArgument(void (*function)(EaglePage *destination, EaglePage *source1, EaglePage *source2, void *obj),
                                                             const char *name,
                                                             EagleData::Type returnType,
                                                             EagleData::Type argType);

/**
 * Shortcut for EagleDbSqlFunction_New() with two arguments.
 * @param [in] function Function pointer.
 * @param [in] name The name of the function.
 * @param [in] returnType The return type given out from the function.
 * @param [in] argType1 The type for the first argument.
 * @param [in] argType2 The type for the second argument.
 * @return A new function definition.
 */
EagleDbSqlFunction* EagleDbSqlFunction_NewWithTwoArguments(void (*function)(EaglePage *destination, EaglePage *source1, EaglePage *source2, void *obj),
                                                           const char *name,
                                                           EagleData::Type returnType,
                                                           EagleData::Type argType1,
                                                           EagleData::Type argType2);

/**
 * Shortcut for EagleDbSqlFunction_New() with four arguments.
 * @param [in] function Function pointer.
 * @param [in] name The name of the function.
 * @param [in] returnType The return type given out from the function.
 * @param [in] argType1 The type for the first argument.
 * @param [in] argType2 The type for the second argument.
 * @param [in] argType3 The type for the third argument.
 * @param [in] argType4 The type for the forth argument.
 * @return A new function definition.
 */
EagleDbSqlFunction* EagleDbSqlFunction_NewWithFourArguments(void (*function)(EaglePage *destination, EaglePage *source1, EaglePage *source2, void *obj),
                                                            const char *name,
                                                            EagleData::Type returnType,
                                                            EagleData::Type argType1,
                                                            EagleData::Type argType2,
                                                            EagleData::Type argType3,
                                                            EagleData::Type argType4);

/**
 * Delete (free) a function definition.
 * @param [in] f Function definition.
 */
void EagleDbSqlFunction_Delete(EagleDbSqlFunction *f);

/**
 * @brief Find a function definition from its signature.
 * You do not need to initialize the native function definition stack, this will be done automatically the first time.
 * @param [in] name The name of the function.
 * @param [in] types The argument types as per \p argCount
 * @param [in] argCount The number of fixed arguments this function will accept.
 * @return If the function defintion could be found it will be returned, otherwise NULL.
 */
EagleDbSqlFunction* EagleDbSqlFunction_Find(char *name, EagleData::Type *types, int argCount);

/**
 * Internal method to initialize the built-in function stack. If this is called more than once (which is shouldn't) it
 * will leak.
 */
void EagleDbSqlFunction_LoadBuiltIns_(void);

#endif
