#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <string>
#include "EagleDbSqlSelect.h"
#include "EagleDbSqlExpressionType.h"
#include "eagle/db/TableData.h"
#include "EagleDbInstance.h"
#include "eagle/plan/job/SelectJob.h"
#include "eagle/plan/stage/AbstractStage.h"
#include "eagle/plan/stage/SelectStage.h"
#include "eagle/plan/stage/EntireSort.h"

EagleDbSqlSelect* EagleDbSqlSelect_New(void)
{
    EagleDbSqlSelect *select = new EagleDbSqlSelect();
    
    select->expressionType = EagleDbSqlExpressionTypeSelect;
    select->whereExpression = NULL;
    select->selectExpressions = NULL;
    select->orderByExpression = NULL;
    
    return select;
}

void EagleDbSqlSelect_Delete(EagleDbSqlSelect *select)
{
    if(NULL == select) {
        return;
    }
    
    delete select;
}

void EagleDbSqlSelect_DeleteRecursive(EagleDbSqlSelect *select)
{
    if(NULL == select) {
        return;
    }
    
    delete select->selectExpressions;
    EagleDbSqlExpression_DeleteRecursive(select->whereExpression);
    EagleDbSqlValue_Delete(select->orderByExpression);
    delete select;
}

int EagleDbSqlSelect_getFieldCount(EagleDbSqlSelect *select)
{
    return select->selectExpressions->length();
}

int EagleDbSqlSelect_getExpressionsCount(EagleDbSqlSelect *select)
{
    //! @t eagle::EagleDbSqlSelectTest::getExpressionsCountWithoutWhere
    int exprs = EagleDbSqlSelect_getFieldCount(select);
    if(NULL != select->whereExpression) {
        //! @t eagle::EagleDbSqlSelectTest::getExpressionsCountWithWhere
        ++exprs;
    }
    return exprs;
}

eagle::Plan* EagleDbSqlSelect_parse(EagleDbSqlSelect *select, struct EagleDbInstance *db)
{
    int i;
    int exprCount, whereExpressionId = -1, expri = 0;
    EagleDbSqlExpression **expr;
    eagle::db::TableData *td;
    
    if(NULL == select) {
        return NULL;
    }
    if(NULL == db) {
        return NULL;
    }
    
    /* create the plan skeleton */
    eagle::Plan *plan = new eagle::Plan();
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(plan, db->pageSize, db->cores);
    plan->addStage(selectStage, true);
    
    /* get data */
    td = EagleDbInstance_getTable(db, select->tableName);
    if(NULL == td) {
        ((eagle::plan::stage::AbstractStage*) selectStage)->setError(EaglePlanErrorNoSuchTable, select->tableName);
        return plan;
    }
    if(NULL != td->table->columns) {
        for(i = 0; i < td->table->columns->length(); ++i) {
            eagle::PlanBufferProvider *bp;
            
            td->providers[i]->reset();
            bp = new eagle::PlanBufferProvider(i, td->providers[i], false);
            selectStage->addBufferProvider(bp, true);
        }
    }
    
    /* merge expressions */
    exprCount = EagleDbSqlSelect_getExpressionsCount(select);
    expr = new EagleDbSqlExpression*[exprCount];
    
    for(expri = 0; expri < EagleDbSqlSelect_getFieldCount(select); ++expri) {
        expr[expri] = (EagleDbSqlExpression*) select->selectExpressions->get(expri);
    }
    if(NULL != select->whereExpression) {
        whereExpressionId = expri;
        expr[whereExpressionId] = select->whereExpression;
    }
    
    // Translate the ORDER BY field in to the page provider we can use to sort later.
    if(NULL != select->orderByExpression) {
        // It would be really convienient if they put the field we want to sort by into the select itself so we can use
        // that provider directly.
        EagleLinkedList_Foreach(select->selectExpressions, EagleDbSqlExpression*, e)
        {
            if(e->expressionType == EagleDbSqlExpressionTypeValue &&
               ((EagleDbSqlValue*) e)->type == EagleDbSqlValueTypeIdentifier &&
               !strcmp(((EagleDbSqlValue*) e)->value.identifier, select->orderByExpression->value.identifier)) {
                // create the sort stage
                eagle::plan::stage::EntireSort *sortStage = new eagle::plan::stage::EntireSort(plan, db->cores);
                sortStage->setOrderByProviderId(_i_e);
                plan->addStage(sortStage, true);
                
                break;
            }
        }
        EagleLinkedList_ForeachEnd
    }
    
    /* compile plan */
    EagleDbSqlExpression_CompilePlan(expr, exprCount, whereExpressionId, selectStage);
    delete expr;
    
    return plan;
}

char* EagleDbSqlSelect_toString(EagleDbSqlSelect *select)
{
    EagleLinkedList<EagleDbSqlExpression*>::Item *item = NULL;
    std::string str("SELECT ");
    
    if(NULL != select->selectExpressions) {
        for(item = select->selectExpressions->begin(); item; item = item->getNext()) {
            char *s;
            if(item != select->selectExpressions->begin()) {
                str += ", ";
            }
            
            s = EagleDbSqlExpression_toString((EagleDbSqlExpression*) item->getObject());
            str += s;
            delete s;
        }
    }
    
    if(!select->tableName.empty()) {
        str += " FROM ";
        str += select->tableName;
    }
    
    if(NULL != select->whereExpression) {
        char *s = EagleDbSqlExpression_toString(select->whereExpression);
        str += " WHERE ";
        str += s;
        delete s;
    }
    
    if(NULL != select->orderByExpression) {
        char *s = EagleDbSqlValue_toString(select->orderByExpression);
        str += " ORDER BY ";
        str += s;
        delete s;
    }
    
    return strdup(str.c_str());
}
