#ifndef eagle_EagleDbConsole_h
#define eagle_EagleDbConsole_h

/**
 Console instance.
 */
typedef struct {
    
    /**
     The absolute time when the console was started.
     */
    unsigned long startTime;
    
} EagleDbConsole;

/**
 * Create a new console.
 * @return New console instance.
 */
EagleDbConsole* EagleDbConsole_New(void);

/**
 Begin the console input.
 */
void EagleDbConsole_run();

/**
 Free the console.
 @param [in] console Console instance.
 */
void EagleDbConsole_Delete(EagleDbConsole *console);

/**
 * Read a line from the user.
 * http://stackoverflow.com/a/314422/1470961
 * @return The line input from the user.
 */
char* EagleDbConsole_GetLine(void);

#endif
