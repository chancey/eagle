#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "EagleDbSqlValue.h"

EagleDbSqlValue* EagleDbSqlValue_NewWithInteger(EagleDataTypeInteger value)
{
    EagleDbSqlValue *v = new EagleDbSqlValue();
    
    v->expressionType = EagleDbSqlExpressionTypeValue;
    v->type = EagleDbSqlValueTypeInteger;
    v->value.intValue = value;
    
    return v;
}

EagleDbSqlValue* EagleDbSqlValue_NewWithFloat(EagleDataTypeFloat value)
{
    EagleDbSqlValue *v = new EagleDbSqlValue();
    
    v->expressionType = EagleDbSqlExpressionTypeValue;
    v->type = EagleDbSqlValueTypeFloat;
    v->value.floatValue = value;
    
    return v;
}

EagleDbSqlValue* EagleDbSqlValue_NewWithAsterisk(void)
{
    EagleDbSqlValue *v = new EagleDbSqlValue();
    
    v->expressionType = EagleDbSqlExpressionTypeValue;
    v->type = EagleDbSqlValueTypeAsterisk;
    
    return v;
}

EagleDbSqlValue* EagleDbSqlValue_NewWithIdentifier(char *identifier)
{
    EagleDbSqlValue *v = new EagleDbSqlValue();
    
    v->expressionType = EagleDbSqlExpressionTypeValue;
    v->type = EagleDbSqlValueTypeIdentifier;
    v->value.identifier = (NULL == identifier ? NULL : strdup(identifier));
    
    return v;
}

EagleDbSqlValue* EagleDbSqlValue_NewWithString(char *str, bool process)
{
    EagleDbSqlValue *v = new EagleDbSqlValue();
    
    v->expressionType = EagleDbSqlExpressionTypeValue;
    v->type = EagleDbSqlValueTypeString;
    
    if(false == process) {
        v->value.identifier = (NULL == str ? NULL : strdup(str));
    }
    else {
        /* process the string */
        unsigned long len, i, j;
        char *pstr;
        
        len = strlen(str);
        pstr = new char[len + 1];
        
        for(i = 1, j = 0; i < len - 1; ++i, ++j) {
            if((str[i] == '\\' && str[i + 1] == '\'') || (str[i] == '\'' && str[i + 1] == '\'')) {
                pstr[j] = '\'';
                ++i;
            }
            else {
                pstr[j] = str[i];
            }
        }
        pstr[j] = '\0';
        
        v->value.identifier = pstr;
    }
    
    return v;
}

void EagleDbSqlValue_Delete(EagleDbSqlValue *value)
{
    if(NULL == value) {
        return;
    }
    
    switch(value->type) {
            
        case EagleDbSqlValueTypeAsterisk:
        case EagleDbSqlValueTypeInteger:
        case EagleDbSqlValueTypeFloat:
            break;
            
        case EagleDbSqlValueTypeIdentifier:
        case EagleDbSqlValueTypeString:
            delete value->value.identifier;
            break;
            
    }
    
    delete value;
}

char* EagleDbSqlValue_toString(EagleDbSqlValue *value)
{
    char *r = NULL;
    
    switch(value->type) {
            
        case EagleDbSqlValueTypeInteger:
        {
            char buf[32];
            sprintf(buf, "%d", value->value.intValue);
            r = strdup(buf);
            break;
        }
            
        case EagleDbSqlValueTypeFloat:
        {
            char buf[32];
            sprintf(buf, "%g", value->value.floatValue);
            r = strdup(buf);
            break;
        }
            
        case EagleDbSqlValueTypeAsterisk:
            r = strdup("*");
            break;
            
        case EagleDbSqlValueTypeIdentifier:
            r = strdup(value->value.identifier);
            break;
            
        case EagleDbSqlValueTypeString:
        {
            char *buf = new char[strlen(value->value.identifier) + 3];
            sprintf(buf, "'%s'", value->value.identifier);
            r = buf;
            break;
        }

    }

    return r;
}

EagleDataTypeInteger EagleDbSqlValue_getInteger(EagleDbSqlValue *value, bool *success)
{
    EagleDataTypeInteger r = 0;
    
    switch(value->type) {
            
        case EagleDbSqlValueTypeAsterisk:
        case EagleDbSqlValueTypeIdentifier:
            
        //! @t eagle::EagleDbSqlValueTest::getIntegerWithString
        case EagleDbSqlValueTypeString:
            *success = false;
            break;
            
        case EagleDbSqlValueTypeFloat:
            *success = true;
            r = (EagleDataTypeInteger) value->value.floatValue;
            break;
            
        case EagleDbSqlValueTypeInteger:
            *success = true;
            r = value->value.intValue;
            break;
            
    }

    return r;
}

EagleDataTypeFloat EagleDbSqlValue_getFloat(EagleDbSqlValue *value, bool *success)
{
    EagleDataTypeFloat r = 0.0;
    
    switch(value->type) {
            
        case EagleDbSqlValueTypeAsterisk:
        case EagleDbSqlValueTypeIdentifier:
        case EagleDbSqlValueTypeString:
            *success = false;
            break;
            
        case EagleDbSqlValueTypeFloat:
            *success = true;
            r = value->value.floatValue;
            break;
            
        case EagleDbSqlValueTypeInteger:
            *success = true;
            r = (EagleDataTypeFloat) value->value.intValue;
            break;
            
    }

    return r;
}

EagleDataTypeVarchar EagleDbSqlValue_getVarchar(EagleDbSqlValue *value, bool *success)
{
    EagleDataTypeVarchar r = NULL;
    
    switch(value->type) {
            
        case EagleDbSqlValueTypeAsterisk:
        case EagleDbSqlValueTypeIdentifier:
        case EagleDbSqlValueTypeFloat:
        case EagleDbSqlValueTypeInteger:
            *success = false;
            break;
            
        case EagleDbSqlValueTypeString:
            *success = true;
            r = value->value.identifier;
            break;
            
    }

    return r;
}

bool EagleDbSqlValue_castable(EagleDbSqlValue *value, EagleData::Type type)
{
    bool canCast = false;
    
    switch(type) {
            
        case EagleData::Type::Integer:
            EagleDbSqlValue_getInteger(value, &canCast);
            break;
            
        case EagleData::Type::Float:
            EagleDbSqlValue_getFloat(value, &canCast);
            break;
            
        case EagleData::Type::Unknown:
            canCast = false;
            break;
            
        case EagleData::Type::Varchar:
            EagleDbSqlValue_getVarchar(value, &canCast);
            break;
            
    }
    
    return canCast;
}
