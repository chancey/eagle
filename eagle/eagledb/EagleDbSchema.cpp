#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "eagledb/EagleDbSchema.h"
#include "eagle/EagleLogger.h"
#include "eagle/EagleSynchronizedLinkedList.h"

const char *EagleDbSchema::DefaultSchemaName = "default";

const char *EagleDbSchema::InformationSchemaName = "information_schema";

EagleDbSchema::EagleDbSchema(const char *theName)
{
    name = (NULL == theName ? NULL : strdup(theName));
    tables = new EagleSynchronizedLinkedList<eagle::db::TableData*>();
}

EagleDbSchema::~EagleDbSchema()
{
    delete tables;
    delete name;
}

eagle::db::TableData* EagleDbSchema::getTable(const char *tableName)
{
    EagleLinkedList_Foreach(tables, eagle::db::TableData*, table)
    {
        if(0 == strcmp(tableName, table->table->name)) {
            return table;
        }
    }
    EagleLinkedList_ForeachEnd
    
    return NULL;
}

bool EagleDbSchema::addTable(eagle::db::TableData *td)
{
    /* check if the table already exists */
    if(NULL != getTable(td->table->name)) {
        char msg[1024];
        sprintf(msg, "Error: Table \"%s.%s\" already exists.", name, td->table->name);
        EagleLogger::Log(EagleLogger::Severity::UserError, msg);
        return false;
    }
    
    tables->addObject(td, false, NULL);
    
    return true;
}
