#include <string.h>
#include <stdio.h>
#include "eagledb/EagleDbInformationSchema.h"
#include "eagle/db/Table.h"
#include "eagle/db/TableData.h"
#include "eagle/EagleLogger.h"
#include "eagle/EagleData.h"

EagleDbInformationSchema* EagleDbInformationSchema_New(EagleDbInstance *db, std::string columnName)
{
    EagleDbInformationSchema *infoSchema = new EagleDbInformationSchema();
    
    infoSchema->db = db;
    infoSchema->columnName = columnName;
    
    return infoSchema;
}

void EagleDbInformationSchema_Init(EagleDbInstance *db, EagleDbSchema *schema)
{
    int i;
    EagleDbInformationSchema *obj;
    eagle::db::TableData *td;
    eagle::db::Table *table;
    
    /* table definition */
    table = new eagle::db::Table("information_schema_tables");
    table->addColumn(new eagle::db::Column("table_schema", EagleData::Type::Varchar));
    table->addColumn(new eagle::db::Column("table_name", EagleData::Type::Varchar));
    
    /* replace the stream providers with virtual providers */
    td = new eagle::db::TableData(table, db->pageSize);
    for(i = 0; i < td->usedProviders; ++i) {
        eagle::PageProvider *temp = td->providers[i];
        
        /* information schema object */
        obj = EagleDbInformationSchema_New(db, temp->name);
        
        td->providers[i] = new EagleDbInformationSchemaTables(
            db->pageSize,
            temp->name,
            temp->type,
            obj
        );
        
        delete temp;
    }
    
    schema->addTable(td);
}

EagleDbInformationSchemaTables::~EagleDbInformationSchemaTables()
{
    EagleDbInformationSchema_Delete(infoSchema);
}

int EagleDbInformationSchemaTables::pagesRemaining()
{
    EagleDbSchema *schema = EagleDbInstance_getSchema(infoSchema->db, EagleDbSchema::DefaultSchemaName);
    int totalTables = schema->tables->length();
    return eagle::PageProvider::TotalPages(totalTables - infoSchema->recordOffset, infoSchema->db->pageSize);
}

EaglePage* EagleDbInformationSchemaTables::nextPage()
{
    EaglePage *page;
    int totalTables = 0, tableCount = 0;
    
    /* count all tables */
    EagleLinkedList_Foreach(infoSchema->db->schemas, EagleDbSchema*, schema)
    {
        totalTables += schema->tables->length();
    }
    EagleLinkedList_ForeachEnd
    
    char **data = new char*[totalTables];
    
    EagleLinkedList_Foreach(infoSchema->db->schemas, EagleDbSchema*, schema)
    {
        EagleLinkedList_Foreach(schema->tables, eagle::db::TableData*, td)
        {
            if(infoSchema->columnName == "table_schema") {
                data[tableCount] = strdup(schema->name);
            }
            else if(infoSchema->columnName == "table_name") {
                data[tableCount] = strdup(td->table->name);
            }
        
            ++tableCount;
        }
        EagleLinkedList_ForeachEnd
    }
    EagleLinkedList_ForeachEnd
    
    page = new EaglePage(EagleData::Type::Varchar, data, totalTables, totalTables, 0, true);
    
    infoSchema->recordOffset += totalTables;
    return page;
}

void EagleDbInformationSchemaTables::reset()
{
    infoSchema->recordOffset = 0;
}

void EagleDbInformationSchema_Delete(EagleDbInformationSchema *infoSchema)
{
    if(NULL == infoSchema) {
        return;
    }
    
    delete infoSchema;
}

void EagleDbInformationSchema_Cleanup(EagleDbInstance *db)
{
    EagleDbSchema *s = EagleDbInstance_getSchema(db, EagleDbSchema::DefaultSchemaName);
    eagle::db::TableData *td = s->getTable("information_schema_tables");
    s->tables->deleteObject(td);
    
    eagle_db_Table_DeleteWithColumns(td->table);
    delete td;
}

EagleDbInformationSchemaTables::EagleDbInformationSchemaTables(int thePageSize,
                                                               std::string theName,
                                                               EagleData::Type theType,
                                                               EagleDbInformationSchema *theInfoSchema)
{
    recordsPerPage = thePageSize;
    name = theName;
    type = theType;
    infoSchema = theInfoSchema;
}
