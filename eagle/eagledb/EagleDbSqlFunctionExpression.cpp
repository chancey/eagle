#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "eagledb/EagleDbSqlFunctionExpression.h"
#include "eagle/EagleLinkedList.h"
#include "eagledb/EagleDbSqlFunction.h"
#include "eagle/EaglePage.h"
#include "eagle/EagleSynchronizedLinkedList.h"
#include "eagle/algorithm.h"

EagleDbSqlFunctionExpression* EagleDbSqlFunctionExpression_New(const char *name,
                                                               EagleLinkedList<EagleDbSqlExpression*> *exprs)
{
    EagleDbSqlFunctionExpression *exp = new EagleDbSqlFunctionExpression();
    
    exp->expressionType = EagleDbSqlExpressionTypeFunctionExpression;
    exp->exprs = exprs;
    exp->name = strdup(name);
    
    return exp;
}

EagleDbSqlFunctionExpression* EagleDbSqlFunctionExpression_NewWithSingleArgument(const char *name,
                                                                                 EagleDbSqlExpression *expr,
                                                                                 bool freeExpr)
{
    EagleLinkedList<EagleDbSqlExpression*> *exprs = new EagleSynchronizedLinkedList<EagleDbSqlExpression*>();
    exprs->addObject(expr, freeExpr, (void (*)(EagleDbSqlExpression*)) EagleDbSqlExpression_DeleteRecursive);
    return EagleDbSqlFunctionExpression_New(name, exprs);
}

EagleDbSqlFunctionExpression* EagleDbSqlFunctionExpression_NewWithTwoArguments(const char *name,
                                                                               EagleDbSqlExpression *expr1,
                                                                               bool freeExpr1,
                                                                               EagleDbSqlExpression *expr2,
                                                                               bool freeExpr2)
{
    EagleLinkedList<EagleDbSqlExpression*> *exprs = new EagleSynchronizedLinkedList<EagleDbSqlExpression*>();
    exprs->addObject(expr1, freeExpr1, (void (*)(EagleDbSqlExpression*)) EagleDbSqlExpression_DeleteRecursive);
    exprs->addObject(expr2, freeExpr2, (void (*)(EagleDbSqlExpression*)) EagleDbSqlExpression_DeleteRecursive);
    return EagleDbSqlFunctionExpression_New(name, exprs);
}

void EagleDbSqlFunctionExpression_Delete(EagleDbSqlFunctionExpression *expr)
{
    if(NULL == expr) {
        return;
    }
    
    delete expr->exprs;
    delete expr->name;
    delete expr;
}

void EagleDbSqlFunctionExpression_DeleteRecursive(EagleDbSqlFunctionExpression *expr)
{
    if(NULL == expr) {
        return;
    }
    
    delete expr->name;
    delete expr->exprs;
    delete expr;
}

char* EagleDbSqlFunctionExpression_toString(EagleDbSqlFunctionExpression *expr)
{
    char *s = new char[1024];
    std::string exp;
    
    if(eagle::algorithm::equalsIgnoreCase(expr->name, "position")) {
        char *s1 = EagleDbSqlExpression_toString((EagleDbSqlExpression*) expr->exprs->get(0));
        char *s2 = EagleDbSqlExpression_toString((EagleDbSqlExpression*) expr->exprs->get(1));
        sprintf(s, "%s(%s IN %s)", expr->name, s1, s2);
        delete s1;
        delete s2;
    }
    else {
        if(NULL != expr->exprs) {
            exp = expr->exprs->toString((char* (*)(EagleDbSqlExpression*)) EagleDbSqlExpression_toString, ", ");
        }
        sprintf(s, "%s(%s)", expr->name, exp.c_str());
    }
    
    return s;
}
