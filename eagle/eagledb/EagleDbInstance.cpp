#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "eagledb/EagleDbInstance.h"
#include "eagle/db/Table.h"
#include "eagle/db/Tuple.h"
#include "eagledb/EagleDbSqlSelect.h"
#include "eagle/PageProvider.h"
#include "eagle/Instance.h"
#include "eagle/db/TableData.h"
#include "eagledb/EagleDbInstance.h"
#include "eagledb/EagleDbParser.h"
#include "eagle/EagleLogger.h"
#include "eagledb/EagleDbSchema.h"
#include "eagledb/EagleDbInformationSchema.h"
#include "eagle/EagleData.h"
#include "eagle/EagleSynchronizedLinkedList.h"
#include "eagle/plan/stage/SelectStage.h"

EagleDbInstance* EagleDbInstance_New(int pageSize, int cores)
{
    EagleDbInstance *db = new EagleDbInstance();
    EagleDbSchema *defaultSchema, *infoSchema;
    
    db->pageSize = pageSize;
    db->cores = cores;
    db->printResults = true;
    
    /* schemas */
    db->schemas = new EagleSynchronizedLinkedList<EagleDbSchema*>();
    
    defaultSchema = new EagleDbSchema(EagleDbSchema::DefaultSchemaName);
    infoSchema = new EagleDbSchema(EagleDbSchema::InformationSchemaName);
    EagleDbInstance_addSchema(db, defaultSchema);
    EagleDbInstance_addSchema(db, infoSchema);
    
    /* setup information schema */
    EagleDbInformationSchema_Init(db, defaultSchema);
    
    return db;
}

void EagleDbInstance_PrintResults(eagle::plan::stage::AbstractStage *selectStage)
{
    EaglePage **pages;
    int i;
    unsigned long *widths;
    
#ifndef CUNIT
    int j, totalRecords = 0;
#endif
    
    if(NULL == selectStage) {
        return;
    }
    
    if(selectStage->resultFields > 0 && selectStage->result[0]->pagesRemaining() > 0) {
        /* calculate the widths of the fields */
        widths = new unsigned long[selectStage->resultFields];
        for(i = 0; i < selectStage->resultFields; ++i) {
            widths[i] = selectStage->result[i]->name.length();
        }
        
#ifndef CUNIT
        for(i = 0; i < selectStage->resultFields; ++i) {
            EaglePage *page;
            
            while(NULL != (page = selectStage->result[i]->nextPage())) {
                for(j = 0; j < page->count; ++j) {
                    switch(page->type) {
                            
                        case EagleData::Type::Integer:
                        {
                            char buf[30];
                            unsigned long len;
                            sprintf(buf, "%d", ((EagleDataTypeInteger*) page->data)[j]);
                            len = strlen(buf);
                            
                            if(len > widths[i]) {
                                widths[i] = len;
                            }
                            
                            break;
                        }
                            
                        case EagleData::Type::Varchar:
                        {
                            unsigned long len = strlen(((EagleDataTypeVarchar*) page->data)[j]);
                            
                            if(len > widths[i]) {
                                widths[i] = len;
                            }
                            
                            break;
                        }
                            
                        case EagleData::Type::Float:
                        {
                            char buf[30];
                            unsigned long len;
                            sprintf(buf, "%g", ((EagleDataTypeFloat*) page->data)[j]);
                            len = strlen(buf);
                            
                            if(len > widths[i]) {
                                widths[i] = len;
                            }
                            
                            break;
                        }
                            
                        case EagleData::Type::Unknown:
                            break;
                            
                    }
                }
            }
            
            delete page;
        }
        for(i = 0; i < selectStage->resultFields; ++i) {
            selectStage->result[i]->reset();
        }
        
        /* heading */
        printf("\n");
        for(i = 0; i < selectStage->resultFields; ++i) {
            if(i > 0) {
                printf("|");
            }
            printf(" %s ", selectStage->result[i]->name.c_str());
        }
        printf("\n");
        
        for(i = 0; i < selectStage->resultFields; ++i) {
            if(i > 0) {
                printf("+");
            }
            for(j = 0; j < (int) widths[i] + 2; ++j) {
                printf("-");
            }
        }
        printf("\n");
#endif
    
        /* render out */
        pages = new EaglePage*[selectStage->resultFields];
        
#ifndef CUNIT
        while(1) {
            int k;
            int finished = 0;
            
            for(i = 0; i < selectStage->resultFields; ++i) {
                EaglePage *page = selectStage->result[i]->nextPage();
                if(NULL == page) {
                    finished = 1;
                    break;
                }
                pages[i] = page;
            }
            
            if(finished == 0) {
                for(j = 0; j < pages[0]->count; ++j) {
                    for(k = 0; k < selectStage->resultFields; ++k) {
                        if(k > 0) {
                            printf("|");
                        }
                        
                        switch(pages[k]->type) {
                                
                            case EagleData::Type::Unknown:
                                printf(" %*s ", (int) widths[k], "?");
                                break;
                                
                            case EagleData::Type::Integer:
                            {
                                EagleDataTypeInteger *d = (EagleDataTypeInteger*) pages[k]->data;
                                printf(" %*d ", (int) widths[k], d[j]);
                                break;
                            }
                                
                            case EagleData::Type::Varchar:
                            {
                                EagleDataTypeVarchar *d = (EagleDataTypeVarchar*) pages[k]->data;
                                printf(" %-*s ", (int) widths[k], d[j]);
                                break;
                            }
                                
                            case EagleData::Type::Float:
                            {
                                EagleDataTypeFloat *d = (EagleDataTypeFloat*) pages[k]->data;
                                printf(" %*g ", (int) widths[k], d[j]);
                                break;
                            }
                                
                        }
                    }
                    printf("\n");
                    ++totalRecords;
                }
                
                for(k = 0; k < selectStage->resultFields; ++k) {
                    delete pages[k];
                }
            }
            else {
                break;
            }
        }
        
        printf("\n");
#endif
        delete widths;
        delete pages;
    }
    
#ifndef CUNIT
    printf("%d record%s, %.3f seconds\n\n", totalRecords, (totalRecords == 1 ? "" : "s"),
           selectStage->getExecutionSeconds());
#endif
}

bool EagleDbInstance_executeSelect(EagleDbInstance *db,
                                   EagleDbSqlSelect *select,
                                   EagleLogger::Event **error,
                                   eagle::Plan **plan)
{
    bool success = true;
    
    *plan = EagleDbSqlSelect_parse(select, db);
    
    /* catch compilation error */
    if((*plan)->isError()) {
        *error = EagleLogger::Log(EagleLogger::Severity::UserError, (*plan)->getErrorMessage());
        success = false;
    }
    else {
        /* execute */
        eagle::Instance *eagle = new eagle::Instance(db->cores);
        eagle->addPlan(*plan);
        eagle->run();
        
        /* print results */
        if(db->printResults) {
            eagle::plan::stage::AbstractStage *lastStage = (*plan)->stages->last();
            EagleDbInstance_PrintResults(lastStage);
        }
        
        delete eagle;
    }
    
    return success;
}

bool EagleDbInstance_executeInsert(EagleDbInstance *db, const eagle::db::sql::Insert& insert, EagleLogger::Event **error)
{
    char msg[1024];
    EagleLinkedList<EagleDbSqlExpression*>::Item *cursor;
    int i;
    eagle::db::Tuple *tuple;
    
#ifndef CUNIT
    int rowsInserted = 1;
#endif
    
    /* make the table exists */
    eagle::db::TableData *td = EagleDbInstance_getTable(db, insert.tableName);
    if(NULL == td) {
        sprintf(msg, "No such table '%s'", insert.tableName.c_str());
        *error = EagleLogger::Log(EagleLogger::Severity::UserError, msg);
        return false;
    }
    
    /* make sure the number of columns match the number of values */
    if(insert.names.length() != insert.values.length()) {
        sprintf(msg, "There are %d columns and %d values", insert.names.length(),
                insert.values.length());
        *error = EagleLogger::Log(EagleLogger::Severity::UserError, msg);
        return false;
    }
    
    /* make sure all the columns exist */
    for(cursor = insert.names.begin(), i = 0; NULL != cursor; cursor = cursor->getNext(), ++i) {
        EagleDbSqlExpression *expr = cursor->getObject(), *valueExpr;
        EagleDbSqlValue *value;
        
        /* they must be column names, expressions are not acceptable */
        if(EagleDbSqlExpressionTypeValue != expr->expressionType) {
            *error = EagleLogger::Log(EagleLogger::Severity::UserError, "You cannot use expressions for column names");
            return false;
        }
        
        value = (EagleDbSqlValue*) expr;
        if(EagleDbSqlValueTypeIdentifier != value->type) {
            *error = EagleLogger::Log(EagleLogger::Severity::UserError, "You cannot use expressions for column names");
            return false;
        }
        
        /* column name exists in the table? */
        const eagle::db::Column *col = td->table->getColumnByName(value->value.identifier);
        if(NULL == col) {
            sprintf(msg, "No such column '%s' in table '%s'", value->value.identifier, insert.tableName.c_str());
            *error = EagleLogger::Log(EagleLogger::Severity::UserError, msg);
            return false;
        }
        
        /* make sure the types are correct */
        valueExpr = insert.values.get(i);
        if(EagleDbSqlExpressionTypeValue != valueExpr->expressionType) {
            sprintf(msg, "Expressions in VALUES are not yet supported for column '%s'", col->name.c_str());
            *error = EagleLogger::Log(EagleLogger::Severity::UserError, msg);
            return false;
        }
        
        /* match data type */
        if(false == EagleDbSqlValue_castable((EagleDbSqlValue*) valueExpr, col->type)) {
            char *type1 = EagleData::TypeToName(col->type);
            char *type2 = EagleDbSqlValueType_toString(((EagleDbSqlValue*) valueExpr)->type);
            
            sprintf(msg, "Type for column %s is %s, but %s given.", col->name.c_str(), type1, type2);
            
            delete type1;
            delete type2;
            *error = EagleLogger::Log(EagleLogger::Severity::UserError, msg);
            return false;
        }
    }
    
    /* everything looks good, we can create the tuple now */
    tuple = new eagle::db::Tuple(td->table);
    
    for(cursor = insert.names.begin(), i = 0; NULL != cursor; cursor = cursor->getNext(), ++i) {
        char *colName = ((EagleDbSqlValue*) cursor->getObject())->value.identifier;
        int colIndex = td->table->getColumnIndex(colName);
        const eagle::db::Column *col = td->table->getColumnByName(colName);
        EagleDbSqlValue *v = ((EagleDbSqlValue*) insert.values.get(i));
        
        tuple->set(colIndex, v, col->type);
    }
    
    /* do the INSERT */
    td->insert(tuple);
    
    /* cleanup */
    delete tuple;
    
#ifndef CUNIT
    if(db->printResults) {
        printf("%d row%s inserted\n\n", rowsInserted, (rowsInserted == 1 ? "" : "s"));
    }
#endif
    
    return true;
}

bool EagleDbInstance_executeCreateTable(EagleDbInstance *db, eagle::db::Table *table, EagleLogger::Event **error)
{
    bool success = true;
    char msg[1024];
    eagle::db::TableData *td;
    EagleDbSchema *schema;
    
    /* create the table data */
    td = new eagle::db::TableData(table, db->pageSize);
    
    /* add table to default schema */
    schema = EagleDbInstance_getSchema(db, EagleDbSchema::DefaultSchemaName);
    if(true == schema->addTable(td)) {
        sprintf(msg, "Table \"%s.%s\" created.", schema->name, table->name);
        *error = EagleLogger::Log(EagleLogger::Severity::Info, msg);
    }
    else {
        success = false;
        sprintf(msg, "Table \"%s.%s\" already exists.", schema->name, table->name);
        *error = EagleLogger::Log(EagleLogger::Severity::UserError, msg);
        
        /* clean up resources */
        eagle_db_Table_DeleteWithColumns(table);
        delete td;
    }
    
#ifndef CUNIT
    if(db->printResults) {
        printf("%s\n\n", msg);
    }
#endif
    
    return success;
}

bool EagleDbInstance_executeParser(EagleDbInstance *db,
                                   struct EagleDbParser *p,
                                   EagleLogger::Event **error,
                                   eagle::Plan **plan)
{
    bool success = true;
    
    switch(p->yystatementtype) {
            
        case EagleDbSqlStatementTypeNone:
            /* lets not consider this an error and ignore it */
            break;
            
        case EagleDbSqlStatementTypeSelect:
            success = EagleDbInstance_executeSelect(db, (EagleDbSqlSelect*) p->yyparse_ast, error, plan);
            EagleDbSqlSelect_DeleteRecursive((EagleDbSqlSelect*) p->yyparse_ast);
            break;
            
        case EagleDbSqlStatementTypeCreateTable:
            success = EagleDbInstance_executeCreateTable(db, (eagle::db::Table*) p->yyparse_ast, error);
            break;
            
        case EagleDbSqlStatementTypeInsert:
            success = EagleDbInstance_executeInsert(db, *(eagle::db::sql::Insert*) p->yyparse_ast, error);
            delete (eagle::db::sql::Insert*) p->yyparse_ast;
            break;
            
    }
    
    return success;
}

bool EagleDbInstance_execute(EagleDbInstance *db, const char *sql, EagleLogger::Event **error, eagle::Plan **plan)
{
    EagleDbParser *p;
    bool success = true;
    
    /* parse sql */
    p = EagleDbParser_ParseWithString(sql);
    
    /* check for errors */
    if(true == EagleDbParser_hasError(p)) {
        char msg[1024];
        if(db->printResults) {
            sprintf(msg, "Error: %s", EagleDbParser_lastError(p));
        }
        *error = EagleLogger::Log(EagleLogger::Severity::UserError, msg);
        success = false;
    }
    else {
        success = EagleDbInstance_executeParser(db, p, error, plan);
    }
    
    /* clean up */
    EagleDbParser_Delete(p);
    
    return success;
}

void EagleDbInstance_Delete(EagleDbInstance *db)
{
    if(NULL == db) {
        return;
    }
    
    {
        /* always clean up the information schema */
        EagleDbInformationSchema_Cleanup(db);
        
        EagleLinkedList_Foreach(db->schemas, EagleDbSchema*, schema)
        {
            if(0 == strcmp(schema->name, EagleDbSchema::DefaultSchemaName) ||
               0 == strcmp(schema->name, EagleDbSchema::InformationSchemaName)) {
                delete schema;
            }
        }
        EagleLinkedList_ForeachEnd
        
        delete db->schemas;
        delete db;
        
        /* clean up logger */
        EagleLogger::Get()->reset();
    }
}

void EagleDbInstance_DeleteAll(EagleDbInstance *db)
{
    if(NULL == db) {
        return;
    }
    
    {
        /* always clean up the information schema */
        EagleDbInformationSchema_Cleanup(db);
        
        EagleLinkedList_Foreach(db->schemas, EagleDbSchema*, schema)
        {
            EagleLinkedList_Foreach(schema->tables, eagle::db::TableData*, table)
            {
                eagle_db_Table_DeleteWithColumns(table->table);
                delete table;
            }
            EagleLinkedList_ForeachEnd
            
            delete schema;
        }
        EagleLinkedList_ForeachEnd
        
        delete db->schemas;
        delete db;
        
        /* clean up logger */
        EagleLogger::Get()->reset();
    }
}

eagle::db::TableData* EagleDbInstance_getTable(EagleDbInstance *db, std::string tableName)
{
    EagleDbSchema *schema;
    
    if(tableName.empty()) {
        return NULL;
    }
    
    schema = EagleDbInstance_getSchema(db, EagleDbSchema::DefaultSchemaName);
    if(NULL != schema) {
        EagleLinkedList_Foreach(schema->tables, eagle::db::TableData*, table)
        {
            if(table->table->name == tableName) {
                return table;
            }
        }
        EagleLinkedList_ForeachEnd
    }
    
    return NULL;
}

EagleDbSchema* EagleDbInstance_getSchema(EagleDbInstance *db, const char *schemaName)
{
    EagleLinkedList_Foreach(db->schemas, EagleDbSchema*, schema)
    {
        if(0 == strcmp(schemaName, schema->name)) {
            return schema;
        }
    }
    EagleLinkedList_ForeachEnd
    
    return NULL;
}

bool EagleDbInstance_addSchema(EagleDbInstance *db, EagleDbSchema *schema)
{
    /* check if the schema exists */
    if(NULL != EagleDbInstance_getSchema(db, schema->name)) {
        char msg[1024];
        if(db->printResults) {
            sprintf(msg, "Error: Schema \"%s\" already exists.", schema->name);
        }
        EagleLogger::Log(EagleLogger::Severity::UserError, msg);
        return false;
    }
    
    db->schemas->addObject(schema, false, NULL);
    
    return true;
}
