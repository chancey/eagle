#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "eagledb/EagleDbSqlFunction.h"
#include "eagle/EaglePageOperations.h"
#include "eagle/EagleSynchronizedLinkedList.h"
#include "eagle/algorithm.h"

/**
 * Contains built-in functions.
 * @see EagleDbSqlFunction_Find()
 */
EagleLinkedList<EagleDbSqlFunction*> *EagleDbSqlFunction_InstalledFunctions = NULL;

EagleDbSqlFunction* EagleDbSqlFunction_New(void (*function)(EaglePage *destination, EaglePage *source1, EaglePage *source2, void *obj),
                                           const char *name,
                                           EagleData::Type returnType,
                                           int argCount,
                                           EagleData::Type *argTypes)
{
    EagleDbSqlFunction *f = new EagleDbSqlFunction();
    
    f->function = function;
    f->name = strdup(name);
    f->argCount = argCount;
    f->argTypes = argTypes;
    f->returnType = returnType;
    
    return f;
}

EagleDbSqlFunction* EagleDbSqlFunction_NewWithSingleArgument(void (*function)(EaglePage *destination, EaglePage *source1, EaglePage *source2, void *obj),
                                                             const char *name,
                                                             EagleData::Type returnType,
                                                             EagleData::Type argType)
{
    EagleData::Type *argTypes = new EagleData::Type();
    
    argTypes[0] = argType;
    
    return EagleDbSqlFunction_New(function, name, returnType, 1, argTypes);
}

EagleDbSqlFunction* EagleDbSqlFunction_NewWithTwoArguments(EaglePageOperationFunction(function),
                                                           const char *name,
                                                           EagleData::Type returnType,
                                                           EagleData::Type argType1,
                                                           EagleData::Type argType2)
{
    EagleData::Type *argTypes = new EagleData::Type[2];
    
    argTypes[0] = argType1;
    argTypes[1] = argType2;
    
    return EagleDbSqlFunction_New(function, name, returnType, 2, argTypes);
}

EagleDbSqlFunction* EagleDbSqlFunction_NewWithFourArguments(EaglePageOperationFunction(function),
                                                            const char *name,
                                                            EagleData::Type returnType,
                                                            EagleData::Type argType1,
                                                            EagleData::Type argType2,
                                                            EagleData::Type argType3,
                                                            EagleData::Type argType4)
{
    EagleData::Type *argTypes = new EagleData::Type[4];
    
    argTypes[0] = argType1;
    argTypes[1] = argType2;
    argTypes[2] = argType3;
    argTypes[3] = argType4;
    
    return EagleDbSqlFunction_New(function, name, returnType, 4, argTypes);
}

void EagleDbSqlFunction_Delete(EagleDbSqlFunction *f)
{
    if(NULL == f) {
        return;
    }
    
    delete f->name;
    delete f;
}

void EagleDbSqlFunction_LoadBuiltIns_(void)
{
    EagleLinkedList<EagleDbSqlFunction*> *fs = new EagleSynchronizedLinkedList<EagleDbSqlFunction*>();
    
    fs->addObject(EagleDbSqlFunction_Make1(AbsPageFloat, "abs", Float, Float), true, (void(*)(EagleDbSqlFunction*)) EagleDbSqlFunction_Delete);
    fs->addObject(EagleDbSqlFunction_Make2(Atan2PageFloatPageFloat, "atan2", Float, Float, Float), true, (void(*)(EagleDbSqlFunction*)) EagleDbSqlFunction_Delete);
    fs->addObject(EagleDbSqlFunction_Make1(BitLengthPageVarchar, "bit_length", Integer, Varchar), true, (void(*)(EagleDbSqlFunction*)) EagleDbSqlFunction_Delete);
    fs->addObject(EagleDbSqlFunction_Make1(CeilPageFloat, "ceil", Float, Float), true, (void(*)(EagleDbSqlFunction*)) EagleDbSqlFunction_Delete);
    fs->addObject(EagleDbSqlFunction_Make1(CeilPageFloat, "ceiling", Float, Float), true, (void(*)(EagleDbSqlFunction*)) EagleDbSqlFunction_Delete);
    fs->addObject(EagleDbSqlFunction_Make1(CharLengthPageVarchar, "char_length", Integer, Varchar), true, (void(*)(EagleDbSqlFunction*)) EagleDbSqlFunction_Delete);
    fs->addObject(EagleDbSqlFunction_Make1(ExpPageFloat, "exp", Float, Float), true, (void(*)(EagleDbSqlFunction*)) EagleDbSqlFunction_Delete);
    fs->addObject(EagleDbSqlFunction_Make1(FloorPageFloat, "floor", Float, Float), true, (void(*)(EagleDbSqlFunction*)) EagleDbSqlFunction_Delete);
    fs->addObject(EagleDbSqlFunction_Make1(LnPageFloat, "ln", Float, Float), true, (void(*)(EagleDbSqlFunction*)) EagleDbSqlFunction_Delete);
    fs->addObject(EagleDbSqlFunction_Make1(LowerPageVarchar, "lower", Varchar, Varchar), true, (void(*)(EagleDbSqlFunction*)) EagleDbSqlFunction_Delete);
    fs->addObject(EagleDbSqlFunction_Make2(ModPageFloatPageFloat, "mod", Float, Float, Float), true, (void(*)(EagleDbSqlFunction*)) EagleDbSqlFunction_Delete);
    fs->addObject(EagleDbSqlFunction_Make1(OctetLengthPageVarchar, "octet_length", Integer, Varchar), true, (void(*)(EagleDbSqlFunction*)) EagleDbSqlFunction_Delete);
    fs->addObject(EagleDbSqlFunction_Make2(PositionPageVarcharPageVarchar, "position", Integer, Varchar, Varchar), true, (void(*)(EagleDbSqlFunction*)) EagleDbSqlFunction_Delete);
    fs->addObject(EagleDbSqlFunction_Make2(PowerPageFloatPageFloat, "pow", Float, Float, Float), true, (void(*)(EagleDbSqlFunction*)) EagleDbSqlFunction_Delete);
    fs->addObject(EagleDbSqlFunction_Make2(PowerPageFloatPageFloat, "power", Float, Float, Float), true, (void(*)(EagleDbSqlFunction*)) EagleDbSqlFunction_Delete);
    fs->addObject(EagleDbSqlFunction_Make1(SqrtPageFloat, "sqrt", Float, Float), true, (void(*)(EagleDbSqlFunction*)) EagleDbSqlFunction_Delete);
    fs->addObject(EagleDbSqlFunction_Make1(UpperPageVarchar, "upper", Varchar, Varchar), true, (void(*)(EagleDbSqlFunction*)) EagleDbSqlFunction_Delete);
    fs->addObject(EagleDbSqlFunction_Make4(WidthBucketPageFloatPageFloatPageFloatPageInteger, "width_bucket", Integer, Float, Float, Float, Integer), true, (void(*)(EagleDbSqlFunction*)) EagleDbSqlFunction_Delete);
    
    EagleDbSqlFunction_InstalledFunctions = fs;
}

EagleDbSqlFunction* EagleDbSqlFunction_Find(char *name, EagleData::Type *types, int argCount)
{
    /* initialise functions */
    if(NULL == EagleDbSqlFunction_InstalledFunctions) {
        EagleDbSqlFunction_LoadBuiltIns_();
    }
    
    /* locate the function */
    EagleLinkedList_Foreach(EagleDbSqlFunction_InstalledFunctions, EagleDbSqlFunction*, func)
    {
        if(eagle::algorithm::equalsIgnoreCase(name, func->name) && argCount == func->argCount) {
            /* confirm that the types are the same */
            int i;
            
            bool match = true;
            for(i = 0; i < argCount; ++i) {
                if(types[i] != func->argTypes[i]) {
                    match = false;
                    break;
                }
            }
            
            if(false == match) {
                continue;
            }
            
            /* we found a match */
            return func;
        }
    }
    EagleLinkedList_ForeachEnd
    
    return NULL;
}
