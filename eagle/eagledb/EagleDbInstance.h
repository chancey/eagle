#ifndef eagle_EagleDbInstance_h
#define eagle_EagleDbInstance_h

#include "EagleDbSchema.h"
#include "eagle/Plan.h"
#include "EagleDbSqlStatementType.h"
#include "EagleDbSqlSelect.h"
#include "eagle/db/sql/Insert.h"
#include "eagle/EagleLogger.h"

#ifdef __cplusplus
extern "C" {
#endif

struct EagleDbParser;

/**
 The database instance is a wrapper for a eagle::Instance.
 */
typedef struct EagleDbInstance {
    
    /**
     Contains the pointers to the individual schemas. The schema objects themselves are managed externally (i.e.
     deleteing an EagleDbInstance) will not deleted the respective schemas.
     */
    EagleLinkedList<EagleDbSchema*> *schemas;
    
    /**
     The page size to be provided to EaglePlan.
     */
    int pageSize;
    
    /**
     The number of CPU cores.
     */
    int cores;
    
    /**
     * Any SQL that is executed will display some kind of success or error message, this can be used to turn that
     * rendering off.
     */
    bool printResults;
    
} EagleDbInstance;

/**
 * Create a new database instance.
 * @param [in] pageSize The default page size to use for EaglePlan.
 * @param [in] cores The number of CPU cores.
 * @return A new instance.
 */
EagleDbInstance* EagleDbInstance_New(int pageSize, int cores);

/**
 * Free an instance.
 * @param [in] db The database instance.
 */
void EagleDbInstance_Delete(EagleDbInstance *db);

/**
 * Free an instance and ALL of the schemas and tables.
 * @param [in] db The database instance.
 */
void EagleDbInstance_DeleteAll(EagleDbInstance *db);

/**
 * Execute an arbitrary SQL statement.
 * @param [in] db The database instance.
 * @param [in] sql The SQL statement.
 * @param [out] error If an error occurs it will be sent back through this parameter.
 * @param [out] plan Reference to the plan if applicable.
 * @return Success status.
 */
bool EagleDbInstance_execute(EagleDbInstance *db, const char *sql, EagleLogger::Event **error, eagle::Plan **plan);

/**
 * Mostly for internal use. See EagleDbInstance_execute().
 * @param [in] db The database instance.
 * @param [in] p Parser instance.
 * @param [out] error If an error occurs it will be sent back through this parameter.
 * @param [out] plan Reference to the plan if applicable.
 * @return Success status.
 */
bool EagleDbInstance_executeParser(EagleDbInstance *db, struct EagleDbParser *p, EagleLogger::Event **error,
                                   eagle::Plan **plan);

/**
 * Execute a SELECT statement.
 * @param [in] db The database instance.
 * @param [in] select The SELECT statement (compiled).
 * @param [out] error If an error occurs it will be sent back through this parameter.
 * @param [out] plan Reference to the plan.
 * @return Success status.
 */
bool EagleDbInstance_executeSelect(EagleDbInstance *db,
                                   EagleDbSqlSelect *select,
                                   EagleLogger::Event **error,
                                   eagle::Plan **plan);

/**
 * Execute a CREATE TABLE statement.
 * @param [in] db The database instance.
 * @param [in] table The table definition to create.
 * @param [out] error If an error occurs it will be sent back through this parameter.
 * @return Success status.
 */
bool EagleDbInstance_executeCreateTable(EagleDbInstance *db, eagle::db::Table *table, EagleLogger::Event **error);

/**
 * Print a result set (fancy).
 * @param [in] selectStage The stage that contains a pointer to the result set once it is executed.
 */
void EagleDbInstance_PrintResults(eagle::plan::stage::AbstractStage *selectStage);

/**
 * Get a table by name. This will only search the EagleDbSchema::DefaultSchemaName schema. It does not support schema
 * name with a dot, yet.
 * @param [in] db The database instance.
 * @param [in] tableName The raw table name.
 * @return NULL if the table can not be found.
 */
eagle::db::TableData* EagleDbInstance_getTable(EagleDbInstance *db, std::string tableName);

/**
 * Get a schema by name.
 * @param [in] db The database instance.
 * @param [in] schemaName The name of the schema.
 * @return NULL if the schema can not be found.
 */
EagleDbSchema* EagleDbInstance_getSchema(EagleDbInstance *db, const char *schemaName);

/**
 * Add a schema to the instance.
 * @param [in] db The database instance.
 * @param [in] schema The schema.
 * @return true if it is successful.
 */
bool EagleDbInstance_addSchema(EagleDbInstance *db, EagleDbSchema *schema);

/**
 * Execute an INSERT statement.
 * @param [in] db Database instance.
 * @param [in] insert INSERT statement.
 * @param [out] error If an error occurs it will be sent back through this parameter.
 * @return true on success.
 */
bool EagleDbInstance_executeInsert(EagleDbInstance *db, const eagle::db::sql::Insert& insert, EagleLogger::Event **error);

#ifdef __cplusplus
}
#endif

#endif
