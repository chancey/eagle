#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "eagledb/EagleDbConsole.h"
#include "eagledb/EagleDbInstance.h"
#include "eagle/EagleLogger.h"
#include "eagle/Plan.h"
#include "eagle/Calendar.h"

EagleDbConsole* EagleDbConsole_New(void)
{
    EagleDbConsole *console = new EagleDbConsole();
    
    console->startTime = eagle::Calendar::getInstance()->getAbsoluteTime();
    
    return console;
}

char* EagleDbConsole_GetLine(void)
{
    char *line = new char[100];
#ifndef CUNIT
    char *linep = line, *linen = NULL;
    size_t lenmax = 100, len = lenmax;
    int c;
    bool eof = false;
    
    while(1) {
        c = fgetc(stdin);
        if(c == EOF) {
            eof = true;
            break;
        }
        
        if(--len == 0) {
            len = lenmax;
            linen = (char*) realloc(linep, lenmax *= 2);
            
            if(linen == NULL) {
                delete linep;
                return NULL;
            }
            line = linen + (line - linep);
            linep = linen;
        }
        
        if((*line++ = (char) c) == '\n') {
            break;
        }
    }
    *line = '\0';
    
    /* chomp */
    linep[strlen(linep) - 1] = '\0';
    
    /* if there is nothing left on the stream return NULL */
    if(true == eof && strlen(linep) == 0) {
        return NULL;
    }
    
    return linep;
#else
    delete line;
    return NULL;
#endif
}

void EagleDbConsole_run()
{
#ifndef CUNIT
    char *cmd = NULL;
    EagleDbInstance *db = EagleDbInstance_New(1000, 1 /* FIXME */);
    EagleLogger::Event *error = NULL;
    
    while(1) {
        /* get line */
        printf("eagle> ");
        cmd = EagleDbConsole_GetLine();
        
        /* broken input */
        if(NULL == cmd) {
            printf("Error: Broken stdin.\n");
            break;
        }
        
        /* check for quit */
        if(strcmp(cmd, "\\q") == 0) {
            printf("Bye.\n");
            delete cmd;
            break;
        }
        
        /* parse */
        error = NULL;
        eagle::Plan *plan = NULL;
        EagleDbInstance_execute(db, cmd, &error, &plan);
        if(NULL != error) {
            printf("%s\n\n", error->getMessage().c_str());
        }
        
        // we can throw away plan immediately since we don't need it
        delete plan;
        delete cmd;
    }
#endif
}

void EagleDbConsole_Delete(EagleDbConsole *console)
{
    if(NULL == console) {
        return;
    }
    
#ifndef CUNIT
    /* make sure the log is closed cleanly */
    delete EagleLogger::Get();
#endif
    
    delete console;
}
