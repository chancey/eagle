#ifndef eagle_EagleDbSqlValue_h
#define eagle_EagleDbSqlValue_h

#include "EagleDbSqlExpressionHeader.h"
#include "EagleDbSqlValueType.h"
#include "eagle/EagleData.h"

/**
 * Contains the actual value, depending on the type.
 */
typedef union EagleDbSqlValueValue {
    
    /**
     Integer.
     @see EagleDbSqlValueTypeInteger
     */
    EagleDataTypeInteger intValue;
    
    /**
     When using identifier token. This is the name of the identifier.
     */
    char *identifier;
    
    /**
     Floating point number.
     @see EagleDbSqlValueTypeFloat
     */
    EagleDataTypeFloat floatValue;
    
} EagleDbSqlValueValue;

/**
 * Encapsulates a Value of any type.
 */
typedef struct {
    
    /**
     * Header.
     * @see EagleDbSqlExpressionHeader
     */
    EagleDbSqlExpressionHeader;
    
    /**
     * Value type.
     * @see EagleDbSqlValueType
     */
    EagleDbSqlValueType type;
    
    /**
     * Contains the actual value, depending on the type.
     */
    EagleDbSqlValueValue value;
    
} EagleDbSqlValue;

/**
 * Create a new EagleDbSqlValue with a string literal.
 * @param [in] str String value.
 * @param [in] process If this is true then the string will be processed, ie remove the surrounding quotes and
 * unescape inner quotes.
 * @return A new EagleDbSqlValue instance.
 */
EagleDbSqlValue* EagleDbSqlValue_NewWithString(char *str, bool process);

/**
 * Create a new EagleDbSqlValue with an integer.
 * @param [in] value Integer value.
 * @return A new EagleDbSqlValue instance.
 */
EagleDbSqlValue* EagleDbSqlValue_NewWithInteger(EagleDataTypeInteger value);

/**
 * Create a new EagleDbSqlValue with an floating point number.
 * @param [in] value Floating point value.
 * @return A new EagleDbSqlValue instance.
 */
EagleDbSqlValue* EagleDbSqlValue_NewWithFloat(EagleDataTypeFloat value);

/**
 * Create a new EagleDbSqlValue with an asterisk token.
 * @return A new EagleDbSqlValue instance.
 */
EagleDbSqlValue* EagleDbSqlValue_NewWithAsterisk(void);

/**
 * Create a new EagleDbSqlValue with an identifier (a column name).
 * @param [in] name The name of the column.
 * @return A new EagleDbSqlValue instance.
 */
EagleDbSqlValue* EagleDbSqlValue_NewWithIdentifier(char *name);

/**
 * Free a value.
 * @param [in] value The instance.
 */
void EagleDbSqlValue_Delete(EagleDbSqlValue *value);

/**
 * Render a value to a string.
 * @param [in] value The instance.
 * @return A new string representation of the value.
 */
char* EagleDbSqlValue_toString(EagleDbSqlValue *value);

/**
 Get the integer representation of a value.
 @param [in] value The value.
 @param [out] success Set ot true if the returned value is correct.
 @return The integer value of whatever the original type of the \p value was. If the value could now be cast then
 \p success will be set to false and 0 will be returned.
 */
EagleDataTypeInteger EagleDbSqlValue_getInteger(EagleDbSqlValue *value, bool *success);

/**
 Get the float representation of a value.
 @param [in] value The value.
 @param [out] success Set ot true if the returned value is correct.
 @return The float value of whatever the original type of the \p value was. If the value could now be cast then
 \p success will be set to false and 0.0 will be returned.
 */
EagleDataTypeFloat EagleDbSqlValue_getFloat(EagleDbSqlValue *value, bool *success);

/**
 Get the varchar representation of a value.
 @param [in] value The value.
 @param [out] success Set ot true if the returned value is correct.
 @return The varchar value of whatever the original type of the \p value was. If the value could now be cast then
 \p success will be set to false and NULL will be returned.
 */
EagleDataTypeVarchar EagleDbSqlValue_getVarchar(EagleDbSqlValue *value, bool *success);

/**
 Check if a EagleDbSqlValue can be cast into a data type.
 @param [in] value The value to cast (can be any type).
 @param [in] type The data type to attempt to cast to.
 @return true if the value can successfully be cast to the new type.
 */
bool EagleDbSqlValue_castable(EagleDbSqlValue *value, EagleData::Type type);

#endif
