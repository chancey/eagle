#include <stdlib.h>
#include <stdio.h>
#include "EagleDbSqlCastExpression.h"

EagleDbSqlCastExpression* EagleDbSqlCastExpression_New(EagleDbSqlExpression *expr, EagleData::Type castAs)
{
    EagleDbSqlCastExpression *exp = new EagleDbSqlCastExpression();
    
    exp->expressionType = EagleDbSqlExpressionTypeCastExpression;
    exp->expr = expr;
    exp->castAs = castAs;
    
    return exp;
}

void EagleDbSqlCastExpression_Delete(EagleDbSqlCastExpression *expr)
{
    if(NULL == expr) {
        return;
    }
    
    delete expr;
}

void EagleDbSqlCastExpression_DeleteRecursive(EagleDbSqlCastExpression *expr)
{
    if(NULL == expr) {
        return;
    }
    
    EagleDbSqlExpression_DeleteRecursive((EagleDbSqlExpression*) expr->expr);
    delete expr;
}

char* EagleDbSqlCastExpression_toString(EagleDbSqlCastExpression *expr)
{
    char* s = new char[1024], *exp, *castAs;
    
    exp = EagleDbSqlExpression_toString(expr->expr);
    castAs = EagleData::TypeToName(expr->castAs);
    
    sprintf(s, "CAST(%s AS %s)", exp, castAs);
    
    delete exp;
    delete castAs;
    return s;
}

bool EagleDbSqlCastExpression_GetOperation(EagleData::Type left,
                                           EagleData::Type right,
                                           EagleDbSqlCastOperator *match)
{
    unsigned long i;
    static EagleDbSqlCastOperator ops[] = {
        { EagleData::Type::Integer, EagleData::Type::Float, EaglePageOperations_CastPageIntFloat }
    };
    
    for(i = 0; i < sizeof(ops) / sizeof(EagleDbSqlCastOperator); ++i) {
        if(left == ops[i].left && right == ops[i].right) {
            *match = ops[i];
            return true;
        }
    }
    
    return false;
}
