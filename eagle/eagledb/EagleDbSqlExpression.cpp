#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sstream>
#include "eagledb/EagleDbSqlExpression.h"
#include "eagledb/EagleDbSqlValue.h"
#include "eagle/PageProvider.h"
#include "eagle/EagleData.h"
#include "eagledb/EagleDbSqlBinaryExpression.h"
#include "eagledb/EagleDbSqlUnaryExpression.h"
#include "eagle/EaglePageOperations.h"
#include "eagledb/EagleDbSqlSelect.h"
#include "eagledb/EagleDbSqlBinaryExpressionOperator.h"
#include "eagledb/EagleDbSqlUnaryExpressionOperator.h"
#include "eagle/SinglePageProvider.h"
#include "eagle/StreamPageProvider.h"
#include "eagledb/EagleDbSqlFunctionExpression.h"
#include "eagledb/EagleDbSqlCastExpression.h"
#include "eagledb/EagleDbSqlFunction.h"
#include "eagle/plan/Operation.h"
#include "eagle/plan/stage/SelectStage.h"

const int EagleDbSqlExpression_ERROR = -1;

int EagleDbSqlExpression_CompilePlanIntoBuffer_Function_(EagleDbSqlExpression *expression,
                                                         int *destinationBuffer,
                                                         eagle::plan::stage::SelectStage *selectStage)
{
    EagleDbSqlFunctionExpression *cast = (EagleDbSqlFunctionExpression*) expression;
    int destination, exprCount, *destinationExprs, i;
    eagle::plan::Operation *epo;
    char msg[1024], *t1;
    EagleData::Type *argTypes;
    EagleDbSqlFunction *function;
    
    exprCount = (NULL == cast->exprs) ? 0 : cast->exprs->length();
    destinationExprs = new int[exprCount];
    
    /* expressions */
    for(i = 0; i < exprCount; ++i) {
        destinationExprs[i] = EagleDbSqlExpression_CompilePlanIntoBuffer_((EagleDbSqlExpression*) cast->exprs->get(i),
                                                                          destinationBuffer, selectStage, true);
        if(EagleDbSqlExpression_ERROR == destinationExprs[i] || selectStage->isError()) {
            delete destinationExprs;
            return EagleDbSqlExpression_ERROR;
        }
    }
    
    destination = *destinationBuffer;
    
    /* find the function */
    argTypes = new EagleData::Type[exprCount];
    
    for(i = 0; i < exprCount; ++i) {
        argTypes[i] = selectStage->bufferTypes[destinationExprs[i]];
    }
    function = EagleDbSqlFunction_Find(cast->name, argTypes, exprCount);
    
    if(NULL == function) {
        /* function does not exist */
        std::stringstream signature;;
        
        signature << cast->name << "(";
        for(i = 0; i < exprCount; ++i) {
            char *type = EagleData::TypeToName(selectStage->bufferTypes[destinationExprs[i]]);
            if(i > 0) {
                signature << ", ";
            }
            signature << type;
            delete type;
        }
        signature << ")";
        
        sprintf(msg, "Function %s does not exist.", signature.str().c_str());
        
        if(NULL != selectStage) {
            selectStage->setError(EaglePlanErrorIdentifier, msg);
        }
        
        delete destinationExprs;
        delete argTypes;
        return EagleDbSqlExpression_ERROR;
    }
    
    selectStage->bufferTypes[destination] = function->returnType;
    
    /* generate signature with address destinations */
    std::stringstream signature(cast->name);
    
    signature << "(";
    for(i = 0; i < exprCount; ++i) {
        char *name = EagleData::TypeToName(selectStage->bufferTypes[destinationExprs[i]]);
        
        if(i > 0) {
            signature << ", ";
        }
        signature << "<" << destinationExprs[i] << "> (" << name << ")";
        
        delete name;
    }
    signature << ")";
    
    t1 = EagleData::TypeToName(selectStage->bufferTypes[destination]);
    sprintf(msg, "{ %s } into <%d> (%s)", signature.str().c_str(), destination, t1);
    delete t1;
    
    epo = eagle::plan::Operation::NewWithFunction(function->function, destination, destinationExprs, exprCount, true, msg);
    selectStage->addOperation(epo, true);
    ++*destinationBuffer;
    
    delete argTypes;
    return destination;
}

int EagleDbSqlExpression_CompilePlanIntoBuffer_Unary_(EagleDbSqlExpression *expression,
                                                      int *destinationBuffer,
                                                      eagle::plan::stage::SelectStage *selectStage)
{
    EagleDbSqlUnaryExpression *cast = (EagleDbSqlUnaryExpression*) expression;
    int destination, destinationLeft;
    char msg[1024], *t1, *t2, *beforeOp = NULL, *afterOp = NULL;
    eagle::plan::Operation *epo;
    EagleDbSqlUnaryOperator matchOp;
    bool matchedOp;
    
    /* operand */
    destinationLeft = EagleDbSqlExpression_CompilePlanIntoBuffer_(cast->expr, destinationBuffer, selectStage, false);
    if(EagleDbSqlExpression_ERROR == destinationLeft || selectStage->isError()) {
        return EagleDbSqlExpression_ERROR;
    }
    
    /* catch grouping operator */
    if(EagleDbSqlUnaryExpressionOperatorGrouping == cast->op) {
        return destinationLeft;
    }
    
    /* operator */
    destination = *destinationBuffer;
    
    t1 = EagleData::TypeToName(selectStage->bufferTypes[destinationLeft]);
    EagleDbSqlUnaryExpressionOperator_toString(cast->op, &beforeOp, &afterOp);
    
    matchedOp = EagleDbSqlUnaryExpression_GetOperation(cast->op,
                                                       selectStage->bufferTypes[destinationLeft],
                                                       &matchOp);
    if(false == matchedOp) {
        /* operator does not exist */
        sprintf(msg, "No such unary operator %s%s%s", beforeOp, t1, afterOp);
        selectStage->setError(EaglePlanErrorIdentifier, msg);
        
        delete t1;
        delete beforeOp;
        delete afterOp;
        return EagleDbSqlExpression_ERROR;
    }
    
    selectStage->bufferTypes[destination] = EagleData::Type::Integer;
    
    t2 = EagleData::TypeToName(selectStage->bufferTypes[destination]);
    
    sprintf(msg, "{ %s<%d>%s (%s) } into <%d> (%s)", beforeOp, destinationLeft, afterOp, t2, destination, t2);
    
    delete t1;
    delete t2;
    delete beforeOp;
    delete afterOp;
    
    epo = eagle::plan::Operation::NewWithPage(matchOp.func, destination, destinationLeft, -1, NULL, false, msg);
    selectStage->addOperation(epo, true);
    ++*destinationBuffer;
    
    return destination;
}

int EagleDbSqlExpression_CompilePlanIntoBuffer_Cast_(const EagleDbSqlExpression *expression,
                                                     int *destinationBuffer,
                                                     eagle::plan::stage::SelectStage *selectStage)
{
    EagleDbSqlCastExpression *cast = (EagleDbSqlCastExpression*) expression;
    int destination, destinationLeft;
    char msg[1024], *t1, *t2, *t3;
    eagle::plan::Operation *epo;
    EagleDbSqlCastOperator matchOp;
    bool matchedOp;
    
    /* left */
    destinationLeft = EagleDbSqlExpression_CompilePlanIntoBuffer_(cast->expr, destinationBuffer, selectStage, false);
    if(EagleDbSqlExpression_ERROR == destinationLeft || selectStage->isError()) {
        return EagleDbSqlExpression_ERROR;
    }
    
    /* operator */
    destination = *destinationBuffer;
    
    t1 = EagleData::TypeToName(selectStage->bufferTypes[destinationLeft]);
    t2 = EagleData::TypeToName(cast->castAs);
    
    matchedOp = EagleDbSqlCastExpression_GetOperation(selectStage->bufferTypes[destinationLeft],
                                                      cast->castAs,
                                                      &matchOp);
    if(false == matchedOp) {
        /* operator does not exist */
        sprintf(msg, "Can not cast %s to %s.", t1, t2);
        selectStage->setError(EaglePlanErrorIdentifier, msg);
        
        delete t1;
        delete t2;
        return EagleDbSqlExpression_ERROR;
    }
    
    selectStage->bufferTypes[destination] = matchOp.right;
    
    t3 = EagleData::TypeToName(selectStage->bufferTypes[destination]);
    
    sprintf(msg, "{ CAST <%d> (%s) } into <%d> (%s)", destinationLeft, t1, destination, t3);
    
    delete t1;
    delete t2;
    delete t3;
    
    epo = eagle::plan::Operation::NewWithPage(matchOp.func, destination, destinationLeft, -1, NULL, false, msg);
    selectStage->addOperation(epo, true);
    ++*destinationBuffer;
    
    return destination;
}

bool EagleDbSqlExpression_isLiteral(const EagleDbSqlExpression *expression)
{
    bool r = false;
    
    switch(expression->expressionType) {
            
        case EagleDbSqlExpressionTypeValue:
        {
            const EagleDbSqlValue *value = (EagleDbSqlValue*) expression;
            switch(value->type) {
                    
                case EagleDbSqlValueTypeAsterisk:
                case EagleDbSqlValueTypeIdentifier:
                    break;
                    
                case EagleDbSqlValueTypeFloat:
                case EagleDbSqlValueTypeInteger:
                case EagleDbSqlValueTypeString:
                    r = true;
                    break;
                    
            }
        }
            
        case EagleDbSqlExpressionTypeBinaryExpression:
        case EagleDbSqlExpressionTypeCastExpression:
        case EagleDbSqlExpressionTypeFunctionExpression:
        case EagleDbSqlExpressionTypeSelect:
        case EagleDbSqlExpressionTypeUnaryExpression:
            break;
            
    }
    
    return r;
}

int EagleDbSqlExpression_CompilePlanIntoBuffer_Binary_(const EagleDbSqlExpression *expression,
                                                       int *destinationBuffer,
                                                       eagle::plan::stage::SelectStage *selectStage)
{
    const EagleDbSqlBinaryExpression *cast = (EagleDbSqlBinaryExpression*) expression;
    int destination, destinationLeft, destinationRight;
    char msg[1024], *t1, *t2, *t3, *op;
    eagle::plan::Operation *epo;
    EagleDbSqlBinaryOperator matchOp;
    bool matchedOp, leftLiteral, rightLiteral;
    
    /* left */
    destinationLeft = EagleDbSqlExpression_CompilePlanIntoBuffer_(cast->left, destinationBuffer, selectStage, false);
    if(EagleDbSqlExpression_ERROR == destinationLeft || selectStage->isError()) {
        return EagleDbSqlExpression_ERROR;
    }
    
    /* right */
    destinationRight = EagleDbSqlExpression_CompilePlanIntoBuffer_(cast->right, destinationBuffer, selectStage, false);
    if(EagleDbSqlExpression_ERROR == destinationRight || selectStage->isError()) {
        return EagleDbSqlExpression_ERROR;
    }
    
    /* decide what type of binary expression it is */
    leftLiteral = EagleDbSqlExpression_isLiteral(cast->left);
    rightLiteral = EagleDbSqlExpression_isLiteral(cast->right);
    
    if(true == leftLiteral && true == rightLiteral) {
        /* this is a bit stupid and inefficient, but to handle a binary operation on two literals we create providers
         for each literal and handle it like a page operation. Issue #98 will address this. */
        
        /* notice that we do not check the error status after these because the error status has already been checked
         above */
        destinationLeft = EagleDbSqlExpression_CompilePlanIntoBuffer_(cast->left, destinationBuffer, selectStage, true);
        destinationRight = EagleDbSqlExpression_CompilePlanIntoBuffer_(cast->right, destinationBuffer, selectStage, true);
    }
    
    if(false == leftLiteral && true == rightLiteral) {
        matchedOp = EagleDbSqlBinaryExpression_GetLeftOperation(selectStage->bufferTypes[destinationLeft],
                                                                cast->op,
                                                                &matchOp);
    }
    else if(true == leftLiteral && false == rightLiteral) {
        matchedOp = EagleDbSqlBinaryExpression_GetRightOperation(selectStage->bufferTypes[destinationRight],
                                                                 cast->op,
                                                                 &matchOp);
    }
    else {
        matchedOp = EagleDbSqlBinaryExpression_GetPageOperation(selectStage->bufferTypes[destinationLeft],
                                                                cast->op,
                                                                selectStage->bufferTypes[destinationRight],
                                                                &matchOp);
    }
    
    /* operator */
    destination = *destinationBuffer;
    
    t1 = EagleData::TypeToName(selectStage->bufferTypes[destinationLeft]);
    t2 = EagleData::TypeToName(selectStage->bufferTypes[destinationRight]);
    op = EagleDbSqlBinaryExpressionOperator_toString(cast->op);
    
    if(false == matchedOp) {
        /* operator does not exist */
        sprintf(msg, "No such binary operator %s %s %s", t1, op, t2);
        selectStage->setError(EaglePlanErrorIdentifier, msg);
        
        delete t1;
        delete t2;
        delete op;
        return EagleDbSqlExpression_ERROR;
    }
    
    selectStage->bufferTypes[destination] = matchOp.returnType;
    
    t3 = EagleData::TypeToName(selectStage->bufferTypes[destination]);
    
    if(false == leftLiteral && true == rightLiteral) {
        sprintf(msg, "{ <%d> (%s) %s %s } into <%d> (%s)", destinationLeft, t1, op, t2, destination, t3);
        epo = eagle::plan::Operation::NewWithLiteral(matchOp.func, destination, destinationLeft,
                                                 (EagleDbSqlValue*) cast->right, false, msg);
    }
    else if(true == leftLiteral && false == rightLiteral) {
        sprintf(msg, "{ <%d> (%s) %s %s } into <%d> (%s)", destinationRight, t2, op, t1, destination, t3);
        epo = eagle::plan::Operation::NewWithLiteral(matchOp.func, destination, destinationRight,
                                                 (EagleDbSqlValue*) cast->left, false, msg);
    }
    else {
        sprintf(msg, "{ <%d> (%s) %s <%d> (%s) } into <%d> (%s)", destinationLeft, t1, op, destinationRight, t2,
                destination, t3);
        epo = eagle::plan::Operation::NewWithPage(matchOp.func, destination, destinationLeft, destinationRight, NULL, false,
                                              msg);
    }
    
    delete t1;
    delete t2;
    delete t3;
    delete op;
    
    selectStage->addOperation(epo, true);
    ++*destinationBuffer;
    
    return destination;
}

int EagleDbSqlExpression_CompilePlanIntoBuffer_Value_(EagleDbSqlExpression *expression,
                                                      int *destinationBuffer,
                                                      eagle::plan::stage::SelectStage *selectStage,
                                                      bool useProvider)
{
    EagleDbSqlValue *value = (EagleDbSqlValue*) expression;
    int destination = 0;
    
    switch(value->type) {
            
        case EagleDbSqlValueTypeInteger:
        {
            if(true == useProvider) {
                eagle::PageProvider *provider;
                eagle::PlanBufferProvider *bp;
                
                destination = *destinationBuffer;
                provider = eagle::SinglePageProvider::NewInt(value->value.intValue, selectStage->pageSize,
                                                             "(integer literal)");
                bp = new eagle::PlanBufferProvider(destination, provider, true);
                selectStage->addBufferProvider(bp, true);
                ++*destinationBuffer;
            }
            else {
                destination = *destinationBuffer;
            }
            
            selectStage->bufferTypes[destination] = EagleData::Type::Integer;
            break;
        }
        
        case EagleDbSqlValueTypeIdentifier:
        {
            /* find the provider for this column */
            eagle::PlanBufferProvider *provider = selectStage->getBufferProviderByName(value->value.identifier);
            if(NULL == provider) {
                char msg[128];
                sprintf(msg, "Unknown column '%s'", value->value.identifier);
                selectStage->setError(EaglePlanErrorIdentifier, msg);
                return EagleDbSqlExpression_ERROR;
            }
            
            selectStage->bufferTypes[provider->destinationBuffer] = provider->value.provider.provider->type;
            destination = provider->destinationBuffer;
            break;
        }
            
        case EagleDbSqlValueTypeAsterisk:
        {
            char msg[128];
            sprintf(msg, "You can not use the star operator like this.");
            selectStage->setError(EaglePlanErrorCompile, msg);
            destination = EagleDbSqlExpression_ERROR;
            break;
        }
            
        case EagleDbSqlValueTypeString:
        {
            if(true == useProvider) {
                eagle::PageProvider *provider;
                eagle::PlanBufferProvider *bp;
                
                destination = *destinationBuffer;
                provider = eagle::SinglePageProvider::NewVarchar(value->value.identifier, selectStage->pageSize,
                                                                 "(string literal)");
                bp = new eagle::PlanBufferProvider(destination, provider, true);
                selectStage->addBufferProvider(bp, true);
                ++*destinationBuffer;
            }
            else {
                destination = *destinationBuffer;
            }
            
            selectStage->bufferTypes[destination] = EagleData::Type::Varchar;
            break;
        }
            
        case EagleDbSqlValueTypeFloat:
        {
            if(true == useProvider) {
                eagle::PageProvider *provider;
                eagle::PlanBufferProvider *bp;
                
                destination = *destinationBuffer;
                provider = eagle::SinglePageProvider::NewFloat(value->value.floatValue, selectStage->pageSize, "(float)");
                bp = new eagle::PlanBufferProvider(destination, provider, true);
                selectStage->addBufferProvider(bp, true);
                ++*destinationBuffer;
            }
            else {
                destination = *destinationBuffer;
            }
            
            selectStage->bufferTypes[destination] = EagleData::Type::Float;
            break;
        }
            
    }
    
    return destination;
}

int EagleDbSqlExpression_CompilePlanIntoBuffer_(EagleDbSqlExpression *expression,
                                                int *destinationBuffer,
                                                eagle::plan::stage::SelectStage *selectStage,
                                                bool useProviderForValue)
{
    int r = EagleDbSqlExpression_ERROR;
    
    if(NULL == expression) {
        return EagleDbSqlExpression_ERROR;
    }
    if(NULL == selectStage) {
        return EagleDbSqlExpression_ERROR;
    }
    
    switch(expression->expressionType) {
            
        case EagleDbSqlExpressionTypeFunctionExpression:
            r = EagleDbSqlExpression_CompilePlanIntoBuffer_Function_(expression, destinationBuffer, selectStage);
            break;
            
        case EagleDbSqlExpressionTypeUnaryExpression:
            r = EagleDbSqlExpression_CompilePlanIntoBuffer_Unary_(expression, destinationBuffer, selectStage);
            break;
            
        case EagleDbSqlExpressionTypeBinaryExpression:
            r = EagleDbSqlExpression_CompilePlanIntoBuffer_Binary_(expression, destinationBuffer, selectStage);
            break;
            
        case EagleDbSqlExpressionTypeValue:
            r = EagleDbSqlExpression_CompilePlanIntoBuffer_Value_(expression, destinationBuffer, selectStage,
                                                                  useProviderForValue);
            break;
            
        case EagleDbSqlExpressionTypeCastExpression:
            r = EagleDbSqlExpression_CompilePlanIntoBuffer_Cast_(expression, destinationBuffer, selectStage);
            break;
            
        case EagleDbSqlExpressionTypeSelect:
            r = 0;
            break;
            
    }

    return r;
}

void EagleDbSqlExpression_CompilePlan(EagleDbSqlExpression **expressions,
                                      int totalExpressions,
                                      int whereClause,
                                      eagle::plan::stage::SelectStage *selectStage)
{
    int *results, destinationBuffer;
    
    /* for now we will just assume we don't need more than 20 buffers */
    selectStage->prepareBuffers(20);
    
    /* make sure we don't override buffers that are already assigned by providers */
    destinationBuffer = 0;
    
    /* prepare result providers */
    selectStage->resultFields = totalExpressions;
    selectStage->result = new eagle::PageProvider*[selectStage->resultFields];
    for(int i = 0; i < selectStage->resultFields; ++i) {
        selectStage->result[i] = NULL;
    }
    
    EagleLinkedList_Foreach(selectStage->providers, eagle::PlanBufferProvider*, provider)
    {
        if(provider->destinationBuffer >= destinationBuffer) {
            destinationBuffer = provider->destinationBuffer + 1;
        }
        
        /* each provider will go into a page, sync their types */
        selectStage->bufferTypes[provider->destinationBuffer] = provider->value.provider.provider->type;
    }
    EagleLinkedList_ForeachEnd
    
    /* compile expressions */
    results = new int[totalExpressions];
    for(int i = 0; i < totalExpressions; ++i) {
        char *desc;
        eagle::PageProvider *provider;
        EagleData::Type dataType;
        
        results[i] = EagleDbSqlExpression_CompilePlanIntoBuffer_(expressions[i], &destinationBuffer, selectStage, true);
        
        if(results[i] == EagleDbSqlExpression_ERROR || selectStage->isError()) {
            delete results;
            return;
        }
        
        /*
         since we don't know the type of result providers until after we evaluate the expression we can now setup the
         result provider now
         */
        desc = EagleDbSqlExpression_toString(expressions[i]);
        dataType = selectStage->bufferTypes[results[i]];
        provider = new eagle::StreamPageProvider(dataType, selectStage->pageSize, desc);
        selectStage->result[i] = provider;
        delete desc;
    }
    
    /* if there is a WHERE clause expression only send those records */
    for(int i = 0; i < totalExpressions; ++i) {
        eagle::plan::Operation *epo;
        char msg[1024];
        
        if(i != whereClause) {
            if(whereClause >= 0) {
                /* send some result data to the provider */
                char *t1, *t2, *t3;
                
                t1 = EagleData::TypeToName(selectStage->bufferTypes[results[whereClause]]);
                t2 = EagleData::TypeToName(selectStage->bufferTypes[results[i]]);
                t3 = EagleData::TypeToName(selectStage->result[i]->type);
                
                sprintf(msg, "WHERE <%d> (%s), send <%d> (%s) to provider <%d> (%s)", results[whereClause], t1,
                        results[i], t2, i, t3);
                epo = eagle::plan::Operation::NewWithPage(EaglePageOperations_SendPageToProvider, -1, results[whereClause],
                                                     results[i], selectStage->result[i], false, msg);
                selectStage->addOperation(epo, true);
                
                delete t1;
                delete t2;
                delete t3;
            }
            else {
                char *t1, *t2;
                
                t1 = EagleData::TypeToName(selectStage->bufferTypes[results[i]]);
                t2 = EagleData::TypeToName(selectStage->result[i]->type);
                
                /* send all the result data to the provider */
                sprintf(msg, "ALL <%d> (%s) to provider <%d> (%s)", results[i], t1, i, t2);
                epo = eagle::plan::Operation::NewWithPage(EaglePageOperations_SendPageToProvider, -1, -1, results[i],
                                                     selectStage->result[i], false, msg);
                selectStage->addOperation(epo, true);
                
                delete t1;
                delete t2;
            }
        }
    }
    
    delete results;
}

void EagleDbSqlExpression_Delete(EagleDbSqlExpression *expr)
{
    if(NULL == expr) {
        return;
    }
    
    switch(expr->expressionType) {
            
        case EagleDbSqlExpressionTypeUnaryExpression:
            EagleDbSqlUnaryExpression_Delete((EagleDbSqlUnaryExpression*) expr);
            break;
            
        case EagleDbSqlExpressionTypeBinaryExpression:
            EagleDbSqlBinaryExpression_Delete((EagleDbSqlBinaryExpression*) expr);
            break;
            
        case EagleDbSqlExpressionTypeSelect:
            EagleDbSqlSelect_Delete((EagleDbSqlSelect*) expr);
            break;
            
        case EagleDbSqlExpressionTypeValue:
            EagleDbSqlValue_Delete((EagleDbSqlValue*) expr);
            break;
            
        case EagleDbSqlExpressionTypeFunctionExpression:
            EagleDbSqlFunctionExpression_Delete((EagleDbSqlFunctionExpression*) expr);
            break;
            
        case EagleDbSqlExpressionTypeCastExpression:
            EagleDbSqlCastExpression_Delete((EagleDbSqlCastExpression*) expr);
            break;
            
    }
}

void EagleDbSqlExpression_DeleteRecursive(EagleDbSqlExpression *expr)
{
    if(NULL == expr) {
        return;
    }
    
    switch(expr->expressionType) {
            
        case EagleDbSqlExpressionTypeUnaryExpression:
            EagleDbSqlUnaryExpression_DeleteRecursive((EagleDbSqlUnaryExpression*) expr);
            break;
            
        case EagleDbSqlExpressionTypeBinaryExpression:
            EagleDbSqlBinaryExpression_DeleteRecursive((EagleDbSqlBinaryExpression*) expr);
            break;
            
        case EagleDbSqlExpressionTypeSelect:
            EagleDbSqlSelect_DeleteRecursive((EagleDbSqlSelect*) expr);
            break;
            
        case EagleDbSqlExpressionTypeValue:
            EagleDbSqlValue_Delete((EagleDbSqlValue*) expr);
            break;
            
        case EagleDbSqlExpressionTypeFunctionExpression:
            EagleDbSqlFunctionExpression_DeleteRecursive((EagleDbSqlFunctionExpression*) expr);
            break;
            
        case EagleDbSqlExpressionTypeCastExpression:
            EagleDbSqlCastExpression_DeleteRecursive((EagleDbSqlCastExpression*) expr);
            break;
            
    }
}

char* EagleDbSqlExpression_toString(EagleDbSqlExpression *expr)
{
    char *r = NULL;
    
    if(NULL == expr) {
        return strdup("");
    }
    
    switch(expr->expressionType) {
            
        case EagleDbSqlExpressionTypeUnaryExpression:
            r = EagleDbSqlUnaryExpression_toString((EagleDbSqlUnaryExpression*) expr);
            break;
            
        case EagleDbSqlExpressionTypeBinaryExpression:
            r = EagleDbSqlBinaryExpression_toString((EagleDbSqlBinaryExpression*) expr);
            break;
            
        case EagleDbSqlExpressionTypeSelect:
            r = EagleDbSqlSelect_toString((EagleDbSqlSelect*) expr);
            break;
            
        case EagleDbSqlExpressionTypeValue:
            r = EagleDbSqlValue_toString((EagleDbSqlValue*) expr);
            break;
            
        case EagleDbSqlExpressionTypeFunctionExpression:
            r = EagleDbSqlFunctionExpression_toString((EagleDbSqlFunctionExpression*) expr);
            break;
            
        case EagleDbSqlExpressionTypeCastExpression:
            r = EagleDbSqlCastExpression_toString((EagleDbSqlCastExpression*) expr);
            break;
            
    }

    return r;
}
