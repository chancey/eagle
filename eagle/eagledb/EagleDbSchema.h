#ifndef eagle_EagleDbSchema_h
#define eagle_EagleDbSchema_h

#include "eagle/db/TableData.h"
#include "eagle/EagleLinkedList.h"

/**
 * A database schema definition.
 */
class EagleDbSchema
{
    
public:
    
    /**
     * The name of the default schema.
     */
    static const char *DefaultSchemaName;
    
    /**
     * The name of the information_schema.
     */
    static const char *InformationSchemaName;
    
    /**
     * This is semi managed because the linked list that contains the pointers to the tables is managed internally but
     * the table objects themselves are managed externally (i.e. deleteing a EagleDbSchema will not delete the tables it
     * contains)
     */
    EagleLinkedList<eagle::db::TableData*> *tables;
    
    /**
     * Schema name.
     */
    char *name;
    
public:
    
    EagleDbSchema(const char *name);
    
    virtual ~EagleDbSchema();
    
    bool addTable(eagle::db::TableData *td);
    
    eagle::db::TableData* getTable(const char *tableName);
    
};

#endif
