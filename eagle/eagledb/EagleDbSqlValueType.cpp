#include <string.h>
#include "EagleDbSqlValueType.h"

char* EagleDbSqlValueType_toString(EagleDbSqlValueType type)
{
    char *r = NULL;
    
    switch(type) {
            
        case EagleDbSqlValueTypeAsterisk:
            r = strdup("Asterisk");
            break;
            
        case EagleDbSqlValueTypeFloat:
            r = strdup("FLOAT");
            break;
            
        case EagleDbSqlValueTypeIdentifier:
            r = strdup("Identifier");
            break;
            
        case EagleDbSqlValueTypeInteger:
            r = strdup("INTEGER");
            break;
            
        case EagleDbSqlValueTypeString:
            r = strdup("VARCHAR");
            break;
            
    }

    return r;
}
