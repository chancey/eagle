#ifndef eagle_EagleDbSqlFunctionExpression_h
#define eagle_EagleDbSqlFunctionExpression_h

#include "EagleDbSqlExpression.h"

/**
 * Expression type is EagleDbSqlExpressionTypeFunctionExpression.
 * @see EagleDbSqlExpressionType
 */
typedef struct {
    
    /**
     Header.
     @see EagleDbSqlExpressionHeader
     */
    EagleDbSqlExpressionHeader;
    
    /**
     The name of the function.
     */
    char *name;
    
    /**
     Zero or more arguments.
     */
    EagleLinkedList<EagleDbSqlExpression*> *exprs;
    
} EagleDbSqlFunctionExpression;

/**
 * Create a new EagleDbSqlFunctionExpression.
 * @param [in] name Function name.
 * @param [in] exprs A list of zero or more EagleDbSqlExpression's.
 * @return A new instance.
 */
EagleDbSqlFunctionExpression* EagleDbSqlFunctionExpression_New(const char *name,
                                                               EagleLinkedList<EagleDbSqlExpression*> *exprs);

/**
 * Free a function expression.
 * @note It is safer to use EagleDbSqlExpression_Delete() since it will automatically use the correct Delete function.
 * @param [in] expr The instance.
 */
void EagleDbSqlFunctionExpression_Delete(EagleDbSqlFunctionExpression *expr);

/**
 * Free a function expression (recursively).
 * @note It is safer to use EagleDbSqlExpression_DeleteRecursive() since it will automatically use the correct Delete
 *       function.
 * @param [in] expr The instance.
 */
void EagleDbSqlFunctionExpression_DeleteRecursive(EagleDbSqlFunctionExpression *expr);

/**
 * Render a function expression into a string.
 * @param [in] expr The instance.
 * @return A new string representation of the expression.
 */
char* EagleDbSqlFunctionExpression_toString(EagleDbSqlFunctionExpression *expr);

/**
 * Shortcut for EagleDbSqlFunctionExpression_New().
 * @param [in] name The name of the function.
 * @param [in] expr The first and only argument expression.
 * @param [in] freeExpr Free the \p expr with this object.
 * @return A new EagleDbSqlFunctionExpression instance.
 */
EagleDbSqlFunctionExpression* EagleDbSqlFunctionExpression_NewWithSingleArgument(const char *name,
                                                                                 EagleDbSqlExpression *expr,
                                                                                 bool freeExpr);

/**
 * Shortcut for EagleDbSqlFunctionExpression_New().
 * @param [in] name The name of the function.
 * @param [in] expr1 The first argument expression.
 * @param [in] freeExpr1 Free the \p expr1 with this object.
 * @param [in] expr2 The second argument expression.
 * @param [in] freeExpr2 Free the \p expr2 with this object.
 * @return A new EagleDbSqlFunctionExpression instance.
 */
EagleDbSqlFunctionExpression* EagleDbSqlFunctionExpression_NewWithTwoArguments(const char *name,
                                                                               EagleDbSqlExpression *expr1,
                                                                               bool freeExpr1,
                                                                               EagleDbSqlExpression *expr2,
                                                                               bool freeExpr2);

/**
 * Get the string signature for a function.
 * @param [in] expr The function expression.
 * @return The string signature for the function.
 */
char* EagleDbSqlFunctionExpression_getSignature(EagleDbSqlFunctionExpression *expr);

#endif
