#ifndef eagle_EagleDbInformationSchema_h
#define eagle_EagleDbInformationSchema_h

#include "EagleDbSchema.h"
#include "EagleDbInstance.h"

/**
 * This object is used for callback functions on virtual tables.
 */
typedef struct {
    
    /**
     * The DB instance.
     */
    EagleDbInstance *db;
    
    /**
     * Record offset.
     */
    int recordOffset;
    
    /**
     * The column (field) name.
     */
    std::string columnName;
    
} EagleDbInformationSchema;

/**
 * Represents INFORMATION_SCHEMA.TABLES
 */
class EagleDbInformationSchemaTables : public eagle::PageProvider
{
    
protected:
    
    /**
     * The information schema.
     */
    EagleDbInformationSchema *infoSchema;
    
public:
    
    EagleDbInformationSchemaTables(int thePageSize, std::string theName, EagleData::Type theType,
                                   EagleDbInformationSchema *theInfoSchema);
    
    /**
     * Free.
     */
    virtual ~EagleDbInformationSchemaTables();
    
    virtual int pagesRemaining();
    
    virtual EaglePage* nextPage();
    
    virtual void reset();
    
};

/**
 Create a new EagleDbInformationSchema object.
 @param [in] db Instance.
 @param [in] columnName The column name.
 @return A new object.
 */
EagleDbInformationSchema* EagleDbInformationSchema_New(EagleDbInstance *db, std::string columnName);

/**
 Initialise the virtual tables for the information_schema.
 @param [in] db Instance.
 @param [in] schema Schema to load virtual tables into.
 */
void EagleDbInformationSchema_Init(EagleDbInstance *db, EagleDbSchema *schema);

/**
 Remove/free the virtual tables.
 @param [in] db Instance.
 */
void EagleDbInformationSchema_Cleanup(EagleDbInstance *db);

/**
 Free a EagleDbInformationSchema.
 @param [in] infoSchema Information schema.
 */
void EagleDbInformationSchema_Delete(EagleDbInformationSchema *infoSchema);

#endif
