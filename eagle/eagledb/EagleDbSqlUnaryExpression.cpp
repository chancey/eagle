#include <stdlib.h>
#include <stdio.h>
#include "EagleDbSqlUnaryExpression.h"

EagleDbSqlUnaryExpression* EagleDbSqlUnaryExpression_New(EagleDbSqlUnaryExpressionOperator op, EagleDbSqlExpression *expr)
{
    EagleDbSqlUnaryExpression *exp = new EagleDbSqlUnaryExpression();
    
    exp->expressionType = EagleDbSqlExpressionTypeUnaryExpression;
    exp->expr = expr;
    exp->op = op;
    
    return exp;
}

void EagleDbSqlUnaryExpression_Delete(EagleDbSqlUnaryExpression *expr)
{
    if(NULL == expr) {
        return;
    }
    
    delete expr;
}

void EagleDbSqlUnaryExpression_DeleteRecursive(EagleDbSqlUnaryExpression *expr)
{
    if(NULL == expr) {
        return;
    }
    
    EagleDbSqlExpression_DeleteRecursive((EagleDbSqlExpression*) expr->expr);
    delete expr;
}

char* EagleDbSqlUnaryExpression_toString(EagleDbSqlUnaryExpression *expr)
{
    char* s = new char[1024], *exp;
    char *beforeOp = NULL, *afterOp = NULL;
    
    EagleDbSqlUnaryExpressionOperator_toString(expr->op, &beforeOp, &afterOp);
    exp = EagleDbSqlExpression_toString(expr->expr);
    
    sprintf(s, "%s%s%s", beforeOp, exp, afterOp);
    
    delete beforeOp;
    delete afterOp;
    delete exp;
    return s;
}

bool EagleDbSqlUnaryExpression_GetOperation(EagleDbSqlUnaryExpressionOperator op,
                                            EagleData::Type right,
                                            EagleDbSqlUnaryOperator *match)
{
    unsigned long i;
    static EagleDbSqlUnaryOperator ops[] = {
        /* Integer                   operator  right    function           returns */
        EagleDbSqlUnaryOperator_Make(Negate,   Integer, NegatePageInt,     Integer),
        EagleDbSqlUnaryOperator_Make(Not,      Integer, NotPageInt,        Integer),
        
        /* Float                     operator  right    function           returns */
        EagleDbSqlUnaryOperator_Make(Negate,   Float,   NegatePageFloat,   Float),
    };
    
    for(i = 0; i < sizeof(ops) / sizeof(EagleDbSqlUnaryOperator); ++i) {
        if(right == ops[i].right && op == ops[i].op) {
            *match = ops[i];
            return true;
        }
    }
    
    return false;
}
