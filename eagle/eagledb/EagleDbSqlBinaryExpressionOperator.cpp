#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "EagleDbSqlBinaryExpressionOperator.h"

char* EagleDbSqlBinaryExpressionOperator_toString(EagleDbSqlBinaryExpressionOperator op)
{
    char *r = NULL;
    
    switch(op) {
            
        case EagleDbSqlBinaryExpressionOperatorPlus:
            r = strdup("+");
            break;
            
        case EagleDbSqlBinaryExpressionOperatorEquals:
            r = strdup("=");
            break;
            
        case EagleDbSqlBinaryExpressionOperatorModulus:
            r = strdup("%");
            break;
            
        case EagleDbSqlBinaryExpressionOperatorMultiply:
            r = strdup("*");
            break;
            
        case EagleDbSqlBinaryExpressionOperatorNotEquals:
            r = strdup("!=");
            break;
            
        case EagleDbSqlBinaryExpressionOperatorGreaterThan:
            r = strdup(">");
            break;
            
        case EagleDbSqlBinaryExpressionOperatorLessThan:
            r = strdup("<");
            break;
            
        case EagleDbSqlBinaryExpressionOperatorGreaterThanEqual:
            r = strdup(">=");
            break;
            
        case EagleDbSqlBinaryExpressionOperatorLessThanEqual:
            r = strdup("<=");
            break;
            
        case EagleDbSqlBinaryExpressionOperatorMinus:
            r = strdup("-");
            break;
            
        case EagleDbSqlBinaryExpressionOperatorDivide:
            r = strdup("/");
            break;
            
        case EagleDbSqlBinaryExpressionOperatorOr:
            r = strdup("OR");
            break;
            
        case EagleDbSqlBinaryExpressionOperatorAnd:
            r = strdup("AND");
            break;
            
    }

    return r;
}
