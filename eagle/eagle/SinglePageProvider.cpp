#include <string.h>
#include "eagle/SinglePageProvider.h"
#include "eagle/ArrayPageProvider.h"

eagle::SinglePageProvider* eagle::SinglePageProvider::NewInt(EagleDataTypeInteger value, int recordsPerPage,
                                                             std::string name)
{
    eagle::SinglePageProvider *pageProvider = new eagle::SinglePageProvider();
    
    pageProvider->name = name;
    pageProvider->type = EagleData::Type::Integer;
    pageProvider->recordsPerPage = recordsPerPage;
    pageProvider->value.intValue = value;
    
    return pageProvider;
}

eagle::SinglePageProvider* eagle::SinglePageProvider::NewFloat(EagleDataTypeFloat value, int recordsPerPage,
                                                               std::string name)
{
    eagle::SinglePageProvider *pageProvider = new eagle::SinglePageProvider();
    
    pageProvider->name = name;
    pageProvider->type = EagleData::Type::Float;
    pageProvider->recordsPerPage = recordsPerPage;
    pageProvider->value.floatValue = value;
    
    return pageProvider;
}

eagle::SinglePageProvider* eagle::SinglePageProvider::NewVarchar(const char *value, int recordsPerPage,
                                                                 std::string name)
{
    eagle::SinglePageProvider *pageProvider = new eagle::SinglePageProvider();
    
    pageProvider->name = name;
    pageProvider->type = EagleData::Type::Varchar;
    pageProvider->recordsPerPage = recordsPerPage;
    pageProvider->value.strValue = (NULL == value ? NULL : strdup(value));
    
    return pageProvider;
}

eagle::SinglePageProvider::~SinglePageProvider()
{
    switch(type) {
            
        case EagleData::Type::Integer:
        case EagleData::Type::Unknown:
        case EagleData::Type::Float:
            break;
            
        case EagleData::Type::Varchar:
            delete value.strValue;
            break;
            
    }
}

EaglePage* eagle::SinglePageProvider::nextPage()
{
    EaglePage *page = NULL;
    
    /* fill page */
    switch(type) {
            
        case EagleData::Type::Unknown:
            return NULL;
            
        case EagleData::Type::Integer:
        {
            // Allocate data for page.
            EagleDataTypeInteger *data = new EagleDataTypeInteger[recordsPerPage];
            
            for(int i = 0; i < recordsPerPage; ++i) {
                data[i] = value.intValue;
            }
            
            page = new EaglePage(EagleData::Type::Integer, data, recordsPerPage, recordsPerPage, 0, true);
            break;
        }
            
        case EagleData::Type::Varchar:
        {
            // Allocate data for page.
            EagleDataTypeVarchar *data = new EagleDataTypeVarchar[recordsPerPage];
            
            for(int i = 0; i < recordsPerPage; ++i) {
                data[i] = strdup(value.strValue);
            }
            
            page = new EaglePage(EagleData::Type::Varchar, data, recordsPerPage, recordsPerPage, 0, true);
            break;
        }
            
        case EagleData::Type::Float:
        {
            // Allocate data for page.
            EagleDataTypeFloat *data = new EagleDataTypeFloat[recordsPerPage];
            
            for(int i = 0; i < recordsPerPage; ++i) {
                data[i] = value.floatValue;
            }
            
            page = new EaglePage(EagleData::Type::Float, data, recordsPerPage, recordsPerPage, 0, true);
            break;
        }
            
    }
    
    return page;
}

int eagle::SinglePageProvider::pagesRemaining()
{
    /* unlimited supply of pages */
    return 1;
}

bool eagle::SinglePageProvider::isRandomAccess()
{
    return true;
}

EaglePage* eagle::SinglePageProvider::getPage(int pageNumber)
{
    EaglePage *page;
    
    page = nextPage();
    if(NULL != page) {
        page->recordOffset = pageNumber * recordsPerPage;
    }
    
    return page;
}
