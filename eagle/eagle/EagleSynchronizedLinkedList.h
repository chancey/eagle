#ifndef eagle_EagleSynchronizedLinkedList_h
#define eagle_EagleSynchronizedLinkedList_h

#include "EagleLinkedList.h"

/**
 * A synchronized linked list (FIFO).
 */
template <typename ItemType = void*>
class EagleSynchronizedLinkedList : public EagleLinkedList<ItemType>
{
    
protected:
    
    /**
     * Read-write lock.
     */
    EagleReadWriteLock *lock;
    
public:
    
    /**
     * Create a new synchronized linked list.
     * @return A new linked list.
     */
    EagleSynchronizedLinkedList();
    
    /**
     * @brief Delete a linked list and all the items contained in it.
     */
    virtual ~EagleSynchronizedLinkedList();
    
    virtual void add(typename EagleLinkedList<ItemType>::Item *item);
    
    virtual int length() const;
    
    virtual ItemType first() const;
    
    virtual ItemType last() const;
    
    virtual typename EagleLinkedList<ItemType>::Item* pop();
    
    virtual bool isEmpty() const;
    
    virtual typename EagleLinkedList<ItemType>::Item* begin() const;
    
    virtual typename EagleLinkedList<ItemType>::Item* end() const;
    
    virtual ItemType* toArray(int *theSize) const;
    
    virtual std::string toString(char* (*toStringFunction)(ItemType), std::string glue) const;
    
    virtual bool deleteObject(ItemType obj);
    
    virtual ItemType get(int idx) const;
    
    virtual typename EagleLinkedList<ItemType>::Item* shift();
    
};

template <typename ItemType>
EagleSynchronizedLinkedList<ItemType>::~EagleSynchronizedLinkedList()
{
    delete lock;
}

template <typename ItemType>
EagleSynchronizedLinkedList<ItemType>::EagleSynchronizedLinkedList()
{
    this->init();
    lock = new EagleReadWriteLock();
}

template <typename ItemType>
void EagleSynchronizedLinkedList<ItemType>::add(typename EagleLinkedList<ItemType>::Item *item)
{
    lock->holdWriteLock();
    this->add_(item);
    lock->unlock();
}

template <typename ItemType>
int EagleSynchronizedLinkedList<ItemType>::length() const
{
    int l = 0;
    
    lock->holdReadLock();
    l = this->length_();
    lock->unlock();
    
    return l;
}

template <typename ItemType>
ItemType EagleSynchronizedLinkedList<ItemType>::first() const
{
    ItemType r = NULL;
    
    lock->holdReadLock();
    r = this->first_();
    lock->unlock();
    
    return r;
}

template <typename ItemType>
ItemType EagleSynchronizedLinkedList<ItemType>::last() const
{
    ItemType r = NULL;
    
    lock->holdReadLock();
    r = this->last_();
    lock->unlock();
    
    return r;
}

template <typename ItemType>
typename EagleLinkedList<ItemType>::Item* EagleSynchronizedLinkedList<ItemType>::begin() const
{
    typename EagleLinkedList<ItemType>::Item *r = NULL;
    
    lock->holdReadLock();
    r = this->begin_();
    lock->unlock();
    
    return r;
}

template <typename ItemType>
typename EagleLinkedList<ItemType>::Item* EagleSynchronizedLinkedList<ItemType>::end() const
{
    typename EagleLinkedList<ItemType>::Item *r = NULL;
    
    lock->holdReadLock();
    r = this->end_();
    lock->unlock();
    
    return r;
}

template <typename ItemType>
typename EagleLinkedList<ItemType>::Item* EagleSynchronizedLinkedList<ItemType>::shift()
{
    typename EagleLinkedList<ItemType>::Item *r = NULL;
    
    lock->holdWriteLock();
    r = this->shift_();
    lock->unlock();
    
    return r;
}

template <typename ItemType>
typename EagleLinkedList<ItemType>::Item* EagleSynchronizedLinkedList<ItemType>::pop()
{
    typename EagleLinkedList<ItemType>::Item *r = NULL;
    
    lock->holdWriteLock();
    r = this->pop_();
    lock->unlock();
    
    return r;
}

template <typename ItemType>
bool EagleSynchronizedLinkedList<ItemType>::isEmpty() const
{
    bool r = true;
    
    lock->holdReadLock();
    r = this->isEmpty_();
    lock->unlock();
    
    return r;
}

template <typename ItemType>
ItemType* EagleSynchronizedLinkedList<ItemType>::toArray(int *theSize) const
{
    ItemType *array = NULL;
    
    lock->holdReadLock();
    array = this->toArray_(theSize);
    lock->unlock();
    
    return array;
}

template <typename ItemType>
ItemType EagleSynchronizedLinkedList<ItemType>::get(int idx) const
{
    ItemType r = NULL;
    
    lock->holdReadLock();
    r = this->get_(idx);
    lock->unlock();
    
    return r;
}

template <typename ItemType>
bool EagleSynchronizedLinkedList<ItemType>::deleteObject(ItemType obj)
{
    bool r;
    
    lock->holdWriteLock();
    r = this->deleteObject_(obj);
    lock->unlock();
    
    return r;
}

template <typename ItemType>
std::string EagleSynchronizedLinkedList<ItemType>::toString(char* (*toStringFunction)(ItemType), std::string glue) const
{
    lock->holdWriteLock();
    std::string result = this->toString_(toStringFunction, glue);
    lock->unlock();
    return result;
}

#endif
