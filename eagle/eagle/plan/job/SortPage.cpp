#include <stdio.h>
#include <stdlib.h>
#include "eagle/plan/job/SortPage.h"
#include "eagle/page/Sort.h"
#include "eagle/plan/stage/EntireSort.h"

eagle::plan::job::SortPage::SortPage(eagle::plan::stage::EntireSort *theStage, EaglePage *theOriginalPage)
    : eagle::plan::job::AbstractJob(theStage)
{
    // convert the page to a Sort page
    originalPage = theOriginalPage;
    sortPage = eagle::page::Sort::CreateFromPage(originalPage);
    sortPage->working = true;
}

eagle::plan::job::SortPage::~SortPage()
{
    sortPage->working = false;
    delete originalPage;
}

int eagle::plan::job::SortPage::CompareVarchar(const void* a, const void* b)
{
    DataWithId<EagleDataTypeVarchar> *left = (DataWithId<EagleDataTypeVarchar>*) a;
    DataWithId<EagleDataTypeVarchar> *right = (DataWithId<EagleDataTypeVarchar>*) b;
    return strcmp(left->data, right->data);
}

template<typename T>
int eagle::plan::job::SortPage::Compare(const void* a, const void* b)
{
    DataWithId<T> *left = (DataWithId<T>*) a, *right = (DataWithId<T>*) b;
    if(left->data < right->data) {
        return -1;
    }
    if(left->data > right->data) {
        return 1;
    }
    return 0;
}

template<typename T>
void eagle::plan::job::SortPage::sort(int (*compare)(const void*, const void*))
{
    // create a new data set for sorting
    DataWithId<T> *d = new DataWithId<T>[sortPage->totalSize];
    for(int i = 0; i < sortPage->totalSize; ++i) {
        d[i].recordId = sortPage->recordIds[i];
        d[i].data = ((T*) sortPage->data)[i];
    }
    
    // sort data
    qsort(d, (size_t) sortPage->totalSize, sizeof(DataWithId<T>), compare);
    
    // copy sorted data back
    for(int i = 0; i < sortPage->totalSize; ++i) {
        sortPage->recordIds[i] = d[i].recordId;
        ((T*) sortPage->data)[i] = d[i].data;
    }
    
    delete[] d;
}

void eagle::plan::job::SortPage::run()
{
    switch(sortPage->type) {
        
        // primitives can use the template
            
        case EagleData::Type::Integer:
            sort<EagleDataTypeInteger>(Compare<EagleDataTypeInteger>);
            break;
            
        case EagleData::Type::Float:
            sort<EagleDataTypeFloat>(Compare<EagleDataTypeFloat>);
            break;
        
        case EagleData::Type::Varchar:
            sort<EagleDataTypeVarchar>(CompareVarchar);
            break;
            
        // don't know what to do in this case...
        case EagleData::Type::Unknown:
            return;
            
    }
    
    // add this page onto the merge sorter
    ((eagle::plan::stage::EntireSort*) stage)->mergeSortStack->addObject(sortPage, true);
}
