#ifndef eagle_plan_job_MergePages_h
#define eagle_plan_job_MergePages_h

#include "eagle/plan/job/AbstractJob.h"
#include "eagle/EagleData.h"

class EagleLock;

namespace eagle
{
    namespace page
    {
        class Sort;
    }
    
    namespace plan
    {
        namespace job
        {
            class MergePages;
        }
        
        namespace stage
        {
            class EntireSort;
        }
    }
}

class eagle::plan::job::MergePages : public eagle::plan::job::AbstractJob
{
    
    // @todo make protected
public:
    
    /**
     * One of the pages to be merged.
     */
    eagle::page::Sort *page1;
    
    /**
     * One of the pages to be merged.
     */
    eagle::page::Sort *page2;
    
    /**
     * The result page that will have all the data from \c page1 and \c page2 sorted and merged together.
     */
    eagle::page::Sort *mergedPage;
    
public:
    
    MergePages(eagle::plan::stage::EntireSort *theStage, eagle::page::Sort *thePage1, eagle::page::Sort *thePage2,
               eagle::page::Sort *theMergedPage);
    
    /**
     * Delete a stage.
     */
    virtual ~MergePages();
    
    virtual void run();
    
protected:
    
    template<typename T>
    void runForType();
    
    template<typename T>
    static bool LessThan(T a, T b);
    
    template<typename T>
    static T CopyValue(T data);
    
};

#endif
