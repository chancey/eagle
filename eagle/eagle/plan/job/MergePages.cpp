#include <stdio.h>
#include <stdlib.h>
#include "eagle/plan/job/MergePages.h"
#include "eagle/plan/stage/EntireSort.h"
#include "eagle/page/Sort.h"
#include "eagle/EagleLock.h"

eagle::plan::job::MergePages::MergePages(eagle::plan::stage::EntireSort *theStage, eagle::page::Sort *thePage1,
                                         eagle::page::Sort *thePage2, eagle::page::Sort *theMergedPage)
    : eagle::plan::job::AbstractJob(theStage)
{
    page1 = thePage1;
    page2 = thePage2;
    mergedPage = theMergedPage;
}

eagle::plan::job::MergePages::~MergePages()
{
    page1->freeData = true;
    delete page1;
    page2->freeData = true;
    delete page2;
}

namespace eagle {
    namespace plan {
        namespace job {
            template<>
            bool MergePages::LessThan<EagleDataTypeVarchar>(EagleDataTypeVarchar a, EagleDataTypeVarchar b)
            {
                return strcmp(a, b) < 0;
            }
        }
    }
}

template<typename T>
bool eagle::plan::job::MergePages::LessThan(T a, T b)
{
    return a < b;
}

template<typename T>
T eagle::plan::job::MergePages::CopyValue(T data)
{
    return data;
}

namespace eagle {
    namespace plan {
        namespace job {

            template<>
            EagleDataTypeVarchar eagle::plan::job::MergePages::CopyValue(EagleDataTypeVarchar data)
            {
                return strdup(data);
            }

        }
    }
}

template<typename T>
void eagle::plan::job::MergePages::runForType()
{
    T *d1 = (T*) page1->data;
    T *d2 = (T*) page2->data;
    
    int preallocateSize = page1->totalSize + page2->totalSize;
    T *data = new T[preallocateSize];
    int *recordIds = new int[preallocateSize];
    
    for(int i = 0, j = 0, k = 0; k < preallocateSize; ++k) {
        // list has ended for finalOrder
        if(i > page1->count - 1) {
            recordIds[k] = page2->recordIds[j];
            data[k] = CopyValue<T>(d2[j]);
            ++j;
            continue;
        }
        
        // list has ended for page
        if(j > page2->totalSize - 1) {
            recordIds[k] = page1->recordIds[i];
            data[k] = CopyValue<T>(d1[i]);
            ++i;
            continue;
        }
        
        // find the max
        if(LessThan<T>(d1[i], d2[j])) {
            recordIds[k] = page1->recordIds[i];
            data[k] = CopyValue<T>(d1[i]);
            ++i;
            continue;
        }
        
        recordIds[k] = page2->recordIds[j];
        data[k] = CopyValue<T>(d2[j]);
        ++j;
    }
    
    mergedPage->data = data;
    mergedPage->recordIds = recordIds;
    mergedPage->count = mergedPage->totalSize = preallocateSize;
}

void eagle::plan::job::MergePages::run()
{
    switch(page1->type) {
            
        case EagleData::Type::Integer:
            runForType<EagleDataTypeInteger>();
            break;
            
        case EagleData::Type::Float:
            runForType<EagleDataTypeFloat>();
            break;
            
        case EagleData::Type::Varchar:
            runForType<EagleDataTypeVarchar>();
            break;
            
        // don't know what to do in this case...
        case EagleData::Type::Unknown:
            break;
            
    }
    
    page1->working = page2->working = mergedPage->working = false;
}
