#ifndef eagle_plan_job_SortPage_h
#define eagle_plan_job_SortPage_h

#include "eagle/plan/job/AbstractJob.h"
#include "eagle/EagleData.h"

class EaglePage;

namespace eagle
{
    namespace page
    {
        class Sort;
    }
    
    namespace plan
    {
        namespace job
        {
            class SortPage;
        }
        
        namespace stage
        {
            class EntireSort;
        }
    }
}

class eagle::plan::job::SortPage : public eagle::plan::job::AbstractJob
{
    
    // @todo make protected
public:
    
    /**
     * Destination page.
     */
    eagle::page::Sort *sortPage;
    
    /**
     * Reference to the original page.
     */
    EaglePage *originalPage;
    
public:
    
    SortPage(eagle::plan::stage::EntireSort *theStage, EaglePage *theSortPage);
    
    virtual ~SortPage();
    
    virtual void run();
    
    template<typename T>
    struct DataWithId
    {
        
        /**
         * The record ID.
         */
        int recordId;
        
        /**
         * Data.
         */
        T data;
        
    };
    
    template<typename T>
    void sort(int (*compare)(const void*, const void*));
    
    template<typename T>
    static int Compare(const void *a, const void *b);
    
    static int CompareVarchar(const void *a, const void *b);
    
};

#endif
