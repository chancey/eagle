#ifndef eagle_plan_job_AbstractJob_h
#define eagle_plan_job_AbstractJob_h

namespace eagle
{
    class Plan;
    
    namespace plan
    {
        namespace job
        {
            class AbstractJob;
        }
        
        namespace stage
        {
            class AbstractStage;
        }
    }
}

class eagle::plan::job::AbstractJob
{
    
    // @todo change to protected
public:
    
    /**
     * A reference to the stage.
     */
    eagle::plan::stage::AbstractStage *stage;
    
public:
    
    /**
     * Execute a job.
     */
    virtual void runJob();
    
    virtual ~AbstractJob();
    
protected:
    
    AbstractJob(eagle::plan::stage::AbstractStage *theStage);
    
    /**
     * This is called by runJob() and must be implemented by the child class.
     */
    virtual void run() = 0;
    
};

#endif
