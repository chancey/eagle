#ifndef eagle_plan_job_SelectJob_h
#define eagle_plan_job_SelectJob_h

#include "eagle/plan/job/AbstractJob.h"
#include "eagle/plan/Operation.h"

class EaglePage;

namespace eagle
{
    class Plan;
    
    namespace plan
    {
        namespace job
        {
            class SelectJob;
        }
        
        namespace stage
        {
            class SelectStage;
        }
    }
}

class eagle::plan::job::SelectJob : public eagle::plan::job::AbstractJob
{
    
    // @todo change to protected
public:
    
    /**
     * The buffers.
     */
    EaglePage **buffers;
    
public:
    
    /**
     * Create a new job from a stage.
     * @param [in] theStage The parent stage.
     */
    SelectJob(eagle::plan::stage::SelectStage *theStage);
    
    /**
     * Delete a select stage.
     */
    virtual ~SelectJob();
    
    virtual void run();
    
protected:
    
    /**
     * Internal function used by eagle::plan::job::Select::runJob().
     * @param [in] epo The job plan operation.
     */
    void runJobLiteral(eagle::plan::Operation *epo);
    
    /**
     * Internal function used by eagle::plan::job::Select::runJob().
     * @param [in] epo The job plan operation.
     */
    void runJobPage(eagle::plan::Operation *epo);
    
    /**
     * Internal function used by eagle::plan::job::Select::runJob().
     * @param [in] epo The job plan operation.
     */
    void runJobFunction(eagle::plan::Operation *epo);
    
};

#endif
