#include "eagle/plan/job/AbstractJob.h"
#include "eagle/plan/stage/AbstractStage.h"
#include "eagle/Calendar.h"

eagle::plan::job::AbstractJob::AbstractJob(eagle::plan::stage::AbstractStage *theStage)
{
    stage = theStage;
}

eagle::plan::job::AbstractJob::~AbstractJob()
{
}

void eagle::plan::job::AbstractJob::runJob()
{
    if(0 == stage->startTime) {
        stage->startTime = eagle::Calendar::getInstance()->getAbsoluteTime();
    }
    run();
}
