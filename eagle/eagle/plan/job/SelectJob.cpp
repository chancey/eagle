#include "eagle/plan/job/SelectJob.h"
#include "eagle/EaglePage.h"
#include "eagle/plan/stage/SelectStage.h"
#include "eagle/EagleLogger.h"

eagle::plan::job::SelectJob::SelectJob(eagle::plan::stage::SelectStage *theStage)
    : eagle::plan::job::AbstractJob((eagle::plan::stage::AbstractStage*) theStage)
{
    // initialize all buffers now
    buffers = new EaglePage*[theStage->buffersNeeded];
    for(int i = 0; i < theStage->buffersNeeded; ++i) {
        buffers[i] = EaglePage::Alloc(theStage->bufferTypes[i], theStage->pageSize);
    }
}

eagle::plan::job::SelectJob::~SelectJob()
{
    if(NULL != buffers) {
        for(int i = 0; i < ((eagle::plan::stage::SelectStage*) stage)->buffersNeeded; ++i) {
            delete buffers[i];
        }
        delete buffers;
    }
}

void eagle::plan::job::SelectJob::runJobLiteral(eagle::plan::Operation *epo)
{
    EaglePage *destination = NULL, *source1 = NULL;
    EagleDbSqlValue *value;
    eagle::plan::stage::SelectStage *selectStage = (eagle::plan::stage::SelectStage*) stage;
    
    /* prepare arguments */
    if(epo->getDestination() >= 0) {
        if(epo->getDestination() >= selectStage->buffersNeeded) {
            char msg[1024];
            sprintf(msg, "destination %d is greater than allowed %d buffers!", epo->getDestination(), selectStage->buffersNeeded);
            EagleLogger::Log(EagleLogger::Severity::Error, msg);
            return;
        }
        destination = buffers[epo->getDestination()];
    }
    if(epo->getSource1() >= 0) {
        if(epo->getSource1() >= selectStage->buffersNeeded) {
            char msg[1024];
            sprintf(msg, "source1 %d is greater than allowed %d buffers!", epo->getSource1(), selectStage->buffersNeeded);
            EagleLogger::Log(EagleLogger::Severity::Error, msg);
            return;
        }
        source1 = buffers[epo->getSource1()];
    }
    
    /* execute page operation */
    value = (EagleDbSqlValue*) epo->getObject();
    switch(value->type) {
            
        case EagleDbSqlValueTypeAsterisk:
        case EagleDbSqlValueTypeIdentifier:
            /* these are bogus and should never occur */
            break;
            
        case EagleDbSqlValueTypeFloat:
            epo->getFunction()(destination, source1, NULL, &value->value.floatValue);
            break;
            
        case EagleDbSqlValueTypeInteger:
            epo->getFunction()(destination, source1, NULL, &value->value.intValue);
            break;
            
        case EagleDbSqlValueTypeString:
            epo->getFunction()(destination, source1, NULL, &value->value.identifier);
            break;
            
    }
}

void eagle::plan::job::SelectJob::runJobPage(eagle::plan::Operation *epo)
{
    EaglePage *destination = NULL, *source1 = NULL, *source2 = NULL;
    eagle::plan::stage::SelectStage *selectStage = (eagle::plan::stage::SelectStage*) stage;
    
    /* prepare arguments */
    if(epo->getDestination() >= 0) {
        if(epo->getDestination() >= selectStage->buffersNeeded) {
            char msg[1024];
            sprintf(msg, "destination %d is greater than allowed %d buffers!", epo->getDestination(), selectStage->buffersNeeded);
            EagleLogger::Log(EagleLogger::Severity::Error, msg);
            return;
        }
        destination = buffers[epo->getDestination()];
    }
    if(epo->getSource1() >= 0) {
        if(epo->getSource1() >= selectStage->buffersNeeded) {
            char msg[1024];
            sprintf(msg, "source1 %d is greater than allowed %d buffers!", epo->getSource1(), selectStage->buffersNeeded);
            EagleLogger::Log(EagleLogger::Severity::Error, msg);
            return;
        }
        source1 = buffers[epo->getSource1()];
    }
    if(epo->getSource2() >= 0) {
        if(epo->getSource2() >= selectStage->buffersNeeded) {
            char msg[1024];
            sprintf(msg, "source2 %d is greater than allowed %d buffers!", epo->getSource2(), selectStage->buffersNeeded);
            EagleLogger::Log(EagleLogger::Severity::Error, msg);
            return;
        }
        source2 = buffers[epo->getSource2()];
    }
    
    /* execute page operation */
    epo->getFunction()(destination, source1, source2, epo->getObject());
}

void eagle::plan::job::SelectJob::runJobFunction(eagle::plan::Operation *epo)
{
    EaglePage *destination = NULL, **args = new EaglePage*[epo->getArgumentCount()];
    int i, *argDestinations = (int*) epo->getObject();
    eagle::plan::stage::SelectStage *selectStage = (eagle::plan::stage::SelectStage*) stage;
    
    if(epo->getDestination() >= 0) {
        if(epo->getDestination() >= selectStage->buffersNeeded) {
            char msg[1024];
            sprintf(msg, "destination %d is greater than allowed %d buffers!", epo->getDestination(), selectStage->buffersNeeded);
            EagleLogger::Log(EagleLogger::Severity::Error, msg);
            
            delete args;
            return;
        }
        destination = buffers[epo->getDestination()];
    }
    
    /* prepare arguments */
    for(i = 0; i < epo->getArgumentCount(); ++i) {
        if(argDestinations[i] >= selectStage->buffersNeeded) {
            char msg[1024];
            sprintf(msg, "argument destination %d for argument %d is greater than allowed %d buffers!",
                    argDestinations[i], i + 1, selectStage->buffersNeeded);
            EagleLogger::Log(EagleLogger::Severity::Error, msg);
            
            delete args;
            return;
        }
        args[i] = buffers[argDestinations[i]];
    }
    
    /* execute page operation */
    epo->getFunction()(destination, NULL, NULL, (void*) args);
    
    /* clean up */
    delete args;
}

void eagle::plan::job::SelectJob::run()
{
    eagle::plan::stage::SelectStage *selectStage = (eagle::plan::stage::SelectStage*) stage;
    
    EagleLinkedList_Foreach(selectStage->operations, eagle::plan::Operation*, epo)
    {
        switch(epo->getType()) {
                
            case eagle::plan::Operation::Type::Function:
                runJobFunction(epo);
                break;
            
            case eagle::plan::Operation::Type::Page:
                runJobPage(epo);
                break;
                
            case eagle::plan::Operation::Type::Literal:
                runJobLiteral(epo);
                break;
                
        }
    }
    EagleLinkedList_ForeachEnd
    
    selectStage->nextJobLock->lock();
    --selectStage->activeJobs;
    selectStage->nextJobLock->unlock();
}
