#include <stdlib.h>
#include <string.h>
#include "eagle/plan/Operation.h"
#include "eagle/EagleData.h"

eagle::plan::Operation::Operation()
{
}

eagle::plan::Operation* eagle::plan::Operation::NewWithFunction(void (*theFunction)(EaglePage *destination, EaglePage *source1, EaglePage *source2, void *obj),
                                                        int theDestination,
                                                        int *theArgs,
                                                        int theArgCount,
                                                        bool theFreeObj,
                                                        const char *theDescription)
{
    eagle::plan::Operation *epo = eagle::plan::Operation::NewWithPage(theFunction, theDestination, -1, -1, (void*) theArgs,
                                                              theFreeObj, theDescription);
    
    epo->type = eagle::plan::Operation::Type::Function;
    epo->argCount = theArgCount;
    
    return epo;
}

eagle::plan::Operation* eagle::plan::Operation::NewWithPage(EaglePageOperationFunction theFunction,
                                                    int theDestination,
                                                    int theSource1,
                                                    int theSource2,
                                                    void *theObj,
                                                    bool theFreeObj,
                                                    const char *theDescription)
{
    eagle::plan::Operation *epo = new eagle::plan::Operation();
    
    epo->type = eagle::plan::Operation::Type::Page;
    epo->function = theFunction;
    epo->destination = theDestination;
    epo->source1 = theSource1;
    epo->source2 = theSource2;
    epo->obj = theObj;
    epo->freeObj = theFreeObj;
    epo->description = (NULL == theDescription ? NULL : strdup(theDescription));
    epo->argCount = 0;
    
    return epo;
}

eagle::plan::Operation* eagle::plan::Operation::NewWithLiteral(EaglePageOperationFunction theFunction,
                                                       int theDestination,
                                                       int theSource1,
                                                       EagleDbSqlValue *theLiteral,
                                                       bool theFreeLiteral,
                                                       const char *theDescription)
{
    eagle::plan::Operation *epo = new eagle::plan::Operation();
    
    epo->type = eagle::plan::Operation::Type::Literal;
    epo->function = theFunction;
    epo->destination = theDestination;
    epo->source1 = theSource1;
    epo->obj = theLiteral;
    epo->freeObj = theFreeLiteral;
    epo->description = (NULL == theDescription ? NULL : strdup(theDescription));
    epo->argCount = 0;
    
    return epo;
}

std::string eagle::plan::Operation::toString()
{
    return std::string(description);
}

eagle::plan::Operation::~Operation()
{
    if(true == freeObj) {
        free(obj);
    }
    delete description;
}

int eagle::plan::Operation::getDestination() const
{
    return destination;
}

void* eagle::plan::Operation::getObject() const
{
    return obj;
}

int eagle::plan::Operation::getSource1() const
{
    return source1;
}

int eagle::plan::Operation::getSource2() const
{
    return source2;
}

int eagle::plan::Operation::getArgumentCount() const
{
    return argCount;
}

EaglePageOperationFunction eagle::plan::Operation::getFunction() const
{
    return function;
}

eagle::plan::Operation::Type eagle::plan::Operation::getType() const
{
    return type;
}
