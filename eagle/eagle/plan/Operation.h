#ifndef eagle_plan_Operation_h
#define eagle_plan_Operation_h

#include "eagle/EaglePageOperations.h"
#include "eagle/PageProvider.h"
#include "eagledb/EagleDbSqlValue.h"
#include "eagle/Object.h"

namespace eagle
{
    namespace plan
    {
        class Operation;
    }
}

/**
 * Represents a plan operation, or a single step in the executation of an expression.
 */
class eagle::plan::Operation : public eagle::Object
{
    
public:
    
    /**
     * The plan operation type.
     */
    enum Type
    {
        
        /**
         * Page operation.
         */
        Page = 1,
        
        /**
         * Literal operation.
         */
        Literal = 2,
        
        /**
         * Function operation.
         */
        Function = 3
        
    };
    
protected:
    
    /**
     * The plan operation type.
     */
    eagle::plan::Operation::Type type;
    
    /**
     * If \c type is eagle::plan::OperationTypeFunction then this stores how many arguments there are.
     */
    int argCount;
    
    /**
     * Destination buffer ID.
     */
    int destination;
    
    /**
     * First argument buffer ID.
     */
    int source1;
    
    /**
     * Second buffer argument ID.
     */
    int source2;
    
    /**
     * An optional attachment object.
     */
    void *obj;
    
    /**
     * If true the \c obj will be freed when the eagle::plan::Operation is freed.
     */
    bool freeObj;
    
    /**
     * The function pointer to perform the operation.
     */
    EaglePageOperationFunction function;
    
    /**
     * A description of this operation.
     */
    const char *description;
    
protected:
    
    /**
     * This is an abstract class.
     * @see eagle::plan::Operation::NewWithPage()
     * @see eagle::plan::Operation::NewWithLiteral()
     * @see eagle::plan::Operation::NewWithFunction()
     */
    Operation();
    
public:
    
    /**
     * Create a new plan operation with a page operation.
     *
     * @param [in] theFunction The page operation function.
     * @param [in] theDestination Buffer ID for the destination, this can be less than zero for no buffer.
     * @param [in] theSource1 Buffer ID for the left operand, this can be less than zero for no buffer.
     * @param [in] theSource2 Buffer ID for the right operand, this can be less than zero for no buffer.
     * @param [in] theObj An attached object, some page operations need this.
     * @param [in] theFreeObj Free the \p obj when deleting the eagle::plan::Operation.
     * @param [in] theDescription A human readable description to be rendered into eagle::plan::Operation::toString()
     * @return A new eagle::plan::Operation.
     */
    static eagle::plan::Operation* NewWithPage(EaglePageOperationFunction theFunction,
                                               int theDestination,
                                               int theSource1,
                                               int theSource2,
                                               void *theObj,
                                               bool theFreeObj,
                                               const char *theDescription);
    
    /**
     * Create a new plan operation with a literal.
     *
     * @param [in] theFunction The page operation function.
     * @param [in] theDestination Buffer ID for the destination, this can be less than zero for no buffer.
     * @param [in] theSource1 Buffer ID for the left operand, this can be less than zero for no buffer.
     * @param [in] theLiteral The value.
     * @param [in] theFreeLiteral Free the \p literal when deleting the eagle::plan::Operation.
     * @param [in] theDescription A human readable description to be rendered into eagle::plan::Operation_toString()
     * @return A new eagle::plan::Operation.
     */
    static eagle::plan::Operation* NewWithLiteral(EaglePageOperationFunction theFunction,
                                                  int theDestination,
                                                  int theSource1,
                                                  EagleDbSqlValue *theLiteral,
                                                  bool theFreeLiteral,
                                                  const char *theDescription);
    
    /**
     * Create a new plan operation with a function.
     *
     * @param [in] theFunction The page operation function.
     * @param [in] theDestination Buffer ID for the destination, this can be less than zero for no buffer.
     * @param [in] theArgs The buffer IDs for each of the arguments.
     * @param [in] theArgCount The number of arguments.
     * @param [in] theFreeObj Free the \p args when deleting the eagle::plan::Operation.
     * @param [in] theDescription A human readable description to be rendered into eagle::plan::Operation_toString()
     * @return A new eagle::plan::Operation.
     */
    static eagle::plan::Operation* NewWithFunction(EaglePageOperationFunction theFunction,
                                                   int theDestination,
                                                   int *theArgs,
                                                   int theArgCount,
                                                   bool theFreeObj,
                                                   const char *theDescription);
    
    /**
     * Delete an operation.
     */
    virtual ~Operation();
    
    /**
     * Get the string representation of this operation. This is actually just a copy of the description that was passed
     * in static methods, you must free the description when your finished with it.
     * @return A string copy of the description.
     */
    virtual std::string toString();
    
    /**
     * Get the destination.
     * @return \c eagle::plan::Operation::destination
     */
    int getDestination() const;
    
    /**
     * Get the object.
     * @return \c eagle::plan::Operation::obj
     */
    void* getObject() const;
    
    /**
     * Get first source.
     * @return \c eagle::plan::Operation::source1
     */
    int getSource1() const;
    
    /**
     * Get second source.
     * @return \c eagle::plan::Operation::source2
     */
    int getSource2() const;
    
    /**
     * Get the argument count.
     * @return \c eagle::plan::Operation::argCount
     */
    int getArgumentCount() const;
    
    /**
     * Get function pointer.
     * @return \c eagle::plan::Operation::function
     */
    EaglePageOperationFunction getFunction() const;
    
    /**
     * Get the type.
     * @return \c eagle::plan::Operation::type
     */
    eagle::plan::Operation::Type getType() const;
    
};

#endif
