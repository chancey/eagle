#ifndef eagle_plan_stage_EntireSort_h
#define eagle_plan_stage_EntireSort_h

#include <string>
#include "eagle/plan/stage/AbstractStage.h"
#include "eagle/EagleSynchronizedLinkedList.h"
#include "eagle/page/Sort.h"
#include "eagle/ArrayPageProvider.h"
#include "eagle/StreamPageProvider.h"

class EagleLock;

namespace eagle
{
    class Plan;
    class PageProvider;
    
    namespace plan
    {
        namespace stage
        {
            class EntireSort;
            class SelectStage;
        }
    }
}

class eagle::plan::stage::EntireSort : public eagle::plan::stage::AbstractStage
{
    
    // @todo make protected
public:
    
    /**
     * The index of the provider that had the sort column.
     */
    int orderByProviderId;
    
    /**
     * This is used for verification, taken at the start and used to know when the stage is complete.
     */
    int totalPages;
    
    /**
     * This stage has multiple steps that need to be completed in their entirety before more jobs for the next step can
     * be given out.
     */
    int step;
    
    /**
     * The number of MergePages jobs that were required to reduce sort the entire set.
     */
    int merges;
    
    /**
     * This will be the previous stage that will have all the result data to be sorted. It is initialised (set) once by
     * nextJob().
     */
    eagle::plan::stage::SelectStage *selectStage;
    
    /**
     * To synchronise the eagle::Instance::nextJob() method.
     */
    EagleLock *nextJobLock;
    
    /**
     * The merge sort stack. It is a stack of pages that have been individually sorted and are waiting to be sorted with
     * other pages in the stack.
     */
    EagleSynchronizedLinkedList<eagle::page::Sort*> *mergeSortStack;
    
public:
    
    EntireSort(eagle::Plan *thePlan, int theCores);
    
    void setOrderByProviderId(int theOrderByProviderId);
    
    virtual std::string toString();
    
    virtual eagle::plan::job::AbstractJob* nextJob();
    
    virtual ~EntireSort();
    
protected:
    
    template<typename T>
    eagle::ArrayPageProvider* reorderData(eagle::page::Sort *singlePage, eagle::PageProvider *resultProviders);
    
    void init();
    
    eagle::plan::job::AbstractJob* getSortPageJob(bool *doReturn);
    
    eagle::plan::job::AbstractJob* getMergeJobFromStack();
    
    void reorderAllData();
    
};

template<typename T>
eagle::ArrayPageProvider* eagle::plan::stage::EntireSort::reorderData(eagle::page::Sort *singlePage,
                                                                      eagle::PageProvider *resultProviders)
{
    int resultSize = singlePage->totalSize;
    T *newData = new T[resultSize];
    for(int j = 0; j < resultSize; ++j) {
        int recordId = singlePage->recordIds[j];
        newData[j] = *((T*) resultProviders->getDataByRecordId(recordId));
    }
    
    return new eagle::ArrayPageProvider(resultProviders->type, newData, resultSize, resultSize, resultProviders->name,
                                        true);
}

#endif
