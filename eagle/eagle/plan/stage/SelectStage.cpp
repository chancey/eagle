#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sstream>
#include <string>
#include "eagle/plan/stage/SelectStage.h"
#include "eagle/Plan.h"
#include "eagle/plan/job/AbstractJob.h"
#include "eagle/plan/job/SelectJob.h"
#include "eagle/EagleLogger.h"
#include "eagle/plan/Operation.h"

eagle::plan::stage::SelectStage::SelectStage(eagle::Plan *thePlan, int thePageSize, int theCores)
    : eagle::plan::stage::AbstractStage(thePlan, theCores)
{
    nextJobLock = new EagleLock();
    
    pageSize = thePageSize;
    operations = new EagleSynchronizedLinkedList<eagle::plan::Operation*>();
    providers = new EagleSynchronizedLinkedList<eagle::PlanBufferProvider*>();
    
    buffersNeeded = 0;
    bufferTypes = NULL;
    
    for(int i = 0; i < cores; ++i) {
        executionTime[i] = 0;
        lockTime[i] = 0;
    }
    
    nextPageNumber = 0;
    nextPageLock = new EagleLock();
    activeJobs = 0;
}

void eagle::plan::stage::SelectStage::addOperation(eagle::plan::Operation *epo, bool freeObj)
{
    operations->addObject(epo, freeObj);
}

void eagle::plan::stage::SelectStage::addBufferProvider(eagle::PlanBufferProvider *bp, bool free)
{
    providers->addObject(bp, free, (void(*)(eagle::PlanBufferProvider*)) PlanBufferProvider_LegacyDelete);
}

std::string eagle::plan::stage::SelectStage::toString()
{
    char *temp = NULL;
    int i;
    std::ostringstream str;
    str << "Select stage:\n";
    
    // we cannot render if it is under an error
    if(isError()) {
        str << "  Error: " << errorMessage << "\n";
        return str.str();
    }
    
    if(!providers->isEmpty()) {
        str << "  Input Providers:\n";
        
        EagleLinkedList_Foreach(providers, eagle::PlanBufferProvider*, provider)
        {
            temp = provider->toString();
            str << "    " << temp << "\n";
            delete temp;
        }
        EagleLinkedList_ForeachEnd
    }
    
    if(resultFields > 0) {
        str << "  Output Providers:\n";
        
        for(i = 0; i < resultFields; ++i) {
            eagle::PageProvider *provider = result[i];
            temp = EagleData::TypeToName(provider->type);
            str << "    destination = " << i << ", name = " << provider->name << ", type = " << temp << "\n";
            delete temp;
        }
    }
    
    if(!operations->isEmpty()) {
        str << "  Operations:\n";
        
        EagleLinkedList_Foreach(operations, eagle::plan::Operation*, op)
        {
            str << "    " << op->toString() << "\n";
        }
        EagleLinkedList_ForeachEnd
    }
    
    if(buffersNeeded > 0) {
        str << "  Buffers:\n";
        
        for(i = 0; i < buffersNeeded; ++i) {
            std::ostringstream msg;
            char *type;
            
            type = EagleData::TypeToName(bufferTypes[i]);
            msg << "    " << i << " type=" << type << "\n";
            str << msg.str();
            delete type;
        }
    }
    
    return str.str();
}

eagle::plan::stage::SelectStage::~SelectStage()
{
    delete providers;
    delete operations;
    delete bufferTypes;
    delete nextPageLock;
    delete nextJobLock;
}

eagle::PlanBufferProvider* eagle::plan::stage::SelectStage::getBufferProviderByName(char *name)
{
    EagleLinkedList_Foreach(providers, eagle::PlanBufferProvider*, provider)
    {
        if(!provider->value.provider.provider->name.empty() &&
           provider->value.provider.provider->name == name) {
            return provider;
        }
    }
    EagleLinkedList_ForeachEnd
    
    return NULL;
}

void eagle::plan::stage::SelectStage::prepareBuffers(int buffers)
{
    buffersNeeded = buffers;
    bufferTypes = new EagleData::Type[buffersNeeded];
    for(int i = 0; i < buffersNeeded; ++i) {
        bufferTypes[i] = EagleData::Type::Unknown;
    }
}

int eagle::plan::stage::SelectStage::getNextPageNumber()
{
    int theNextPageNumber = 0;
    
    nextPageLock->lock();
    theNextPageNumber = nextPageNumber++;
    nextPageLock->unlock();
    
    return theNextPageNumber;
}

void eagle::plan::stage::SelectStage::NextJobUnsync(eagle::plan::job::AbstractJob **job)
{
    eagle::plan::stage::SelectStage *selectStage = (eagle::plan::stage::SelectStage*) (*job)->stage;
    eagle::plan::job::SelectJob *selectJob = (eagle::plan::job::SelectJob*) *job;
    
    /* get the synchronised page ID */
    int pageNumber = selectStage->getNextPageNumber();
    
    EagleLinkedList_Foreach(selectStage->providers, eagle::PlanBufferProvider*, provider)
    {
        if(provider->destinationBuffer >= selectStage->buffersNeeded) {
            char msg[1024];
            sprintf(msg, "destination %d is greater than allowed %d buffers!\n", provider->destinationBuffer,
                    selectStage->buffersNeeded);
            EagleLogger::Log(EagleLogger::Severity::Error, msg);
            
            delete *job;
            *job = NULL;
            break;
        }
        
        /* attempt to get the page */
        delete selectJob->buffers[provider->destinationBuffer];
        selectJob->buffers[provider->destinationBuffer] = provider->value.provider.provider->getPage(pageNumber);
        
        /* reached the end of the stream */
        if(NULL == selectJob->buffers[provider->destinationBuffer]) {
            delete *job;
            *job = NULL;
            break;
        }
    }
    EagleLinkedList_ForeachEnd
}

void eagle::plan::stage::SelectStage::NextJobSync(eagle::plan::job::AbstractJob **job)
{
    eagle::plan::stage::SelectStage *selectStage = (eagle::plan::stage::SelectStage*) (*job)->stage;
    eagle::plan::job::SelectJob *selectJob = (eagle::plan::job::SelectJob*) *job;
    
    EagleLinkedList_Foreach(selectStage->providers, eagle::PlanBufferProvider*, provider)
    {
        if(provider->destinationBuffer >= selectStage->buffersNeeded) {
            char msg[1024];
            sprintf(msg, "destination %d is greater than allowed %d buffers!\n", provider->destinationBuffer,
                    selectStage->buffersNeeded);
            EagleLogger::Log(EagleLogger::Severity::Error, msg);
            
            delete *job;
            *job = NULL;
            break;
        }
        
        if(provider->value.provider.provider->pagesRemaining() == 0) {
            delete *job;
            *job = NULL;
            break;
        }
        
        delete selectJob->buffers[provider->destinationBuffer];
        selectJob->buffers[provider->destinationBuffer] = provider->value.provider.provider->nextPage();
    }
    EagleLinkedList_ForeachEnd
}

bool eagle::plan::stage::SelectStage::needsSynchronisation()
{
    bool sync = false;
    
    /* if all the providers can be accessed randomly we do not need to syncronise this function */
    EagleLinkedList_Foreach(providers, eagle::PlanBufferProvider*, provider)
    {
        switch(provider->type) {
                
            case eagle::PlanBufferProvider::Type::ValueType:
                /* this case in find, we do not need synchronization for this */
                break;
                
            case eagle::PlanBufferProvider::Type::ProviderType:
                if(false == provider->value.provider.provider->isRandomAccess()) {
                    sync = true;
                }
                break;
                
        }
    }
    EagleLinkedList_ForeachEnd
    
    return sync;
}

eagle::plan::job::AbstractJob* eagle::plan::stage::SelectStage::nextJob()
{
    eagle::plan::job::AbstractJob *job = new eagle::plan::job::SelectJob(this);
    bool sync = needsSynchronisation();
    
    /* synchronize providers */
    if(true == sync) {
        nextJobLock->lock();
        NextJobSync(&job);
        nextJobLock->unlock();
    }
    else {
        NextJobUnsync(&job);
    }
    
    nextJobLock->lock();
    if(NULL != job) {
        ++activeJobs;
    }
    
    if(activeJobs == 0) {
        complete = true;
    }
    nextJobLock->unlock();
    
    return job;
}
