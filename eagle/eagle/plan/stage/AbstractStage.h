#ifndef eagle_plan_stage_AbstractStage_h
#define eagle_plan_stage_AbstractStage_h

#include <string>
#include "eagle/EaglePlanError.h"

namespace eagle
{
    class Plan;
    class PageProvider;
    
    namespace plan
    {
        namespace stage
        {
            class AbstractStage;
        }
        
        namespace job
        {
            class AbstractJob;
        }
    }
}

class eagle::plan::stage::AbstractStage
{
    
    // @todo make protected
public:
    
    /**
     * The total time the query took to execute.
     */
    unsigned long *executionTime;
    
    /**
     * The total time the thread was left waiting for a mutex lock to be release.
     */
    unsigned long *lockTime;
    
    /**
     * The absolute start time.
     */
    unsigned long startTime;
    
    /**
     * The error status.
     */
    EaglePlanError errorCode;
    
    /**
     * Further details about the error, this may be NULL.
     */
    std::string errorMessage;
    
    /**
     * A reference to the plan.
     */
    eagle::Plan *plan;
    
    /**
     * The number of CPU cores.
     */
    int cores;
    
    /**
     * Result set after execution.
     */
    eagle::PageProvider **result;
    
    /**
     * The number of providers in \c result
     */
    int resultFields;
    
    /**
     * A flag that the concrete stage will set to true once it is complete to the plan can move onto the next stage.
     */
    volatile bool complete;
    
public:
    
    unsigned long getStartTime();
    
    /**
     * Render as a descriptive string.
     * @return A new string, you must free it when you are finished with it.
     */
    virtual std::string toString() = 0;
    
    /**
     * Get the next finite job from the stack.
     * @return Initialised EaglePlanJob, or NULL if there are no available jobs.
     */
    virtual eagle::plan::job::AbstractJob* nextJob() = 0;
    
    /**
     * Set the error for the plan.
     * @param [in] errorCode The error code.
     * @param [in] errorMessage The message describing the error.
     */
    virtual void setError(EaglePlanError errorCode, std::string errorMessage);
    
    /**
     * Check if the plan is currently in an error state.
     * @return boolean value.
     */
    bool isError();
    
    /**
     * Get the total execution time for all CPUs in seconds. This does not include IO wait time, only CPU time.
     * @return Number of CPU seconds.
     */
    double getExecutionSeconds();
    
    /**
     * Get the total wait time for all CPUs in seconds.
     * @return Number of CPU seconds.
     */
    double getLockSeconds();
    
    /**
     * Get the real execution seconds. This is the absolute time difference from when the plan begins executing until
     * now.
     * @return The number of absolute execution seconds.
     */
    double getRealExecutionSeconds();
    
    virtual ~AbstractStage();
    
protected:
    
    AbstractStage(eagle::Plan *thePlan, int theCores);
    
};

#endif
