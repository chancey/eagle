#ifndef eagle_plan_stage_SelectStage_h
#define eagle_plan_stage_SelectStage_h

#include "eagle/EagleLinkedList.h"
#include "eagle/plan/Operation.h"
#include "eagle/EagleLock.h"
#include "eagle/plan/stage/AbstractStage.h"
#include "eagle/EaglePlanError.h"

namespace eagle
{
    class Plan;
    class PlanJob;
    class PlanBufferProvider;
    
    namespace plan
    {
        namespace stage
        {
            class SelectStage;
        }
    }
}

class eagle::plan::stage::SelectStage : public eagle::plan::stage::AbstractStage
{
    
public:
    
    /**
     * The operations. An operation is a task done on a page. This is semi managed because the actual array that
     * contains the operations will be managed by the object, but the individual operations will be managed externally.
     */
    EagleLinkedList<eagle::plan::Operation*> *operations;
    
    /**
     * The page providers. This is where external data comes into the expression.
     */
    EagleLinkedList<eagle::PlanBufferProvider*> *providers;
    
    /**
     * The amount of records to read per page.
     */
    int pageSize;
    
    /**
     * The number of buffers needed for the execution.
     */
    int buffersNeeded;
    
    /**
     * The types for each buffer.
     */
    EagleData::Type *bufferTypes;
    
    /**
     * When all the providers allow random access this syncronised the randomly accessed pages.
     */
    int nextPageNumber;
    
    /**
     * Used by functions that handle \c nextPageId
     */
    EagleLock *nextPageLock;
    
    /**
     * To synchronise the eagle::Instance::nextJob() method.
     */
    EagleLock *nextJobLock;
    
    /**
     * The number of active (outstanding) jobs.
     */
    int activeJobs;
    
public:
    
    SelectStage(eagle::Plan *thePlan, int thePageSize, int theCores);
    
    virtual ~SelectStage();
    
    /**
     * Add an operation to a plan.
     * @param [in] epo The operation.
     * @param [in] freeObj Free the object?
     */
    void addOperation(eagle::plan::Operation *epo, bool freeObj);
    
    /**
     * Add a buffer provider to a plan.
     * @param [in] bp The buffer provider.
     * @param [in] free Free the buffer provider with the plan?
     */
    void addBufferProvider(eagle::PlanBufferProvider *bp, bool free);
    
    /**
     * Get a buffer provider by name.
     * @param [in] name The name of the buffer provider.
     * @return NULL if it cannot be found, otherwise the buffer provider will be returned.
     */
    eagle::PlanBufferProvider* getBufferProviderByName(char *name);
    
    /**
     * Prepare the buffers before the expression can be compiled.
     * @param [in] buffers The amount of buffers you will need to carry out all of the expressions.
     */
    void prepareBuffers(int buffers);
    
    /**
     * @brief Get the next page number from a streaming provider.
     * This is basically just a synchronized incrementer, the first time this is called on a provider will return zero
     * and successive calls will return one value higher.
     * @return An integer greater than or equal to 0.
     */
    int getNextPageNumber();
    
    /**
     * Get the next finite job from the stack. This is a syncronised method that all the workers will call independantly
     * when they have nothing to do. If this method returns NULL it is upto the workers to know that there is no more
     * work, they will check periodically after that.
     * @return Initialised EaglePlanJob, or NULL if there are no available jobs.
     */
    eagle::plan::job::AbstractJob* nextJob();
    
    /**
     * Render the plan as a descriptive string.
     * @return A new string, you must free it when you are finished with it.
     */
    virtual std::string toString();
    
    bool needsSynchronisation();
    
protected:
    
    /**
     * This is the internal synchronised version of eagle::Instance_nextJob(). Having the core functionality in another
     * function is mainly to help with profiling code.
     * @param [in] job A reference to the job.
     */
    static void NextJobSync(eagle::plan::job::AbstractJob **job);
    
    /**
     * This is the internal non-synchronised version of eagle::Instance_nextJob(). Having the core functionality in
     * another function is mainly to help with profiling code.
     * @param [in] job A reference to the job.
     */
    static void NextJobUnsync(eagle::plan::job::AbstractJob **job);
    
};

#endif
