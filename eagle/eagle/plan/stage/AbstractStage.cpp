#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "eagle/plan/stage/AbstractStage.h"
#include "eagle/EaglePlanError.h"
#include "eagle/Plan.h"
#include "eagle/Calendar.h"

eagle::plan::stage::AbstractStage::AbstractStage(eagle::Plan *thePlan, int theCores)
{
    errorCode = EaglePlanErrorNone;
    startTime = 0;
    lockTime = NULL;
    plan = thePlan;
    cores = theCores;
    
    /* timers */
    executionTime = new unsigned long[cores];
    lockTime = new unsigned long[cores];
    
    resultFields = 0;
    result = NULL;
    complete = false;
}

eagle::plan::stage::AbstractStage::~AbstractStage()
{
    for(int i = 0; i < resultFields; ++i) {
        delete result[i];
    }
    delete[] result;
    delete[] executionTime;
    delete[] lockTime;
}

void eagle::plan::stage::AbstractStage::setError(EaglePlanError theErrorCode, std::string theErrorMessage)
{
    errorCode = theErrorCode;
    errorMessage = theErrorMessage;
}

bool eagle::plan::stage::AbstractStage::isError()
{
    return (errorCode != EaglePlanErrorNone);
}

double eagle::plan::stage::AbstractStage::getRealExecutionSeconds()
{
    unsigned long elapsed = eagle::Calendar::getInstance()->getAbsoluteTime() - startTime;
    
    /* the timer was not started - bad */
    if(0 == startTime) {
        return 0.0;
    }
    
    return ((double) elapsed / (double) eagle::Calendar::TicksPerSecond);
}

double eagle::plan::stage::AbstractStage::getLockSeconds()
{
    unsigned long total = 0.0;
    int i;
    
    for(i = 0; i < cores; ++i) {
        total += lockTime[i];
    }
    
    return ((double) total / (double) eagle::Calendar::TicksPerSecond);
}

double eagle::plan::stage::AbstractStage::getExecutionSeconds()
{
    unsigned long total = 0.0;
    int i;
    
    for(i = 0; i < cores; ++i) {
        total += executionTime[i];
    }
    
    return ((double) total / (double) eagle::Calendar::TicksPerSecond) - getLockSeconds();
}

unsigned long eagle::plan::stage::AbstractStage::getStartTime()
{
    return startTime;
}
