#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <string>
#include "eagle/plan/stage/EntireSort.h"
#include "eagle/Plan.h"
#include "eagle/plan/stage/SelectStage.h"
#include "eagle/PageProvider.h"
#include "eagle/EagleLock.h"
#include "eagle/page/Sort.h"
#include "eagle/plan/job/SortPage.h"
#include "eagle/ArrayPageProvider.h"
#include "eagledb/EagleDbInstance.h"
#include "eagle/plan/job/MergePages.h"

eagle::plan::stage::EntireSort::EntireSort(eagle::Plan *thePlan, int theCores)
    : eagle::plan::stage::AbstractStage(thePlan, theCores)
{
    selectStage = NULL;
    nextJobLock = new EagleLock();
    mergeSortStack = new EagleSynchronizedLinkedList<eagle::page::Sort*>();
    merges = 0;
    totalPages = 0;
    step = 1;
}

eagle::plan::stage::EntireSort::~EntireSort()
{
    delete nextJobLock;
    delete mergeSortStack;
}

void eagle::plan::stage::EntireSort::setOrderByProviderId(int theOrderByProviderId)
{
    orderByProviderId = theOrderByProviderId;
}

std::string eagle::plan::stage::EntireSort::toString()
{
    std::string str("EntireSort stage:\n");
    str += "  ";
    return str;
}

void eagle::plan::stage::EntireSort::init()
{
    selectStage = (eagle::plan::stage::SelectStage*) plan->stages->get(plan->currentStage - 1);
    totalPages = selectStage->result[orderByProviderId]->pagesRemaining();
    
    resultFields = selectStage->resultFields;
    result = new eagle::PageProvider*[resultFields];
    for(int i = 0; i < resultFields; ++i) {
        result[i] = NULL;
    }
}

eagle::plan::job::AbstractJob* eagle::plan::stage::EntireSort::getSortPageJob(bool *doReturn)
{
    *doReturn = true;
    
    eagle::PageProvider *provider = selectStage->result[orderByProviderId];
    EaglePage *nextPage = provider->nextPage();
    if(NULL != nextPage) {
        return new eagle::plan::job::SortPage(this, nextPage);
    }
    
    // before we proceed, make sure that we have waited for all the pages to be sorted and put onto the merge stack
    if(mergeSortStack->length() < totalPages) {
        // still waiting for pages to be sorted, come back later
        return NULL;
    }
    
    // proceed to step 2
    *doReturn = false;
    return NULL;
}

eagle::plan::job::AbstractJob* eagle::plan::stage::EntireSort::getMergeJobFromStack()
{
    // can we take the next two objects?
    eagle::page::Sort *a = mergeSortStack->begin()->getObject();
    eagle::page::Sort *b = mergeSortStack->begin()->getNext()->getObject();
    if(a->working || b->working) {
        // still waiting for other merges to finish, come back later
        return NULL;
    }
    
    // ok, next two objects are ready for merging
    eagle::page::Sort *page1 = mergeSortStack->shiftObject();
    eagle::page::Sort *page2 = mergeSortStack->shiftObject();
    
    // add the result page to the stack now, this is very important because under a concurrent environment two
    // objects will be removed from the list and the next nextJob() will think that the list is done because there
    // are no more items on the list
    eagle::page::Sort *mergePage = new eagle::page::Sort(page1->type, NULL, 0, 0, 0, true, NULL);
    mergeSortStack->addObject(mergePage, true);
    
    // set working status
    page1->working = page2->working = mergePage->working = true;
    
    // create a merge job
    ++merges;
    return new eagle::plan::job::MergePages(this, page1, page2, mergePage);
}

void eagle::plan::stage::EntireSort::reorderAllData()
{
    eagle::page::Sort *finalOrder = mergeSortStack->popObject();
    for(int i = 0; i < resultFields; ++i) {
        switch(selectStage->result[i]->type) {
                
            case EagleData::Type::Integer:
                result[i] = reorderData<EagleDataTypeInteger>(finalOrder, selectStage->result[i]);
                break;
                
            case EagleData::Type::Float:
                result[i] = reorderData<EagleDataTypeFloat>(finalOrder, selectStage->result[i]);
                break;
                
            case EagleData::Type::Varchar:
                result[i] = reorderData<EagleDataTypeVarchar>(finalOrder, selectStage->result[i]);
                break;
                
            case EagleData::Type::Unknown:
                result[i] = NULL;
                break;
                
        }
    }
    
    // all done
    finalOrder->freeData = true;
    delete finalOrder;
}

eagle::plan::job::AbstractJob* eagle::plan::stage::EntireSort::nextJob()
{
    bool doReturn = false;
    eagle::plan::job::AbstractJob *job;
    nextJobLock->lock();
    
    // first time init
    if(NULL == selectStage) {
        init();
    }
    
    // 1. sort all the indiviual pages from the provider and place them onto the merge sort stack to be merged later
    if(1 == step) {
        job = getSortPageJob(&doReturn);
        
        if(doReturn) {
            nextJobLock->unlock();
            return job;
        }
        
        step = 2;
    }
    
    // 2. we have to reduce the merge sort stack now
    if(mergeSortStack->length() >= 2) {
        job = getMergeJobFromStack();
        nextJobLock->unlock();
        return job;
    }
    
    // catch the final page not merged yet
    if(mergeSortStack->first()->totalSize == 0) {
        // still working, come back later
        nextJobLock->unlock();
        return NULL;
    }
    
    // 3. we need to create new providers with the reordered data
    reorderAllData();
    
    complete = true;
    nextJobLock->unlock();
    return NULL;
}
