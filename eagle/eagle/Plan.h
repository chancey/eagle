#ifndef eagle_Plan_h
#define eagle_Plan_h

#include "EaglePageOperations.h"
#include "eagle/PlanBufferProvider.h"
#include "EaglePlanError.h"
#include "EagleData.h"
#include "EagleLinkedList.h"
#include "eagle/plan/stage/AbstractStage.h"
#include "EagleSynchronizedLinkedList.h"

namespace eagle
{
    class Plan;
    class PlanJob;
    
    namespace plan
    {
        class Operation;
        
        namespace job
        {
            class AbstractJob;
        }
    }
}

/**
 * An execution plan.
 */
class eagle::Plan
{
    
    // @todo make protected
public:
    
    /**
     * The one or more stages that have to be completed before the plan is finished.
     */
    EagleSynchronizedLinkedList<eagle::plan::stage::AbstractStage*> *stages;
    
    /**
     * The current stage index (starting from 0). When nextJob() finishes this will be incremented so that the next
     * stage will start. Once nextJob() returns no more jobs and currentStage is equal to the number of objects in the
     * \c stages variables then the plan is finished.
     */
    volatile int currentStage;
    
    /**
     * Locking for the nextJob() function.
     */
    EagleLock *nextJobLock;
    
    /**
     * If the current plan is 'stalled' it means there are no new jobs to be dished out but we are waiting for other
     * workers to return data.
     */
    volatile bool stalled;
    
public:
    
    /**
     * Protected constructor.
     * @see eagle::Plan::Create()
     */
    Plan();
    
    void addStage(eagle::plan::stage::AbstractStage *stage, bool freeWithPlan);
    
    /**
     * Get the next finite job from the stack. This is a syncronised method that all the workers will call independantly
     * when they have nothing to do. If this method returns NULL it is upto the workers to know that there is no more
     * work, they will check periodically after that.
     * @return Initialised EaglePlanJob, or NULL if there are no available jobs.
     */
    eagle::plan::job::AbstractJob* nextJob();
    
    /**
     * Delete a plan.
     */
    virtual ~Plan();
    
    /**
     * Render the plan as a descriptive string.
     * @return A new string, you must free it when you are finished with it.
     */
    virtual std::string toString();
    
    unsigned long getStartTime();
    
    bool isError();
    
    std::string getErrorMessage();
    
    virtual bool isStalled();
    
};

#endif
