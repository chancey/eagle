#ifndef eagle_EagleLinkedList_h
#define eagle_EagleLinkedList_h

#include <math.h>
#include <string.h>
#include <string>
#include "EagleReadWriteLock.h"

/**
 * Iterate through a linked list. This is safe no nest as long as the third parameter \p _name is unique to each nested
 * loop.
 *
 * @param [in] _list The EagleLinkedList.
 * @param [in] _type The type for each individual EagleLinkedList::Item.
 * @param [in] _name The name of the iterator containing your object.
 *
 * Example
 * @code
 * EagleLinkedList_Foreach(db->schemas, EagleDbSchema*, schema)
 * {
 *     char *name = schema->name;
 * 
 *     // Also exposes:
 *     //   _cursor_schema   The EagleLinkedList::Item
 *     //   _i_schema        The item count (starting at 0)
 * }
 * EagleLinkedList_ForeachEnd
 * @endcode
 */
#define EagleLinkedList_Foreach(_list, _type, _name) { \
EagleLinkedList<_type>::Item *_cursor##_##_name; \
int _i##_##_name; \
for(_cursor##_##_name = (EagleLinkedList<_type>::Item*) _list->begin(), _i##_##_name = 0; NULL != _cursor##_##_name; _cursor##_##_name = _cursor##_##_name->getNext(), ++_i##_##_name) { \
_type _name = (_type) _cursor##_##_name->getObject();

/**
 * This MUST always follow a EagleLinkedList_ForeachEnd
 */
#define EagleLinkedList_ForeachEnd }}

/**
 * A linked list (FIFO).
 */
template <typename ItemType>
class EagleLinkedList
{
    
public:
    
    /**
     * The minimum amount of items in a linked list that will allow the index to be initialized.
     */
    static const int MinimumSizeForIndex = 64;
    
    /**
     * @brief A linked list item.
     * A linked list can only contain instances of EagleLinkedList::Item.
     */
    class Item
    {
        
    protected:
        
        /**
         * The actual item value.
         */
        ItemType obj;
        
        /**
         * If true the \c obj will be freed with the list item.
         */
        bool freeObj;
        
        /**
         * A pointer to the next item.
         */
        EagleLinkedList<ItemType>::Item *next;
        
        /**
         * A pointer to the function that will free \c obj. If this is NULL then the normal free() function will be used.
         */
        void (*free)(ItemType);
        
        /**
         * A optional description of the item.
         */
        char *description;
        
        /**
         * Create a new linked list item with an attached description. The description is often useful for debugging.
         * @param [in] theObj The object.
         * @param [in] theFreeObj Free the object when linked list item is freed.
         * @param [in] theFree The free function.
         * @param [in] theDescription A description.
         * @return A new linked list item.
         */
        void init(ItemType theObj, bool theFreeObj, void (*theFree)(ItemType), char *theDescription);
        
    public:
        
        /**
         * Initialise a new item to append to a linked list.
         * @param [in] theObj The object.
         * @param [in] theFreeObj Free the object when linked list item is freed.
         * @param [in] theFree The free function.
         * @return A new linked list item.
         */
        Item(ItemType theObj, bool theFreeObj, void (*theFree)(ItemType));
        
        /**
         * Create a new linked list item with an attached description. The description is often useful for debugging.
         * @param [in] theObj The object.
         * @param [in] theFreeObj Free the object when linked list item is freed.
         * @param [in] theFree The free function.
         * @param [in] theDescription A description.
         * @return A new linked list item.
         */
        Item(ItemType theObj, bool theFreeObj, void (*theFree)(ItemType), char *theDescription);
        
        /**
         * Create a new linked list item with no description.
         * This linked list item will be freed with the `delete` operator.
         * @param [in] theObj The object.
         * @param [in] theFreeObj Free the object when linked list item is freed.
         * @return A new linked list item.
         */
        Item(ItemType theObj, bool theFreeObj);
        
        /**
         * Free a linked list item.
         */
        ~Item();
        
        /**
         * Get the object for this linked list item.
         * @return The object.
         */
        ItemType getObject();
        
        /**
         * Get the next item.
         * @return NULL if this is the last item in the list. Otherwise a pointer to the next item.
         */
        typename EagleLinkedList<ItemType>::Item* getNext();
        
        /**
         * Set the item after.
         * @param [in] item The item.
         */
        void setNext(typename EagleLinkedList<ItemType>::Item *item);
        
        /**
         * Change the deallocation strategy. If this is set to true then the object will be freed when the linked list
         * item is freed.
         * @param [in] theFreeObj Free the object when the linked list is freed?
         */
        void setFreeObject(bool theFreeObj);
        
    };
    
protected:
    
    /**
     * A pointer to the last item.
     */
    EagleLinkedList<ItemType>::Item *tail;
    
    /**
     * A pointer to the first item.
     */
    EagleLinkedList<ItemType>::Item *head;
    
    /**
     * The amount of items in this linked list.
     * @see EagleLinkedList_length()
     */
    int size;
    
    /**
     * The allocated size of the index. If this value is zero then no index is in use.
     */
    int indexSize;
    
    /**
     * The index holds pointers to list items so that random access lookups can be faster.
     */
    EagleLinkedList<ItemType>::Item **index;
    
public:
    
    /**
     * @brief Delete a linked list and all the items contained in it.
     * We do not synchronize when we free the list, this may be seen as dangerous from a synchronisation perspective
     * however if there is still object trying to use a list that is in the process of being freed then thats an
     * application logic error that needs to be corrected.
     */
    virtual ~EagleLinkedList();
    
    /**
     * Append an item to a linked list.
     * @param [in] item The item.
     */
    virtual void add(EagleLinkedList<ItemType>::Item *item) = 0;
    
    /**
     * Return a pointer to the first item on the list.
     * @return The pointer to the first element in the list.
     */
    virtual EagleLinkedList<ItemType>::Item* begin() const = 0;
    
    /**
     * Return a pointer to the last item on the list.
     * @return The pointer to the last item in the list.
     */
    virtual EagleLinkedList<ItemType>::Item* end() const = 0;
    
    /**
     * Get the first object on the linked list. This is not the same as EagleLinkedList_begin() which returns the
     * EagleLinkedList::Item rather than the object it is wrapping.
     * @return NULL or an object.
     */
    virtual ItemType first() const = 0;
    
    /**
     * Get the last object on the linked list. This is not the same as EagleLinkedList_end() which returns the
     * EagleLinkedList::Item rather than the object it is wrapping.
     * @return NULL or an object.
     */
    virtual ItemType last() const = 0;
    
    /**
     * The number of items in the list.
     * @return The number of items in the list.
     */
    virtual int length() const = 0;
    
    /**
     * Remove the last item on the list and return it. Since this does not free the item you will have to free it
     * yourself.
     * @see EagleLinkedList::~Item()
     * @return The linked list item on the end of the list.
     */
    virtual EagleLinkedList<ItemType>::Item* pop() = 0;
    
    /**
     * Check if the list is empty.
     * @return true is there are no items on the list.
     */
    virtual bool isEmpty() const = 0;
    
    /**
     * Convert a linked list to an array. No data is duplicated so if the items are freed in the linked list they will
     * disappear from your array as well.
     * @param [out] theSize The size of the array.
     * @return An array of objects (no the linked list items, the actual objects)
     */
    virtual ItemType* toArray(int *theSize) const = 0;
    
    /**
     * Alias of get(int).
     * @param [in] idx The index.
     * @return The object is returned (not the linked list item). If the index is out of bounds then NULL will be
     * returned.
     */
    ItemType operator[](int idx) const;
    
    /**
     * Get an item by index. The first item will have the index of 0.
     * @param [in] idx The index.
     * @return The object is returned (not the linked list item). If the index is out of bounds then NULL will be
     * returned.
     */
    virtual ItemType get(int idx) const = 0;
    
    /**
     * Add an object to the list. This is an easier method then creating the linked list item first, but it means you
     * have to free the items with the list.
     * @param [in] obj The object.
     * @param [in] freeObj Free the object when the item is freed.
     * @param [in] free The function responsible for freeing the objec. This may be NULL if you want to use
     * EagleMemory_Free()
     */
    void addObject(ItemType obj, bool freeObj, void (*free)(ItemType obj));
    
    /**
     * Add an object that will be freed with the `delete` operator.
     * @param [in] obj The object.
     * @param [in] freeObj Free the object when the item is freed.
     */
    void addObject(ItemType obj, bool freeObj);
    
    /**
     * Delete an object from a linked list. This will also remove the EagleLinkedList::Item that encapsulates in. If the
     * same pointer exists in multiple items only the first will be removed.
     * @param [in] obj The object to remove.
     * @return true if the item was found and removed.
     */
    virtual bool deleteObject(ItemType obj) = 0;
    
    /**
     * Calculate the ideal index size for a list of a set length.
     * @param [in] len The current length of the list.
     * @return Some value greater than \p len
     * @see EagleLinkedList::MinimumSizeForIndex
     */
    static int CalculateIndexSize(int len);
    
    /**
     * If the list length is 0 then an allocated (but blank) string will be returned.
     * @param [in] toStringFunction You may provide a custom function render each linked list item. If you provide NULL
     * then it is assumed your linked list item is already a NULL-terminated string.
     * @param [in] glue The string to join in between each item.
     * @return A string of all the rendered items joined together.
     */
    virtual std::string toString(char* (*toStringFunction)(ItemType), std::string glue) const = 0;
    
    /**
     * Remove the last item on the list and return its object.
     * @return The object.
     */
    ItemType popObject();
    
    /**
     * Remove the first item on the list and return it. Since this does not free the item you will have to free it
     * yourself.
     * @see EagleLinkedList::~Item()
     * @return The first item on the linked list of NULL if the list is empty.
     */
    virtual EagleLinkedList<ItemType>::Item* shift() = 0;
    
    /**
     * Remove the first item on the list and return its object.
     * @return The object.
     */
    ItemType shiftObject();
    
protected:
    
    /**
     * Do not call this constructor, use one of the constructors of the implementing classes.
     */
    EagleLinkedList();
    
    /**
     * Create a new linked list with specified syncronization.
     * @return A new linked list.
     */
    void init();
    
    /**
     * Internal non-syncronized method.
     * @param [in] item The item.
     * @see EagleLinkedList_add()
     */
    void add_(EagleLinkedList<ItemType>::Item *item);
    
    /**
     * Internal non-syncronized method.
     * @see EagleLinkedList_begin()
     * @return The pointer to the first element in the list.
     */
    EagleLinkedList<ItemType>::Item* begin_() const;
    
    /**
     * Internal non-syncronized method.
     * @return The pointer to the last item in the list.
     * @see EagleLinkedList_end()
     */
    EagleLinkedList<ItemType>::Item* end_() const;
    
    /**
     @brief Initialise the index for the provided linked list.
     @see EagleLinkedList::MinimumSizeForIndex
     */
    void initIndex_();
    
    /**
     * Internal method.
     * @return NULL or an object.
     */
    ItemType first_() const;
    
    /**
     * Internal method.
     * @return NULL or an object.
     */
    ItemType last_() const;
    
    /**
     * Internal method.
     * @param [in] obj The object to remove.
     * @return true if the item was found and removed.
     */
    bool deleteObject_(ItemType obj);
    
    /**
     * Internal non-syncronized method.
     * @return The number of items in the list.
     * @see EagleLinkedList_length()
     */
    int length_() const;
    
    /**
     * Internal non-syncronized method.
     * @return The linked list item on the end of the list.
     * @see EagleLinkedList_pop()
     */
    EagleLinkedList<ItemType>::Item* pop_();
    
    /**
     * Internal non-synchronized method.
     * @return The linked list item at the start of the list.
     * @see EagleLinkedList_shift()
     */
    EagleLinkedList<ItemType>::Item* shift_();
    
    /**
     * Internal non-syncronized method.
     * @see EagleLinkedList_isEmpty()
     * @return Success status.
     */
    bool isEmpty_() const;
    
    /**
     * Internal method.
     * @param [out] theSize The size of the array.
     * @return An array of objects (no the linked list items, the actual objects)
     */
    ItemType* toArray_(int *theSize) const;
    
    /**
     * Internal method.
     * @param [in] idx The index.
     * @return The object is returned (not the linked list item). If the index is out of bounds then NULL will be
     * returned.
     * @see EagleLinkedList::operator[]
     */
    ItemType get_(int idx) const;
    
    /**
     * Grow the index if needed. This will only increase in size, not shrink.
     * @see EagleLinkedList::MinimumSizeForIndex
     */
    void resizeIndex();
    
    /**
     * Internal method.
     * @param [in] toStringFunction You may provide a custom function render each linked list item. If you provide NULL
     * then it is assumed your linked list item is already a NULL-terminated string.
     * @param [in] glue The string to join in between each item.
     * @return A string of all the rendered items joined together.
     */
    std::string toString_(char* (*toStringFunction)(ItemType), std::string glue) const;
    
    /**
     * Update the index by removing an item.
     * @param [in] idx The index (starting with 0) of the item that has been removed from the linked list.
     */
    void deleteFromIndex(int idx);
    
};

template <typename ItemType>
EagleLinkedList<ItemType>::EagleLinkedList()
{
    init();
}

template <typename ItemType>
void EagleLinkedList<ItemType>::init()
{
    head = NULL;
    tail = NULL;
    size = 0;
    indexSize = 0;
    index = NULL;
}

template <typename ItemType>
int EagleLinkedList<ItemType>::CalculateIndexSize(int len)
{
    int l;
    double power = 2.0;
    
    if(len < EagleLinkedList::MinimumSizeForIndex) {
        return EagleLinkedList::MinimumSizeForIndex;
    }
    
    /* we start at EagleLinkedList::MinimumSizeForIndex and double the index size every time it grows */
    l = (int) fabs((log((double) (len + 1) / (double) EagleLinkedList::MinimumSizeForIndex) / log(power)));
    return EagleLinkedList::MinimumSizeForIndex * (int) pow(power, (double) l + 1.0);
}

template <typename ItemType>
void EagleLinkedList<ItemType>::initIndex_()
{
    EagleLinkedList::Item *cursor;
    int i;
    
    /* jump out here if we already have an index or our list is so short its not worth creating an index */
    if(indexSize > 0 || length_() < EagleLinkedList::MinimumSizeForIndex) {
        return;
    }
    
    /* allocate index */
    delete index;
    indexSize = CalculateIndexSize(length_());
    index = new EagleLinkedList::Item*[indexSize];
    
    /* fill index */
    for(cursor = begin_(), i = 0; NULL != cursor; cursor = cursor->getNext(), ++i) {
        index[i] = cursor;
    }
}

template <typename ItemType>
void EagleLinkedList<ItemType>::resizeIndex()
{
    int newSize;
    
    /* if there is no index then exit here */
    if(0 == indexSize) {
        return;
    }
    
    /* calulate the new index size */
    newSize = CalculateIndexSize(length_());
    
    /* at the moment I only care about growing */
    if(newSize > indexSize) {
        /* make a backup of the current index */
        size_t indexBytes = sizeof(EagleLinkedList::Item*) * (size_t) indexSize;
        EagleLinkedList::Item **oldIndex = new EagleLinkedList::Item*[indexBytes];
        memmove(oldIndex, index, indexBytes);
        
        /* allocate the new index */
        delete index;
        indexSize = newSize;
        index = new EagleLinkedList::Item*[indexSize];
        
        /* restore the backup index */
        memmove(index, oldIndex, indexBytes);
        delete oldIndex;
    }
}

template <typename ItemType>
void EagleLinkedList<ItemType>::add_(EagleLinkedList::Item *item)
{
    if(NULL == item) {
        return;
    }
    
    item->setNext(NULL);
    if(NULL == tail) {
        tail = head = item;
    }
    else {
        EagleLinkedList::Item *current = tail;
        current->setNext(item);
        current = current->getNext();
        tail = current;
    }
    ++size;
    
    /* build the index if needed */
    initIndex_();
    
    /* update index */
    resizeIndex();
    if(0 != indexSize && NULL != index) {
        index[size - 1] = item;
    }
}

template <typename ItemType>
int EagleLinkedList<ItemType>::length_() const
{
    return size;
}

template <typename ItemType>
EagleLinkedList<ItemType>::~EagleLinkedList()
{
    EagleLinkedList::Item *p, *next;
    
    /* free items first */
    for(p = head; NULL != p; p = next) {
        next = p->getNext();
        delete p;
    }
    
    delete index;
}

template <typename ItemType>
ItemType EagleLinkedList<ItemType>::first_() const
{
    ItemType r = NULL;
    EagleLinkedList::Item *top = begin_();
    
    if(NULL != top) {
        r = top->getObject();
    }
    
    return r;
}

template <typename ItemType>
ItemType EagleLinkedList<ItemType>::last_() const
{
    ItemType r = NULL;
    EagleLinkedList::Item *bottom = end_();
    
    if(NULL != bottom) {
        r = bottom->getObject();
    }
    
    return r;
}

template <typename ItemType>
typename EagleLinkedList<ItemType>::Item* EagleLinkedList<ItemType>::begin_() const
{
    return head;
}

template <typename ItemType>
typename EagleLinkedList<ItemType>::Item* EagleLinkedList<ItemType>::end_() const
{
    return tail;
}

template <typename ItemType>
typename EagleLinkedList<ItemType>::Item* EagleLinkedList<ItemType>::shift_()
{
    if(length_() == 0) {
        return NULL;
    }
    if(length_() == 1) {
        EagleLinkedList::Item *item = head;
        head = tail = NULL;
        size = 0;
        return item;
    }
    if(length_() == 2) {
        EagleLinkedList::Item *item = head;
        head = head->getNext();
        tail = head;
        size = 1;
        return item;
    }
    
    // connect the head to the second item
    EagleLinkedList::Item *item = head;
    head = head->getNext();
    --size;
    return item;
}

template <typename ItemType>
typename EagleLinkedList<ItemType>::Item* EagleLinkedList<ItemType>::pop_()
{
    EagleLinkedList::Item *next, *r;
    int i;
    
    if(length_() == 0) {
        return NULL;
    }
    if(length_() == 1) {
        EagleLinkedList::Item *item = head;
        head = tail = NULL;
        --size;
        item->setNext(NULL);
        return item;
    }
    
    /* find the second last item */
    next = head;
    for(i = 0; i < size - 2; ++i) {
        next = next->getNext();
    }
    
    --size;
    tail = next;
    next->getNext()->setNext(NULL);
    r = next->getNext();
    tail->setNext(NULL);
    return r;
}

template <typename ItemType>
bool EagleLinkedList<ItemType>::isEmpty_() const
{
    if(0 == size) {
        return true;
    }
    return false;
}

template <typename ItemType>
ItemType* EagleLinkedList<ItemType>::toArray_(int *theSize) const
{
    ItemType *array = NULL;
    EagleLinkedList::Item *p, *next;
    int i;
    
    *theSize = length_();
    array = new ItemType[*theSize];
    
    for(p = head, i = 0; NULL != p; p = next, ++i) {
        array[i] = p->getObject();
        next = p->getNext();
    }
    
    return array;
}

template <typename ItemType>
ItemType EagleLinkedList<ItemType>::operator[](int idx) const
{
    return get(idx);
}

template <typename ItemType>
ItemType EagleLinkedList<ItemType>::get_(int idx) const
{
    EagleLinkedList::Item *next;
    int i, listLength = length_();
    
    /* out of bounds */
    if(idx < 0 || idx >= listLength) {
        return NULL;
    }
    
    /* if we have an index then use that */
    if(indexSize > 0 && NULL != index) {
        return index[idx]->getObject();
    }
    
    /* otherwise revert back to the old way */
    next = head;
    for(i = 0; i < idx; ++i) {
        next = (NULL == next) ? NULL : next->getNext();
    }
    
    return (NULL == next) ? NULL : next->getObject();
}

template <typename ItemType>
void EagleLinkedList<ItemType>::addObject(ItemType obj, bool freeObj, void (*free)(ItemType obj))
{
    add(new EagleLinkedList::Item(obj, freeObj, free));
}

template <typename ItemType>
void EagleLinkedList<ItemType>::addObject(ItemType obj, bool freeObj)
{
    add(new EagleLinkedList::Item(obj, freeObj));
}

template <typename ItemType>
void EagleLinkedList<ItemType>::deleteFromIndex(int idx)
{
    if(indexSize > 0 && NULL != index) {
        int i = idx;
        for(; i < indexSize - 1; ++i) {
            index[i] = index[i + 1];
        }
    }
}

template <typename ItemType>
bool EagleLinkedList<ItemType>::deleteObject_(ItemType obj)
{
    EagleLinkedList::Item *bottom, *cursor;
    int i;
    
    /* problems */
    if(NULL == obj) {
        return false;
    }
    if(isEmpty_()) {
        return false;
    }
    
    /* one item */
    if(length_() == 1) {
        if(head->getObject() == obj) {
            delete head;
            size = 0;
            head = NULL;
            tail = NULL;
            
            /* update index */
            deleteFromIndex(0);
            
            return true;
        }
        return false;
    }
    
    /* begin item */
    if(begin_()->getObject() == obj) {
        EagleLinkedList::Item *second = head->getNext();
        
        delete head;
        --size;
        head = second;
        
        /* update index */
        deleteFromIndex(0);
        
        return true;
    }
    
    /* some item after the first */
    bottom = begin_();
    for(cursor = bottom->getNext(), i = 1; NULL != cursor; cursor = cursor->getNext(), bottom = bottom->getNext(), ++i) {
        if(obj == cursor->getObject()) {
            /* if this is the last item we need to fix the last pointer */
            if(tail == cursor) {
                tail = bottom;
            }
            
            /* update index */
            deleteFromIndex(i);
            
            /* update list */
            bottom->setNext(cursor->getNext());
            delete cursor;
            --size;
            
            return true;
        }
    }
    
    /* object not found on list */
    return false;
}

template <typename ItemType>
std::string EagleLinkedList<ItemType>::toString_(char* (*toStringFunction)(ItemType), std::string glue) const
{
    std::string string;
    EagleLinkedList::Item *cursor = NULL;
    
    cursor = begin_();
    
    for(; cursor != NULL; cursor = cursor->getNext()) {
        /* render item */
        std::string item;
        if(NULL == toStringFunction) {
            item = std::string((char*) cursor->getObject());
        }
        else {
            char *str = toStringFunction(cursor->getObject());
            item = std::string(str);
            delete str;
        }
        
        /* add to result string */
        if(cursor != head) {
            string += glue;
        }
        string += item;
    }
    
    return string;
}

template <typename ItemType>
EagleLinkedList<ItemType>::Item::Item(ItemType theObj, bool theFreeObj)
{
    init(theObj, theFreeObj, NULL, NULL);
}

template <typename ItemType>
EagleLinkedList<ItemType>::Item::Item(ItemType theObj, bool theFreeObj, void (*theFree)(ItemType))
{
    init(theObj, theFreeObj, theFree, NULL);
}

template <typename ItemType>
void EagleLinkedList<ItemType>::Item::init(ItemType theObj, bool theFreeObj, void (*theFree)(ItemType),
                                           char *theDescription)
{
    obj = theObj;
    freeObj = theFreeObj;
    next = NULL;
    free = theFree;
    description = theDescription;
}

template <typename ItemType>
EagleLinkedList<ItemType>::Item::~Item()
{
    if(freeObj) {
        if(NULL == free) {
            delete obj;
        }
        else {
            free(obj);
        }
    }
    delete description;
}

template <typename ItemType>
ItemType EagleLinkedList<ItemType>::Item::getObject()
{
    return obj;
}

template <typename ItemType>
typename EagleLinkedList<ItemType>::Item* EagleLinkedList<ItemType>::Item::getNext()
{
    return next;
}

template <typename ItemType>
void EagleLinkedList<ItemType>::Item::setNext(typename EagleLinkedList<ItemType>::Item *item)
{
    next = item;
}

template <typename ItemType>
void EagleLinkedList<ItemType>::Item::setFreeObject(bool theFreeObj)
{
    freeObj = theFreeObj;
}

template <typename ItemType>
ItemType EagleLinkedList<ItemType>::popObject()
{
    EagleLinkedList<ItemType>::Item *item = pop();
    ItemType obj = item->getObject();
    item->setFreeObject(false);
    delete item;
    return obj;
}

template <typename ItemType>
ItemType EagleLinkedList<ItemType>::shiftObject()
{
    EagleLinkedList<ItemType>::Item *item = shift();
    ItemType obj = item->getObject();
    item->setFreeObject(false);
    delete item;
    return obj;
}

#endif
