#include "EagleLock.h"
#include "eagle/Worker.h"
#include "eagle/Calendar.h"

EagleLock::~EagleLock()
{
    pthread_mutex_destroy(&mutex);
    pthread_mutexattr_destroy(&attr);
}

void EagleLock::lock()
{
    eagle::Worker *currentWorker = eagle::Worker::GetForCurrentThread();
    unsigned long start = eagle::Calendar::getInstance()->getAbsoluteTime();
    pthread_mutex_lock(&mutex);
    ++lockTimes;
    if(NULL != currentWorker) {
        currentWorker->lockTime += eagle::Calendar::getInstance()->getAbsoluteTime() - start;
    }
}

EagleLock::EagleLock()
{
    lockTimes = 0;
    pthread_mutex_init(&mutex, NULL);
}

void EagleLock::unlock()
{
    pthread_mutex_unlock(&mutex);
}
