#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "EagleData.h"

EagleDataTypeInteger* EagleData::CreateInt(EagleDataTypeInteger value)
{
    EagleDataTypeInteger *r = new EagleDataTypeInteger;
    *r = value;
    return r;
}

EagleDataTypeFloat* EagleData::CreateFloat(EagleDataTypeFloat value)
{
    EagleDataTypeFloat *r = new EagleDataTypeFloat;
    *r = value;
    return r;
}

EagleData::Type EagleData::NameToType(char *name)
{
    if(NULL == name) {
        return EagleData::Type::Unknown;
    }
    
    if(!strcasecmp(name, "int") || !strcasecmp(name, "integer")) {
        return EagleData::Type::Integer;
    }
    if(!strcasecmp(name, "varchar") || !strcasecmp(name, "text")) {
        return EagleData::Type::Varchar;
    }
    if(!strcasecmp(name, "float") || !strcasecmp(name, "double") ||
       !strcasecmp(name, "double precision") || !strcasecmp(name, "real")) {
        return EagleData::Type::Float;
    }
    
    return EagleData::Type::Unknown;
}

char* EagleData::TypeToName(EagleData::Type type)
{
    char *r = NULL;
    
    switch(type) {
            
        case EagleData::Type::Unknown:
            r = strdup("UNKNOWN");
            break;
            
        case EagleData::Type::Integer:
            r = strdup("INTEGER");
            break;
            
        case EagleData::Type::Varchar:
            r = strdup("VARCHAR");
            break;
            
        case EagleData::Type::Float:
            r = strdup("FLOAT");
            break;
            
    }
    
    return r;
}
