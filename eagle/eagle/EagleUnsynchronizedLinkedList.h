#ifndef eagle_EagleUnsynchronizedLinkedList_h
#define eagle_EagleUnsynchronizedLinkedList_h

#include "EagleLinkedList.h"

/**
 * An unsynchronized linked list (FIFO).
 */
template <typename ItemType = void*>
class EagleUnsynchronizedLinkedList : public EagleLinkedList<ItemType>
{
    
public:
    
    /**
     * Create a new unynchronized linked list.
     * @return A new linked list.
     */
    EagleUnsynchronizedLinkedList();
    
    virtual void add(typename EagleLinkedList<ItemType>::Item *item);
    
    virtual int length() const;
    
    virtual ItemType first() const;
    
    virtual ItemType last() const;
    
    virtual typename EagleLinkedList<ItemType>::Item* begin() const;
    
    virtual typename EagleLinkedList<ItemType>::Item* end() const;
    
    virtual typename EagleLinkedList<ItemType>::Item* pop();
    
    virtual bool isEmpty() const;
    
    virtual ItemType* toArray(int *theSize) const;
    
    virtual ItemType get(int idx) const;
    
    virtual bool deleteObject(ItemType obj);
    
    virtual std::string toString(char* (*toStringFunction)(ItemType), std::string glue) const;
    
    virtual typename EagleLinkedList<ItemType>::Item* shift();
    
};

template <typename ItemType>
EagleUnsynchronizedLinkedList<ItemType>::EagleUnsynchronizedLinkedList()
{
    this->init();
}

template <typename ItemType>
void EagleUnsynchronizedLinkedList<ItemType>::add(typename EagleLinkedList<ItemType>::Item *item)
{
    this->add_(item);
}

template <typename ItemType>
int EagleUnsynchronizedLinkedList<ItemType>::length() const
{
    return this->length_();
}

template <typename ItemType>
ItemType EagleUnsynchronizedLinkedList<ItemType>::first() const
{
    return this->first_();
}

template <typename ItemType>
ItemType EagleUnsynchronizedLinkedList<ItemType>::last() const
{
    return this->last_();
}

template <typename ItemType>
typename EagleLinkedList<ItemType>::Item* EagleUnsynchronizedLinkedList<ItemType>::begin() const
{
    return this->begin_();
}

template <typename ItemType>
typename EagleLinkedList<ItemType>::Item* EagleUnsynchronizedLinkedList<ItemType>::end() const
{
    return this->end_();
}

template <typename ItemType>
typename EagleLinkedList<ItemType>::Item* EagleUnsynchronizedLinkedList<ItemType>::pop()
{
    return this->pop_();
}

template <typename ItemType>
typename EagleLinkedList<ItemType>::Item* EagleUnsynchronizedLinkedList<ItemType>::shift()
{
    return this->shift_();
}

template <typename ItemType>
bool EagleUnsynchronizedLinkedList<ItemType>::isEmpty() const
{
    return this->isEmpty_();
}

template <typename ItemType>
ItemType* EagleUnsynchronizedLinkedList<ItemType>::toArray(int *theSize) const
{
    return this->toArray_(theSize);
}

template <typename ItemType>
ItemType EagleUnsynchronizedLinkedList<ItemType>::get(int idx) const
{
    return this->get_(idx);
}

template <typename ItemType>
bool EagleUnsynchronizedLinkedList<ItemType>::deleteObject(ItemType obj)
{
    return this->deleteObject_(obj);
}

template <typename ItemType>
std::string EagleUnsynchronizedLinkedList<ItemType>::toString(char* (*toStringFunction)(ItemType), std::string glue) const
{
    return this->toString_(toStringFunction, glue);
}

#endif
