#include <stdlib.h>
#include <stdio.h>
#include <string>
#include "eagle/Plan.h"
#include "eagle/PageProvider.h"
#include "eagle/Worker.h"
#include "EagleSynchronizedLinkedList.h"
#include "eagle/plan/Operation.h"
#include "EagleLogger.h"
#include "eagle/plan/stage/SelectStage.h"
#include "eagle/plan/stage/AbstractStage.h"
#include "EaglePlanError.h"

eagle::Plan::Plan()
{
    stages = new EagleSynchronizedLinkedList<eagle::plan::stage::AbstractStage*>();
    currentStage = 0;
    nextJobLock = new EagleLock();
    stalled = false;
}

bool eagle::Plan::isStalled()
{
    return stalled;
}

void eagle::Plan::addStage(eagle::plan::stage::AbstractStage *stage, bool freeWithPlan)
{
    stages->addObject(stage, freeWithPlan);
}

std::string eagle::Plan::toString()
{
    std::string str;
    
    EagleLinkedList_Foreach(stages, eagle::plan::stage::AbstractStage*, stage)
    {
        str += stage->toString();
    }
    EagleLinkedList_ForeachEnd
    
    return str;
}

eagle::Plan::~Plan()
{
    delete stages;
    delete nextJobLock;
}

eagle::plan::job::AbstractJob* eagle::Plan::nextJob()
{
    nextJobLock->lock();
    eagle::plan::job::AbstractJob *r = NULL;
    
    while(true) {
        // detect end of work
        if(currentStage >= stages->length()) {
            // all stages are finished
            break;
        }
        
        // try to get the next job from the current stage
        eagle::plan::stage::AbstractStage *stage = stages->get(currentStage);
        eagle::plan::job::AbstractJob *job = stage->nextJob();
        
        // catch stalled status
        if(NULL == job && !stage->complete) {
            stalled = true;
            break;
        }
        else {
            stalled = false;
        }
        
        // end of the stage
        if(NULL == job) {
            ++currentStage;
            continue;
        }
        
        r = job;
        break;
    }
    
    nextJobLock->unlock();
    return r;
}

unsigned long eagle::Plan::getStartTime()
{
    eagle::plan::stage::AbstractStage *stage = stages->first();
    if(NULL == stage) {
        return 0;
    }
    return stage->getStartTime();
}

bool eagle::Plan::isError()
{
    return stages->get(currentStage)->isError();
}

std::string eagle::Plan::getErrorMessage()
{
    return stages->get(currentStage)->errorMessage;
}
