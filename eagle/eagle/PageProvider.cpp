#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include "eagle/ArrayPageProvider.h"
#include "eagle/SinglePageProvider.h"
#include "eagle/StreamPageProvider.h"

int eagle::PageProvider::TotalPages(int totalRecords, int recordsPerPage)
{
    //! @t eagle::PageProviderTest::TotalPages
    
    int pagesLeft;
    
    if(0 == totalRecords || 0 == recordsPerPage) {
        return 0;
    }
    
    pagesLeft = totalRecords / recordsPerPage;
    if(totalRecords % recordsPerPage > 0) {
        ++pagesLeft;
    }
    
    return pagesLeft;
}

eagle::PageProvider::PageProvider()
{
    usageCount = 1;
}

eagle::PageProvider::~PageProvider()
{
}

bool eagle::PageProvider::add(void*)
{
    return false;
}

int eagle::PageProvider::pagesRemaining()
{
    return 0;
}

EaglePage* eagle::PageProvider::nextPage()
{
    return NULL;
}

void eagle::PageProvider::reset()
{
}

bool eagle::PageProvider::isRandomAccess()
{
    return false;
}

EaglePage* eagle::PageProvider::getPage(int)
{
    return NULL;
}

void* eagle::PageProvider::getDataByRecordId(int /*recordId*/)
{
    return NULL;
}

void eagle::PageProvider::operator delete(void *obj)
{
    eagle::PageProvider *provider = (eagle::PageProvider*) obj;
    
    /* decrement the usage count */
    --provider->usageCount;
    
    /* if this object is still in use then we can't free it yet */
    if(provider->usageCount > 0) {
        return;
    }
    
    // Delete the object.
    free(provider);
}
