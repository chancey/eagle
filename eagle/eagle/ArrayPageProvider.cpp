#include <string.h>
#include <stdlib.h>
#include "eagle/ArrayPageProvider.h"
#include "eagle/Worker.h"
#include "eagle/Maths.h"

eagle::ArrayPageProvider::ArrayPageProvider(EagleData::Type theType, void *theRecords, int theTotalRecords,
                                            int theRecordsPerPage, std::string theName, bool theFreeRecords)
{
    type = theType;
    name = theName;
    offsetRecords = 0;
    totalRecords = theTotalRecords;
    records = theRecords;
    nextPageLock = new EagleLock();
    recordsPerPage = theRecordsPerPage;
    freeRecords = theFreeRecords;
}

eagle::ArrayPageProvider::~ArrayPageProvider()
{
    delete nextPageLock;
    if(freeRecords) {
        free(records);
    }
}

EaglePage* eagle::ArrayPageProvider::nextPage()
{
    int pageSize = eagle::Maths::Min(recordsPerPage, totalRecords - offsetRecords);
    EaglePage *page = NULL;
    
    if(offsetRecords >= totalRecords) {
        return NULL;
    }
    
    nextPageLock->lock();
    switch(type) {
            
        case EagleData::Type::Integer:
        {
            EagleDataTypeInteger *begin = (EagleDataTypeInteger*) records;
            page = new EaglePage(type, begin + offsetRecords, pageSize, pageSize, offsetRecords, false);
            break;
        }
            
        case EagleData::Type::Float:
        {
            EagleDataTypeFloat *begin = (EagleDataTypeFloat*) records;
            page = new EaglePage(type, begin + offsetRecords, pageSize, pageSize, offsetRecords, false);
            break;
        }
            
        case EagleData::Type::Varchar:
        {
            EagleDataTypeVarchar *begin = (EagleDataTypeVarchar*) records;
            page = new EaglePage(type, begin + offsetRecords, pageSize, pageSize, offsetRecords, false);
            break;
        }
            
        case EagleData::Type::Unknown:
            break;
            
    }
    offsetRecords += pageSize;
    nextPageLock->unlock();
    
    return page;
}

void eagle::ArrayPageProvider::reset()
{
    nextPageLock->lock();
    offsetRecords = 0;
    nextPageLock->unlock();
}

bool eagle::ArrayPageProvider::isRandomAccess()
{
    return true;
}

int eagle::ArrayPageProvider::pagesRemaining()
{
    int r;
    
    nextPageLock->lock();
    r = TotalPages(totalRecords - offsetRecords, recordsPerPage);
    nextPageLock->unlock();
    
    return r;
}

void* eagle::ArrayPageProvider::getDataByRecordId(int recordId)
{
    void *r = NULL;
    
    // check for out of bounds
    if(recordId >= totalRecords || recordId < 0) {
        return r;
    }
    
    switch(type) {
            
        case EagleData::Type::Integer:
        {
            EagleDataTypeInteger *d = (EagleDataTypeInteger*) records;
            r = &d[recordId];
            break;
        }
            
        case EagleData::Type::Float:
        {
            EagleDataTypeFloat *d = (EagleDataTypeFloat*) records;
            r = &d[recordId];
            break;
        }
            
        case EagleData::Type::Varchar:
        {
            EagleDataTypeVarchar *d = (EagleDataTypeVarchar*) records;
            r = &d[recordId];
            break;
        }
            
        case EagleData::Type::Unknown:
            break;
        
    }
    
    return r;
}

EaglePage* eagle::ArrayPageProvider::getPage(int pageNumber)
{
    EaglePage *page = NULL;
    int theOffsetRecords = pageNumber * recordsPerPage;
    int pageSize = eagle::Maths::Min(recordsPerPage, totalRecords - theOffsetRecords);
    
    /* check for out of range */
    if(pageNumber < 0 || pageNumber >= eagle::PageProvider::TotalPages(totalRecords, recordsPerPage)) {
        return NULL;
    }
    
    switch(type) {
            
        case EagleData::Type::Integer:
        {
            EagleDataTypeInteger *begin = (EagleDataTypeInteger*) records;
            page = new EaglePage(type, begin + theOffsetRecords, pageSize, pageSize, theOffsetRecords, false);
            break;
        }
            
        case EagleData::Type::Float:
        {
            EagleDataTypeFloat *begin = (EagleDataTypeFloat*) records;
            page = new EaglePage(type, begin + theOffsetRecords, pageSize, pageSize, theOffsetRecords, false);
            break;
        }
            
        case EagleData::Type::Varchar:
        {
            EagleDataTypeVarchar *begin = (EagleDataTypeVarchar*) records;
            page = new EaglePage(type, begin + theOffsetRecords, pageSize, pageSize, theOffsetRecords, false);
            break;
        }
            
        case EagleData::Type::Unknown:
            break;
            
    }
    
    return page;
}
