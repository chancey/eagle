#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "EaglePage.h"
#include "EagleLogger.h"
#include "eagle/Comparable.h"

EaglePage::EaglePage(EagleData::Type theType, void *theData, int theTotalSize, int theCount, int theRecordOffset,
                     bool theFreeData)
{
    init(theType, theData, theTotalSize, theCount, theRecordOffset, theFreeData, 1);
}

void EaglePage::init(EagleData::Type theType, void *theData, int theTotalSize, int theCount, int theRecordOffset,
                     bool theFreeData, int theUsageCount)
{
    //! @t eagle::EaglePageTest::EaglePage
    type = theType;
    data = theData;
    totalSize = theTotalSize;
    count = theCount;
    recordOffset = theRecordOffset;
    freeData = theFreeData;
    usageCount = theUsageCount;
}

bool EaglePage::equals(eagle::Comparable *obj)
{
    EaglePage *page2 = (EaglePage*) obj;
    
    if(true == eagle::Comparable::equals(page2)) {
        return true;
    }
    
    /* check the individual properties */
    if(count != page2->count) {
        return false;
    }
    if(totalSize != page2->totalSize) {
        return false;
    }
    if(type != page2->type) {
        return false;
    }
    
    /* and finally, deep check data */
    if(data != page2->data) {
        switch(type) {
                
            case EagleData::Type::Unknown:
                return false;
                
            case EagleData::Type::Integer:
                if(0 != memcmp(data, page2->data, sizeof(EagleDataTypeInteger) * (size_t) count)) {
                    return false;
                }
                break;
                
            case EagleData::Type::Float:
                if(0 != memcmp(data, page2->data, sizeof(EagleDataTypeFloat) * (size_t) count)) {
                    return false;
                }
                break;
                
            case EagleData::Type::Varchar:
            {
                int i;
                EagleDataTypeVarchar *p1data = (EagleDataTypeVarchar*) data;
                EagleDataTypeVarchar *p2data = (EagleDataTypeVarchar*) page2->data;
                
                for(i = 0; i < count; ++i) {
                    if(0 != strcmp(p1data[i], p2data[i])) {
                        return false;
                    }
                }
                break;
            }
                
        }
    }
    
    /* page contents are equal */
    return true;
}

EaglePage* EaglePage::AllocInt(int count)
{
    void *data = (void*) new EagleDataTypeInteger[count];
    return new EaglePage(EagleData::Type::Integer, data, count, count, 0, true);
}

EaglePage* EaglePage::AllocFloat(int count)
{
    void *data = (void*) new EagleDataTypeFloat[count];
    return new EaglePage(EagleData::Type::Float, data, count, count, 0, true);
}

EaglePage* EaglePage::AllocVarchar(int count)
{
    void *data = (void*) new EagleDataTypeVarchar[count];
    
    // We must zero the the data out for the destructor.
    bzero(data, sizeof(EagleDataTypeVarchar) * (size_t) count);
    
    return new EaglePage(EagleData::Type::Varchar, data, count, count, 0, true);
}

void EaglePage::operator delete(void *obj)
{
    EaglePage *page = (EaglePage*) obj;
    
    /* decrement the usage count */
    --page->usageCount;
    
    /* if this object is still in use then we can't free it yet */
    if(page->usageCount > 0) {
        return;
    }
    
    if(true == page->freeData) {
        switch(page->type) {
                
            case EagleData::Type::Integer:
            case EagleData::Type::Unknown:
            case EagleData::Type::Float:
                break;
                
            case EagleData::Type::Varchar:
            {
                /* free all strings first */
                int i;
                EagleDataTypeVarchar *d = ((EagleDataTypeVarchar*) page->data);
                
                for(i = 0; i < page->count; ++i) {
                    delete d[i];
                }
                break;
            }
                
        }
        
        free(page->data);
    }
    
    free(page);
}

EaglePage* EaglePage::copy()
{
    /* increment the usage count but return the same page */
    ++usageCount;
    return this;
}

EaglePage* EaglePage::RealCopy(EaglePage *page)
{
    EaglePage *r = NULL;
    
    if(NULL == page) {
        return NULL;
    }
    
    switch(page->type) {
            
        case EagleData::Type::Unknown:
            EagleLogger::Log(EagleLogger::Severity::Error, "Cannot page of Unknown type.");
            r = NULL;
            break;
            
        case EagleData::Type::Integer:
            r = EaglePage::RealCopyInt(page);
            break;
            
        case EagleData::Type::Varchar:
            r = EaglePage::RealCopyVarchar(page);
            break;
            
        case EagleData::Type::Float:
            r = EaglePage::RealCopyFloat(page);
            break;
            
    }

    return r;
}

EaglePage* EaglePage::RealCopyInt(EaglePage *page)
{
    size_t memorySize;
    EagleDataTypeInteger *newData;
    
    memorySize = (size_t) page->count * sizeof(EagleDataTypeInteger);
    newData = new EagleDataTypeInteger[page->count];
    
    memmove(newData, page->data, memorySize);
    return new EaglePage(page->type, newData, page->totalSize, page->count, page->recordOffset, page->freeData);
}

EaglePage* EaglePage::RealCopyFloat(EaglePage *page)
{
    size_t memorySize;
    EagleDataTypeFloat *newData;
    
    memorySize = (size_t) page->count * sizeof(EagleDataTypeFloat);
    newData = new EagleDataTypeFloat[page->count];
    
    memmove(newData, page->data, memorySize);
    return new EaglePage(page->type, newData, page->totalSize, page->count, page->recordOffset, page->freeData);
}

EaglePage* EaglePage::RealCopyVarchar(EaglePage *page)
{
    EagleDataTypeVarchar *newData;
    EagleDataTypeVarchar *d;
    int i;
    
    newData = new EagleDataTypeVarchar[page->count];
    
    d = (EagleDataTypeVarchar*) page->data;
    for(i = 0; i < page->count; ++i) {
        newData[i] = strdup(d[i]);
    }
    
    return new EaglePage(page->type, newData, page->totalSize, page->count, page->recordOffset, page->freeData);
}

EaglePage* EaglePage::Alloc(EagleData::Type type, int count)
{
    EaglePage *r = NULL;
    
    switch(type) {
            
        case EagleData::Type::Unknown:
            r = NULL;
            break;
            
        case EagleData::Type::Integer:
            r = EaglePage::AllocInt(count);
            break;
            
        case EagleData::Type::Varchar:
            r = EaglePage::AllocVarchar(count);
            break;
            
        case EagleData::Type::Float:
            r = EaglePage::AllocFloat(count);
            break;
            
    }

    return r;
}

char* EaglePage::toString()
{
    char* buf = new char[8192], *theType;
    
    theType = EagleData::TypeToName(type);
    sprintf(buf, "EaglePage { type = %s, size = %d, count = %d, offset = %d }", theType, totalSize, count,
            recordOffset);
    delete theType;
    
    return buf;
}

EaglePage::~EaglePage()
{
    // this does nothing but it needs to be virtual to satisfy the parent class
}
