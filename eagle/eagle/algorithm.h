#ifndef eagle_algorithm_h
#define eagle_algorithm_h

namespace eagle
{
    namespace algorithm
    {
        
        /**
         * Create a new string that is lower case.
         * @param [in] str Input string to be duplicated.
         * @return A new string.
         */
        char* toLowerCase(const char *str);
        
        /**
         * Create a new string that is upper case.
         * @param [in] str Input string to be duplicated.
         * @return A new string.
         */
        char* toUpperCase(const char *str);
        
        /**
         * Test if two strings are equal (ignoring case).
         * @note This is not safe for non-ASCII strings.
         * @param [in] a First string.
         * @param [in] b First string.
         * @return true if the strings are equal.
         */
        bool equalsIgnoreCase(const char *a, const char *b);
        
    }
}

#endif
