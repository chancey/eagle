#ifndef eagle_PageProvider_h
#define eagle_PageProvider_h

#include <pthread.h>
#include "EaglePage.h"
#include "EagleLock.h"
#include "EagleLinkedList.h"
#include "EagleData.h"

namespace eagle
{
    class PageProvider;
}

/**
 * A page provider.
 */
class eagle::PageProvider
{
    
    // @todo make protected
public:
    
    /**
     * Data type.
     */
    EagleData::Type type;
    
    /**
     * The name of this provider.
     */
    std::string name;
    
    /**
     * How many records in this page.
     */
    int recordsPerPage;
    
    /**
     * Usage count for this object.
     */
    int usageCount;
    
public:
    
    /**
     * Calculate the amount of pages required to serve a given amount of records.
     * @param [in] totalRecords The total records.
     * @param [in] recordsPerPage The maximum records a single page can hold.
     * @return The number of pages left.
     */
    static int TotalPages(int totalRecords, int recordsPerPage);
    
    /**
     * Reset the cursor of a provider.
     */
    virtual void reset();
    
    /**
     * Delete a provider.
     */
    virtual ~PageProvider();
    
    /**
     * Add a record to a writable provider (if possible).
     * @param [in] data The data to add to the stream.
     * @return true on success.
     */
    virtual bool add(void *data);
    
    /**
     * Return the amount of pages remaining.
     * @return The number of pages available for reading.
     */
    virtual int pagesRemaining();
    
    /**
     * Get the next page from a provider.
     * @return The next page, or NULL.
     */
    virtual EaglePage* nextPage();
    
    /**
     * Check is a providers pages can be access in any order with EaglePageProvider_getPage().
     * @return Success status.
     */
    virtual bool isRandomAccess();
    
    /**
     * Get a random page from a provider. You may only use this if EaglePageProvider_isRandomAccess() returns true.
     * @param [in] pageNumber The number of the page where the first page will be 0.
     * @return The page, or NULL.
     */
    virtual EaglePage* getPage(int pageNumber);
    
    virtual void* getDataByRecordId(int recordId);
    
    PageProvider();
    
    void operator delete(void *page);
    
};

#endif
