#ifndef eagle_Workers_h
#define eagle_Workers_h

namespace eagle
{
    class Workers;
    class Worker;
    class Instance;
}

/**
 * A pool of workers.
 */
class eagle::Workers
{
    
    // @todo make protected
public:
    
    /**
     * The workers in this pool. All workers are managed by this object, if you delete this object you will also kill all
     * the of the workers.
     */
    eagle::Worker **workers;
    
    /**
     * The number of workers in this pool.
     */
    int totalWorkers;
    
public:
    
    /**
     * Create a new pool of workers.
     * @param [in] totalWorkers All the workers will be initialised (but not started) when the pool is created.
     * @param [in] instance The eagle instance.
     */
    Workers(int totalWorkers, eagle::Instance *instance);
    
    /**
     * Delete a pool of workers.
     */
    ~Workers();
    
    /**
     * Start the pool of workers.
     */
    void start();
    
    /**
     * Wait for all workers to finish. This is used when closing down the instance.
     */
    void joinAll();
    
};

#endif
