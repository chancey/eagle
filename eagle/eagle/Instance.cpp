#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include "eagle/Instance.h"
#include "EagleLogger.h"
#include "eagle/Workers.h"
#include "eagle/Plan.h"
#include "eagle/plan/stage/EntireSort.h"

eagle::Instance::Instance(int totalWorkers)
{
    workers = new eagle::Workers(totalWorkers, this);
    plan = NULL;
}

void eagle::Instance::run()
{
    /* start workers */
    workers->start();
    
    /* close workers */
    workers->joinAll();
}

void eagle::Instance::addPlan(eagle::Plan *thePlan)
{
    plan = thePlan;
}

eagle::plan::job::AbstractJob* eagle::Instance::nextJob()
{
    while(true) {
        // try to get the next job
        eagle::plan::job::AbstractJob *job = plan->nextJob();
        
        // if the result is NULL but we are not complete then we have to wait for a bit
        if(plan->isStalled()) {
            usleep(10000);
            continue;
        }
        
        return job;
    }
}

eagle::Instance::~Instance()
{
    delete workers;
}
