#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <pthread.h>

#ifdef darwin
#include <mach/mach_time.h>
#endif

#ifdef linux
#include <sys/syscall.h>
#include <time.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#endif

#include "eagle/Calendar.h"

eagle::Calendar *eagle::Calendar::instance = NULL;

eagle::Calendar *eagle::Calendar::getInstance()
{
    if(NULL == instance) {
        instance = new eagle::Calendar();
    }
    return instance;
}

eagle::Calendar::Calendar()
{
}

unsigned long eagle::Calendar::getAbsoluteTime(void)
{
#ifdef linux
    struct timespec ts;
#endif
    
    /* return the true absolute time */
#ifdef linux
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return (ts.tv_sec * eagle::Calendar::TicksPerSecond) + ts.tv_nsec;
#elif defined(darwin)
    return mach_absolute_time();
#else
#error "Unknown platform"
#endif
}
