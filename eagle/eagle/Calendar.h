#ifndef eagle_Calendar_h
#define eagle_Calendar_h

namespace eagle
{
    class Calendar;
}

class eagle::Calendar
{
    
public:
    
    /**
     * The number of ticks per second. Be very careful when changing this number as it may have different affects on
     * different operating systems.
     */
    const static unsigned long TicksPerSecond = 1000000000L;
    
    /**
     * Get the absolute time. One second is 1 billion clock ticks.
     * @return The absolute time of the mocked time.
     * @see eagle::Calendar::TicksPerSecond
     */
    virtual unsigned long getAbsoluteTime(void);
    
    /**
     * Get the global, reusable instance of the eagle::Calendar object.
     */
    static eagle::Calendar *getInstance();
    
protected:
    
    /**
     * This is an abstract class.
     * @see eagle::Calendar::getInstance()
     */
    Calendar();
    
    /**
     * The cached eagle::Calendar instance.
     */
    static eagle::Calendar *instance;
    
};

#endif
