#ifndef eagle_SinglePageProvider_h
#define eagle_SinglePageProvider_h

#include "eagle/PageProvider.h"

namespace eagle
{
    class SinglePageProvider;
}

class eagle::SinglePageProvider : public eagle::PageProvider
{
    
    // @todo make protected
public:
 
    union {
        
        /**
         Integer value.
         */
        EagleDataTypeInteger intValue;
        
        /**
         String value.
         */
        EagleDataTypeVarchar strValue;
        
        /**
         Floating point value.
         */
        EagleDataTypeFloat floatValue;
        
    }
    
    /**
     * Contains the value.
     */
    value;
    
public:
    
    /**
     * This creates a page provider that provides a single page filled with a fixed int.
     * @param [in] value The value to fill the pages with.
     * @param [in] recordsPerPage The number of records to return with each page.
     * @param [in] name The name of the provider. Can contain any string, this may be a column name, an expression, etc.
     * @return A new provider.
     */
    static eagle::SinglePageProvider* NewInt(EagleDataTypeInteger value, int recordsPerPage, std::string name);
    
    /**
     * This creates a page provider that provides a single page filled with a fixed floating point number.
     * @param [in] value The value to fill the pages with.
     * @param [in] recordsPerPage The number of records to return with each page.
     * @param [in] name The name of the provider. Can contain any string, this may be a column name, an expression, etc.
     * @return A new provider.
     */
    static eagle::SinglePageProvider* NewFloat(EagleDataTypeFloat value, int recordsPerPage, std::string name);
    
    /**
     * This creates a page provider that provides a single page filled with a fixed string.
     * @param [in] value The value to fill the pages with.
     * @param [in] recordsPerPage The number of records to return with each page.
     * @param [in] name The name of the provider. Can contain any string, this may be a column name, an expression, etc.
     * @return A new provider.
     */
    static eagle::SinglePageProvider* NewVarchar(const char *value, int recordsPerPage, std::string name);
    
    /**
     * Free a page provider.
     */
    virtual ~SinglePageProvider();
    
    /**
     * Return the amount of pages remaining.
     * @return The number of pages available for reading.
     */
    int pagesRemaining();
    
    /**
     * Get the next page from a provider.
     * @return The next page, or NULL.
     */
    virtual EaglePage* nextPage();
    
    /**
     * Check is a providers pages can be access in any rder with EaglePageProvider_getPage().
     * @return true if this provider can be accessed at a random page.
     */
    virtual bool isRandomAccess();
    
    /**
     * Get a random page from a provider. You may only use this if EaglePageProvider_isRandomAccess() returns true.
     * @param [in] pageNumber The number of the page where the first page will be 0.
     * @return The page, or NULL.
     */
    virtual EaglePage* getPage(int pageNumber);
    
};

#endif
