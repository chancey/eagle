#ifndef eagle_EagleData_h
#define eagle_EagleData_h

/**
 * The global data type for handling EagleDataTypeInteger.
 */
typedef int EagleDataTypeInteger;

/**
 * The global data type for handling EagleDataTypeVarchar.
 */
typedef char* EagleDataTypeVarchar;

/**
 * The global data type for handling EagleDataTypeFloat.
 */
typedef double EagleDataTypeFloat;

/**
 * Functions for handling data types in eagle.
 */
class EagleData
{
    
public:
        
    /**
     * Data types for pages.
     */
    enum Type
    {
    
        /**
         * Unknown/invalid data type.
         */
        Unknown = 0,
        
        /**
         * \c INT or \c INTEGER: 32bit integer.
         */
        Integer = 1,
        
        /**
         * \c VARCHAR: A NULL terminated string.
         */
        Varchar = 2,
        
        /**
         * \c FLOAT: 64bit floating point.
         */
        Float = 3
        
    };
    
    /**
     * Convert a type name to its enum value. This function is not case-sensitive.
     * @param [in] name The name of the type.
     * @return EagleData::Type::Unknown if the type is unknown. Otherwise the correct enum value for the type.
     * @see EagleDataType
     */
    static EagleData::Type NameToType(char *name);
    
    /**
     * Convert a type to a string.
     * @param [in] type The data type.
     * @return A new string (you must free it)
     * @see EagleDataType
     */
    static char* TypeToName(EagleData::Type type);
    
    /**
     * Allocate an integer and return the pointer.
     * @param [in] value Value to set into newly allocated space.
     * @return New allocation containing the integer.
     */
    static EagleDataTypeInteger* CreateInt(EagleDataTypeInteger value);
    
    /**
     * Allocate a float and return the pointer.
     * @param [in] value Value to set into newly allocated space.
     * @return New allocation containing the float.
     */
    static EagleDataTypeFloat* CreateFloat(EagleDataTypeFloat value);
    
};

#endif
