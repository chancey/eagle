#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "eagle/Random.h"

/**
 * Privately used.
 * @see eagle::Random::GetDefault()
 */
eagle::Random* eagle::Random::Default = NULL;

eagle::Random::Random()
{
#ifdef darwin
    setSeed(arc4random());
#else
    setSeed((unsigned int) rand());
#endif
}

eagle::Random::Random(unsigned int theSeed)
{
    setSeed(theSeed);
}

int eagle::Random::nextInt()
{
    unsigned int next;
    long spreadOut;
    
    /* get the next positive random number */
    next = nextPositiveInt();
    
    /* spread the new numbers out across negative range */
    spreadOut = ((long) next - (long) Max) * 2;
    
    /* correct the even-ness */
    if(next % 2 == 1) {
        ++spreadOut;
    }
    
    return (int) spreadOut;
}

unsigned int eagle::Random::nextPositiveInt()
{
    return seed = (seed * 1103515245 + 1) & Max;
}

eagle::Random* eagle::Random::GetDefault()
{
    if(NULL == Default) {
        Default = new eagle::Random();
    }
    
    return Default;
}

bool eagle::Random::setSeed(unsigned int theSeed)
{
    /* make sure they are not trying to change the seed for the default generator */
    if(Default == this) {
        return false;
    }
    
    seed = theSeed;
    
    /* skip the first one because it's always predictable */
    nextPositiveInt();
    
    return true;
}

int eagle::Random::nextIntInRange(int min, int max)
{
    int next, truncated;
    
    if(min == max) {
        /* the only number we can return is min or max */
        return min;
    }
    
    if(0 == (max - min + 1)) {
        /* this would cause us to divide by zero */
        return max;
    }
    
    next = nextInt();
    
    /* correct range */
    truncated = abs(next) % (max - min + 1);
    return min + truncated;
}

float eagle::Random::nextFloat()
{
    return (float) nextInt() / (float) Max;
}
