#ifndef eagle_Instance_h
#define eagle_Instance_h

#include <pthread.h>
#include "eagle/Workers.h"
#include "eagle/PageProvider.h"
#include "EagleLock.h"

namespace eagle
{
    class Instance;
    class Plan;
    
    namespace plan
    {
        namespace job
        {
            class AbstractJob;
        }
    }
}

/**
 * An eagle instance (a database).
 */
class eagle::Instance
{
    
protected:
    
    /**
     * The workers.
     */
    eagle::Workers *workers;
    
    /**
     * The plan that needs to be executed.
     */
    eagle::Plan *plan;
    
public:
    
    /**
     * Create a new eagle instance. The instance is effectivly a database, with a certain amount of workers (threads)
     * that will carry out the work. Do not create multiple instances for the same database as there is no way to
     * synchronise them.
     * @param [in] totalWorkers The number of workers to launch, this does not include the main thread that will handle
     * the connections and look after the workers.
     */
    Instance(int totalWorkers);
    
    /**
     * Free eagle::Instance
     */
    ~Instance();
    
    /**
     * Add an EaglePlan to an eagle::Instance.
     * @param [in] thePlan The plan to execute.
     */
    void addPlan(eagle::Plan *thePlan);
    
    /**
     * Run the instance. This will start it.
     */
    void run();
    
    /**
     * Get the next finite job from the stack. This is a syncronised method that all the workers will call independantly
     * when they have nothing to do. If this method returns NULL it is upto the workers to know that there is no more
     * work, they will check periodically after that.
     * @return Initialised eagle::plan::job::Abstract, or NULL if there are no available jobs.
     */
    eagle::plan::job::AbstractJob* nextJob();
    
};

#endif
