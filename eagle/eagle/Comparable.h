#ifndef eagle_Comparable_h
#define eagle_Comparable_h

#include <stdint.h>

namespace eagle
{
    class Comparable;
}

class eagle::Comparable
{
    
public:
    
    /**
     * Check if two objects are equal. If either of the objects are NULL or both of them are NULL then the result will
     * be false.
     * @param [in] obj2 Second object.
     * @return true if both parameters are not NULL and they have the same pointer address.
     */
    virtual bool equals(eagle::Comparable *obj2);
    
    /**
     * @brief This virtual function must be implemented by all classes that wish to implement eagle::Comparable.
     * 
     * This works much the same way as java where a object returns an integer that if compared to the same object type
     * with the same hash code means that it is the same object, even through they may be allocated as different
     * objects, the are deemed "equal".
     *
     * If you do not implement this then the actual pointer value will be used so twoobjects can only be equal if they
     * are precisely the same object.
     *
     * @return An integer value that represents the contents of this object, you can think of it like a checksum.
     */
    virtual intptr_t hashCode();
    
};

#endif
