#ifndef eagle_Object_h
#define eagle_Object_h

#include <string>

namespace eagle
{
    class Object;
}

class eagle::Object
{
    
public:
    
    /**
     * Get the string representation of the object.
     * @return std::string representation of this object.
     */
    virtual std::string toString() = 0;
    
};

#endif
