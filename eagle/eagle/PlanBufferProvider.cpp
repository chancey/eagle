#include <stdlib.h>
#include <stdio.h>
#include "eagle/PlanBufferProvider.h"
#include "eagledb/EagleDbSqlValue.h"

eagle::PlanBufferProvider::PlanBufferProvider(int theDestinationBuffer, eagle::PageProvider *theProvider,
                                              bool theFreeProvider)
{
    type = eagle::PlanBufferProvider::Type::ProviderType;
    destinationBuffer = theDestinationBuffer;
    value.provider.provider = theProvider;
    value.provider.freeProvider = theFreeProvider;
}

eagle::PlanBufferProvider::PlanBufferProvider(int theDestinationBuffer, EagleDbSqlValue *theValue)
{
    type = eagle::PlanBufferProvider::Type::ValueType;
    destinationBuffer = theDestinationBuffer;
    value.value = theValue;
}

char* eagle::PlanBufferProvider::toString()
{
    char *msg = new char[1024], *name = NULL;
    
    switch(type) {
            
        case eagle::PlanBufferProvider::Type::ProviderType:
        {
            name = EagleData::TypeToName(value.provider.provider->type);
            sprintf(msg, "destination = %d, name = %s, type = %s", destinationBuffer,
                    value.provider.provider->name.c_str(), name);
            delete name;
            break;
        }
            
        case eagle::PlanBufferProvider::Type::ValueType:
        {
            name = EagleDbSqlValueType_toString(value.value->type);
            sprintf(msg, "destination = %d, type = %s", destinationBuffer, name);
            delete name;
            break;
        }
            
    }
    
    return msg;
}

void PlanBufferProvider_LegacyDelete(eagle::PlanBufferProvider *bp)
{
    delete bp;
}

eagle::PlanBufferProvider::~PlanBufferProvider()
{
    switch(type) {
            
        case eagle::PlanBufferProvider::Type::ProviderType:
        {
            if(value.provider.freeProvider) {
                delete value.provider.provider;
            }
            break;
        }
            
        case eagle::PlanBufferProvider::Type::ValueType:
            break;
            
    }
}
