#ifndef eagle_Maths_h
#define eagle_Maths_h

namespace eagle
{
    class Maths;
}

/**
 * This class is called Maths as to not conflict with the std math.h
 */
class eagle::Maths
{
    
public:
    
    /**
     * Get the minimum value between a pair.
     * @param [in] a Value 1.
     * @param [in] b Value 2.
     * @return The minimum (smallest) value between the two.
     */
    template <typename T>
    static T Min(T a, T b)
    {
        //! @t eagle::MathsTest::Min
        return (a < b) ? a : b;
    }
    
protected:
    
    /**
     * This is an abstract class and has no reason to be instantiated.
     */
    Maths();
    
};

#endif
