#include <stdlib.h>
#include <string.h>
#include <string.h>
#include "EagleLogger.h"
#include "EagleLock.h"
#include "eagle/Worker.h"

const char *EagleLogger_DefaultLogLocation = "/tmp/eagledb.log";

EagleLogger::~EagleLogger()
{
    if(NULL != out && stderr != out && stdout != out) {
        fclose(out);
        out = NULL;
    }
    
    delete logLock;
    delete theLastEvent;
}

void EagleLogger::reset()
{
    delete theLastEvent;
    theLastEvent = NULL;
}

EagleLogger::EagleLogger(FILE *outHandle)
{
    totalMessages = 0;
    logLock = new EagleLock();
    out = outHandle;
    theLastEvent = NULL;
}

EagleLogger::Event* EagleLogger::LastEvent()
{
    return EagleLogger::Get()->lastEvent();
}

EagleLogger::Event* EagleLogger::lastEvent()
{
    return theLastEvent;
}

FILE* EagleLogger::GetLogFile(const char *location)
{
    FILE *logFile = fopen(location, "a");
    if(NULL == logFile) {
        /*fprintf(stderr, "Unable to open log file for writing '%s'.", location);*/
        logFile = stderr;
    }
    
    return logFile;
}

EagleLogger* EagleLogger::Get()
{
    static EagleLogger *EagleLogger_Logger = NULL;
    static EagleLock *lock = NULL;
    
    /* this function must be synchronized */
    if(NULL == lock) {
        lock = new EagleLock();
    }
    lock->lock();
    
    /* initialise if its not started */
    if(NULL == EagleLogger_Logger) {
        /* use the default log location */
        FILE *logFile = EagleLogger::GetLogFile(EagleLogger_DefaultLogLocation);
        
        EagleLogger_Logger = new EagleLogger(logFile);
    }
    
    lock->unlock();
    return EagleLogger_Logger;
}

EagleLogger::Event* EagleLogger::log(EagleLogger::Severity severity, std::string message)
{
    EagleLogger::Event *event = new EagleLogger::Event(severity, message);
    logEvent(event);
    return event;
}

EagleLogger::Event* EagleLogger::Log(EagleLogger::Severity severity, std::string message)
{
    return EagleLogger::Get()->log(severity, message);
}

void EagleLogger::logEvent(EagleLogger::Event *event)
{
    char *timestamp;
    
    if(NULL == event) {
        return;
    }
    
    /* this function must be synchronized */
    logLock->lock();
    
    /* pretty time */
    time_t when;
    time(&when);
    timestamp = ctime(&when);
    event->setWhen(when);
    timestamp[strlen(timestamp) - 1] = '\0';
    
    if(NULL != out) {
        fprintf(out, "%s: [%s] %s\n", timestamp, EagleLogger::SeverityToString(event->getSeverity()), event->getMessage().c_str());
        fflush(out);
    }
    ++totalMessages;
    
    /* track most recent event */
    if(NULL != theLastEvent) {
        delete theLastEvent;
    }
    theLastEvent = event;
    
    logLock->unlock();
}

void EagleLogger::LogEvent(EagleLogger::Event *event)
{
    EagleLogger::Get()->logEvent(event);
}

const char* EagleLogger::SeverityToString(EagleLogger::Severity severity)
{
    const char *r = NULL;
    
    switch(severity) {
            
        case EagleLogger::Severity::Debug:
            r = "DEBUG";
            break;
            
        case EagleLogger::Severity::Info:
            r = "INFO";
            break;
            
        case EagleLogger::Severity::UserError:
            r = "USERERROR";
            break;
            
        case EagleLogger::Severity::Error:
            r = "ERROR";
            break;
            
        case EagleLogger::Severity::Fatal:
            r = "FATAL";
            break;
            
    }
    
    return r;
}

EagleLogger::Event::Event(EagleLogger::Severity theSeverity, std::string theMessage)
{
    //! @t eagle::EagleLoggerTest::Event
    when = time(NULL);
    severity = theSeverity;
    message = theMessage;
    obj = NULL;
}

void* EagleLogger::Event::getObject()
{
    return obj;
}

time_t EagleLogger::Event::getWhen()
{
    return when;
}

string EagleLogger::Event::getMessage()
{
    return message;
}

void EagleLogger::Event::setWhen(time_t theWhen)
{
    when = theWhen;
}

EagleLogger::Severity EagleLogger::Event::getSeverity()
{
    return severity;
}

FILE* EagleLogger::getOutHandle()
{
    return out;
}

void EagleLogger::disableOutHandle()
{
    out = NULL;
}

int EagleLogger::getTotalMessages()
{
    return totalMessages;
}
