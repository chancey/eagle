#ifndef eagle_EagleLock_h
#define eagle_EagleLock_h

#include <pthread.h>

/**
 * A lock is used to synchronize code blocks between threads.
 */
class EagleLock
{
    
protected:
    
    /**
     Internal mutex that holds the lock.
     */
    pthread_mutex_t mutex;
    
    /**
     Currently not in use, but it contains attributes for the internal mutex.
     */
    pthread_mutexattr_t attr;
    
    /**
     The number of times a lock has been requested.
     */
    unsigned int lockTimes;
    
public:
    
    /**
     * Free a lock.
     */
    ~EagleLock();
    
    /**
     * Unlock an EagleLock.
     * Only one thread may hold a lock at any given time so the next thread waiting for this lock will be able to continue.
     * @see lock()
     */
    void unlock();
    
    /**
     * Create a new EagleLock.
     * Locks can be used to synchronize block of code over multiple threads.
     * @return A new lock instance.
     */
    EagleLock();
    
    /**
     * Lock an EagleLock.
     * Only one thread may hold a lock at any given time so other threads trying to get access to this lock will wait until it
     * is unlocked.
     * This is a macro for time profiling.
     * @see unlock()
     */
    void lock();
    
};

#endif
