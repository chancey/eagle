#ifndef eagle_StreamPageProvider_h
#define eagle_StreamPageProvider_h

#include "EagleData.h"
#include "EaglePage.h"
#include "eagle/PageProvider.h"

namespace eagle
{
    class StreamPageProvider;
}

/**
 * A streaming page provider.
 */
class eagle::StreamPageProvider : public eagle::PageProvider
{
    
    // @todo make protected
public:
 
    /**
     This is used by streams. It allows it to keep the page it is upto when requesting the next page.
     */
    EagleLinkedList<EaglePage*>::Item *cursor;
    
    /**
     A list containing EaglePage's with the records.
     */
    EagleLinkedList<EaglePage*> *list;
    
    /**
     The cursor position. An internal counter of the position of the stream. If the stream has more pages this will be
     incremented with each nextPage().
     */
    int offsetRecords;
    
    /**
     The total amount of records. You should never access this directly since its value may be virtual or invalid -
     instead use EaglePageProvider_pagesRemaining()
     */
    int totalRecords;
    
    /**
     Synchronize EaglePageProvider_nextPage() and EaglePageProvider_pagesRemaining()
     */
    EagleLock *nextPageLock;
    
public:
    
    virtual void reset();
    
    /**
     * Create a new writable page provider with zero records.
     * @param [in] type The data type for the provider (and its pages).
     * @param [in] recordsPerPage The amount of records per page.
     * @param [in] name The name of the provider.
     * @return A new read/write provider.
     */
    StreamPageProvider(EagleData::Type type, int recordsPerPage, std::string name);
    
    virtual ~StreamPageProvider();
    
    virtual bool add(void *data);
    
    virtual int pagesRemaining();
    
    virtual EaglePage* nextPage();
    
    virtual EaglePage* getPage(int pageNumber);
    
    virtual bool isRandomAccess();
    
    virtual void* getDataByRecordId(int recordId);
    
};

#endif
