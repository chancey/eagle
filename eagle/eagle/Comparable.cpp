#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "eagle/Comparable.h"

bool eagle::Comparable::equals(eagle::Comparable *obj2)
{
    /* check for NULLs */
    if(NULL == obj2) {
        return false;
    }
        
    /* compare with hash code */
    return hashCode() == obj2->hashCode();
}

intptr_t eagle::Comparable::hashCode()
{
    return (intptr_t) this;
}
