#ifndef eagle_EaglePage_h
#define eagle_EaglePage_h

#include "EagleData.h"
#include "eagle/Comparable.h"

/**
 * Represents a single page of data.
 */
class EaglePage : public eagle::Comparable
{
    
    // @todo make protected
public:
    
    /**
     * The data type for this page.
     */
    EagleData::Type type;
    
    /**
     * An arbitrary counter for the row ID offset. The first record in this page will have \c recordOffset as the record
     * ID, the next record will be recordOffset + 1, etc
     */
    int recordOffset;
    
    /**
     * The allocated records for this page.
     */
    int totalSize;
    
    /**
     * The amount of used records in this page.
     */
    int count;
    
    /**
     * The actual data for this page.
     */
    void *data;
    
    /**
     * Free \c data when freeing the page.
     */
    bool freeData;
    
    /**
     * This value starts at 1 and is incremented with EaglePage::copy() and decremented with delete. The page is only
     * really freed when this counter hits zero.
     */
    int usageCount;
    
protected:
    
    /**
     * Initialise an EaglePage.
     * You will be required to provide a data block with the number of records in that data block.
     * @param [in] theType The data for this page.
     * @param [in] theData The preallocated data block. This may be NULL, but the \p count must be 0.
     * @param [in] theTotalSize The number of records in the data block.
     * @param [in] theCount The number of used records (must be less than \p totalSize)
     * @param [in] theRecordOffset An arbitrary number stored in the page. This number is used as an offset when
     * calculating the record ID of a given record inside a page. If you are unsure as to what this value should be then
     * you can use 0.
     * @param [in] theFreeData If true the \p data will be freed with the page.
     * @param [in] theUsageCount This page is in use several times.
     */
    void init(EagleData::Type theType, void *theData, int theTotalSize, int theCount, int theRecordOffset,
              bool theFreeData, int theUsageCount);
    
    /**
     * Private function. Copy an INTEGER page.
     * @param [in] page The page.
     * @return A duplicate page.
     * @see EaglePage::Copy()
     */
    static EaglePage* RealCopyInt(EaglePage *page);
    
    /**
     * Private function. Copy an FLOAT page.
     * @param [in] page The page.
     * @return A duplicate page.
     * @see EaglePage_Copy()
     */
    static EaglePage* RealCopyFloat(EaglePage *page);
    
    /**
     * Private function. Copy a VARCHAR page.
     * @param [in] page The page.
     * @return A duplicate page.
     * @see EaglePage_Copy()
     */
    static EaglePage* RealCopyVarchar(EaglePage *page);
    
public:
    
    /**
     * Create a new EaglePage.
     * You will be required to provide a data block with the number of records in that data block.
     * @param [in] theType The data for this page.
     * @param [in] theData The preallocated data block. This may be NULL, but the \p count must be 0.
     * @param [in] theTotalSize The number of records in the data block.
     * @param [in] theCount The number of used records (must be less than \p totalSize)
     * @param [in] theRecordOffset An arbitrary number stored in the page. This number is used as an offset when
     * calculating the record ID of a given record inside a page. If you are unsure as to what this value should be then
     * you can use 0.
     * @param [in] theFreeData If true the \p data will be freed with the page.
     */
    EaglePage(EagleData::Type theType, void *theData, int theTotalSize, int theCount, int theRecordOffset,
              bool theFreeData);
    
    /**
     * Allocate a new page.
     * @param [in] type The data type.
     * @param [in] count The size of the page.
     * @return The new page.
     */
    static EaglePage* Alloc(EagleData::Type type, int count);
    
    /**
     * Allocate a new page of given size.
     * This function differs from new EaglePage() because it will allocate the data block for you.
     * @note The page allocated will not be filled with any data, make sure you sanitize the page if you intend to read
     * from it.
     * @param [in] count The number of records of the page.
     * @return The new page.
     */
    static EaglePage* AllocInt(int count);
    
    /**
     * Allocate a new page of given size.
     * This function differs from new EaglePage() because it will allocate the data block for you.
     * @note The page allocated will not be filled with any data, make sure you sanitize the page if you intend to read
     * from it.
     * @param [in] count The number of records of the page.
     * @return The new page.
     */
    static EaglePage* AllocFloat(int count);
    
    /**
     * Allocate a new page of given size.
     * This function differs from new EaglePage() because it will allocate the data block for you.
     * @note The page allocated will not be filled with any data, make sure you sanitize the page if you intend to read
     * from it.
     * @param [in] count The number of records of the page.
     * @return The new page.
     */
    static EaglePage* AllocVarchar(int count);
    
    /**
     * Cheap copy a page.
     * @return This object.
     */
    EaglePage* copy();
    
    /**
     * Copy a page. This will do a true deep copy which is different from EaglePage_Copy() which simply registered a new
     * reference to a real only page.
     * @param [in] page The page to copy.
     * @return A new copy of the page, or NULL on error.
     */
    static EaglePage* RealCopy(EaglePage *page);
    
    /**
     * Override the delete operator to handle reference counting.
     * @param [in] page The page.
     */
    void operator delete(void *page);
    
    /**
     * Render information about the page as a string.
     * @return A new string (you must free this yourself)
     */
    char* toString();
    
    /**
     * Compare the content two pages.
     * @param [in] page2 Second page (EaglePage).
     * @return true if the contents of the pages are equal.
     */
    virtual bool equals(eagle::Comparable *page2);
    
    /**
     * This does nothing but it needs to be virtual to satisfy the parent class.
     */
    virtual ~EaglePage();
    
};

#endif
