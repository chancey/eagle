#ifndef eagle_EagleLogger_h
#define eagle_EagleLogger_h

#include <stdio.h>
#include "EagleLock.h"
#include "EagleLinkedList.h"
#include "EagleLogger.h"

using namespace std;

/**
 The location to save the logs.
 */
extern const char *EagleLogger_DefaultLogLocation;

/**
 * A logger instance.
 */
class EagleLogger
{
    
public:
    
    /**
     * Every logged event has a severity. This is a value between 0 (the least severe) to 100 (the most severe).
     */
    enum Severity
    {
        
        /**
         * A fatal error means that the program can not continue. This is the most serious level.
         */
        Fatal = 100,
        
        /**
         * An error is a problem that has stopped a task from continuing but the state of the program is recoverable and
         * it will continue.
         */
        Error = 50,
        
        /**
         * This is an error because it stopped the execution of a task, but the error is not within eagle. A bad input
         * has been provided. A good example would be a mistake in the SQL syntax of a query.
         */
        UserError = 25,
        
        /**
         * This describes events in the system that have no effect on the flow of execution, such as start up and
         * shutdown messages.
         */
        Info = 10,
        
        /**
         * Debug messages can be very verbose and explain this that are happening that may have nothing to do with the
         * actual execution.
         */
        Debug = 1
        
    };
    
    /**
     * A single logged event.
     */
    class Event
    {
        
    protected:
        
        /**
         * When the event was raised.
         */
        time_t when;
        
        /**
         * How severe the event was.
         */
        EagleLogger::Severity severity;
        
        /**
         * A message describing the event.
         */
        string message;
        
        /**
         * An optional attached object.
         */
        void *obj;
        
    public:
        
        /**
         * Create a new logger event.
         * @param [in] theSeverity How severe the event is.
         * @param [in] theMessage The message.
         * @return A new logger event instance.
         */
        Event(EagleLogger::Severity theSeverity, std::string theMessage);
        
        /**
         * Get the message for this event.
         * @return The message.
         */
        string getMessage();
        
        /**
         * Set the timestamp of the event.
         * @param [in] theWhen The timestamp for the event.
         */
        void setWhen(time_t theWhen);
        
        /**
         * Get the severity for this event.
         * @return The severity.
         */
        EagleLogger::Severity getSeverity();
        
        /**
         * Get the timestamp of the event.
         * @return The timestamp for the event.
         */
        time_t getWhen();
        
        /**
         * Get the optional object for this event.
         * @return The optional object.
         */
        void* getObject();
        
    };
    
protected:
    
    /**
     * The total messages that have been reported since this logged was initialised.
     */
    int totalMessages;
    
    /**
     * This lock synchronizes the addition of messages reported by multiple threads.
     */
    EagleLock *logLock;
    
    /**
     * A file handle to output the messages to. This may be an actual file or stdout, stderr etc.
     */
    FILE *out;
    
    /**
     * The most recent event.
     */
    EagleLogger::Event *theLastEvent;
    
public:
    
    /**
     * Get the default logger. If the default logged has not been initialised it will be initialised automatically and
     * returned.
     * @return The default logger.
     */
    static EagleLogger* Get();
    
    /**
     * Log an event.
     * @param [in] severity The severity.
     * @param [in] message The message.
     * @return The event instance that was created.
     */
    static EagleLogger::Event* Log(EagleLogger::Severity severity, std::string message);
    
    /**
     * Log an event with the default logger.
     * @param [in] event The event.
     */
    static void LogEvent(EagleLogger::Event *event);
    
    /**
     * Delete a logger.
     * @note Do not call this function unless you have a good reason. Once the logger is created it will be reused
     * automatically.
     */
    ~EagleLogger();
    
    /**
     * Create a new logger. This is really only used for tests, you should use EagleLogger::Get() to get the default
     * logger.
     * @param [in] outHandle where the logged items will flush to.
     * @return A new logger.
     */
    EagleLogger(FILE *outHandle);
    
    /**
     * Get the file handle for a log location. If the log file cannot be open then stderr is returned.
     * @param [in] location The file location.
     * @return Always a valid FILE handle, even in case of failure.
     */
    static FILE* GetLogFile(const char *location);
    
    /**
     * Get the last event from the default logger.
     * @return The last event or NULL if there have been no events.
     */
    static EagleLogger::Event* LastEvent();
    
    /**
     * Log an event.
     * @param [in] severity The severity.
     * @param [in] message The message.
     * @return The event instance that was created.
     */
    EagleLogger::Event* log(EagleLogger::Severity severity, std::string message);
    
    /**
     * Log an event.
     * @param [in] event The event.
     */
    void logEvent(EagleLogger::Event *event);
    
    /**
     * Get the last event from a logger.
     * @return The last event or NULL if there have been no events.
     */
    EagleLogger::Event* lastEvent();
    
    /**
     * Reset a logger. Free the memory associated with the last error event.
     */
    void reset();
    
    /**
     * Translate an EagleLogger::Severity value into its string representation.
     * @param [in] severity The severity.
     * @return String representation of a severity.
     */
    static const char* SeverityToString(EagleLogger::Severity severity);

    /**
     * Get the out handle.
     * @return The file output handle.
     */
    FILE* getOutHandle();

    /**
     * Disable the output handle.
     */
    void disableOutHandle();

    /**
     * Get the total number of messages.
     * @return The number of messages.
     */
    int getTotalMessages();

};

#endif
