#include <string.h>
#include <algorithm>
#include "eagle/algorithm.h"

char* eagle::algorithm::toLowerCase(const char *str)
{
    size_t len = strlen(str);
    char *out = new char[len + 1];
    out[len] = 0;
    std::transform(str, str + len, out, ::tolower);
    return out;
}

char* eagle::algorithm::toUpperCase(const char *str)
{
    size_t len = strlen(str);
    char *out = new char[len + 1];
    out[len] = 0;
    std::transform(str, str + len, out, ::toupper);
    return out;
}

bool eagle::algorithm::equalsIgnoreCase(const char *a, const char *b)
{
    size_t aLen = strlen(a), bLen = strlen(b);
    if(aLen != bLen) {
        return false;
    }
    
    for(size_t i = 0; i < aLen; ++i) {
        if(tolower(a[i]) != tolower(b[i])) {
            return false;
        }
    }
    
    return true;
}
