#ifndef eagle_Worker_h
#define eagle_Worker_h

#include <pthread.h>

namespace eagle
{
    class Worker;
    class Instance;
}

/**
 * A worker is a thread.
 */
class eagle::Worker
{
    
    // @todo make protected
public:
    
    /**
     A unique ID for the worker thread.
     */
    int workerId;
    
    /**
     The actual thread this worker represents.
     */
    pthread_t thread;
    
    /**
     The instance this worker belongs to.
     */
    eagle::Instance *instance;
    
    /**
     Total lock time.
     */
    unsigned long lockTime;
    
public:
    
    /**
     * Create a new worker (thread)
     * @param [in] theWorkerId The worker ID is just an arbitrary number that should be unique to the worker for logging
     * purposes.
     * @param [in] theInstance The instance the workers belong to.
     */
    Worker(int theWorkerId, eagle::Instance *theInstance);
    
    /**
     * Start a worker.
     */
    void start();
    
    /**
     * Wait for a worker to complete and shutdown.
     */
    void join();
    
    /**
     * Get the worker instance for this thread.
     * @return NULL on threads that are not workers.
     */
    static eagle::Worker* GetForCurrentThread();
    
    /**
     * Set the worker instance for this thread.
     * @param [in] worker The worker instance.
     */
    static void SetForCurrentThread(eagle::Worker *worker);
    
protected:
    
    /**
     * Begin a worker.
     * @param [in] obj This is a EagleWorker* but it must have void* to fit the definition for the pthread.
     * @return Ignored.
     */
    static void* Begin(void *obj);
    
};

#endif
