#ifndef eagle_PlanBufferProvider_h
#define eagle_PlanBufferProvider_h

#include "eagle/PageProvider.h"
#include "eagledb/EagleDbSqlValue.h"

namespace eagle
{
    class PlanBufferProvider;
}

/**
 * This structure acts as a link between a page providers data and the buffer for an expression it will be loaded into.
 */
class eagle::PlanBufferProvider
{
    
public:
    
    class ValueProvider
    {
        
    public:
        
        /**
         * The provider that contains the actual data for the buffer. This is semi managed because it depends on
         * \c freeProvider
         */
        eagle::PageProvider *provider;
        
        /**
         * Free the provider when the plan buffer provider is released.
         */
        bool freeProvider;
        
    };
    
    /**
     * A buffer provider can be a container for a traditional generic page provider or for a single value.
     */
    enum Type
    {
        
        /**
         * This buffer provider wraps a generic page provider.
         */
        ProviderType = 1,
        
        /**
         * This buffer provider wraps a EagleDbSqlValue
         */
        ValueType = 2
        
    };
    
    union Value
    {
        
        /**
         * Value for a provider.
         */
        PlanBufferProvider::ValueProvider provider;
        
        /**
         Fixed single value.
         */
        EagleDbSqlValue *value;
        
    };
    
    // @todo make protected
public:
    
    /**
     * The buffer ID where the next page will be loaded into.
     */
    int destinationBuffer;
    
    /**
     * The provider type.
     */
    PlanBufferProvider::Type type;
    
    /**
     * The value.
     */
    PlanBufferProvider::Value value;
    
public:
    
    /**
     * Create a new plan buffer provider with a generic provider.
     * @param [in] theDestinationBuffer The destination buffer ID.
     * @param [in] theProvider The provider.
     * @param [in] theFreeProvider Free the \p provider when this instance is deleted?
     * @return A new plan buffer provider.
     */
    PlanBufferProvider(int theDestinationBuffer, eagle::PageProvider *theProvider, bool theFreeProvider);
    
    /**
     * Create a new plan buffer provider with a EagleDbSqlValue.
     * @param [in] theDestinationBuffer The destination buffer ID.
     * @param [in] theValue The value.
     * @return A new plan buffer provider.
     */
    PlanBufferProvider(int theDestinationBuffer, EagleDbSqlValue *theValue);
    
    /**
     * Delete a plan buffer provider.
     */
    ~PlanBufferProvider();
    
    /**
     * Get the description for a plan buffer provider. This will return a new string that you will have to free when
     * you're finished with it.
     * @return A new string description.
     */
    char* toString();
    
};

/**
 * Legacy function pointer to be removed by EDB-105.
 * @param [in] bp The buffer provider.
 */
void PlanBufferProvider_LegacyDelete(eagle::PlanBufferProvider *bp);

#endif
