#ifndef eagle_page_Sort_h
#define eagle_page_Sort_h

#include "eagle/EaglePage.h"

class EagleLock;

namespace eagle
{
    namespace page
    {
        class Sort;
    }
}

class eagle::page::Sort : public EaglePage
{
    
    // @todo make protected
public:
    
    /**
     * Each used record in the page will be given an associated record id. This is so when the sort reorders the records
     * they can be matched back to the original row.
     */
    int *recordIds;
    
    /**
     * A simple switch to know if a worker it currently operating on this page.
     */
    volatile bool working;
    
public:
    
    Sort(EagleData::Type theType, void *theData, int theTotalSize, int theCount, int theRecordOffset,
         bool theFreeData, int *recordIds);
    
    /**
     * This does nothing but it needs to be virtual to satisfy the parent class.
     */
    virtual ~Sort();
    
    static eagle::page::Sort* CreateFromPage(EaglePage *page);
    
};

#endif
