#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "eagle/page/Sort.h"
#include "eagle/EagleLock.h"

eagle::page::Sort::~Sort()
{
    delete[] recordIds;
}

eagle::page::Sort::Sort(EagleData::Type theType, void *theData, int theTotalSize, int theCount, int theRecordOffset,
                        bool theFreeData, int *theRecordIds) : EaglePage(theType, theData, theTotalSize, theCount,
                                                                         theRecordOffset, theFreeData)
{
    if(NULL == theRecordIds && totalSize > 0) {
        // calculate record IDs
        theRecordIds = new int[theTotalSize];
        for(int i = 0; i < theTotalSize; ++i) {
            theRecordIds[i] = theRecordOffset + i;
        }
    }
    recordIds = theRecordIds;
    working = false;
}

eagle::page::Sort* eagle::page::Sort::CreateFromPage(EaglePage *page)
{
    if(NULL == page) {
        return NULL;
    }
    
    // real copy
    EaglePage *copy = EaglePage::RealCopy(page);
    
    // convert
    eagle::page::Sort *sortPage = new eagle::page::Sort(copy->type, copy->data, copy->totalSize, copy->count,
                                                        copy->recordOffset, true, NULL);
    
    // sort pages have to use absolute record IDs
    for(int i = 0; i < sortPage->totalSize; ++i) {
        sortPage->recordIds[i] = page->recordOffset + i;
    }
    
    copy->freeData = false;
    delete copy;
    return sortPage;
}
