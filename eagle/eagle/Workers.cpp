#include <stdlib.h>
#include "eagle/Workers.h"
#include "eagle/Worker.h"

eagle::Workers::Workers(int theTotalWorkers, eagle::Instance *instance)
{
    workers = new eagle::Worker*[theTotalWorkers];
    for(int i = 0; i < theTotalWorkers; ++i) {
        workers[i] = new eagle::Worker(i, instance);
    }
    totalWorkers = theTotalWorkers;
}

void eagle::Workers::start()
{
    for(int i = 0; i < totalWorkers; ++i) {
        workers[i]->start();
    }
}

void eagle::Workers::joinAll()
{
    for(int i = 0; i < totalWorkers; ++i) {
        workers[i]->join();
    }
}

eagle::Workers::~Workers()
{
    for(int i = 0; i < totalWorkers; ++i) {
        delete workers[i];
    }
    delete workers;
}
