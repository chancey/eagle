#include <stdlib.h>
#include <stdio.h>
#include "eagle/Worker.h"
#include "eagle/Instance.h"
#include "EagleLogger.h"
#include "eagle/plan/Operation.h"
#include "eagle/plan/job/AbstractJob.h"
#include "eagle/plan/stage/AbstractStage.h"
#include "eagle/Calendar.h"

/**
 * Used by EagleWorker::GetForCurrentThread() and EagleWorker_SetForCurrentThread()
 */
__thread eagle::Worker *eagle_Worker_ThisWorker = NULL;

eagle::Worker::Worker(int theWorkerId, eagle::Instance *theInstance)
{
    workerId = theWorkerId;
    instance = theInstance;
    lockTime = 0;
}

void* eagle::Worker::Begin(void *obj)
{
    eagle::Worker *worker = (eagle::Worker*) obj;
    eagle::Worker::SetForCurrentThread(worker);
    
    while(true) {
        eagle::plan::job::AbstractJob *job = NULL;
        
        /* start the timers at zero */
        unsigned long start = eagle::Calendar::getInstance()->getAbsoluteTime();
        worker->lockTime = 0;
        
        /* ask the instance for the next job */
        job = worker->instance->nextJob();
        
        /* run the job is one is returned */
        if(NULL != job) {
            /* run operations */
            job->runJob();
            
            /* add time */
            job->stage->executionTime[worker->workerId] += eagle::Calendar::getInstance()->getAbsoluteTime() - start;
            job->stage->lockTime[worker->workerId] += worker->lockTime;
            
            /* free */
            delete job;
        }
        else {
            return NULL;
        }
    }
    
    return NULL;
}

void eagle::Worker::start()
{
    pthread_create(&thread, NULL, eagle::Worker::Begin, this);
}

void eagle::Worker::join()
{
    pthread_join(thread, NULL);
}

eagle::Worker* eagle::Worker::GetForCurrentThread()
{
    return eagle_Worker_ThisWorker;
}

void eagle::Worker::SetForCurrentThread(eagle::Worker *worker)
{
    //! @t eagle::WorkerTest::SetForCurrentThread
    eagle_Worker_ThisWorker = worker;
}
