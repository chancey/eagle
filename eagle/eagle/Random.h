#ifndef eagle_eagleRandom_h
#define eagle_eagleRandom_h

namespace eagle
{
    class Random;
}

/**
 * http://en.wikipedia.org/wiki/Linear_congruential_generator
 */
class eagle::Random
{
    
    // @todo make this protected
public:
    
    static Random *Default;
    
    /**
     * The seed value. You should treat this as read only. Changing it after the generator is created may or may not
     * have an effect on the future sequence of generated numbers.
     */
    unsigned int seed;
    
public:
    
    /**
     * The maximum positive value that can be generated from eagle::Random::GetDefault()->nextPositiveInt().
     */
    const static unsigned int Max = ((1U << 31) - 1);
    
    /**
     * Create a new random generator with a random seed.
     */
    Random();
    
    /**
     * Create a new random generator with a given seed. If multiple generators are created with the same seed they will
     * generate the same sequence of values at any time and independantly.
     * @param [in] theSeed The seed value.
     */
    Random(unsigned int theSeed);
    
    /**
     * Get the default global generator.
     * @return The default generator.
     */
    static Random* GetDefault();
    
    /**
     * Get the next positive integer.
     * @return The next random number between 0 and eagle::Random_Max
     */
    unsigned int nextPositiveInt();
    
    /**
     * Get the next integer.
     * @return The next random number between -Max and Max
     */
    int nextInt();
    
    /**
     * Get the next float between 0 and 1.
     * @return Next random float.
     */
    float nextFloat();
    
    /**
     * Get the next integer within a set range.
     * @param [in] min Minimum value (inclusive).
     * @param [in] max Maximum value (inclusive).
     * @return The next random number between \p min and \p max
     */
    int nextIntInRange(int min, int max);
    
    /**
     * Set the seed for a random number generator.
     * You can not set the seed for the default generator, if you try nothing will happen.
     * @param [in] theSeed The new seed.
     * @return true on success.
     */
    bool setSeed(unsigned int theSeed);
    
};

#endif
