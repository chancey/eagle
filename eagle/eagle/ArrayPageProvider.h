#ifndef eagle_ArrayPageProvider_h
#define eagle_ArrayPageProvider_h

#include "eagle/PageProvider.h"

namespace eagle
{
    class ArrayPageProvider;
}

class eagle::ArrayPageProvider : public eagle::PageProvider
{
    
    // @todo make protected
public:

    /**
     The cursor position. An internal counter of the position of the stream. If the stream has more pages this will be
     incremented with each nextPage().
     */
    int offsetRecords;
    
    /**
     The total amount of records. You should never access this directly since its value may be virtual or invalid -
     instead use EaglePageProvider_pagesRemaining()
     */
    int totalRecords;
    
    /**
     A pointer to the actual data that will be fed into pages. You should not rely on accessing this directly because
     its type and implementation will be different depending on the type of page provider. Use the appropriate methods
     nextPage() and pagesRemaining().
     
     The memory for this instance variable is only managed when the page provider is a stream type.
     */
    void *records;
    
    /**
     Synchronize EaglePageProvider_nextPage() and EaglePageProvider_pagesRemaining()
     */
    EagleLock *nextPageLock;
    
    /**
     * Free the records with this object.
     */
    bool freeRecords;
    
public:
    
    virtual void reset();
    
    /**
     * Create a new read only page provider from a fixed size array.
     * @param [in] type The data type.
     * @param [in] records The data records.
     * @param [in] totalRecords The total records.
     * @param [in] recordsPerPage The amount of records to serve out per page.
     * @param [in] name The name of the provider.
     * @param [in] freeRecords Free the records with this object.
     * @return A new provider.
     */
    ArrayPageProvider(EagleData::Type type, void *records, int totalRecords, int recordsPerPage, std::string name,
                      bool freeRecords);
    
    virtual ~ArrayPageProvider();
    
    virtual int pagesRemaining();
    
    virtual EaglePage* nextPage();
    
    virtual bool isRandomAccess();
    
    virtual EaglePage* getPage(int pageNumber);
    
    virtual void* getDataByRecordId(int recordId);
    
};

#endif
