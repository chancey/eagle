#ifndef eagle_EagleReadWriteLock_h
#define eagle_EagleReadWriteLock_h

#include <pthread.h>
#include <stdexcept>

/**
 * Use for exclusive write, concurrent read locking.
 */
class EagleReadWriteLock
{
    
protected:
    
    /**
     * Internal mutex that holds the lock.
     */
    pthread_rwlock_t lock;
    
protected:
    
    /**
     * For internal use to wrap native pthread_rwlock_rdlock().
     * @return 0 on success. Non-zero on failure.
     */
    virtual int aquireReadLock();
    
    /**
     * For internal use to wrap native pthread_rwlock_wrlock().
     * @return 0 on success. Non-zero on failure.
     */
    virtual int aquireWriteLock();
    
    /**
     * For internal use to wrap native pthread_rwlock_unlock().
     * @return 0 on success. Non-zero on failure.
     */
    virtual int releaseLock();
    
public:
    
    /**
     * Create a new read-write lock.
     * @return A new lock or NULL on error.
     */
    EagleReadWriteLock();
    
    /**
     * Require a read lock.
     * @throw std::runtime_error
     */
    void holdReadLock();
    
    /**
     * Require an exclusive write lock.
     * @throw std::runtime_error
     */
    void holdWriteLock();
    
    /**
     * Release the current lock for this thread.
     * @throw std::runtime_error
     */
    void unlock();
    
    /**
     * Free a lock.
     */
    virtual ~EagleReadWriteLock();
    
};

#endif
