#include <string.h>
#include "eagle/StreamPageProvider.h"
#include "EagleLogger.h"
#include "eagle/Worker.h"
#include "EagleSynchronizedLinkedList.h"

eagle::StreamPageProvider::StreamPageProvider(EagleData::Type theType, int theRecordsPerPage, std::string theName)
{
    type = theType;
    recordsPerPage = theRecordsPerPage;
    offsetRecords = 0;
    totalRecords = 0;
    nextPageLock = new EagleLock();
    name = theName;
    cursor = NULL;
    list = new EagleSynchronizedLinkedList<EaglePage*>();
}

void* eagle::StreamPageProvider::getDataByRecordId(int recordId)
{
    void *r = NULL;
    EaglePage *p = list->get(recordId / recordsPerPage);
    
    switch(type) {
            
        case EagleData::Type::Integer:
        {
            EagleDataTypeInteger *d = (EagleDataTypeInteger*) p->data;
            r = &d[recordId % recordsPerPage];
            break;
        }
            
        case EagleData::Type::Float:
        {
            EagleDataTypeFloat *d = (EagleDataTypeFloat*) p->data;
            r = &d[recordId % recordsPerPage];
            break;
        }
            
        case EagleData::Type::Varchar:
        {
            EagleDataTypeVarchar *d = (EagleDataTypeVarchar*) p->data;
            r = &d[recordId % recordsPerPage];
            break;
        }
            
        case EagleData::Type::Unknown:
            break;
            
    }
    
    return r;
}

eagle::StreamPageProvider::~StreamPageProvider()
{
    delete nextPageLock;
    delete list;
}

bool eagle::StreamPageProvider::add(void *data)
{
    EagleLinkedList<EaglePage*>::Item *head;
    EaglePage *page = NULL;
    
    nextPageLock->lock();
    
    head = list->end();
    if(NULL == head || head->getObject()->count >= recordsPerPage) {
        /* the list is empty we need to create a new page */
        EagleLinkedList<EaglePage*>::Item *item;
        page = EaglePage::Alloc(type, recordsPerPage);
        page->count = 0;
        item = new EagleLinkedList<EaglePage*>::Item(page, true);
        list->add(item);
    }
    else {
        page = head->getObject();
    }
    
    switch(type) {
            
        case EagleData::Type::Unknown:
            EagleLogger::Log(EagleLogger::Severity::Error, "Unknown type.");
            nextPageLock->unlock();
            return false;
            
        case EagleData::Type::Integer:
        {
            EagleDataTypeInteger d = *((EagleDataTypeInteger*) data);
            ((EagleDataTypeInteger*) page->data)[page->count++] = d;
            break;
        }
            
        case EagleData::Type::Varchar:
        {
            EagleDataTypeVarchar d = (EagleDataTypeVarchar) data;
            ((EagleDataTypeVarchar*) page->data)[page->count++] = d;
            break;
        }
            
        case EagleData::Type::Float:
        {
            EagleDataTypeFloat d = *((EagleDataTypeFloat*) data);
            ((EagleDataTypeFloat*) page->data)[page->count++] = d;
            break;
        }
            
    }
    
    ++totalRecords;
    nextPageLock->unlock();
    
    return true;
}

int eagle::StreamPageProvider::pagesRemaining()
{
    int r;
    
    nextPageLock->lock();
    r = eagle::PageProvider::TotalPages(totalRecords - offsetRecords, recordsPerPage);
    nextPageLock->unlock();
    
    return r;
}

EaglePage* eagle::StreamPageProvider::nextPage()
{
    EaglePage *page;
    
    nextPageLock->lock();
    
    if(NULL == cursor) {
        cursor = list->begin();
    }
    
    /* there is no more pages to give */
    if(offsetRecords >= totalRecords || NULL == cursor) {
        nextPageLock->unlock();
        return NULL;
    }
    
    /* always give a duplicate page, so that the original page is not modified or freed */
    page = cursor->getObject()->copy();
    page->recordOffset = offsetRecords;
    offsetRecords += page->count;
    cursor = cursor->getNext();
    
    nextPageLock->unlock();
    return page;
}

void eagle::StreamPageProvider::reset()
{
    offsetRecords = 0;
    cursor = NULL;
}

EaglePage* eagle::StreamPageProvider::getPage(int pageNumber)
{
    EaglePage *theCursor, *page;
    
    /* get the page (if possible) */
    theCursor = (EaglePage*) list->get(pageNumber);
    if(NULL == theCursor) {
        return NULL;
    }
    
    /* always give a duplicate page, so that the original page is not modified or freed */
    page = theCursor->copy();
    
    return page;
}

bool eagle::StreamPageProvider::isRandomAccess()
{
    return true;
}
