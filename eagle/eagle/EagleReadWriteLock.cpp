#include <stdlib.h>
#include <stdio.h>
#include "EagleReadWriteLock.h"

EagleReadWriteLock::EagleReadWriteLock()
{
    pthread_rwlock_init(&lock, NULL);
}

void EagleReadWriteLock::holdReadLock()
{
    int retval = aquireReadLock();
    if(0 != retval) {
        throw std::runtime_error("Read lock acquisition failed.");
    }
}

void EagleReadWriteLock::holdWriteLock()
{
    int retval = aquireWriteLock();
    if(0 != retval) {
        throw std::runtime_error("Write lock acquisition failed.");
    }
}

void EagleReadWriteLock::unlock()
{
    int retval = releaseLock();
    if(0 != retval) {
        throw std::runtime_error("Unlock failed.");
    }
}

EagleReadWriteLock::~EagleReadWriteLock()
{
    pthread_rwlock_destroy(&lock);
}

int EagleReadWriteLock::aquireReadLock()
{
    return pthread_rwlock_rdlock(&lock);
}

int EagleReadWriteLock::aquireWriteLock()
{
    return pthread_rwlock_wrlock(&lock);
}

int EagleReadWriteLock::releaseLock()
{
    return pthread_rwlock_unlock(&lock);
}
