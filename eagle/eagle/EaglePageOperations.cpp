#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "EaglePageOperations.h"
#include "eagle/PageProvider.h"
#include "EagleLogger.h"
#include "eagle/algorithm.h"

#include <unistd.h>

void EaglePageOperations_LessThanInt(EaglePage *destination, EaglePage *source1, EaglePage*, void *obj)
{
    int i;
    EagleDataTypeInteger value = *((EagleDataTypeInteger*) obj), *destdata = (EagleDataTypeInteger*) destination->data, *source1data = (EagleDataTypeInteger*) source1->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = (source1data[i] < value);
    }
}

void EaglePageOperations_GreaterThanInt(EaglePage *destination, EaglePage *source1, EaglePage*, void *obj)
{
    int i;
    EagleDataTypeInteger value = *((EagleDataTypeInteger*) obj), *destdata = (EagleDataTypeInteger*) destination->data, *source1data = (EagleDataTypeInteger*) source1->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = (source1data[i] > value);
    }
}

void EaglePageOperations_LessThanEqualsInt(EaglePage *destination, EaglePage *source1, EaglePage*, void *obj)
{
    int i;
    EagleDataTypeInteger value = *((EagleDataTypeInteger*) obj), *destdata = (EagleDataTypeInteger*) destination->data, *source1data = (EagleDataTypeInteger*) source1->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = (source1data[i] <= value);
    }
}

void EaglePageOperations_GreaterThanEqualsInt(EaglePage *destination, EaglePage *source1, EaglePage*, void *obj)
{
    int i;
    EagleDataTypeInteger value = *((EagleDataTypeInteger*) obj), *destdata = (EagleDataTypeInteger*) destination->data, *source1data = (EagleDataTypeInteger*) source1->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = (source1data[i] >= value);
    }
}

void EaglePageOperations_EqualsInt(EaglePage *destination, EaglePage *source1, EaglePage*, void *obj)
{
    int i;
    EagleDataTypeInteger value = *((EagleDataTypeInteger*) obj), *destdata = (EagleDataTypeInteger*) destination->data, *source1data = (EagleDataTypeInteger*) source1->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = (source1data[i] == value);
    }
}

void EaglePageOperations_NotEqualsInt(EaglePage *destination, EaglePage *source1, EaglePage*, void *obj)
{
    int i;
    EagleDataTypeInteger value = *((EagleDataTypeInteger*) obj), *destdata = (EagleDataTypeInteger*) destination->data, *source1data = (EagleDataTypeInteger*) source1->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = (source1data[i] != value);
    }
}

void EaglePageOperations_AdditionInt(EaglePage *destination, EaglePage *source1, EaglePage*, void *obj)
{
    int i;
    EagleDataTypeInteger value = *((EagleDataTypeInteger*) obj), *destdata = (EagleDataTypeInteger*) destination->data, *source1data = (EagleDataTypeInteger*) source1->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = (source1data[i] + value);
    }
}

void EaglePageOperations_ModulusLeftInt(EaglePage *destination, EaglePage *source1, EaglePage*, void *obj)
{
    int i;
    EagleDataTypeInteger value = *((EagleDataTypeInteger*) obj), *destdata = (EagleDataTypeInteger*) destination->data, *source1data = (EagleDataTypeInteger*) source1->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        if(0 == value) {
            destdata[i] = 0;
        }
        else {
            destdata[i] = (source1data[i] % value);
        }
    }
}

void EaglePageOperations_ModulusRightInt(EaglePage *destination, EaglePage *source1, EaglePage*, void *obj)
{
    int i;
    EagleDataTypeInteger value = *((EagleDataTypeInteger*) obj), *destdata = (EagleDataTypeInteger*) destination->data, *source1data = (EagleDataTypeInteger*) source1->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        if(0 == source1data[i]) {
            destdata[i] = 0;
        }
        else {
            destdata[i] = (value % source1data[i]);
        }
    }
}

void EaglePageOperations_SubtractLeftInt(EaglePage *destination, EaglePage *source1, EaglePage*, void *obj)
{
    int i;
    EagleDataTypeInteger value = *((EagleDataTypeInteger*) obj), *destdata = (EagleDataTypeInteger*) destination->data, *source1data = (EagleDataTypeInteger*) source1->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = (source1data[i] - value);
    }
}

void EaglePageOperations_SubtractRightInt(EaglePage *destination, EaglePage *source1, EaglePage*, void *obj)
{
    int i;
    EagleDataTypeInteger value = *((EagleDataTypeInteger*) obj), *destdata = (EagleDataTypeInteger*) destination->data, *source1data = (EagleDataTypeInteger*) source1->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = (value - source1data[i]);
    }
}

void EaglePageOperations_MultiplyInt(EaglePage *destination, EaglePage *source1, EaglePage*, void *obj)
{
    int i;
    EagleDataTypeInteger value = *((EagleDataTypeInteger*) obj), *destdata = (EagleDataTypeInteger*) destination->data, *source1data = (EagleDataTypeInteger*) source1->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = (source1data[i] * value);
    }
}

void EaglePageOperations_DivideLeftInt(EaglePage *destination, EaglePage *source1, EaglePage*, void *obj)
{
    int i;
    EagleDataTypeInteger value = *((EagleDataTypeInteger*) obj), *destdata = (EagleDataTypeInteger*) destination->data, *source1data = (EagleDataTypeInteger*) source1->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        if(0 == value) {
            destdata[i] = 0;
        }
        else {
            destdata[i] = (source1data[i] / value);
        }
    }
}

void EaglePageOperations_DivideRightInt(EaglePage *destination, EaglePage *source1, EaglePage*, void *obj)
{
    int i;
    EagleDataTypeInteger value = *((EagleDataTypeInteger*) obj), *destdata = (EagleDataTypeInteger*) destination->data, *source1data = (EagleDataTypeInteger*) source1->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        if(0 == source1data[i]) {
            destdata[i] = 0;
        }
        else {
            destdata[i] = (value / source1data[i]);
        }
    }
}

void EaglePageOperations_LessThanFloat(EaglePage *destination, EaglePage *source1, EaglePage*, void *obj)
{
    int i;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data;
    EagleDataTypeFloat value = *((EagleDataTypeFloat*) obj), *source1data = (EagleDataTypeFloat*) source1->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = (source1data[i] < value);
    }
}

void EaglePageOperations_GreaterThanFloat(EaglePage *destination, EaglePage *source1, EaglePage*, void *obj)
{
    int i;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data;
    EagleDataTypeFloat value = *((EagleDataTypeFloat*) obj), *source1data = (EagleDataTypeFloat*) source1->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = (source1data[i] > value);
    }
}

void EaglePageOperations_LessThanEqualsFloat(EaglePage *destination, EaglePage *source1, EaglePage*, void *obj)
{
    int i;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data;
    EagleDataTypeFloat value = *((EagleDataTypeFloat*) obj), *source1data = (EagleDataTypeFloat*) source1->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = (source1data[i] <= value);
    }
}

void EaglePageOperations_GreaterThanEqualsFloat(EaglePage *destination, EaglePage *source1, EaglePage*, void *obj)
{
    int i;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data;
    EagleDataTypeFloat value = *((EagleDataTypeFloat*) obj), *source1data = (EagleDataTypeFloat*) source1->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = (source1data[i] >= value);
    }
}

void EaglePageOperations_EqualsFloat(EaglePage *destination, EaglePage *source1, EaglePage*, void *obj)
{
    int i;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data;
    EagleDataTypeFloat value = *((EagleDataTypeFloat*) obj), *source1data = (EagleDataTypeFloat*) source1->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = (source1data[i] == value);
    }
}

void EaglePageOperations_NotEqualsFloat(EaglePage *destination, EaglePage *source1, EaglePage*, void *obj)
{
    int i;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data;
    EagleDataTypeFloat value = *((EagleDataTypeFloat*) obj), *source1data = (EagleDataTypeFloat*) source1->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = (source1data[i] != value);
    }
}

void EaglePageOperations_AdditionFloat(EaglePage *destination, EaglePage *source1, EaglePage*, void *obj)
{
    int i;
    EagleDataTypeFloat value = *((EagleDataTypeFloat*) obj), *destdata = (EagleDataTypeFloat*) destination->data, *source1data = (EagleDataTypeFloat*) source1->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = (source1data[i] + value);
    }
}

void EaglePageOperations_SubtractLeftFloat(EaglePage *destination, EaglePage *source1, EaglePage*, void *obj)
{
    int i;
    EagleDataTypeFloat value = *((EagleDataTypeFloat*) obj), *destdata = (EagleDataTypeFloat*) destination->data, *source1data = (EagleDataTypeFloat*) source1->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = (source1data[i] - value);
    }
}

void EaglePageOperations_SubtractRightFloat(EaglePage *destination, EaglePage *source1, EaglePage*, void *obj)
{
    int i;
    EagleDataTypeFloat value = *((EagleDataTypeFloat*) obj), *destdata = (EagleDataTypeFloat*) destination->data, *source1data = (EagleDataTypeFloat*) source1->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = (value - source1data[i]);
    }
}

void EaglePageOperations_MultiplyFloat(EaglePage *destination, EaglePage *source1, EaglePage*, void *obj)
{
    int i;
    EagleDataTypeFloat value = *((EagleDataTypeFloat*) obj), *destdata = (EagleDataTypeFloat*) destination->data, *source1data = (EagleDataTypeFloat*) source1->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = (source1data[i] * value);
    }
}

void EaglePageOperations_DivideLeftFloat(EaglePage *destination, EaglePage *source1, EaglePage*, void *obj)
{
    EagleDataTypeFloat value = *((EagleDataTypeFloat*) obj), *destdata = (EagleDataTypeFloat*) destination->data, *source1data = (EagleDataTypeFloat*) source1->data;
    int i;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        if(0 == value) {
            destdata[i] = 0;
        }
        else {
            destdata[i] = (source1data[i] / value);
        }
    }
}

void EaglePageOperations_DivideRightFloat(EaglePage *destination, EaglePage *source1, EaglePage*, void *obj)
{
    EagleDataTypeFloat value = *((EagleDataTypeFloat*) obj), *destdata = (EagleDataTypeFloat*) destination->data, *source1data = (EagleDataTypeFloat*) source1->data;
    int i;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        if(0 == source1data[i]) {
            destdata[i] = 0;
        }
        else {
            destdata[i] = (value / source1data[i]);
        }
    }
}

void EaglePageOperations_AndPageInt(EaglePage *destination, EaglePage *source1, EaglePage *source2, void*)
{
    int i;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data, *source1data = (EagleDataTypeInteger*) source1->data, *source2data = (EagleDataTypeInteger*) source2->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = source1data[i] && source2data[i];
    }
}

void EaglePageOperations_OrPageInt(EaglePage *destination, EaglePage *source1, EaglePage *source2, void*)
{
    int i;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data, *source1data = (EagleDataTypeInteger*) source1->data, *source2data = (EagleDataTypeInteger*) source2->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = source1data[i] || source2data[i];
    }
}

void EaglePageOperations_NotPageInt(EaglePage *destination, EaglePage *source1, EaglePage*, void*)
{
    int i;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data, *source1data = (EagleDataTypeInteger*) source1->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = !source1data[i];
    }
}

void EaglePageOperations_NegatePageInt(EaglePage *destination, EaglePage *source1, EaglePage*, void*)
{
    int i;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data, *source1data = (EagleDataTypeInteger*) source1->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = -source1data[i];
    }
}

void EaglePageOperations_CastPageIntFloat(EaglePage *destination, EaglePage *source1, EaglePage*, void*)
{
    int i;
    EagleDataTypeFloat *destdata = (EagleDataTypeFloat*) destination->data;
    EagleDataTypeInteger *source1data = (EagleDataTypeInteger*) source1->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = (EagleDataTypeFloat) source1data[i];
    }
}

void EaglePageOperations_NegatePageFloat(EaglePage *destination, EaglePage *source1, EaglePage*, void*)
{
    int i;
    EagleDataTypeFloat *destdata = (EagleDataTypeFloat*) destination->data, *source1data = (EagleDataTypeFloat*) source1->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = -source1data[i];
    }
}

void EaglePageOperations_AdditionPageInt(EaglePage *destination, EaglePage *source1, EaglePage *source2, void*)
{
    int i;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data, *source1data = (EagleDataTypeInteger*) source1->data, *source2data = (EagleDataTypeInteger*) source2->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = source1data[i] + source2data[i];
    }
}

void EaglePageOperations_AdditionPageFloat(EaglePage *destination, EaglePage *source1, EaglePage *source2, void*)
{
    int i;
    EagleDataTypeFloat *destdata = (EagleDataTypeFloat*) destination->data, *source1data = (EagleDataTypeFloat*) source1->data, *source2data = (EagleDataTypeFloat*) source2->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = source1data[i] + source2data[i];
    }
}

void EaglePageOperations_MultiplyPageInt(EaglePage *destination, EaglePage *source1, EaglePage *source2, void*)
{
    int i;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data, *source1data = (EagleDataTypeInteger*) source1->data, *source2data = (EagleDataTypeInteger*) source2->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = source1data[i] * source2data[i];
    }
}

void EaglePageOperations_MultiplyPageFloat(EaglePage *destination, EaglePage *source1, EaglePage *source2, void*)
{
    int i;
    EagleDataTypeFloat *destdata = (EagleDataTypeFloat*) destination->data, *source1data = (EagleDataTypeFloat*) source1->data, *source2data = (EagleDataTypeFloat*) source2->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = source1data[i] * source2data[i];
    }
}

void EaglePageOperations_SendPageToProviderInteger_(eagle::PageProvider *provider,
                                                    EaglePage *source1,
                                                    EaglePage *source2)
{
    int i;
    EagleDataTypeInteger *source1data;
    EagleDataTypeInteger *source2data = (EagleDataTypeInteger*) source2->data;
    
    /* send all */
    if(NULL == source1) {
        for(i = 0; i < source2->count; ++i) {
            provider->add(&source2data[i]);
        }
        return;
    }
    
    /* send some */
    source1data = (EagleDataTypeInteger*) source1->data;
    for(i = 0; i < source2->count; ++i) {
        if(source1data[i]) {
            provider->add(&source2data[i]);
        }
    }
}

void EaglePageOperations_SendPageToProviderVarchar_(eagle::PageProvider *provider,
                                                    EaglePage *source1,
                                                    EaglePage *source2)
{
    int i;
    EagleDataTypeInteger *source1data;
    EagleDataTypeVarchar *source2data = (EagleDataTypeVarchar*) source2->data;
    
    /* send all */
    if(NULL == source1) {
        for(i = 0; i < source2->count; ++i) {
            provider->add(strdup(source2data[i]));
        }
        return;
    }
    
    /* send some */
    source1data = (EagleDataTypeInteger*) source1->data;
    for(i = 0; i < source2->count; ++i) {
        if(source1data[i]) {
            provider->add(strdup(source2data[i]));
        }
    }
}

void EaglePageOperations_SendPageToProviderFloat_(eagle::PageProvider *provider,
                                                  EaglePage *source1,
                                                  EaglePage *source2)
{
    int i;
    EagleDataTypeInteger *source1data;
    EagleDataTypeFloat *source2data = (EagleDataTypeFloat*) source2->data;
    
    /* send all */
    if(NULL == source1) {
        for(i = 0; i < source2->count; ++i) {
            provider->add(&source2data[i]);
        }
        return;
    }
    
    /* send some */
    source1data = (EagleDataTypeInteger*) source1->data;
    for(i = 0; i < source2->count; ++i) {
        if(source1data[i]) {
            provider->add(&source2data[i]);
        }
    }
}

void EaglePageOperations_SendPageToProvider(EaglePage*, EaglePage *source1, EaglePage *source2, void *obj)
{
    eagle::PageProvider *provider = (eagle::PageProvider*) obj;
    
    switch(provider->type) {
            
        case EagleData::Type::Unknown:
            EagleLogger::Log(EagleLogger::Severity::Error, "Unknown type.");
            return;
            
        case EagleData::Type::Integer:
            EaglePageOperations_SendPageToProviderInteger_(provider, source1, source2);
            break;
            
        case EagleData::Type::Varchar:
            EaglePageOperations_SendPageToProviderVarchar_(provider, source1, source2);
            break;
            
        case EagleData::Type::Float:
            EaglePageOperations_SendPageToProviderFloat_(provider, source1, source2);
            break;
            
    }
}

void EaglePageOperations_EqualsPageInt(EaglePage *destination, EaglePage *source1, EaglePage *source2, void*)
{
    int i;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data, *source1data = (EagleDataTypeInteger*) source1->data, *source2data = (EagleDataTypeInteger*) source2->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = source1data[i] == source2data[i];
    }
}

void EaglePageOperations_EqualsPageFloat(EaglePage *destination, EaglePage *source1, EaglePage *source2, void*)
{
    int i;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data;
    EagleDataTypeFloat *source1data = (EagleDataTypeFloat*) source1->data, *source2data = (EagleDataTypeFloat*) source2->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = source1data[i] == source2data[i];
    }
}

void EaglePageOperations_NotEqualsPageInt(EaglePage *destination, EaglePage *source1, EaglePage *source2, void*)
{
    int i;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data, *source1data = (EagleDataTypeInteger*) source1->data, *source2data = (EagleDataTypeInteger*) source2->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = source1data[i] != source2data[i];
    }
}

void EaglePageOperations_NotEqualsPageFloat(EaglePage *destination, EaglePage *source1, EaglePage *source2, void*)
{
    int i;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data;
    EagleDataTypeFloat *source1data = (EagleDataTypeFloat*) source1->data, *source2data = (EagleDataTypeFloat*) source2->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = source1data[i] != source2data[i];
    }
}

void EaglePageOperations_ModulusPageInt(EaglePage *destination, EaglePage *source1, EaglePage *source2, void*)
{
    int i;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data, *source1data = (EagleDataTypeInteger*) source1->data, *source2data = (EagleDataTypeInteger*) source2->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        if(0 == source2data[i]) {
            destdata[i] = 0;
        }
        else {
            destdata[i] = source1data[i] % source2data[i];
        }
    }
}

void EaglePageOperations_GreaterThanPageInt(EaglePage *destination, EaglePage *source1, EaglePage *source2, void*)
{
    int i;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data, *source1data = (EagleDataTypeInteger*) source1->data, *source2data = (EagleDataTypeInteger*) source2->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = (source1data[i] > source2data[i]);
    }
}

void EaglePageOperations_GreaterThanPageFloat(EaglePage *destination, EaglePage *source1, EaglePage *source2, void*)
{
    int i;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data;
    EagleDataTypeFloat *source1data = (EagleDataTypeFloat*) source1->data, *source2data = (EagleDataTypeFloat*) source2->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = (source1data[i] > source2data[i]);
    }
}

void EaglePageOperations_LessThanPageInt(EaglePage *destination, EaglePage *source1, EaglePage *source2, void*)
{
    int i;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data, *source1data = (EagleDataTypeInteger*) source1->data, *source2data = (EagleDataTypeInteger*) source2->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = (source1data[i] < source2data[i]);
    }
}

void EaglePageOperations_LessThanPageFloat(EaglePage *destination, EaglePage *source1, EaglePage *source2, void*)
{
    int i;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data;
    EagleDataTypeFloat *source1data = (EagleDataTypeFloat*) source1->data, *source2data = (EagleDataTypeFloat*) source2->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = (source1data[i] < source2data[i]);
    }
}

void EaglePageOperations_GreaterThanEqualPageInt(EaglePage *destination, EaglePage *source1, EaglePage *source2, void*)
{
    int i;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data, *source1data = (EagleDataTypeInteger*) source1->data, *source2data = (EagleDataTypeInteger*) source2->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = (source1data[i] >= source2data[i]);
    }
}

void EaglePageOperations_GreaterThanEqualPageFloat(EaglePage *destination, EaglePage *source1, EaglePage *source2, void*)
{
    int i;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data;
    EagleDataTypeFloat *source1data = (EagleDataTypeFloat*) source1->data, *source2data = (EagleDataTypeFloat*) source2->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = (source1data[i] >= source2data[i]);
    }
}

void EaglePageOperations_LessThanEqualPageInt(EaglePage *destination, EaglePage *source1, EaglePage *source2, void*)
{
    int i;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data, *source1data = (EagleDataTypeInteger*) source1->data, *source2data = (EagleDataTypeInteger*) source2->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = (source1data[i] <= source2data[i]);
    }
}

void EaglePageOperations_LessThanEqualPageFloat(EaglePage *destination, EaglePage *source1, EaglePage *source2, void*)
{
    int i;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data;
    EagleDataTypeFloat *source1data = (EagleDataTypeFloat*) source1->data, *source2data = (EagleDataTypeFloat*) source2->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = (source1data[i] <= source2data[i]);
    }
}

void EaglePageOperations_SubtractPageInt(EaglePage *destination, EaglePage *source1, EaglePage *source2, void*)
{
    int i;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data, *source1data = (EagleDataTypeInteger*) source1->data, *source2data = (EagleDataTypeInteger*) source2->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = source1data[i] - source2data[i];
    }
}

void EaglePageOperations_SubtractPageFloat(EaglePage *destination, EaglePage *source1, EaglePage *source2, void*)
{
    int i;
    EagleDataTypeFloat *destdata = (EagleDataTypeFloat*) destination->data, *source1data = (EagleDataTypeFloat*) source1->data, *source2data = (EagleDataTypeFloat*) source2->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        destdata[i] = source1data[i] - source2data[i];
    }
}

void EaglePageOperations_DividePageInt(EaglePage *destination, EaglePage *source1, EaglePage *source2, void*)
{
    int i;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data, *source1data = (EagleDataTypeInteger*) source1->data, *source2data = (EagleDataTypeInteger*) source2->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        if(0 == source2data[i]) {
            destdata[i] = 0;
        }
        else {
            destdata[i] = source1data[i] / source2data[i];
        }
    }
}

void EaglePageOperations_DividePageFloat(EaglePage *destination, EaglePage *source1, EaglePage *source2, void*)
{
    int i;
    EagleDataTypeFloat *destdata = (EagleDataTypeFloat*) destination->data, *source1data = (EagleDataTypeFloat*) source1->data, *source2data = (EagleDataTypeFloat*) source2->data;
    
    destination->recordOffset = source1->recordOffset;
    destination->count = source1->count;
    
    for(i = 0; i < source1->count; ++i) {
        if(0 == source2data[i]) {
            destdata[i] = 0;
        }
        else {
            destdata[i] = source1data[i] / source2data[i];
        }
    }
}

void EaglePageOperations_SqrtPageFloat(EaglePage *destination, EaglePage*, EaglePage*, void *obj)
{
    int i;
    EaglePage **args = (EaglePage**) obj;
    EagleDataTypeFloat *destdata = (EagleDataTypeFloat*) destination->data;
    EagleDataTypeFloat *arg0data = (EagleDataTypeFloat*) args[0]->data;
    
    destination->recordOffset = args[0]->recordOffset;
    destination->count = args[0]->count;
    
    for(i = 0; i < args[0]->count; ++i) {
        if(arg0data[i] < 0) {
            destdata[i] = NAN;
        }
        else {
            destdata[i] = (EagleDataTypeFloat) sqrt(arg0data[i]);
        }
    }
}

void EaglePageOperations_Atan2PageFloatPageFloat(EaglePage *destination, EaglePage*, EaglePage*, void *obj)
{
    int i;
    EaglePage **args = (EaglePage**) obj;
    EagleDataTypeFloat *destdata = (EagleDataTypeFloat*) destination->data;
    EagleDataTypeFloat *arg0data = (EagleDataTypeFloat*) args[0]->data;
    EagleDataTypeFloat *arg1data = (EagleDataTypeFloat*) args[1]->data;
    
    destination->recordOffset = args[0]->recordOffset;
    destination->count = args[0]->count;
    
    for(i = 0; i < args[0]->count; ++i) {
        destdata[i] = (EagleDataTypeFloat) atan2(arg0data[i], arg1data[i]);
    }
}

void EaglePageOperations_AbsPageFloat(EaglePage *destination, EaglePage*, EaglePage*, void *obj)
{
    int i;
    EaglePage **args = (EaglePage**) obj;
    EagleDataTypeFloat *destdata = (EagleDataTypeFloat*) destination->data;
    EagleDataTypeFloat *arg0data = (EagleDataTypeFloat*) args[0]->data;
    
    destination->recordOffset = args[0]->recordOffset;
    destination->count = args[0]->count;
    
    for(i = 0; i < args[0]->count; ++i) {
        destdata[i] = (EagleDataTypeFloat) fabs(arg0data[i]);
    }
}

void EaglePageOperations_UpperPageVarchar(EaglePage *destination, EaglePage*, EaglePage*, void *obj)
{
    int i;
    EaglePage **args = (EaglePage**) obj;
    EagleDataTypeVarchar *destdata = (EagleDataTypeVarchar*) destination->data;
    EagleDataTypeVarchar *arg0data = (EagleDataTypeVarchar*) args[0]->data;
    
    destination->recordOffset = args[0]->recordOffset;
    destination->count = args[0]->count;
    
    for(i = 0; i < args[0]->count; ++i) {
        destdata[i] = eagle::algorithm::toUpperCase(arg0data[i]);
    }
}

void EaglePageOperations_LowerPageVarchar(EaglePage *destination, EaglePage*, EaglePage*, void *obj)
{
    EaglePage **args = (EaglePage**) obj;
    EagleDataTypeVarchar *destdata = (EagleDataTypeVarchar*) destination->data;
    EagleDataTypeVarchar *arg0data = (EagleDataTypeVarchar*) args[0]->data;
    
    destination->recordOffset = args[0]->recordOffset;
    destination->count = args[0]->count;
    
    for(int i = 0; i < args[0]->count; ++i) {
        destdata[i] = eagle::algorithm::toLowerCase(arg0data[i]);
    }
}

void EaglePageOperations_FloorPageFloat(EaglePage *destination, EaglePage*, EaglePage*, void *obj)
{
    int i;
    EaglePage **args = (EaglePage**) obj;
    EagleDataTypeFloat *destdata = (EagleDataTypeFloat*) destination->data;
    EagleDataTypeFloat *arg0data = (EagleDataTypeFloat*) args[0]->data;
    
    destination->recordOffset = args[0]->recordOffset;
    destination->count = args[0]->count;
    
    for(i = 0; i < args[0]->count; ++i) {
        destdata[i] = (EagleDataTypeFloat) floor(arg0data[i]);
    }
}

void EaglePageOperations_CeilPageFloat(EaglePage *destination, EaglePage*, EaglePage*, void *obj)
{
    int i;
    EaglePage **args = (EaglePage**) obj;
    EagleDataTypeFloat *destdata = (EagleDataTypeFloat*) destination->data;
    EagleDataTypeFloat *arg0data = (EagleDataTypeFloat*) args[0]->data;
    
    destination->recordOffset = args[0]->recordOffset;
    destination->count = args[0]->count;
    
    for(i = 0; i < args[0]->count; ++i) {
        destdata[i] = (EagleDataTypeFloat) ceil(arg0data[i]);
    }
}

void EaglePageOperations_ExpPageFloat(EaglePage *destination, EaglePage*, EaglePage*, void *obj)
{
    int i;
    EaglePage **args = (EaglePage**) obj;
    EagleDataTypeFloat *destdata = (EagleDataTypeFloat*) destination->data;
    EagleDataTypeFloat *arg0data = (EagleDataTypeFloat*) args[0]->data;
    
    destination->recordOffset = args[0]->recordOffset;
    destination->count = args[0]->count;
    
    for(i = 0; i < args[0]->count; ++i) {
        destdata[i] = (EagleDataTypeFloat) exp(arg0data[i]);
    }
}

void EaglePageOperations_LnPageFloat(EaglePage *destination, EaglePage*, EaglePage*, void *obj)
{
    int i;
    EaglePage **args = (EaglePage**) obj;
    EagleDataTypeFloat *destdata = (EagleDataTypeFloat*) destination->data;
    EagleDataTypeFloat *arg0data = (EagleDataTypeFloat*) args[0]->data;
    
    destination->recordOffset = args[0]->recordOffset;
    destination->count = args[0]->count;
    
    for(i = 0; i < args[0]->count; ++i) {
        destdata[i] = (EagleDataTypeFloat) log(arg0data[i]);
    }
}

void EaglePageOperations_PowerPageFloatPageFloat(EaglePage *destination, EaglePage*, EaglePage*, void *obj)
{
    int i;
    EaglePage **args = (EaglePage**) obj;
    EagleDataTypeFloat *destdata = (EagleDataTypeFloat*) destination->data;
    EagleDataTypeFloat *arg0data = (EagleDataTypeFloat*) args[0]->data;
    EagleDataTypeFloat *arg1data = (EagleDataTypeFloat*) args[1]->data;
    
    destination->recordOffset = args[0]->recordOffset;
    destination->count = args[0]->count;
    
    for(i = 0; i < args[0]->count; ++i) {
        destdata[i] = (EagleDataTypeFloat) pow(arg0data[i], arg1data[i]);
    }
}

void EaglePageOperations_OctetLengthPageVarchar(EaglePage *destination, EaglePage*, EaglePage*, void *obj)
{
    int i;
    EaglePage **args = (EaglePage**) obj;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data;
    EagleDataTypeVarchar *arg0data = (EagleDataTypeVarchar*) args[0]->data;
    
    destination->recordOffset = args[0]->recordOffset;
    destination->count = args[0]->count;
    
    for(i = 0; i < args[0]->count; ++i) {
        destdata[i] = (EagleDataTypeInteger) strlen(arg0data[i]);
    }
}

void EaglePageOperations_BitLengthPageVarchar(EaglePage *destination, EaglePage*, EaglePage*, void *obj)
{
    int i;
    EaglePage **args = (EaglePage**) obj;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data;
    EagleDataTypeVarchar *arg0data = (EagleDataTypeVarchar*) args[0]->data;
    
    destination->recordOffset = args[0]->recordOffset;
    destination->count = args[0]->count;
    
    for(i = 0; i < args[0]->count; ++i) {
        destdata[i] = 8 * (EagleDataTypeInteger) strlen(arg0data[i]);
    }
}

void EaglePageOperations_CharLengthPageVarchar(EaglePage *destination, EaglePage*, EaglePage*, void *obj)
{
    int i;
    EaglePage **args = (EaglePage**) obj;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data;
    EagleDataTypeVarchar *arg0data = (EagleDataTypeVarchar*) args[0]->data;
    
    destination->recordOffset = args[0]->recordOffset;
    destination->count = args[0]->count;
    
    for(i = 0; i < args[0]->count; ++i) {
        destdata[i] = (EagleDataTypeInteger) strlen(arg0data[i]);
    }
}

void EaglePageOperations_ModPageFloatPageFloat(EaglePage *destination, EaglePage*, EaglePage*, void *obj)
{
    int i;
    EaglePage **args = (EaglePage**) obj;
    EagleDataTypeFloat *destdata = (EagleDataTypeFloat*) destination->data;
    EagleDataTypeFloat *arg0data = (EagleDataTypeFloat*) args[0]->data;
    EagleDataTypeFloat *arg1data = (EagleDataTypeFloat*) args[1]->data;
    
    destination->recordOffset = args[0]->recordOffset;
    destination->count = args[0]->count;
    
    for(i = 0; i < args[0]->count; ++i) {
        destdata[i] = (EagleDataTypeFloat) fmod(arg0data[i], arg1data[i]);
    }
}

void EaglePageOperations_PositionPageVarcharPageVarchar(EaglePage *destination, EaglePage*, EaglePage*, void *obj)
{
    int i;
    EaglePage **args = (EaglePage**) obj;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data;
    EagleDataTypeVarchar *arg0data = (EagleDataTypeVarchar*) args[0]->data;
    EagleDataTypeVarchar *arg1data = (EagleDataTypeVarchar*) args[1]->data;
    
    destination->recordOffset = args[0]->recordOffset;
    destination->count = args[0]->count;
    
    for(i = 0; i < args[0]->count; ++i) {
        std::string s(arg1data[i]);
        destdata[i] = (EagleDataTypeInteger) s.find(arg0data[i]);
    }
}

void EaglePageOperations_WidthBucketPageFloatPageFloatPageFloatPageInteger(EaglePage *destination, EaglePage*, EaglePage*, void *obj)
{
    int i;
    EaglePage **args = (EaglePage**) obj;
    EagleDataTypeInteger *destdata = (EagleDataTypeInteger*) destination->data;
    EagleDataTypeFloat *arg0data = (EagleDataTypeFloat*) args[0]->data;
    EagleDataTypeFloat *arg1data = (EagleDataTypeFloat*) args[1]->data;
    EagleDataTypeFloat *arg2data = (EagleDataTypeFloat*) args[2]->data;
    EagleDataTypeInteger *arg3data = (EagleDataTypeInteger*) args[3]->data;
    
    destination->recordOffset = args[0]->recordOffset;
    destination->count = args[0]->count;
    
    for(i = 0; i < args[0]->count; ++i) {
        /* the number of buckets must be a positive integer */
        if(0 == arg3data[i]) {
            destdata[i] = (EagleDataTypeInteger) 0;
        }
        
        /* does min == max */
        else if(arg1data[i] == arg2data[i]) {
            destdata[i] = (EagleDataTypeInteger) 0;
        }
        
        /* positive */
        else if(arg1data[i] < arg2data[i]) {
            if(arg0data[i] < arg1data[i] || arg0data[i] > arg2data[i]) {
                /* is the value outside of min and max? */
                destdata[i] = (EagleDataTypeInteger) 0;
            }
            else {
                /* find out which bucket the value falls into */
                EagleDataTypeFloat bucketSize = (arg2data[i] - arg1data[i]) / arg3data[i];
                EagleDataTypeFloat diff = arg0data[i] - arg1data[i];
                destdata[i] = (EagleDataTypeInteger) ceil(diff / bucketSize);
            }
        }
        
        /* negative */
        else {
            if(arg0data[i] > arg1data[i] || arg0data[i] < arg2data[i]) {
                /* is the value outside of min and max? */
                destdata[i] = (EagleDataTypeInteger) 0;
            }
            else {
                /* find out which bucket the value falls into */
                EagleDataTypeFloat bucketSize = fabs((arg1data[i] - arg2data[i]) / arg3data[i]);
                EagleDataTypeFloat diff = -(arg0data[i] - arg1data[i]);
                destdata[i] = (EagleDataTypeInteger) ceil(diff / bucketSize);
            }
        }
    }
}
