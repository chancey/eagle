#ifndef eagle_db_Table_h
#define eagle_db_Table_h

#include "eagle/db/Column.h"
#include "eagle/EagleLinkedList.h"

namespace eagle
{
    namespace db
    {
        class Table;
    }
}

/**
 * A table definition.
 */
class eagle::db::Table
{
    
public:
    
    /**
     The raw name of the table.
     */
    char *name;
    
    /**
     The table columns. This is self managed because the array that contains the pointers to the columns is managed
     internally, but the columns themselves are managed externally (i.e. deleting the table will not delete the columns)
     */
    EagleLinkedList<eagle::db::Column*> *columns;
    
public:
    
    /**
     * Create a new table definition. You will also need to create a EagleDbTableData if you intended
     * the table to hold data.
     * @param [in] name The name of the table.
     * @return A new instance.
     */
    Table(const char *name);
    
    /**
     * Add a column to the definition of a table.
     * @param [in] column The column definition.
     */
    void addColumn(eagle::db::Column *column);
    
    /**
     * Set all of the column definitions of a table.
     * @param [in] columns New columns.
     */
    void setColumns(EagleLinkedList<eagle::db::Column*> *columns);
    
    /**
     * Count the columns in a table definition.
     * @return The number of columns in the table.
     */
    int countColumns();
    
    /**
     * Get a column of a table by its index. The first column will have the index of 0.
     * @param [in] index Column index.
     * @return NULL if the column does not exist.
     * @see eagle::db::Table::getColumnIndex()
     */
    eagle::db::Column getColumn(int index);
    
    /**
     * Get a column of a table by its name.
     * @param [in] name Column name.
     * @return NULL if the column does not exist.
     */
    const eagle::db::Column* getColumnByName(char *name);
    
    /**
     * Find the column index by its name.
     * @param [in] name Column name.
     * @return -1 if the column does not exist.
     * @see eagle::db::Table::getColumnByName()
     */
    int getColumnIndex(char *name);
    
    /**
     * Free a table definition. This will not free the column instances.
     */
    ~Table();
    
};

/**
 * Free a table with its column definitions.
 * @param [in] table The table instance.
 * @see eagle::db::Table_Delete()
 */
void eagle_db_Table_DeleteWithColumns(eagle::db::Table *table);

#endif
