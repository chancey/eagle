#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "eagle/db/TableData.h"
#include "eagle/EagleLogger.h"
#include "eagle/StreamPageProvider.h"

eagle::db::TableData::TableData(eagle::db::Table *theTable, int pageSize)
{
    table = theTable;
    usedProviders = theTable->countColumns();
    if(usedProviders > 0) {
        providers = new eagle::PageProvider*[usedProviders];
    }
    else {
        providers = NULL;
    }
    
    for(int i = 0; i < usedProviders; ++i) {
        eagle::db::Column column = table->getColumn(i);
        providers[i] = new eagle::StreamPageProvider(column.type, pageSize, column.name);
    }
}

eagle::db::TableData::~TableData()
{
    for(int i = 0; i < usedProviders; ++i) {
        delete providers[i];
    }
    delete providers;
}

void eagle::db::TableData::insert(eagle::db::Tuple *tuple)
{
    for(int i = 0; i < table->columns->length(); ++i) {
        switch(providers[i]->type) {
                
            case EagleData::Type::Unknown:
                EagleLogger::Log(EagleLogger::Severity::Error, "Unknown type.");
                return;
                
            case EagleData::Type::Integer:
            case EagleData::Type::Float:
                providers[i]->add(tuple->data[i]);
                break;
                
            case EagleData::Type::Varchar:
                providers[i]->add(strdup((const char*) tuple->data[i]));
                break;
                
        }
    }
}
