#include <stdlib.h>
#include <string.h>
#include "eagle/db/Table.h"
#include "eagle/EagleSynchronizedLinkedList.h"

eagle::db::Table::Table(const char *theName)
{
    //! @t eagle::db::TableTest::Instantiate
    name = (NULL == theName ? (char*) NULL : strdup(theName));
    columns = NULL;
}

void eagle::db::Table::setColumns(EagleLinkedList<eagle::db::Column*> *theColumns)
{
    /* set new columns */
    columns = theColumns;
}

eagle::db::Table::~Table()
{
    delete name;
}

void eagle_db_Table_DeleteWithColumns(eagle::db::Table *table)
{
    if(NULL == table) {
        return;
    }
    
    delete table->columns;
    delete table;
}

void eagle::db::Table::addColumn(eagle::db::Column *column)
{
    EagleLinkedList<eagle::db::Column*>::Item *item;
    item = new EagleLinkedList<eagle::db::Column*>::Item(column, false);
    
    if(NULL == columns) {
        columns = new EagleSynchronizedLinkedList<eagle::db::Column*>();
    }
    
    columns->add(item);
}

int eagle::db::Table::countColumns()
{
    if(NULL == columns) {
        return 0;
    }
    return columns->length();
}

eagle::db::Column eagle::db::Table::getColumn(int index)
{
    return *columns->get(index);
}

const eagle::db::Column* eagle::db::Table::getColumnByName(char *theName)
{
    EagleLinkedList<eagle::db::Column*>::Item *cursor;
    
    for(cursor = columns->begin(); NULL != cursor; cursor = cursor->getNext()) {
        const eagle::db::Column *col = cursor->getObject();
        if(col->name == theName) {
            return col;
        }
    }
    
    return NULL;
}

int eagle::db::Table::getColumnIndex(char *theName)
{
    EagleLinkedList<eagle::db::Column*>::Item *cursor;
    int i;
    
    for(cursor = columns->begin(), i = 0; NULL != cursor; cursor = cursor->getNext(), ++i) {
        const eagle::db::Column *col = cursor->getObject();
        if(col->name == theName) {
            return i;
        }
    }
    
    return -1;
}
