#ifndef eagle_db_sql_Insert_h
#define eagle_db_sql_Insert_h

#include "eagle/EagleUnsynchronizedLinkedList.h"
#include "eagledb/EagleDbSqlExpression.h"

namespace eagle
{
    namespace db
    {
        namespace sql
        {
            class Insert;
        }
    }
}

/**
 * Represents an INSERT statement.
 */
class eagle::db::sql::Insert
{
    
public:
    
    /**
     * The name of the table to insert into.
     */
    std::string tableName;
    
    /**
     * A linked list of C strings for columns names.
     */
    EagleUnsynchronizedLinkedList<EagleDbSqlExpression*> names;
    
    /**
     * A linked list of EagleDbSqlExpression's for VALUES.
     */
    EagleUnsynchronizedLinkedList<EagleDbSqlExpression*> values;
    
public:
    
    /**
     * Create a new INSERT.
     * @param [in] theTableName The name of the table.
     */
    Insert(std::string theTableName);
    
};

#endif
