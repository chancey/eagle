#include <stdlib.h>
#include <string>
#include "eagle/db/Column.h"

eagle::db::Column::Column(std::string theName, EagleData::Type theType)
{
    //! @t eagle::ColumnTest::Instantiate()
    name = theName;
    type = theType;
}
