#ifndef eagle_db_Column_h
#define eagle_db_Column_h

#include <string>
#include "eagle/EagleData.h"

namespace eagle
{
    namespace db
    {
        class Column;
    }
}

/**
 * The definition for a table column.
 */
class eagle::db::Column
{
    
public:
    
    /**
     * The name of the column.
     */
    std::string name;
    
    /**
     * Column data type.
     */
    EagleData::Type type;
    
public:
    
    /**
     * Create a new table column.
     * @param [in] theName The name will be duplicated.
     * @param [in] theType The data type for this column.
     * @return A new column instance.
     */
    Column(std::string theName, EagleData::Type theType);
    
};

#endif
