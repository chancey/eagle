#ifndef eagle_db_Tuple_h
#define eagle_db_Tuple_h

#include "eagle/db/Column.h"
#include "eagle/db/Table.h"
#include "eagledb/EagleDbSqlValue.h"

namespace eagle
{
    namespace db
    {
        class Tuple;
    }
}

/**
 * A tuple represents a row in a table.
 */
class eagle::db::Tuple
{
    
public:
    
    /**
     The data contained in the tuple. Do not access this directy, use appopriate methods.
     */
    void **data;
    
    /**
     A reference to the table structure that the tuple is based on.
     */
    eagle::db::Table *table;
    
public:
    
    /**
     * Create a new tuple (record).
     * @param [in] table Table instance.
     */
    Tuple(eagle::db::Table *table);
    
    /**
     * Delete a tuple.
     */
    virtual ~Tuple();
    
    /**
     * Render a tuple as a string.
     * @return A new string instance.
     */
    virtual char* toString();
    
    /**
     * Set an INTEGER value to a column in a tuple.
     * @param [in] position The column position, the first column will be index 0.
     * @param [in] value The value.
     */
    void setInteger(int position, EagleDataTypeInteger value);
    
    /**
     * Set a FLOAT value to a column in a tuple.
     * @param [in] position The column position, the first column will be index 0.
     * @param [in] value The value.
     */
    void setFloat(int position, EagleDataTypeFloat value);
    
    /**
     * Set a VARCHAR value to a column in a tuple.
     * @param [in] position The column position, the first column will be index 0.
     * @param [in] value The value.
     */
    void setVarchar(int position, EagleDataTypeVarchar value);
    
    /**
     * Set a tuple column with a dynamic value.
     * @param [in] position The column position, the first column will be index 0.
     * @param [in] value The value (of any type).
     * @param [in] columnType The type of the column in the tuple.
     * @return true if the \p value can be cast successfully and put into the tuple. Otherwise false will be
     *         returned and the tuple will remain unmodified.
     */
    bool set(int position, EagleDbSqlValue *value, EagleData::Type columnType);
    
};

#endif
