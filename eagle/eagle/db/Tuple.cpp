#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sstream>
#include "eagle/db/Tuple.h"
#include "eagle/EagleData.h"
#include "eagle/EagleLogger.h"
#include "eagledb/EagleDbSqlValue.h"

eagle::db::Tuple::Tuple(eagle::db::Table *theTable)
{
    table = theTable;
    data = new void*[table->countColumns()];
    
    // This is required at the moment becuase the destructor will free these objects.
    for(int i = 0; i < table->countColumns(); ++i) {
        data[i] = 0;
    }
}

eagle::db::Tuple::~Tuple()
{
    if(NULL != data) {
        for(int i = 0; i < table->countColumns(); ++i) {
            free(data[i]);
        }
        delete[] data;
    }
}

void eagle::db::Tuple::setInteger(int position, EagleDataTypeInteger value)
{
    //! @t eagle::db::TupleTestFixture2::setInteger
    
    if(table->getColumn(position).type != EagleData::Type::Integer) {
        EagleLogger::Log(EagleLogger::Severity::Error, "Wrong type.");
        return;
    }
    data[position] = EagleData::CreateInt(value);
}

void eagle::db::Tuple::setFloat(int position, EagleDataTypeFloat value)
{
    //! @t eagle::db::TupleTestFixture2::setFloat
    
    if(table->getColumn(position).type != EagleData::Type::Float) {
        EagleLogger::Log(EagleLogger::Severity::Error, "Wrong type.");
        return;
    }
    data[position] = EagleData::CreateFloat(value);
}

void eagle::db::Tuple::setVarchar(int position, EagleDataTypeVarchar value)
{
    //! @t eagle::db::TupleTestFixture2::setVarchar
    
    if(table->getColumn(position).type != EagleData::Type::Varchar) {
        EagleLogger::Log(EagleLogger::Severity::Error, "Wrong type.");
        return;
    }
    
    ((EagleDataTypeVarchar*) data)[position] = strdup(value);
}

bool eagle::db::Tuple::set(int position, EagleDbSqlValue *v, EagleData::Type columnType)
{
    //! @t eagle::db::TupleTestFixture2::set
    
    bool canCast = false;
    
    switch(columnType) {
            
        case EagleData::Type::Integer:
        {
            EagleDataTypeInteger value = EagleDbSqlValue_getInteger(v, &canCast);
            setInteger(position, value);
            break;
        }
            
        case EagleData::Type::Float:
        {
            EagleDataTypeFloat value = EagleDbSqlValue_getFloat(v, &canCast);
            setFloat(position, value);
            break;
        }
            
        case EagleData::Type::Varchar:
        {
            EagleDataTypeVarchar value = EagleDbSqlValue_getVarchar(v, &canCast);
            setVarchar(position, value);
            break;
        }
            
        case EagleData::Type::Unknown:
        {
            canCast = false;
            break;
        }
            
    }
    
    return canCast;
}

char* eagle::db::Tuple::toString()
{
    //! @t eagle::db::TupleTest::toString
    
    std::stringstream desc;
    desc << "(";
    
    for(int i = 0; i < table->countColumns(); ++i) {
        if(i > 0) {
            desc << ",";
        }
        desc << table->getColumn(i).name << "=";
        
        switch(table->getColumn(i).type) {
                
            case EagleData::Type::Unknown:
                desc << "?";
                break;
                
            case EagleData::Type::Integer:
                desc << *(((EagleDataTypeInteger**) data)[i]);
                break;
                
            case EagleData::Type::Varchar:
                desc << '"' << ((EagleDataTypeVarchar*) data)[i] << '"';
                break;
                
            case EagleData::Type::Float:
                desc << *((EagleDataTypeFloat**) data)[i];
                break;
                
        }
    }
    desc << ")";
    
    return strdup(desc.str().c_str());
}
