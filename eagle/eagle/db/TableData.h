#ifndef eagle_db_TableData_h
#define eagle_db_TableData_h

#include "eagle/db/Table.h"
#include "eagle/PageProvider.h"
#include "eagle/db/Tuple.h"

namespace eagle
{
    namespace db
    {
        class TableData;
    }
}

/**
 * Provides data for a table.
 */
class eagle::db::TableData
{
    
public:
    
    /**
     * Table definition.
     */
    eagle::db::Table *table;
    
    /**
     * Providers for each column.
     */
    eagle::PageProvider **providers;
    
    /**
     * The number of used \c providers
     */
    int usedProviders;
    
public:
    
    /**
     * Create a new table data instance. This structure extends the definition of the table to
     * encapsulate the actual column data for the table.
     * @param [in] table The table instance.
     * @param [in] pageSize The page size for the providers (when data is read from the table).
     * @return A new instance.
     */
    TableData(eagle::db::Table *table, int pageSize);
    
    /**
     * Free a table data. This will not free the table definition.
     */
    ~TableData();
    
    /**
     * INSERT a new tuple (record) onto the table data.
     * @param [in] tuple The tuple to append.
     */
    void insert(eagle::db::Tuple *tuple);
    
};

#endif
