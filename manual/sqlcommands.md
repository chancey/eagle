SQL Commands {#sqlcommands}
============

* [CREATE TABLE](#sqlcreatetable) -- create a table
* [INSERT](#sqlinsert) -- add data to a table
* [SELECT](#sqlselect) -- fetch data from a table

Subpages
--------

* \subpage sqlcreatetable
* \subpage sqlinsert
* \subpage sqlselect
