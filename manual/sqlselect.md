SELECT {#sqlselect}
======

[TOC]

Syntax {#sqlselect-syntax}
======

    SELECT columns
    FROM table_name
    WHERE condition
    ORDER BY column_name

