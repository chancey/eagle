CREATE TABLE {#sqlcreatetable}
============

[TOC]

Syntax {#sqlcreatetable-syntax}
======

    CREATE TABLE table_name (
        column_definitions
    )
