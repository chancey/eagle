Download {#download}
========

You can get the latest successful binary build from Bamboo at:

[https://elliot.atlassian.net/builds/browse/EDB-EDGE/latestSuccessful/artifact/DBIN/dist/bin](https://elliot.atlassian.net/builds/browse/EDB-EDGE/latestSuccessful/artifact/DBIN/dist/bin)