Manual {#manual}
======

Eagle is a highly parallel column-oriented embedded SQL engine.

Table of Contents {#manual-toc}
=================

* [Preface](\ref preface)
 * [About](\ref about)

* [Reference](\ref reference)
 * [SQL Commands](\ref sqlcommands)
 * [SQL Functions](\ref sqlfunctions)
 * [SQL Standard Conformance](\ref conformance)

* [Internals](\ref internals)
 * [Sorting](\ref sorting)
