SQL Functions {#sqlfunctions}
=============

[TOC]

Mathematical Functions {#sqlfunctions-math}
======================
 
ABS(x) {#sqlfunctions-abs}
------
 
Absolute value.
 
ATAN2(y, x) {#sqlfunctions-atan2}
-----------
 
Arc tangent with two arguments.
 
BIT_LENGTH(value) {#sqlfunctions-bitlength}
-----------------
 
Return the bit length of a value.
 
CEIL(x) {#sqlfunctions-ceil}
-------

Ceiling (round up to nearest integer).

CEILING(x) {#sqlfunctions-ceiling}
----------

Alias of [CEIL(x)](#sqlfunctions-ceil). It exists to be compatible with the SQL standard.

CHAR_LENGTH(value) {#sqlfunctions-charlength}
------------------
 
Return the character length of a value.
 
EXP(x) {#sqlfunctions-exp}
------
 
Exponential.
 
FLOOR(x) {#sqlfunctions-floor}
--------
 
Floor (round down to nearest integer).
 
LN(x) {#sqlfunctions-ln}
-----
 
Natural logarithm.
 
LOWER(x) {#sqlfunctions-lower}
--------
 
Convert a string to lower case.
 
MOD(x, y) {#sqlfunctions-mod}
---------
 
Modulo.
 
OCTET_LENGTH(value) {#sqlfunctions-octetlength}
-------------------
 
Return the octet length of a value. This is the same thing as <manual-sql-sqlfunctions-charlength>.
 
POSITION(needle IN haystack) {#sqlfunctions-position}
----------------------------
 
Find the position of a substring (needle) in a larger string (haystack).
 
POW(base, exp) {#sqlfunctions-pow}
--------------
 
Alias for [POWER()](#sqlfunctions-power).
 
POWER(base, exp) {#sqlfunctions-power}
----------------

Raise a number to a power.

SQRT(value) {#sqlfunctions-sqrt}
-----------
 
Square root.

@note \c value must be greater than or equal to zero.
 
UPPER(x) {#sqlfunctions-upper}
--------
 
Convert a string to upper case.
 
WIDTH_BUCKET(op, b1, b2, count) {#sqlfunctions-widthbucket}
-------------------------------
 
Return the bucket to which operand would be assigned in an equidepth histogram with count buckets, in the range b1 to b2.


Misc Functions {#sqlfunctions-misc}
==============

CAST(value AS type) {#sqlfunctions-cast}
-------------------
 
Cast a value from one data type to another.
