SQL Standard Conformance {#conformance}
========================

[TOC]

Key {#conformance-key}
===

| Key        | Description                                                                    |
|:-----------|:-------------------------------------------------------------------------------|
| \d{Strike} | These items are not supported and are not planned to be supported.             |
| Normal     | These items are not supported but plan to be supported in the future.          |
| _Italics_  | These items are partly supported and are available since the version provided. |
| **Bold**   | These items are fully supported and are available since the version provided.  |

B {#conformance-b}
===

B0 {#conformance-b0}
---

| Identifier  | Description                                       | Comments |
|:------------|:--------------------------------------------------|:---------|
| \d{B011}    | Embedded Ada                                      | |
| \d{B012}    | Embedded C                                        | |
| \d{B013}    | Embedded COBOL                                    | |
| \d{B014}    | Embedded Fortran                                  | |
| \d{B015}    | Embedded MUMPS                                    | |
| \d{B016}    | Embedded Pascal                                   | |
| \d{B017}    | Embedded PL/I                                     | |
| \d{B021}    | Direct SQL                                        | |
| \d{B031}    | Basic dynamic SQL                                 | |
| \d{B032}    | Extended dynamic SQL                              | |
| \d{B032-01} | describe input statement                          | |
| \d{B033}    | Untyped SQL-invoked function arguments            | |
| \d{B034}    | Dynamic specification of cursor attributes        | |
| \d{B035}    | Non-extended descriptor names                     | |
| \d{B041}    | Extensions to embedded SQL exception declarations | |
| \d{B051}    | Enhanced execution rights                         | |

B1 {#conformance-b1}
---

| Identifier | Description              | Comments |
|:-----------|:-------------------------|:---------|
| \d{B111}   | Module language Ada      | |
| \d{B112}   | Module language C        | |
| \d{B113}   | Module language COBOL    | |
| \d{B114}   | Module language Fortran  | |
| \d{B115}   | Module language MUMPS    | |
| \d{B116}   | Module language Pascal   | |
| \d{B117}   | Module language PL/I     | |
| \d{B121}   | Routine language Ada     | |
| \d{B122}   | Routine language C       | |
| \d{B123}   | Routine language COBOL   | |
| \d{B124}   | Routine language Fortran | |
| \d{B125}   | Routine language MUMPS   | |
| \d{B126}   | Routine language Pascal  | |
| \d{B127}   | Routine language PL/I    | |
| \d{B128}   | Routine language SQL     | |

B2 {#conformance-b2}
---

| Identifier | Description                                       | Comments |
|:-----------|:--------------------------------------------------|:---------|
| \d{B211}   | Module language Ada: VARCHAR and NUMERIC support  | |
| \d{B221}   | Routine language Ada: VARCHAR and NUMERIC support | |

E {#conformance-e}
===

E0 {#conformance-e0}
---

| Identifier | Description                                                            | Comments |
|:-----------|:-----------------------------------------------------------------------|:---------|
| E011       | Numeric data types                                                     | |
| E011-01    | INTEGER and SMALLINT data types                                        | |
| E011-02    | REAL, DOUBLE PRECISION, and FLOAT data types                           | |
| E011-03    | DECIMAL and NUMERIC data types                                         | |
| E011-04    | Arithmetic operators                                                   | |
| E011-05    | Numeric comparison                                                     | |
| E011-06    | Implicit casting among the numeric data types                          | |
| E021       | Character data types                                                   | |
| E021-01    | CHARACTER data type                                                    | |
| E021-02    | CHARACTER VARYING data type                                            | |
| E021-03    | Character literals                                                     | |
| E021-04    | CHARACTER_LENGTH function                                              | |
| E021-05    | OCTET_LENGTH function                                                  | |
| E021-06    | SUBSTRING function                                                     | |
| E021-07    | Character concatenation                                                | |
| E021-08    | UPPER and LOWER functions                                              | |
| E021-09    | TRIM function                                                          | |
| E021-10    | Implicit casting among the character string types                      | |
| E021-11    | POSITION function                                                      | |
| E021-12    | Character comparison                                                   | |
| E031       | Identifiers                                                            | |
| E031-01    | Delimited identifiers                                                  | |
| E031-02    | Lower case identifiers                                                 | |
| E031-03    | Trailing underscore                                                    | |
| E051       | Basic query specification                                              | |
| E051-01    | SELECT DISTINCT                                                        | |
| E051-02    | GROUP BY clause                                                        | |
| E051-04    | GROUP BY can contain columns not in select list                        | |
| E051-05    | Select list items can be renamed                                       | |
| E051-06    | HAVING clause                                                          | |
| E051-07    | Qualified * in select list                                             | |
| E051-08    | Correlation names in the FROM clause                                   | |
| E051-09    | Rename columns in the FROM clause                                      | |
| E061       | Basic predicates and search conditions                                 | |
| E061-01    | Comparison predicate                                                   | |
| E061-02    | BETWEEN predicate                                                      | |
| E061-03    | IN predicate with list of values                                       | |
| E061-04    | LIKE predicate                                                         | |
| E061-05    | LIKE predicate ESCAPE clause                                           | |
| E061-06    | NULL predicate                                                         | |
| E061-07    | Quantified comparison predicate                                        | |
| E061-08    | EXISTS predicate                                                       | |
| E061-09    | Subqueries in comparison predicate                                     | |
| E061-11    | Subqueries in IN predicate                                             | |
| E061-12    | Subqueries in quantified comparison predicate                          | |
| E061-13    | Correlated subqueries                                                  | |
| E061-14    | Search condition                                                       | |
| E071       | Basic query expressions                                                | |
| E071-01    | UNION DISTINCT table operator                                          | |
| E071-02    | UNION ALL table operator                                               | |
| E071-03    | EXCEPT DISTINCT table operator                                         | |
| E071-05    | Columns combined via table operators need not have exactly the same data type | |
| E071-06    | Table operators in subqueries                                          | |
| E081       | Basic Privileges                                                       | |
| E081-01    | SELECT privilege                                                       | |
| E081-02    | DELETE privilege                                                       | |
| E081-03    | INSERT privilege at the table level                                    | |
| E081-04    | UPDATE privilege at the table level                                    | |
| E081-05    | UPDATE privilege at the column level                                   | |
| E081-06    | REFERENCES privilege at the table level                                | |
| E081-07    | REFERENCES privilege at the column level                               | |
| E081-08    | WITH GRANT OPTION                                                      | |
| E081-09    | USAGE privilege                                                        | |
| E081-10    | EXECUTE privilege                                                      | |
| E091       | Set functions                                                          | |
| E091-01    | AVG                                                                    | |
| E091-02    | COUNT                                                                  | |
| E091-03    | MAX                                                                    | |
| E091-04    | MIN                                                                    | |
| E091-05    | SUM                                                                    | |
| E091-06    | ALL quantifier                                                         | |
| E091-07    | DISTINCT quantifier                                                    | |

E1 {#conformance-e1}
---

| Identifier | Description                                                            | Comments |
|:-----------|:-----------------------------------------------------------------------|:---------|
| E101       | Basic data manipulation                                                | |
| E101-01    | INSERT statement                                                       | |
| E101-03    | Searched UPDATE statement                                              | |
| E101-04    | Searched DELETE statement                                              | |
| E111       | Single row SELECT statement                                            | |
| E121       | Basic cursor support                                                   | |
| E121-01    | DECLARE CURSOR                                                         | |
| E121-02    | ORDER BY columns need not be in select list                            | |
| E121-03    | Value expressions in ORDER BY clause                                   | |
| E121-04    | OPEN statement                                                         | |
| E121-06    | Positioned UPDATE statement                                            | |
| E121-07    | Positioned DELETE statement                                            | |
| E121-08    | CLOSE statement                                                        | |
| E121-10    | FETCH statement implicit NEXT                                          | |
| E121-17    | WITH HOLD cursors                                                      | |
| E131       | Null value support (nulls in lieu of values)                           | |
| E141       | Basic integrity constraints                                            | |
| E141-01    | NOT NULL constraints                                                   | |
| E141-02    | UNIQUE constraints of NOT NULL columns                                 | |
| E141-03    | PRIMARY KEY constraints                                                | |
| E141-04    | Basic FOREIGN KEY constraint with the NO ACTION default for both referential delete action and referential update action | |
| E141-06    | CHECK constraints                                                      | |
| E141-07    | Column defaults                                                        | |
| E141-08    | NOT NULL inferred on PRIMARY KEY                                       | |
| E141-10    | Names in a foreign key can be specified in any order                   | |
| E151       | Transaction support                                                    | |
| E151-01    | COMMIT statement                                                       | |
| E151-02    | ROLLBACK statement                                                     | |
| E152       | Basic SET TRANSACTION statement                                        | |
| E152-01    | SET TRANSACTION statement: ISOLATION LEVEL SERIALIZABLE clause         | |
| E152-02    | SET TRANSACTION statement: READ ONLY and READ WRITE clauses            | |
| E153       | Updatable queries with subqueries                                      | |
| E161       | SQL comments using leading double minus                                | |
| E171       | SQLSTATE support                                                       | |
| E182       | Module language                                                        | |

F {#conformance-f}
===

F0 {#conformance-f0}
---

| Identifier | Description                                                            | Comments |
|:-----------|:-----------------------------------------------------------------------|:---------|
| F021       | Basic information schema                                               | |
| F021-01    | COLUMNS view                                                           | |
| F021-02    | TABLES view                                                            | |
| F021-03    | VIEWS view                                                             | |
| F021-04    | TABLE_CONSTRAINTS view                                                 | |
| F021-05    | REFERENTIAL_CONSTRAINTS view                                           | |
| F021-06    | CHECK_CONSTRAINTS view                                                 | |
| F031       | Basic schema manipulation                                              | |
| F031-01    | CREATE TABLE statement to create persistent base tables                | |
| F031-02    | CREATE VIEW statement                                                  | |
| F031-03    | GRANT statement                                                        | |
| F031-04    | ALTER TABLE statement: ADD COLUMN clause                               | |
| F031-13    | DROP TABLE statement: RESTRICT clause                                  | |
| F031-16    | DROP VIEW statement: RESTRICT clause                                   | |
| F031-19    | REVOKE statement: RESTRICT clause                                      | |
| F032       | CASCADE drop behavior                                                  | |
| F033       | ALTER TABLE statement: DROP COLUMN clause                              | |
| F034       | Extended REVOKE statement                                              | |
| F034-01    | REVOKE statement performed by other than the owner of a schema object  | |
| F034-02    | REVOKE statement: GRANT OPTION FOR clause                              | |
| F034-03    | REVOKE statement to revoke a privilege that the grantee has WITH GRANT OPTION | |
| F041       | Basic joined table                                                     | |
| F041-01    | Inner join (but not necessarily the INNER keyword)                     | |
| F041-02    | INNER keyword                                                          | |
| F041-03    | LEFT OUTER JOIN                                                        | |
| F041-04    | RIGHT OUTER JOIN                                                       | |
| F041-05    | Outer joins can be nested                                              | |
| F041-07    | The inner table in a left or right outer join can also be used in an inner join | |
| F041-08    | All comparison operators are supported (rather than just =)            | |
| F051       | Basic date and time                                                    | |
| F051-01    | DATE data type (including support of DATE literal)                     | |
| F051-02    | TIME data type (including support of TIME literal) with fractional seconds precision of at least 0 | |
| F051-03    | TIMESTAMP data type (including support of TIMESTAMP literal) with fractional seconds precision of at least 0 and 6 | |
| F051-04    | Comparison predicate on DATE, TIME, and TIMESTAMP data types           | |
| F051-05    | Explicit CAST between datetime types and character string types        | |
| F051-06    | CURRENT_DATE                                                           | |
| F051-07    | LOCALTIME                                                              | |
| F051-08    | LOCALTIMESTAMP                                                         | |
| F052       | Intervals and datetime arithmetic                                      | |
| F053       | OVERLAPS predicate                                                     | |
| F054       | TIMESTAMP in DATE type precedence list                                 | |
| F081       | UNION and EXCEPT in views                                              | |

F1 {#conformance-f1}
---

| Identifier  | Description                                                            | Comments |
|:------------|:-----------------------------------------------------------------------|:---------|
| \d{F111}    | Isolation levels other than SERIALIZABLE                               | |
| \d{F111-01} | READ UNCOMMITTED isolation level                                       | |
| \d{F111-02} | READ COMMITTED isolation level                                         | |
| \d{F111-03} | REPEATABLE READ isolation level                                        | |
| \d{F121}    | Basic diagnostics management                                           | |
| \d{F121-01} | GET DIAGNOSTICS statement                                              | |
| \d{F121-02} | SET TRANSACTION statement: DIAGNOSTICS SIZE clause                     | |
| \d{F122}    | Enhanced diagnostics management                                        | |
| \d{F123}    | All diagnostics                                                        | |
| \d{F131}    | Grouped operations                                                     | |
| \d{F131-01} | WHERE, GROUP BY, and HAVING clauses supported in queries with grouped views | |
| \d{F131-02} | Multiple tables supported in queries with grouped views                | |
| \d{F131-03} | Set functions supported in queries with grouped views                  | |
| \d{F131-04} | Subqueries with GROUP BY and HAVING clauses and grouped views          | |
| \d{F131-05} | Single row SELECT with GROUP BY and HAVING clauses and grouped views   | |
| \d{F171}    | Multiple schemas per user                                              | |
| \d{F181}    | Multiple module support                                                | |
| \d{F191}    | Referential delete actions                                             | |

F2 {#conformance-f2}
---

| Identifier | Description                                          | Comments |
|:-----------|:-----------------------------------------------------|:---------|
| F200       | TRUNCATE TABLE statement                             | |
| _F201_     | _CAST function_                                      | |
| F202       | TRUNCATE TABLE: identity column restart option       | |
| F221       | Explicit defaults                                    | |
| F222       | INSERT statement: DEFAULT VALUES clause              | |
| F231       | Privilege tables                                     | |
| F231-01    | TABLE_PRIVILEGES view                                | |
| F231-02    | COLUMN_PRIVILEGES view                               | |
| F231-03    | USAGE_PRIVILEGES view                                | |
| F251       | Domain support                                       | |
| F261       | CASE expression                                      | |
| F261-01    | Simple CASE                                          | |
| F261-02    | Searched CASE                                        | |
| F261-03    | NULLIF                                               | |
| F261-04    | COALESCE                                             | |
| F262       | Extended CASE expression                             | |
| F263       | Comma-separated predicates in simple CASE expression | |
| F271       | Compound character literals                          | |
| F281       | LIKE enhancements                                    | |
| F291       | UNIQUE predicate                                     | |

F3 {#conformance-f3}
---

| Identifier | Description                                   | Comments |
|:-----------|:----------------------------------------------|:---------|
| F301       | CORRESPONDING in query expressions            | |
| F302       | INTERSECT table operator                      | |
| F302-01    | INTERSECT DISTINCT table operator             | |
| F302-02    | INTERSECT ALL table operator                  | |
| F304       | EXCEPT ALL table operator                     | |
| F311       | Schema definition statement                   | |
| F311-01    | CREATE SCHEMA                                 | [EDB-3](https://elliot.atlassian.net/browse/EDB-3) |
| _F311-02_  | _CREATE TABLE for persistent base tables_     | |
| F311-03    | CREATE VIEW                                   | |
| F311-04    | CREATE VIEW: WITH CHECK OPTION                | |
| F311-05    | GRANT statement                               | |
| F312       | MERGE statement                               | |
| F313       | Enhanced MERGE statement                      | |
| F314       | MERGE statement with DELETE branch            | |
| F321       | User authorization                            | |
| F341       | Usage tables                                  | |
| F361       | Subprogram support                            | |
| F381       | Extended schema manipulation                  | |
| F381-01    | ALTER TABLE statement: ALTER COLUMN clause    | |
| F381-02    | ALTER TABLE statement: ADD CONSTRAINT clause  | |
| F381-03    | ALTER TABLE statement: DROP CONSTRAINT clause | |
| F382       | Alter column data type                        | |
| F383       | Set column not null clause                    | |
| F384       | Drop identity property clause                 | |
| F385       | Drop column generation expression clause      | |
| F386       | Set identity column generation clause         | |
| F391       | Long identifiers                              | |
| F392       | Unicode escapes in identifiers                | |
| F393       | Unicode escapes in literals                   | |
| F394       | Optional normal form specification            | |

F4 {#conformance-f4}
---

| Identifier | Description                                        | Comments |
|:-----------|:---------------------------------------------------|:---------|
| F401       | Extended joined table                              | |
| F401-01    | NATURAL JOIN                                       | |
| F401-02    | FULL OUTER JOIN                                    | |
| F401-04    | CROSS JOIN                                         | |
| F402       | Named column joins for LOBs, arrays, and multisets | |
| F403       | Partitioned joined tables                          | |
| F411       | Time zone specification                            | |
| F421       | National character                                 | |
| F431       | Read-only scrollable cursors                       | |
| F431-01    | FETCH with explicit NEXT                           | |
| F431-02    | FETCH FIRST                                        | |
| F431-03    | FETCH LAST                                         | |
| F431-04    | FETCH PRIOR                                        | |
| F431-05    | FETCH ABSOLUTE                                     | |
| F431-06    | FETCH RELATIVE                                     | |
| F441       | Extended set function support                      | |
| F442       | Mixed column references in set functions           | |
| F451       | Character set definition                           | |
| F461       | Named character sets                               | |
| F471       | Scalar subquery values                             | |
| F481       | Expanded NULL predicate                            | |
| F491       | Constraint management                              | |
| F492       | Optional table constraint enforcement              | |

F5 {#conformance-f5}
---

| Identifier | Description                    | Comments |
|:-----------|:-------------------------------|:---------|
| F501       | Features and conformance views | |
| F501-01    | SQL_FEATURES view              | |
| F501-02    | SQL_SIZING view                | |
| F501-03    | SQL_LANGUAGES view             | |
| F502       | Enhanced documentation tables  | |
| F502-01    | SQL_SIZING_PROFILES view       | |
| F502-02    | SQL_IMPLEMENTATION_INFO view   | |
| F502-03    | SQL_PACKAGES view              | |
| F521       | Assertions                     | |
| F531       | Temporary tables               | |
| F555       | Enhanced seconds precision     | |
| F561       | Full value expressions         | |
| F571       | Truth value tests              | |
| F591       | Derived tables                 | |

F6 {#conformance-f6}
---

| Identifier | Description                              | Comments |
|:-----------|:-----------------------------------------|:---------|
| F611       | Indicator data types                     | |
| F641       | Row and table constructors               | |
| F651       | Catalog name qualifiers                  | |
| F661       | Simple tables                            | |
| F671       | Subqueries in CHECK                      | |
| F672       | Retrospective check constraints          | |
| F690       | Collation support                        | |
| F692       | Extended collation support               | |
| F693       | SQL-session and client module collations | |
| F695       | Translation support                      | |
| F696       | Additional translation documentation     | |

F7 {#conformance-f7}
---

| Identifier | Description                 | Comments |
|:-----------|:----------------------------|:---------|
| \d{F701}   | Referential update actions  | |
| \d{F711}   | ALTER domain                | |
| \d{F721}   | Deferrable constraints      | |
| \d{F731}   | INSERT column privileges    | |
| \d{F741}   | Referential MATCH types     | |
| \d{F751}   | View CHECK enhancements     | |
| \d{F761}   | Session management          | |
| \d{F762}   | CURRENT_CATALOG             | |
| \d{F763}   | CURRENT_SCHEMA              | |
| \d{F771}   | Connection management       | |
| \d{F781}   | Self-referencing operations | |
| \d{F791}   | Insensitive cursors         | |

F8 {#conformance-f8}
---

| Identifier | Description                                        | Comments |
|:-----------|:---------------------------------------------------|:---------|
| F801       | Full set function                                  | |
| F812       | Basic flagging                                     | |
| F813       | Extended flagging                                  | |
| F821       | Local table references                             | |
| F831       | Full cursor update                                 | |
| F831-01    | Updatable scrollable cursors                       | |
| F831-02    | Updatable ordered cursors                          | |
| F841       | LIKE_REGEX predicate                               | |
| F842       | OCCURENCES_REGEX function                          | |
| F843       | POSITION_REGEX function                            | |
| F844       | SUBSTRING_REGEX function                           | |
| F845       | TRANSLATE_REGEX function                           | |
| F846       | Octet support in regular expression operators      | |
| F847       | Nonconstant regular expressions                    | |
| F850       | Top-level order by clause in query expression      | |
| F851       | order by clause in subqueries                      | |
| F852       | Top-level order by clause in views                 | |
| F855       | Nested order by clause in query expression         | |
| F856       | Nested fetch first clause in query expression      | |
| F857       | Top-level fetch first clause in query expression   | |
| F858       | fetch first clause in subqueries                   | |
| F859       | Top-level fetch first clause in views              | |
| F860       | fetch first row count in fetch first clause        | |
| F861       | Top-level result offset clause in query expression | |
| F862       | result offset clause in subqueries                 | |
| F863       | Nested result offset clause in query expression    | |
| F864       | Top-level result offset clause in views            | |
| F865       | offset row count in result offset clause           | |
| F866       | FETCH FIRST clause: PERCENT option                 | |
| F867       | FETCH FIRST clause: WITH TIES option               | |

M {#conformance-m}
===

M0 {#conformance-m0}
---

| Identifier | Description                                        | Comments |
|:-----------|:---------------------------------------------------|:---------|
| \d{M001}   | Datalinks                                          | |
| \d{M002}   | Datalinks via SQL/CLI                              | |
| \d{M003}   | Datalinks via Embedded SQL                         | |
| \d{M004}   | Foreign data support                               | |
| \d{M005}   | Foreign schema support                             | |
| \d{M006}   | GetSQLString routine                               | |
| \d{M007}   | TransmitRequest                                    | |
| \d{M009}   | GetOpts and GetStatistics routines                 | |
| \d{M010}   | Foreign data wrapper support                       | |
| \d{M011}   | Datalinks via Ada                                  | |
| \d{M012}   | Datalinks via C                                    | |
| \d{M013}   | Datalinks via COBOL                                | |
| \d{M014}   | Datalinks via Fortran                              | |
| \d{M015}   | Datalinks via M                                    | |
| \d{M016}   | Datalinks via Pascal                               | |
| \d{M017}   | Datalinks via PL/I                                 | |
| \d{M018}   | Foreign data wrapper interface routines in Ada     | |
| \d{M019}   | Foreign data wrapper interface routines in C       | |
| \d{M020}   | Foreign data wrapper interface routines in COBOL   | |
| \d{M021}   | Foreign data wrapper interface routines in Fortran | |
| \d{M022}   | Foreign data wrapper interface routines in MUMPS   | |
| \d{M023}   | Foreign data wrapper interface routines in Pascal  | |
| \d{M024}   | Foreign data wrapper interface routines in PL/I    | |
| \d{M030}   | SQL-server foreign data support                    | |
| \d{M031}   | Foreign data wrapper general routines              | |

S {#conformance-s}
===

S0 {#conformance-s0}
---

| Identifier  | Description                                    | Comments |
|:------------|:-----------------------------------------------|:---------|
| \d{S011}    | Distinct data types                            | |
| \d{S011-01} | USER_DEFINED_TYPES view                        | |
| \d{S023}    | Basic structured types                         | |
| \d{S024}    | Enhanced structured types                      | |
| \d{S025}    | Final structured types                         | |
| \d{S026}    | Self-referencing structured types              | |
| \d{S027}    | Create method by specific method name          | |
| \d{S028}    | Permutable UDT options list                    | |
| \d{S041}    | Basic reference types                          | |
| \d{S043}    | Enhanced reference types                       | |
| \d{S051}    | Create table of type                           | |
| \d{S071}    | SQL paths in function and type name resolution | |
| \d{S081}    | Subtables                                      | |
| \d{S091}    | Basic array support                            | |
| \d{S091-01} | Arrays of built-in data types                  | |
| \d{S091-02} | Arrays of distinct types                       | |
| \d{S091-03} | Array expressions                              | |
| \d{S092}    | Arrays of user-defined types                   | |
| \d{S094}    | Arrays of reference types                      | |
| \d{S095}    | Array constructors by query                    | |
| \d{S096}    | Optional array bounds                          | |
| \d{S097}    | Array element assignment                       | |
| \d{S098}    | ARRAY_AGG                                      | |

S1 {#conformance-s1}
---

| Identifier | Description                      | Comments |
|:-----------|:---------------------------------|:---------|
| S111       | ONLY in query expressions        | |
| S151       | Type predicate                   | |
| S161       | Subtype treatment                | |
| S162       | Subtype treatment for references | |

S2 {#conformance-s2}
---

| Identifier  | Description                       | Comments |
|:------------|:----------------------------------|:---------|
| \d{S201}    | SQL-invoked routines on arrays    | |
| \d{S201-01} | Array parameters                  | |
| \d{S201-02} | Array as result type of functions | |
| \d{S202}    | SQL-invoked routines on multisets | |
| \d{S211}    | User-defined cast functions       | |
| \d{S231}    | Structured type locators          | |
| \d{S232}    | Array locators                    | |
| \d{S233}    | Multiset locators                 | |
| \d{S241}    | Transform functions               | |
| \d{S242}    | Alter transform statement         | |
| \d{S251}    | User-defined orderings            | |
| \d{S261}    | Specific type method              | |
| \d{S271}    | Basic multiset support            | |
| \d{S272}    | Multisets of user-defined types   | |
| \d{S274}    | Multisets of reference types      | |
| \d{S275}    | Advanced multiset support         | |
| \d{S281}    | Nested collection types           | |
| \d{S291}    | Unique constraint on entire row   | |

S3 {#conformance-s3}
---

| Identifier | Description     | Comments |
|:-----------|:----------------|:---------|
| \d{S301}   | Enhanced UNNEST | |

S4 {#conformance-s4}
---

| Identifier | Description                            | Comments |
|:-----------|:---------------------------------------|:---------|
| S401       | Distinct types based on array types    | |
| S402       | Distinct types based on distinct types | |
| S403       | ARRAY_MAX_CARDINALITY                  | |
| S404       | TRIM_ARRAY                             | |

T {#conformance-t}
===

T0 {#conformance-t0}
---

| Identifier | Description                                                                      | Comments |
|:-----------|:---------------------------------------------------------------------------------|:---------|
| T011       | Timestamp in Information Schema                                                  | |
| T021       | BINARY and VARBINARY data types                                                  | |
| T022       | Advanced support for BINARY and VARBINARY data types                             | |
| T023       | Compound binary literal                                                          | |
| T024       | Spaces in binary literals                                                        | |
| T031       | BOOLEAN data type                                                                | |
| T041       | Basic LOB data type support                                                      | |
| T041-01    | BLOB data type                                                                   | |
| T041-02    | CLOB data type                                                                   | |
| T041-03    | POSITION, LENGTH, LOWER, TRIM, UPPER, and SUBSTRING functions for LOB data types | |
| T041-04    | Concatenation of LOB data types                                                  | |
| T041-05    | LOB locator: non-holdable                                                        | |
| T042       | Extended LOB data type support                                                   | |
| T043       | Multiplier T                                                                     | |
| T044       | Multiplier P                                                                     | |
| T051       | Row types                                                                        | |
| T052       | MAX and MIN for row types                                                        | |
| T053       | Explicit aliases for all-fields reference                                        | |
| T061       | UCS support                                                                      | |
| T071       | BIGINT data type                                                                 | |

T1 {#conformance-t1}
---

| Identifier | Description                                       | Comments |
|:-----------|:--------------------------------------------------|:---------|
| T101       | Enhanced nullability determination                | |
| T111       | Updatable joins, unions, and columns              | |
| T121       | WITH (excluding RECURSIVE) in query expression    | |
| T122       | WITH (excluding RECURSIVE) in subquery            | |
| T131       | Recursive query                                   | |
| T132       | Recursive query in subquery                       | |
| T141       | SIMILAR predicate                                 | |
| T151       | DISTINCT predicate                                | |
| T152       | DISTINCT predicate with negation                  | |
| T171       | LIKE clause in table definition                   | |
| T172       | AS subquery clause in table definition            | |
| T173       | Extended LIKE clause in table definition          | |
| T174       | Identity columns                                  | |
| T175       | Generated columns                                 | |
| T176       | Sequence generator support                        | |
| T177       | Sequence generator support: simple restart option | |
| T178       | Identity columns: simple restart option           | |
| T180       | System-versioned tables                           | |
| T181       | Application-time period tables                    | |
| T191       | Referential action RESTRICT                       | |

T2 {#conformance-t2}
---

| Identifier  | Description                                                            | Comments |
|:------------|:-----------------------------------------------------------------------|:---------|
| \d{T201}    | Comparable data types for referential constraints                      | |
| \d{T211}    | Basic trigger capability                                               | |
| \d{T211-01} | Triggers activated on UPDATE, INSERT, or DELETE of one base table      | |
| \d{T211-02} | BEFORE triggers                                                        | |
| \d{T211-03} | AFTER triggers                                                         | |
| \d{T211-04} | FOR EACH ROW triggers                                                  | |
| \d{T211-05} | Ability to specify a search condition that must be true before the trigger is invoked | |
| \d{T211-06} | Support for run-time rules for the interaction of triggers and constraints | |
| \d{T211-07} | TRIGGER privilege                                                      | |
| \d{T211-08} | Multiple triggers for the same event are executed in the order in which they were created in the catalog | |
| \d{T212}    | Enhanced trigger capability                                            | |
| \d{T213}    | INSTEAD OF triggers                                                    | |
| \d{T231}    | Sensitive cursors                                                      | |
| \d{T241}    | START TRANSACTION statement                                            | |
| \d{T251}    | SET TRANSACTION statement: LOCAL option                                | |
| \d{T261}    | Chained transactions                                                   | |
| \d{T271}    | Savepoints                                                             | |
| \d{T272}    | Enhanced savepoint management                                          | |
| \d{T281}    | SELECT privilege with column granularity                               | |
| \d{T285}    | Enhanced derived column names                                          | |

T3 {#conformance-t3}
---

| Identifier | Description                                         | Comments |
|:-----------|:----------------------------------------------------|:---------|
| T301       | Functional dependencies                             | |
| T312       | OVERLAY function                                    | |
| T321       | Basic SQL-invoked routines                          | |
| T321-01    | User-defined functions with no overloading          | |
| T321-02    | User-defined stored procedures with no overloading  | |
| T321-03    | Function invocation                                 | |
| T321-04    | CALL statement                                      | |
| T321-05    | RETURN statement                                    | |
| T321-06    | ROUTINES view                                       | |
| T321-07    | PARAMETERS view                                     | |
| T322       | Declared data type attributes                       | |
| T323       | Explicit security for external routines             | |
| T324       | Explicit security for SQL routines                  | |
| T325       | Qualified SQL parameter references                  | |
| T326       | Table functions                                     | |
| T331       | Basic roles                                         | |
| T332       | Extended roles                                      | |
| T341       | Overloading of SQL-invoked functions and procedures | |
| T351       | Bracketed SQL comments                              | |

T4 {#conformance-t4}
---

| Identifier | Description                           | Comments |
|:-----------|:--------------------------------------|:---------|
| T431       | Extended grouping capabilities        | |
| T432       | Nested and concatenated GROUPING SETS | |
| T433       | Multiargument GROUPING function       | |
| T434       | GROUP BY DISTINCT                     | |
| T441       | ABS and MOD functions                 | |
| T461       | Symmetric BETWEEN predicate           | |
| T471       | Result sets return value              | |
| T472       | DESCRIBE CURSOR                       | |
| T491       | LATERAL derived table                 | |
| T495       | Combined data change and retrieval    | |

T5 {#conformance-t5}
---

| Identifier | Description                                                | Comments |
|:-----------|:-----------------------------------------------------------|:---------|
| T501       | Enhanced EXISTS predicate                                  | |
| T502       | Period predicates                                          | |
| T511       | Transaction counts                                         | |
| T521       | Named arguments in CALL statement                          | |
| T522       | Default values for IN parameters of SQL-invoked procedures | |
| T541       | Updatable table references                                 | |
| T551       | Optional key words for default syntax                      | |
| T561       | Holdable locators                                          | |
| T571       | Array-returning external SQL-invoked functions             | |
| T572       | Multiset-returning external SQL-invoked functions          | |
| T581       | Regular expression substring function                      | |
| T591       | UNIQUE constraints of possibly null columns                | |

T6 {#conformance-t6}
---

| Identifier | Description                                      | Comments |
|:-----------|:-------------------------------------------------|:---------|
| \d{T601}   | Local cursor references                          | |
| \d{T611}   | Elementary OLAP operations                       | |
| \d{T612}   | Advanced OLAP operations                         | |
| \d{T613}   | Sampling                                         | |
| \d{T614}   | NTILE function                                   | |
| \d{T615}   | LEAD and LAG functions                           | |
| \d{T616}   | Null treatment option for LEAD and LAG functions | |
| \d{T617}   | FIRST_VALUE and LAST_VALUE function              | |
| \d{T618}   | NTH_VALUE function                               | |
| \d{T619}   | Nested window functions                          | |
| \d{T620}   | WINDOW clause: GROUPS option                     | |
| \d{T621}   | Enhanced numeric functions                       | |
| \d{T631}   | IN predicate with one list element               | |
| \d{T641}   | Multiple column assignment                       | |
| \d{T651}   | SQL-schema statements in SQL routines            | |
| \d{T652}   | SQL-dynamic statements in SQL routines           | |
| \d{T653}   | SQL-schema statements in external routines       | |
| \d{T654}   | SQL-dynamic statements in external routines      | |
| \d{T655}   | Cyclically dependent routines                    | |

X {#conformance-x}
===

X0 {#conformance-x0}
---

| Identifier | Description                                                      | Comments |
|:-----------|:-----------------------------------------------------------------|:---------|
| \d{X010}   | XML type                                                         | |
| \d{X011}   | Arrays of XML type                                               | |
| \d{X012}   | Multisets of XML type                                            | |
| \d{X013}   | Distinct types of XML type                                       | |
| \d{X014}   | Attributes of XML type                                           | |
| \d{X015}   | Fields of XML type                                               | |
| \d{X016}   | Persistent XML values                                            | |
| \d{X020}   | XMLConcat                                                        | |
| \d{X025}   | XMLCast                                                          | |
| \d{X030}   | XMLDocument                                                      | |
| \d{X031}   | XMLElement                                                       | |
| \d{X032}   | XMLForest                                                        | |
| \d{X034}   | XMLAgg                                                           | |
| \d{X035}   | XMLAgg: ORDER BY option                                          | |
| \d{X036}   | XMLComment                                                       | |
| \d{X037}   | XMLPI                                                            | |
| \d{X038}   | XMLText                                                          | |
| \d{X040}   | Basic table mapping                                              | |
| \d{X041}   | Basic table mapping: nulls absent                                | |
| \d{X042}   | Basic table mapping: null as nil                                 | |
| \d{X043}   | Basic table mapping: table as forest                             | |
| \d{X044}   | Basic table mapping: table as element                            | |
| \d{X045}   | Basic table mapping: with target namespace                       | |
| \d{X046}   | Basic table mapping: data mapping                                | |
| \d{X047}   | Basic table mapping: metadata mapping                            | |
| \d{X048}   | Basic table mapping: base64 encoding of binary strings           | |
| \d{X049}   | Basic table mapping: hex encoding of binary strings              | |
| \d{X050}   | Advanced table mapping                                           | |
| \d{X051}   | Advanced table mapping: nulls absent                             | |
| \d{X052}   | Advanced table mapping: null as nil                              | |
| \d{X053}   | Advanced table mapping: table as forest                          | |
| \d{X054}   | Advanced table mapping: table as element                         | |
| \d{X055}   | Advanced table mapping: target namespace                         | |
| \d{X056}   | Advanced table mapping: data mapping                             | |
| \d{X057}   | Advanced table mapping: metadata mapping                         | |
| \d{X058}   | Advanced table mapping: base64 encoding of binary strings        | |
| \d{X059}   | Advanced table mapping: hex encoding of binary strings           | |
| \d{X060}   | XMLParse: Character string input and CONTENT option              | |
| \d{X061}   | XMLParse: Character string input and DOCUMENT option             | |
| \d{X065}   | XMLParse: BLOB input and CONTENT option                          | |
| \d{X066}   | XMLParse: BLOB input and DOCUMENT option                         | |
| \d{X068}   | XMLSerialize: BOM                                                | |
| \d{X069}   | XMLSerialize: INDENT                                             | |
| \d{X070}   | XMLSerialize: Character string serialization and CONTENT option  | |
| \d{X071}   | XMLSerialize: Character string serialization and DOCUMENT option | |
| \d{X072}   | XMLSerialize: Character string serialization                     | |
| \d{X073}   | XMLSerialize: BLOB serialization and CONTENT option              | |
| \d{X074}   | XMLSerialize: BLOB serialization and DOCUMENT option             | |
| \d{X075}   | XMLSerialize: BLOB serialization                                 | |
| \d{X076}   | XMLSerialize: VERSION                                            | |
| \d{X077}   | XMLSerialize: explicit ENCODING option                           | |
| \d{X078}   | XMLSerialize: explicit XML declaration                           | |
| \d{X080}   | Namespaces in XML publishing                                     | |
| \d{X081}   | Query-level XML namespace declarations                           | |
| \d{X082}   | XML namespace declarations in DML                                | |
| \d{X083}   | XML namespace declarations in DDL                                | |
| \d{X084}   | XML namespace declarations in compound statements                | |
| \d{X085}   | Predefined namespace prefixes                                    | |
| \d{X086}   | XML namespace declarations in XMLTable                           | |
| \d{X090}   | XML document predicate                                           | |
| \d{X091}   | XML content predicate                                            | |
| \d{X096}   | XMLExists                                                        | |

X1 {#conformance-x1}
---

| Identifier | Description                                               | Comments |
|:-----------|:----------------------------------------------------------|:---------|
| \d{X100}   | Host language support for XML: CONTENT option             | |
| \d{X101}   | Host language support for XML: DOCUMENT option            | |
| \d{X110}   | Host language support for XML: VARCHAR mapping            | |
| \d{X111}   | Host language support for XML: CLOB mapping               | |
| \d{X112}   | Host language support for XML: BLOB mapping               | |
| \d{X113}   | Host language support for XML: STRIP WHITESPACE option    | |
| \d{X114}   | Host language support for XML: PRESERVE WHITESPACE option | |
| \d{X120}   | XML parameters in SQL routines                            | |
| \d{X121}   | XML parameters in external routines                       | |
| \d{X131}   | Query-level XMLBINARY clause                              | |
| \d{X132}   | XMLBINARY clause in DML                                   | |
| \d{X133}   | XMLBINARY clause in DDL                                   | |
| \d{X134}   | XMLBINARY clause in compound statements                   | |
| \d{X135}   | XMLBINARY clause in subqueries                            | |
| \d{X141}   | IS VALID predicate: data-driven case                      | |
| \d{X142}   | IS VALID predicate: ACCORDING TO clause                   | |
| \d{X143}   | IS VALID predicate: ELEMENT clause                        | |
| \d{X144}   | IS VALID predicate: schema location                       | |
| \d{X145}   | IS VALID predicate outside check constraints              | |
| \d{X151}   | IS VALID predicate with DOCUMENT option                   | |
| \d{X152}   | IS VALID predicate with CONTENT option                    | |
| \d{X153}   | IS VALID predicate with SEQUENCE option                   | |
| \d{X155}   | IS VALID predicate: NAMESPACE without ELEMENT clause      | |
| \d{X157}   | IS VALID predicate: NO NAMESPACE with ELEMENT clause      | |
| \d{X160}   | Basic Information Schema for registered XML Schemas       | |
| \d{X161}   | Advanced Information Schema for registered XML Schemas    | |
| \d{X170}   | XML null handling options                                 | |
| \d{X171}   | NIL ON NO CONTENT option                                  | |
| \d{X181}   | XML(DOCUMENT(UNTYPED)) type                               | |
| \d{X182}   | XML(DOCUMENT(ANY)) type                                   | |
| \d{X190}   | XML(SEQUENCE) type                                        | |
| \d{X191}   | XML(DOCUMENT(XMLSCHEMA)) type                             | |
| \d{X192}   | XML(CONTENT(XMLSCHEMA)) type                              | |

X2 {#conformance-x2}
---

| Identifier | Description                                            | Comments |
|:-----------|:-------------------------------------------------------|:---------|
| \d{X200}   | XMLQuery                                               | |
| \d{X201}   | XMLQuery: RETURNING CONTENT                            | |
| \d{X202}   | XMLQuery: RETURNING SEQUENCE                           | |
| \d{X203}   | XMLQuery: passing a context item                       | |
| \d{X204}   | XMLQuery: initializing an XQuery variable              | |
| \d{X205}   | XMLQuery: EMPTY ON EMPTY option                        | |
| \d{X206}   | XMLQuery: NULL ON EMPTY option                         | |
| \d{X211}   | XML 1.1 support                                        | |
| \d{X221}   | XML passing mechanism BY VALUE                         | |
| \d{X222}   | XML passing mechanism BY REF                           | |
| \d{X231}   | XML(CONTENT(UNTYPED)) type                             | |
| \d{X232}   | XML(CONTENT(ANY)) type                                 | |
| \d{X241}   | RETURNING CONTENT in XML publishing                    | |
| \d{X242}   | RETURNING SEQUENCE in XML publishing                   | |
| \d{X251}   | Persistent XML values of XML(DOCUMENT(UNTYPED)) type   | |
| \d{X252}   | Persistent XML values of XML(DOCUMENT(ANY)) type       | |
| \d{X253}   | Persistent XML values of XML(CONTENT(UNTYPED)) type    | |
| \d{X254}   | Persistent XML values of XML(CONTENT(ANY)) type        | |
| \d{X255}   | Persistent XML values of XML(SEQUENCE) type            | |
| \d{X256}   | Persistent XML values of XML(DOCUMENT(XMLSCHEMA)) type | |
| \d{X257}   | Persistent XML values of XML(CONTENT(XMLSCHEMA)) type  | |
| \d{X260}   | XML type: ELEMENT clause                               | |
| \d{X261}   | XML type: NAMESPACE without ELEMENT clause             | |
| \d{X263}   | XML type: NO NAMESPACE with ELEMENT clause             | |
| \d{X264}   | XML type: schema location                              | |
| \d{X271}   | XMLValidate: data-driven case                          | |
| \d{X272}   | XMLValidate: ACCORDING TO clause                       | |
| \d{X273}   | XMLValidate: ELEMENT clause                            | |
| \d{X274}   | XMLValidate: schema location                           | |
| \d{X281}   | XMLValidate: with DOCUMENT option                      | |
| \d{X282}   | XMLValidate with CONTENT option                        | |
| \d{X283}   | XMLValidate with SEQUENCE option                       | |
| \d{X284}   | XMLValidate NAMESPACE without ELEMENT clause           | |
| \d{X286}   | XMLValidate: NO NAMESPACE with ELEMENT clause          | |

X3 {#conformance-x3}
---

| Identifier | Description                               | Comments |
|:-----------|:------------------------------------------|:---------|
| \d{X300}   | XMLTable                                  | |
| \d{X301}   | XMLTable: derived column list option      | |
| \d{X302}   | XMLTable: ordinality column option        | |
| \d{X303}   | XMLTable: column default option           | |
| \d{X304}   | XMLTable: passing a context item          | |
| \d{X305}   | XMLTable: initializing an XQuery variable | |

X4 {#conformance-x4}
---

| Identifier | Description                      | Comments |
|:-----------|:---------------------------------|:---------|
| \d{X400}   | Name and identifier mapping      | |
| \d{X410}   | Alter column data type: XML type | |
