Development & Testing {#development}
=====================

The build pipelines is broken into three discreet stages. The process can only continue if all the jobs in that stage successfully pass.

![Build Pipeline](img/BuildPipeline.svg)

[TOC]

Build Pipeline
--------------

### Compile

There are three targets compiled.

#### eagle

This is the main production executable. It will be compiled with all the production settings and this binary will be transfered to the last stage to package up the RPM.

To pass this job the the executable is compiled with strict requirements;

* All warnings that are available for the platform are turned on.
* All warnings are errors.

#### eagle_test

This is the legacy CUNIT tests. Slowly these will be converted to Google Test over time and this target will be removed.

To pass this job all the unit tests must pass.

#### test

This is the newer Google Test suite.

To pass this job all the unit tests must pass.

### Testing

Once the targets have been compiled they are ready to undergo testing.

#### CLI Tests

Traditional command-line tests simulate a user typing in statments and assert the result returned by the exectuable. This is not testing SQL statements (that is done with unit tests).

#### Unit Tests

Possibly the most important. This is a combination of all the unit tests run by `eagle_test` and `test`

#### Memory Leaks

This does the same thing as Unit Tests but verifies (`valgrind` on Linux, `leaks` on Mac) to make sure that no memory leaks occur after all the unit tests are run.

#### Documention

Run by doxygen. Generates HTML and XML. The XML is processed through a script that find faults in the docuemtnation like elements that are not documented.

#### Code Coverage

Running all unit tests and generated a HTML report. It is a requirement to pass this job that the core application be 100% covered.

### Package

#### Build RPM

Package the executable into a binary RPM that can be installed on Linux distributions.

Running Locally
---------------

Ultimately the final say on testing is Bamboo; which once all testing succeeds the code (feature branch) can be integrated into the continuous stable branch.

The feedback is provided fairly quickly by Bamboo-usually within minutes-sometimes you will want to be able to run the same tests locally.

Each of the jobs can be run inderpendantly with the following syntax:

    OS=bamboo.<kernel> make <target>
    
`<os>` is `linux` or `darwin` (Mac).

`<target>` can be one of the following:

| Stage   | Job           | Target                 |
|---------|---------------|------------------------|
| Compile | eagle         | `bin/eagle`            |
| Compile | eagle_test    | `bin/eagle_test`       |
| Compile | test          | `bin/test`             |
| Testing | CLI Tests     | `bamboo-cli-tests`     |
| Testing | Unit Tests    | `bamboo-unit-tests`    |
| Testing | Memory Leaks  | `bamboo-memory-leaks`  |
| Testing | Documentation | `bamboo-documentation` |
| Testing | Code Coverage | `bamboo-code-coverage` |
| Package | Build RPM     | `bamboo-build-rpm`     |

Some examples would be:

Clean all, then run code coverage on mac:

    OS=bamboo.darwin make clean bamboo-code-coverage

Compile the production executable on linux:

    OS=bamboo.linux make bin/eagle
