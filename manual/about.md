About {#about}
=====

[TOC]

Introduction {#about-intro}
============

Traditionally relational databases are row-oriented. Each row of the table can be variable length (depending on the column data types and their content) and it is stored on the disk sequentially. Most databases contain these rows in "pages" which is a block of fixed size that is always written to the disk atomically. This method is a good trade off of space efficiency and speed. The problem is rows (and data for that matter) can not be easily located without reading through many variable length rows until you find the one, or many you want. Filtering (aka WHERE clause) is almost never done on all the columns in your table, so it often doesn't make sense to read a row, dissect it and filter just the data you need.

Column-orientated databases take a different approach. Each column is always a fixed length (for variable length data like text the column is just a pointer to the data in another file). Each column is represented in its own file. Pages are still used in much the same way but pages are sized by the amount or records they contain not a fixed byte size, any can change dynamically for each query. Records can be located immediately by record ID by simply multiplying the size of a field by the number of offset records. The other advantage is that data can be read and processed in bulk much faster. Since you are only reading the data you want and can process it thousands of records at a time instead of one filtering is much faster and can be parallelized much easier.

So why don't all databases use this technique if it's so great? Well, it does have some drawbacks;

Hardware Has Changed {#about-hardware}
====================

When the mature relational databases of today were first written the hardware environments were very different. CPUs were scaling up, not out (CPUs were getting faster but stayed a single core for a long time). Disk IO was considered and assumed to be much more expensive since mechanical disks were the norm (now SSDs are becoming more prevalent). How the engine of a database works is so fundamental is would require the core database engine to be rewritten completely from the ground up so older databases can't simple adopt or incorporate column based data.

In a column-orriented database the major expense comes in the form of fetching the "rest of the tuple", for example:

```sql
SELECT * FROM people WHERE age > 18;
```
 
If the `people` table contains many columns this data must be fetched after the filtering. More columns means more expense. This is a big problem for high latency storage like mechanical disks because fetching a single page often takes thousands of times longer than the actual processing time. But SSDs basically remove this problem.

Software Concepts Have Changed {#about-software}
==============================

The era of scaling CPUs up is long over. In fact, CPUs are being made progressively slower to save on power and CPU dies are containing more cores - overall the CPUs are getting faster but the software that runs on them has to be fundamentally different to adapt to this multicore nature.

Databases of today use a one-connection-one-process methodology (for the purpose of explaining, threads are the same as processes). This for the most part works great on fast CPUs with few users; but it's a ticking time bomb for the future;

1. Less connections than cores: If you have a 16-core CPU and only one person using the database at one time you can only offer them a fraction of the hardware performance since the task will almost exclusively be on a single thread/process.

2. More connections than cores: Thread switching is expensive, but worse, it is not linear. If your CPU has 4 cores and you have 40 people running a query on your database each core will be juggling 10 threads, trying to do all 10 things at one time - but a core can only do one thing at one time so it wastes a lot of time switching between the context of each thread/process (this is thread switching). So to finish the 10 tasks it may take 15 times as long because 30% of the CPU time is spent thread switching to try and perform all tasks at once. But it gets worse, each extra thread adds **more** overhead, so 20 threads is more than twice as slow as 10 threads. If you tried to pump thousands of simultaneous connections into a database the CPU would be so busy trying to get everything done at once it would in fact get nothing done and every client would suffer.

3. Sleeping connections: Even if you have thousands of clients connected to your database only a small percentage will be actually doing something. But having thousands of threads/processes lying around is still a massive drain on performance (especially memory). To prevent your CPU dying from to much concurrency an extra layer is put in place between the database and your clients. It is a connection pool. It is in charge of handling thousands of client connections and relaying the active tasks through much less connections to the actual database daemon.

How would these problems be solved using more modern techniques? The eagle engine is designed from the ground up to avoid these problems;

1. Non-blocking IO. Blocking databases (which is all of them) require a new thread of process per connection so that each connection has its own little "worker". Nonblocking IO can run 1 or 1000 clients on a single thread by queuing and dispatching tasks as its sees fit to other workers. The number of workers is automatic depending on the amount of cores you have available, this prevents the need for thread switching with many connections and connections that are not doing anything take zero resources so theres also no need for a connection pool layer.

2. Event stack. Rather than large continuous blocks of work like "find all records with age > 18" all tasks are split into pieces and added onto a global stack for dispatch. This makes sure all workers (cores) are kept busy as long as there is work to be done. Also prevents thread switching, each core can concentrate on getting a single part of a large task done as efficiently as possible without ever being slowed down byt he number of tasks it ultimately must do.

3. Parallel Processing. Almost all database tasks can be done in parallel, filtering, sorting, etc. So eagle does this, seamlessly. By using the event stack it splits continuous or long tasks into smaller tasks in the event stack. This means tasks that take longer, simply take longer, they do not affect the speed of quicker operations happening at the time.