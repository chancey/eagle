INSERT {#sqlinsert}
======

[TOC]

Syntax {#sqlinsert-syntax}
======

    INSERT INTO table_name (columns) VALUES (values)
