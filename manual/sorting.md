Sorting {#sorting}
=======

[TOC]
 
Overview {#sorting-overview}
========

Eagle supports two algorithms for sorting. The query planner will decide which one to use on a number of factors. The
two types it will decide from are;

- [Entire Sort](#sorting-entiresort)
- [Top Sort](#sorting-topsort)


Entire Sort {#sorting-entiresort}
===========

Entire sort, as the name suggests is used when the whole result of a provider needs to be sorted. The basic algorithm
is as follows:

![Entire Sort](img/EntireSort.svg)


Top Sort {#sorting-topsort}
========

Top sort works on the principle that you are likely to only need a subset of a large returned result set. It's basic
algorithm works as follows:

![Top Sort](img/TopSort.svg)


The Top Sort Provider
---------------------

Extends the Steam Provider with the following extra attributes:

    TYPE minimumValue
    TYPE maximumValue


Next Page
---------

Read the next page from the provider.


Find Min/Max
------------

This is just a simple loop to find the minimum and maximum value of the page, using:

    TYPE min = page[0]
    TYPE max = page[0]
    FOR each item in page, skip first
        IF item < min THEN
             min = item
        ENDIF
        IF item > max THEN
             max = item
        ENDIF
    ENDFOR


Get Top Min/Max
---------------

Ask the sort provider for the current minimum and maximum value, this might have to be synchronized but since it is
read-only a pair of `volatile` variables might suffice.


Page in Range?
--------------

Using the min and max values calculated from the previous step, as well as the min and max values provided by the
Sort provider this can test using the following condition:

    TYPE pageMin provided by previous step
    TYPE topMax provided by the previous step
    IF pageMin >= topMax THEN
        this page can be safely discared and the worker job is now finished
    ELSE
        the page intersects with that the top data, and must continue to be processed
    ENDIF


Sort Page
---------

Using quick sort the page is sorted internally.


Truncate Page
-------------

Each page contains two attributes, the allocated size (real size) and the count (the number of items in the page).
Since the page is now sorted we can reduce the count such that the last item now becomes less than the maximum value
provided by the Sort Provider.

    TYPE topMax
    INT count = 0
    request the topMax from the Sort Provider
    FOR each item in page
        IF item >= topMax THEN
            ENDFOR
        ENDIF
        incremement count by 1
    ENDFOR
    set page count to count


Merger Busy?
------------

Check if the Merger is already locked (currently working). Only one worker can be occupying the Merge task at a given
time, this presents a natural bottle neck for all the other worker waiting to have their pages insert into the Top set
so they can go back and fetch more data.

To get around this issue a worker will first check if the worker is busy;

- If it is then this worker continues to perform Insert Sort Page
- If it is not then the worker must add the variable sized pages onto a Buffer. The Buffer is a preallocated page is
  (the number of workers * the maximum page size)


Insert Sort Page
----------------

A very simple way for two pre-sorted lists to be merged together disregarding the values that are outside the Top size.

    INT i = 0
    INT topCursor = 0
    INT pageCursor = 0

    SYNCHRONIZE
        WHILE i < Top Size THEN
            -- find the smallest value between the next two items
            IF topBuffer[topCursor] < pageBuffer[pageCursor] THEN
                finalBuffer[i] = topBuffer[topCursor]
                increment i
                increment topCursor
            ELSE
                finalBuffer[i] = pageBuffer[pageCursor]
                increment i
                increment pageCursor
            ENDIF
        ENDWHILE
    ENDSYNCHRONIZE


Buffer Full?
------------

The Buffer as described in "Merger Busy?"

Get Min/Max
-----------

This step is separated from Find Min/Max because it can be sure the page has already been through a process that has
sorted it, and therefore the first and last item will be the minimum and maximum value respectivly.
