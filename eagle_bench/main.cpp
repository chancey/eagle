#include <iostream>
#include "eagle/test/performance/TestSuite.h"
#include "eagle/test/performance/OrderByTest.h"

int main(int argc, const char * argv[])
{
    eagle::test::performance::TestSuite suite;
    suite.testCases->addObject(new eagle::test::performance::OrderByTest(), false, NULL);
    suite.runAll();
    
    return 0;
}
