#include <stdio.h>
#include <stdlib.h>
#include "eagle/test/performance/TestCase.h"
#include "EagleDbInstance.h"
#include "eagle/Calendar.h"

eagle::test::performance::TestCase::TestCase(int theCores)
{
    cores = theCores;
}

void eagle::test::performance::TestCase::start()
{
    for(int i = 0; i < runRepeat(); ++i) {
        int pageSize = 1000;
        instance = EagleDbInstance_New(pageSize, cores);
        instance->printResults = false;
        
        unsigned long start = eagle::Calendar::getInstance()->getAbsoluteTime();
        this->init();
        unsigned long end = eagle::Calendar::getInstance()->getAbsoluteTime();
        printf("Init: %f seconds\n", (double) (end - start) / (double) eagle::Calendar::TicksPerSecond);
        
        start = eagle::Calendar::getInstance()->getAbsoluteTime();
        this->run();
        end = eagle::Calendar::getInstance()->getAbsoluteTime();
        printf(" Run: %f seconds\n", (double) (end - start) / (double) eagle::Calendar::TicksPerSecond);
        
        EagleDbInstance_Delete(instance);
    }
}

int eagle::test::performance::TestCase::runRepeat()
{
    return 1;
}

void eagle::test::performance::TestCase::init()
{
    // do nothing
}

eagle::Plan* eagle::test::performance::TestCase::runSql(const char* sql)
{
    EagleLogger::Event *error = NULL;
    eagle::Plan *plan = NULL;
    bool success = EagleDbInstance_execute(instance, sql, &error, &plan);
    if(!success) {
        printf("Failed to run SQL: %s\n", sql);
        printf("            Error: %s\n", error->getMessage());
        exit(1);
    }
    return plan;
}

eagle::test::performance::TestCase::~TestCase()
{
}
