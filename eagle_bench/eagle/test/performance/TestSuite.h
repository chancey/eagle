#ifndef eagle_test_performance_TestSuite_h
#define eagle_test_performance_TestSuite_h

#include "EagleUnsynchronizedLinkedList.h"

namespace eagle
{
    namespace test
    {
        namespace performance
        {
            class TestSuite;
            class TestCase;
        }
    }
}

class eagle::test::performance::TestSuite
{
    
public:
    
    TestSuite();
    
    EagleUnsynchronizedLinkedList<eagle::test::performance::TestCase*> *testCases;
    
    virtual void runAll();
    
};

#endif
