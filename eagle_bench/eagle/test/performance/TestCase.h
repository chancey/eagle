#ifndef eagle_test_performance_TestCase_h
#define eagle_test_performance_TestCase_h

class EagleDbInstance;

namespace eagle
{
    class Plan;
    
    namespace test
    {
        namespace performance
        {
            class TestCase;
        }
    }
}

class eagle::test::performance::TestCase
{
    
protected:
    
    EagleDbInstance *instance;
    
    eagle::Plan* runSql(const char* sql);
    
    int cores;
    
public:
    
    TestCase(int theCores);

    void start();
    
    virtual int runRepeat();
    
    virtual void init();
    
    virtual void run() = 0;
    
    virtual ~TestCase();
    
};

#endif
