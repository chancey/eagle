#include <stdio.h>
#include <stdlib.h>
#include "eagle/test/performance/TestSuite.h"
#include "EagleUnsynchronizedLinkedList.h"
#include "eagle/test/performance/TestCase.h"

eagle::test::performance::TestSuite::TestSuite()
{
    testCases = new EagleUnsynchronizedLinkedList<eagle::test::performance::TestCase*>();
}

void eagle::test::performance::TestSuite::runAll()
{
    EagleLinkedList_Foreach(testCases, eagle::test::performance::TestCase*, testCase)
    {
        testCase->start();
    }
    EagleLinkedList_ForeachEnd
}
