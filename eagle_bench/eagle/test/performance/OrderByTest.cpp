#include <stdio.h>
#include <stdlib.h>
#include "eagle/test/performance/OrderByTest.h"
#include "eagle/Plan.h"
#include "EagleDbInstance.h"
#include "eagle/ArrayPageProvider.h"
#include "eagle/StreamPageProvider.h"

eagle::test::performance::OrderByTest::OrderByTest() : eagle::test::performance::TestCase(2)
{
}

void eagle::test::performance::OrderByTest::init()
{
    int size = 1000000;
    
    // create the table
    runSql("CREATE TABLE mytable (col1 INT, col2 FLOAT);");
    
    // traditional insert (slow)
    
    for(int i = 0; i < size; ++i) {
        char sql[1024];
        sprintf(sql, "INSERT INTO mytable (col1, col2) VALUES (%d, %g);", rand() % 10000, 0.1 * (double) (rand() % 10000));
        runSql(sql);
    }
    
    // StreamPageProvider (fast)
    
    /*EagleDbSchema *defaultSchema = EagleDbInstance_getSchema(instance, EagleDbSchema::DefaultSchemaName);
    eagle::db::TableData *td = EagleDbSchema_getTable(defaultSchema, "mytable");
    td->providers = new eagle::PageProvider*[2];
    
    td->providers[0] = new eagle::StreamPageProvider(EagleData::Type::Integer, instance->pageSize, (char*) "col1");
    td->providers[1] = new eagle::StreamPageProvider(EagleData::Type::Integer, instance->pageSize, (char*) "col2");
    
    EagleDataTypeInteger *a = EagleData::CreateInt(rand() % 10000);
    for(int i = 0; i < size; ++i) {
        td->providers[0]->add(a);
        td->providers[1]->add(a);
    }*/
    
    // ArrayPageProvider (very fast)
    
    /*EagleDbSchema *defaultSchema = EagleDbInstance_getSchema(instance, EagleDbSchema::DefaultSchemaName);
    eagle::db::TableData *td = EagleDbSchema_getTable(defaultSchema, "mytable");
    td->providers = new eagle::PageProvider*[2];
    
    EagleDataTypeInteger *col1 = new EagleDataTypeInteger[size];
    EagleDataTypeFloat *col2 = new EagleDataTypeFloat[size];
    td->providers[0] = new eagle::ArrayPageProvider(EagleData::Type::Integer, col1, size, instance->pageSize, (char*) "col1", true);
    td->providers[1] = new eagle::ArrayPageProvider(EagleData::Type::Float, col2, size, instance->pageSize, (char*) "col2", true);
    
    for(int i = 0; i < size; ++i) {
        col1[i] = rand() % 10000;
        col2[i] = 0.1 * (double) (rand() % 10000);
    }*/
}

void eagle::test::performance::OrderByTest::run()
{
    //eagle::Plan *plan = runSql("SELECT col2, col1 FROM mytable;");
    //printf("Records returned: %d\n", plan->stages->last()->result[1]->nextPage()->count);
    
    eagle::Plan *plan = runSql("SELECT col2, col1 FROM mytable ORDER BY col1;");
    //printf("Records returned: %d\n", plan->stages->last()->result[0]->nextPage()->count);
    delete plan;
}

int eagle::test::performance::OrderByTest::runRepeat()
{
    return 2;
}
