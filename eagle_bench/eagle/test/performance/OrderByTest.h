#ifndef eagle_test_performance_OrderByTest_h
#define eagle_test_performance_OrderByTest_h

#include "eagle/test/performance/TestCase.h"

namespace eagle
{
    namespace test
    {
        namespace performance
        {
            class OrderByTest;
        }
    }
}

class eagle::test::performance::OrderByTest : public eagle::test::performance::TestCase
{
    
public:
    
    OrderByTest();
    
    void init();
    
    virtual void run();
    
    virtual int runRepeat();
    
};

#endif
