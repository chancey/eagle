#include "TestSuite.h"
#include <stdlib.h>

CUnitTest* CUnitTest_New(char *strName, CU_TestFunc pTestFunc)
{
    CUnitTest *test = (CUnitTest*) malloc(sizeof(CUnitTest));
    
    test->strName = strName;
    test->pTestFunc = pTestFunc;
    
    return test;
}

CUnitTests* CUnitTests_New(int allocatedTests)
{
    CUnitTests *tests = (CUnitTests*) malloc(sizeof(CUnitTests));
    
    tests->allocatedTests = allocatedTests;
    tests->usedTests = 0;
    tests->tests = (CUnitTest**) calloc((size_t) tests->allocatedTests, sizeof(CUnitTest*));
    
    return tests;
}

void CUnitTests_addTest(CUnitTests *tests, CUnitTest *test)
{
    if(tests->usedTests >= tests->allocatedTests) {
        printf("Suite isn't big enough (suite has limit of %d)!\n", tests->allocatedTests);
        exit(1);
    }
    
    // CUnit has a bug that it does not escape XML, fix this now
    unsigned long newLen = strlen(test->strName) + 1;
    for(int i = 0; i < strlen(test->strName); ++i) {
        if(test->strName[i] == '<' || test->strName[i] == '>') {
            newLen += 3;
        }
    }
    
    char *newName = (char*) malloc(newLen);
    newName[0] = 0;
    for(int i = 0; i < strlen(test->strName); ++i) {
        switch(test->strName[i]) {
                
            case '<':
                strcat(newName, "&lt;");
                break;
                
            case '>':
                strcat(newName, "&gt;");
                break;
                
            default:
                strncat(newName, test->strName + i, 1);
                
        }
    }
    
    test->strName = newName; 
    tests->tests[tests->usedTests++] = test;
}

void CUnitTest_Delete(CUnitTest *obj)
{
    free(obj->strName);
    delete obj;
}

void CUnitTests_Delete(CUnitTests *obj)
{
    for(int i = 0; i < obj->usedTests; ++i) {
        CUnitTest_Delete(obj->tests[i]);
    }
    delete obj->tests;
    delete obj;
}
