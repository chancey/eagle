#include "TestSuite.h"
#include <CUnit/Automated.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "suites/MainSuite.h"
#include "suites/DBSuite.h"
#include "suites/SQLSuite.h"
#include "eagle/EagleLogger.h"
#include "suites/SQLFuzzSuite.h"
#include "suites/OperatorSuite.h"
#include "suites/FunctionSuite.h"

typedef enum {
    CUnitModeAutomated = 1,
    CUnitModeBasic = 2
} CUnitMode;

typedef struct CUnitOptions {
    bool run_leaks;
    bool suite_main;
    bool suite_db;
    bool suite_sql;
    bool suite_sqlfuzz;
    bool suite_operator;
    bool suite_function;
    CUnitMode mode;
} CUnitOptions;

CUnitOptions CUnitOptions_New()
{
    CUnitOptions co;
    
    co.run_leaks = false;
    co.suite_main = false;
    co.suite_db = false;
    co.suite_sql = false;
    co.suite_sqlfuzz = false;
    co.suite_operator = false;
    co.suite_function = false;
    co.mode = CUnitModeBasic;
    
    return co;
}

CU_ErrorCode addSuiteSingle(const char *name, CU_InitializeFunc pInit, CU_CleanupFunc pClean, CUnitTests* (*testsFunction)())
{
    // add a suite to the registry
    CU_pSuite pSuite = CU_add_suite(name, pInit, pClean);
    if(NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    
    // add the tests to the suite
    CUnitTests *tests = testsFunction();
    for(int i = 0; i < tests->usedTests; ++i) {
        CUnitTest *test = tests->tests[i];
        if(NULL == CU_add_test(pSuite, test->strName, test->pTestFunc)) {
            CU_cleanup_registry();
            return CU_get_error();
        }
    }
    
    // clean up
    CUnitTests_Delete(tests);
    return CUE_SUCCESS;
}

CU_ErrorCode addSuite(const char *name, CU_InitializeFunc pInit, CU_CleanupFunc pClean, CU_pSuite *pSuite)
{
    // add a suite to the registry
    *pSuite = CU_add_suite(name, pInit, pClean);
    if(NULL == *pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    
    return CUE_SUCCESS;
}

CU_ErrorCode addSuiteTests(CU_pSuite pSuite, CUnitTests* (*testsFunction)())
{
    // add the tests to the suite
    CUnitTests *tests = testsFunction();
    for(int i = 0; i < tests->usedTests; ++i) {
        CUnitTest *test = tests->tests[i];
        if(NULL == CU_add_test(pSuite, test->strName, test->pTestFunc)) {
            CU_cleanup_registry();
            return CU_get_error();
        }
    }
    
    // clean up
    CUnitTests_Delete(tests);
    return CUE_SUCCESS;
}

int mainRun(CUnitOptions op)
{
    // initialize the CUnit test registry
    if (CUE_SUCCESS != CU_initialize_registry()) {
        return CU_get_error();
    }
    
    // add suites
    CU_pSuite suite;
    if(true == op.suite_main) {
        if(CUE_SUCCESS != addSuite("MainSuite", MainSuite_init, MainSuite_clean, &suite) ||
           CUE_SUCCESS != addSuiteTests(suite, MainSuite1_tests) ||
           CUE_SUCCESS != addSuiteTests(suite, MainSuite2_tests) ||
           CUE_SUCCESS != addSuiteTests(suite, MainSuite3_tests) ||
           CUE_SUCCESS != addSuiteTests(suite, MainSuite4_tests)) {
            return CU_get_error();
        }
    }
    
    if(true == op.suite_db) {
        if(CUE_SUCCESS != addSuite("DBSuite", DBSuite_init, DBSuite_clean, &suite) ||
           CUE_SUCCESS != addSuiteTests(suite, DBSuite1_tests) ||
           CUE_SUCCESS != addSuiteTests(suite, DBSuite2_tests) ||
           CUE_SUCCESS != addSuiteTests(suite, DBSuite3_tests)) {
            return CU_get_error();
        }
    }
    
    if(true == op.suite_sql && CUE_SUCCESS != addSuiteSingle("SQLSuite", SQLSuite_init, SQLSuite_clean, SQLSuite_tests)) {
        return CU_get_error();
    }
    
    if(true == op.suite_sqlfuzz && CUE_SUCCESS != addSuiteSingle("SQLFuzzSuite", SQLFuzzSuite_init, SQLFuzzSuite_clean, SQLFuzzSuite_tests)) {
        return CU_get_error();
    }
    
    if(true == op.suite_operator && CUE_SUCCESS != addSuiteSingle("OperatorSuite", OperatorSuite_init, OperatorSuite_clean, OperatorSuite_tests)) {
        return CU_get_error();
    }
    
    if(true == op.suite_function && CUE_SUCCESS != addSuiteSingle("FunctionSuite", FunctionSuite_init, FunctionSuite_clean, FunctionSuite_tests)) {
        return CU_get_error();
    }
    
    // Run all tests using the CUnit Basic interface
    CU_basic_set_mode(CU_BRM_NORMAL);
    CU_set_error_action(CUEA_ABORT);
    
    switch(op.mode) {
            
        case CUnitModeAutomated:
            CU_set_output_filename("build/CUnit");
            CU_automated_run_tests();
            CU_list_tests_to_file();
            break;
            
        case CUnitModeBasic:
            CU_basic_run_tests();
            break;
            
    }
    
    int exitCode = CU_get_error();
    return exitCode;
}

/**
 * The main() function for setting up and running the tests.
 * Returns a CUE_SUCCESS on successful running, another CUnit error code on failure.
 */
int main(int argc, char **argv)
{
    // usage
    if(argc < 2) {
        fprintf(stderr, "\n");
        fprintf(stderr, "Usage: eagle_test [--run-leaks] [--all-suites] [--suite-X] [--exclude-X]\n");
        fprintf(stderr, "         --all-suites  Run all test suites.\n");
        fprintf(stderr, "         --run-leaks   Run 'leaks' tool (Mac OS X only).\n");
        fprintf(stderr, "         --suite-X     X can be:\n");
        fprintf(stderr, "         --exclude-X     main\n");
        fprintf(stderr, "                         db\n");
        fprintf(stderr, "                         sql\n");
        fprintf(stderr, "                         memory\n");
        fprintf(stderr, "                         sqlfuzz\n");
        fprintf(stderr, "                         operator\n");
        fprintf(stderr, "                         function\n");
        fprintf(stderr, "\n");
        exit(1);
    }
    
    // options
    CUnitOptions op = CUnitOptions_New();
    
    for(int i = 1; i < argc; ++i) {
        if(!strcmp(argv[i], "--run-leaks")) {
            op.run_leaks = true;
        }
        else if(!strcmp(argv[i], "--suite-main")) {
            op.suite_main = true;
        }
        else if(!strcmp(argv[i], "--suite-db")) {
            op.suite_db = true;
        }
        else if(!strcmp(argv[i], "--suite-sql")) {
            op.suite_sql = true;
        }
        else if(!strcmp(argv[i], "--suite-sqlfuzz")) {
            op.suite_sqlfuzz = true;
        }
        else if(!strcmp(argv[i], "--suite-operator")) {
            op.suite_operator = true;
        }
        else if(!strcmp(argv[i], "--suite-function")) {
            op.suite_function = true;
        }
        else if(!strcmp(argv[i], "--all-suites")) {
            op.suite_main = true;
            op.suite_db = true;
            op.suite_sql = true;
            op.suite_sqlfuzz = true;
            op.suite_operator = true;
            op.suite_function = true;
        }
        else if(!strcmp(argv[i], "--exclude-main")) {
            op.suite_main = false;
        }
        else if(!strcmp(argv[i], "--exclude-db")) {
            op.suite_db = false;
        }
        else if(!strcmp(argv[i], "--exclude-sql")) {
            op.suite_sql = false;
        }
        else if(!strcmp(argv[i], "--exclude-sqlfuzz")) {
            op.suite_sqlfuzz = false;
        }
        else if(!strcmp(argv[i], "--exclude-operator")) {
            op.suite_operator = false;
        }
        else if(!strcmp(argv[i], "--exclude-function")) {
            op.suite_function = false;
        }
        else if(!strcmp(argv[i], "--mode=basic")) {
            op.mode = CUnitModeBasic;
        }
        else if(!strcmp(argv[i], "--mode=automated")) {
            op.mode = CUnitModeAutomated;
        }
        else {
            fprintf(stderr, "\nUnknown option \"%s\"\n\n", argv[i]);
            exit(1);
        }
    }
    
    int exitCode = mainRun(op);
    delete EagleLogger::Get();
    
    if(op.run_leaks) {
        char cmd[1024];
        sprintf(cmd, "leaks %d -nocontext", getpid());
        int leaksCode = system(cmd);
        
        // the tests pass but there was memory leaks
        if(0 != leaksCode) {
            return 2;
        }
    }
    
    return exitCode;
}
