#ifndef eagle_FunctionSuite_h
#define eagle_FunctionSuite_h

#include "TestSuite.h"
#include <CUnit/Basic.h>
#include "eagle/EaglePage.h"
#include "OperatorSuite.h"

int FunctionSuite_init();
CUnitTests* FunctionSuite_tests();
int FunctionSuite_clean();

#endif
