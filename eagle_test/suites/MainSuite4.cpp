#include "MainSuite.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "eagle/EagleLinkedList.h"
#include "eagle/Plan.h"
#include "eagle/Worker.h"
#include "eagle/EagleLogger.h"
#include "eagle/Random.h"
#include "eagle/EagleSynchronizedLinkedList.h"
#include "eagle/EagleUnsynchronizedLinkedList.h"
#include "eagle/plan/Operation.h"
#include "eagle/ArrayPageProvider.h"
#include "eagle/page/Sort.h"
#include "eagle/plan/job/SortPage.h"
#include "eagle/plan/stage/EntireSort.h"
#include "eagle/SinglePageProvider.h"
#include "eagle/plan/job/MergePages.h"
#include "eagle/Instance.h"
#include "eagle/Object.h"
#include "eagle/Worker.h"

CUNIT_TEST(MainSuite, EagleLinkedList_toString, Simple)
{
    EagleLinkedList<const char*> *list = new EagleSynchronizedLinkedList<const char*>();
    list->addObject("some", false, NULL);
    list->addObject("cool", false, NULL);
    list->addObject("strings", false, NULL);
    
    std::string join = list->toString(NULL, ",");
    CUNIT_VERIFY_EQUAL_STRING(join.c_str(), "some,cool,strings");
    
    delete list;
}

char* _crop(char *string)
{
    string[strlen(string) - 1] = 0;
    return string;
}

CUNIT_TEST(MainSuite, EagleLinkedList_toString, Custom)
{
    EagleLinkedList<char*> *list = new EagleSynchronizedLinkedList<char*>();
    list->addObject(strdup("some"), false, NULL);
    list->addObject(strdup("cool"), false, NULL);
    list->addObject(strdup("strings"), false, NULL);
    
    std::string join = list->toString((char* (*)(char*)) _crop, ":");
    CUNIT_VERIFY_EQUAL_STRING(join.c_str(), "som:coo:string");
    
    delete list;
}

CUNIT_TEST(MainSuite, EagleData, NameToTypeNull)
{
    CUNIT_VERIFY_EQUAL_INT(EagleData::NameToType(NULL), EagleData::Type::Unknown);
}

CUNIT_TEST(MainSuite, EagleWorker, runJobFunction)
{
    int pageSize = 1;
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, pageSize, 1);
    selectStage->prepareBuffers(2);
    
    eagle::plan::job::SelectJob *job = new eagle::plan::job::SelectJob(selectStage);
    job->buffers[0] = EaglePage::AllocFloat(pageSize);
    job->buffers[1] = EaglePage::AllocFloat(pageSize);
    
    int *args = (int*) calloc(sizeof(int), 1);
    args[0] = 1;
    eagle::plan::Operation *epo = eagle::plan::Operation::NewWithFunction(EaglePageOperations_SqrtPageFloat, 0, args, 1, true, "dummy");
    selectStage->addOperation(epo, false);
    
    job->runJob();
    
    delete job;
    delete selectStage;
    delete epo;
}

typedef struct {
    
    EagleLinkedList<EagleDataTypeInteger*> *list;
    
    EagleDataTypeInteger **stuff;
    
    int totalObjects;
    
} MainSuite_EagleLinkedList_ThreadData;

void* MainSuite_EagleLinkedList_Thread(void *arg)
{
    // in this thread make lots of modifications to the linked list
    MainSuite_EagleLinkedList_ThreadData *data = (MainSuite_EagleLinkedList_ThreadData*) arg;
    
    // run modifications
    for(int i = 0; i < data->totalObjects; ++i) {
        switch(eagle::Random::GetDefault()->nextIntInRange(0, 2)) {
                
            case 0:
                data->list->addObject(data->stuff[i], false, NULL);
                break;
                
            case 1:
            {
                EagleDataTypeInteger *obj = data->stuff[eagle::Random::GetDefault()->nextIntInRange(0, data->totalObjects - 1)];
                data->list->deleteObject(obj);
                break;
            }
                
            case 2:
            {
                int len = data->list->length();
                EagleDataTypeInteger *obj = ((EagleLinkedList<EagleDataTypeInteger*>*) data->list)->get(eagle::Random::GetDefault()->nextIntInRange(0, len - 1));
                if(NULL != obj && (*obj < 0 || *obj >= data->totalObjects)) {
                    CUNIT_FAIL("Invalid returned number: %d", *obj);
                }
                break;
            }
                
        }
    }
    
    return (void*) data;
}

void MainSuite_EagleLinkedList_ThreadTest(int threads, int totalObjects, EagleLinkedList<EagleDataTypeInteger*> *list)
{
    pthread_t *t = (pthread_t*) calloc(threads, sizeof(pthread_t));
    int rc;
    
    // some random data
    EagleDataTypeInteger **stuff = (EagleDataTypeInteger**) calloc(totalObjects, sizeof(EagleDataTypeInteger*));
    for(int i = 0; i < totalObjects; ++i) {
        stuff[i] = EagleData::CreateInt(i);
    }
    
    // start
    for(int i = 0; i < threads; ++i) {
        MainSuite_EagleLinkedList_ThreadData *data = (MainSuite_EagleLinkedList_ThreadData*) malloc(sizeof(MainSuite_EagleLinkedList_ThreadData));
        data->totalObjects = totalObjects;
        data->stuff = stuff;
        data->list = list;
        
        rc = pthread_create(&(t[i]), NULL, &MainSuite_EagleLinkedList_Thread, (void*) data);
        CUNIT_ASSERT_EQUAL_INT(rc, 0);
    }
    
    // wait for them to complete
    for(int i = 0; i < threads; ++i) {
        MainSuite_EagleLinkedList_ThreadData *data = NULL;
        rc = pthread_join(t[i], (void**) &data);
        CUNIT_ASSERT_EQUAL_INT(rc, 0);
        
        // clean up
        free(data);
    }
    
    // clean up
    delete list;
    for(int i = 0; i < totalObjects; ++i) {
        free(stuff[i]);
    }
    free(stuff);
    free(t);
}

CUNIT_TEST(MainSuite, EagleLinkedList, Sync)
{
    EagleLinkedList<EagleDataTypeInteger*> *list = new EagleSynchronizedLinkedList<EagleDataTypeInteger*>();
    MainSuite_EagleLinkedList_ThreadTest(10, 1000, list);
}

CUNIT_TEST(MainSuite, EagleLinkedList, NoSync)
{
    EagleLinkedList<EagleDataTypeInteger*> *list = new EagleUnsynchronizedLinkedList<EagleDataTypeInteger*>();
    
    /* since this is not synchronized we can only use one thread */
    MainSuite_EagleLinkedList_ThreadTest(1, 1000, list);
}

CUNIT_TEST(MainSuite, EagleLinkedList_first, Never)
{
    EagleLinkedList<EagleDataTypeInteger*> *list = new EagleUnsynchronizedLinkedList<EagleDataTypeInteger*>();
    CUNIT_VERIFY_NULL(list->first());
    delete list;
}

CUNIT_TEST(MainSuite, EagleLinkedList_last, Never)
{
    EagleLinkedList<EagleDataTypeInteger*> *list = new EagleUnsynchronizedLinkedList<EagleDataTypeInteger*>();
    CUNIT_VERIFY_NULL(list->last());
    delete list;
}

CUNIT_TEST(MainSuite, EagleLinkedList_begin, Never)
{
    EagleLinkedList<EagleDataTypeInteger*> *list = new EagleUnsynchronizedLinkedList<EagleDataTypeInteger*>();
    CUNIT_VERIFY_NULL(list->begin());
    delete list;
}

CUNIT_TEST(MainSuite, EagleLinkedList_end, Never)
{
    EagleLinkedList<EagleDataTypeInteger*> *list = new EagleUnsynchronizedLinkedList<EagleDataTypeInteger*>();
    CUNIT_VERIFY_NULL(list->end());
    delete list;
}

CUNIT_TEST(MainSuite, EagleLinkedList_isEmpty, Never)
{
    EagleLinkedList<EagleDataTypeInteger*> *list = new EagleUnsynchronizedLinkedList<EagleDataTypeInteger*>();
    CUNIT_VERIFY_TRUE(list->isEmpty());
    delete list;
}

CUNIT_TEST(MainSuite, EagleLinkedList_toArray, Never)
{
    EagleLinkedList<EagleDataTypeInteger*> *list = new EagleUnsynchronizedLinkedList<EagleDataTypeInteger*>();
    int size = 0;
    EagleDataTypeInteger **array = list->toArray(&size);
    CUNIT_VERIFY_NOT_NULL(array);
    delete list;
    free(array);
}

CUNIT_TEST(MainSuite, eagleRandom_nextIntInRange, Equal)
{
    CUNIT_ASSERT_EQUAL_INT(eagle::Random::GetDefault()->nextIntInRange(123, 123), 123);
}

CUNIT_TEST(MainSuite, eagleRandom_nextIntInRange, DivideZero)
{
    CUNIT_ASSERT_EQUAL_INT(eagle::Random::GetDefault()->nextIntInRange(6, 5), 5);
}


inline int EagleReadWriteLockMock::aquireReadLock()
{
    return -1;
}

inline int EagleReadWriteLockMock::aquireWriteLock()
{
    return -1;
}

inline int EagleReadWriteLockMock::releaseLock()
{
    return -1;
}

CUNIT_TEST(MainSuite, EagleReadWriteLock, holdReadLock)
{
    try {
        EagleReadWriteLockMock lock;
        lock.holdReadLock();
        CUNIT_FAIL("%s", "Did not throw exception.");
    }
    catch(std::runtime_error &e) {
        CUNIT_VERIFY_EQUAL_STRING(e.what(), "Read lock acquisition failed.");
    }
}

CUNIT_TEST(MainSuite, EagleReadWriteLock, holdWriteLock)
{
    try {
        EagleReadWriteLockMock lock;
        lock.holdWriteLock();
        CUNIT_FAIL("%s", "Did not throw exception.");
    }
    catch(std::runtime_error &e) {
        CUNIT_VERIFY_EQUAL_STRING(e.what(), "Write lock acquisition failed.");
    }
}

CUNIT_TEST(MainSuite, EagleReadWriteLock, unlock)
{
    try {
        EagleReadWriteLockMock lock;
        lock.unlock();
        CUNIT_FAIL("%s", "Did not throw exception.");
    }
    catch(std::runtime_error &e) {
        CUNIT_VERIFY_EQUAL_STRING(e.what(), "Unlock failed.");
    }
}

CUNIT_TEST(MainSuite, EagleLogger, InvalidGetLogFile)
{
    CUNIT_ASSERT_EQUAL_PTR(EagleLogger::GetLogFile(NULL), stderr);
}

CUNIT_TEST(MainSuite, eagle_Comparable, equalsNull)
{
    eagle::Comparable *c = new eagle::Comparable();
    CUNIT_ASSERT_FALSE(c->equals(NULL));
    delete c;
}

void EagleLinkedList_testShift(EagleLinkedList<EagleDataTypeInteger*> *list)
{
    // empty list
    {
        CUNIT_VERIFY_EQUAL_INT(list->length(), 0);
        CUNIT_VERIFY_EQUAL_INT(list->isEmpty(), true);
        CUNIT_VERIFY_EQUAL_PTR(list->begin(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->end(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->first(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->last(), NULL);
        
        EagleLinkedList<EagleDataTypeInteger*>::Item *item = list->shift();
        CUNIT_ASSERT_NULL(item);
        CUNIT_VERIFY_EQUAL_INT(list->length(), 0);
        CUNIT_VERIFY_EQUAL_INT(list->isEmpty(), true);
        CUNIT_VERIFY_EQUAL_PTR(list->begin(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->end(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->first(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->last(), NULL);
    }
    
    // one item
    {
        EagleLinkedList<EagleDataTypeInteger*>::Item *origItem;
        origItem = new EagleLinkedList<EagleDataTypeInteger*>::Item(EagleData::CreateInt(123), true, NULL);
        list->add(origItem);
        CUNIT_VERIFY_EQUAL_INT(list->length(), 1);
        CUNIT_VERIFY_EQUAL_INT(list->isEmpty(), false);
        CUNIT_VERIFY_EQUAL_PTR(list->begin(), origItem);
        CUNIT_VERIFY_EQUAL_PTR(list->end(), origItem);
        CUNIT_VERIFY_EQUAL_PTR(list->first(), origItem->getObject());
        CUNIT_VERIFY_EQUAL_PTR(list->last(), origItem->getObject());
        
        EagleLinkedList<EagleDataTypeInteger*>::Item *item = list->shift();
        
        CUNIT_VERIFY_EQUAL_INT(list->length(), 0);
        CUNIT_VERIFY_EQUAL_INT(list->isEmpty(), true);
        CUNIT_VERIFY_EQUAL_PTR(list->begin(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->end(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->first(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->last(), NULL);
        CUNIT_VERIFY_EQUAL_INT(*(item->getObject()), 123);
        
        delete origItem;
    }
    
    // two items
    {
        EagleLinkedList<EagleDataTypeInteger*>::Item *item;
        EagleLinkedList<EagleDataTypeInteger*>::Item *origItem1;
        origItem1 = new EagleLinkedList<EagleDataTypeInteger*>::Item(EagleData::CreateInt(234), true, NULL);
        EagleLinkedList<EagleDataTypeInteger*>::Item *origItem2;
        origItem2 = new EagleLinkedList<EagleDataTypeInteger*>::Item(EagleData::CreateInt(567), true, NULL);
        
        list->add(origItem1);
        list->add(origItem2);
        CUNIT_VERIFY_EQUAL_INT(list->length(), 2);
        CUNIT_VERIFY_EQUAL_INT(list->isEmpty(), false);
        CUNIT_VERIFY_EQUAL_PTR(list->begin(), origItem1);
        CUNIT_VERIFY_EQUAL_PTR(list->end(), origItem2);
        CUNIT_VERIFY_EQUAL_PTR(list->first(), origItem1->getObject());
        CUNIT_VERIFY_EQUAL_PTR(list->last(), origItem2->getObject());
        
        item = list->shift();
        CUNIT_VERIFY_EQUAL_INT(list->length(), 1);
        CUNIT_VERIFY_EQUAL_INT(list->isEmpty(), false);
        CUNIT_VERIFY_EQUAL_INT(*(item->getObject()), 234);
        CUNIT_VERIFY_EQUAL_PTR(list->begin(), origItem2);
        CUNIT_VERIFY_EQUAL_PTR(list->end(), origItem2);
        CUNIT_VERIFY_EQUAL_PTR(list->first(), origItem2->getObject());
        CUNIT_VERIFY_EQUAL_PTR(list->last(), origItem2->getObject());
        
        item = list->shift();
        CUNIT_VERIFY_EQUAL_INT(list->length(), 0);
        CUNIT_VERIFY_EQUAL_INT(list->isEmpty(), true);
        CUNIT_VERIFY_EQUAL_PTR(list->begin(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->end(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->first(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->last(), NULL);
        CUNIT_VERIFY_EQUAL_INT(*(item->getObject()), 567);
        
        delete origItem1;
        delete origItem2;
    }
    
    // several items
    {
        EagleLinkedList<EagleDataTypeInteger*>::Item *item;
        EagleLinkedList<EagleDataTypeInteger*>::Item *origItem1;
        origItem1 = new EagleLinkedList<EagleDataTypeInteger*>::Item(EagleData::CreateInt(345), true, NULL);
        EagleLinkedList<EagleDataTypeInteger*>::Item *origItem2;
        origItem2 = new EagleLinkedList<EagleDataTypeInteger*>::Item(EagleData::CreateInt(678), true, NULL);
        EagleLinkedList<EagleDataTypeInteger*>::Item *origItem3;
        origItem3 = new EagleLinkedList<EagleDataTypeInteger*>::Item(EagleData::CreateInt(901), true, NULL);
        
        list->add(origItem1);
        list->add(origItem2);
        list->add(origItem3);
        CUNIT_VERIFY_EQUAL_INT(list->length(), 3);
        CUNIT_VERIFY_EQUAL_INT(list->isEmpty(), false);
        CUNIT_VERIFY_EQUAL_PTR(list->begin(), origItem1);
        CUNIT_VERIFY_EQUAL_PTR(list->end(), origItem3);
        CUNIT_VERIFY_EQUAL_PTR(list->first(), origItem1->getObject());
        CUNIT_VERIFY_EQUAL_PTR(list->last(), origItem3->getObject());
        CUNIT_VERIFY_NULL(list->end()->getNext());
        
        item = list->shift();
        CUNIT_VERIFY_EQUAL_INT(list->length(), 2);
        CUNIT_VERIFY_EQUAL_INT(list->isEmpty(), false);
        CUNIT_VERIFY_EQUAL_PTR(list->begin(), origItem2);
        CUNIT_VERIFY_EQUAL_PTR(list->end(), origItem3);
        CUNIT_VERIFY_EQUAL_PTR(list->first(), origItem2->getObject());
        CUNIT_VERIFY_EQUAL_PTR(list->last(), origItem3->getObject());
        CUNIT_VERIFY_EQUAL_INT(*(item->getObject()), 345);
        CUNIT_VERIFY_NULL(list->end()->getNext());
        
        item = list->shift();
        CUNIT_VERIFY_EQUAL_INT(list->length(), 1);
        CUNIT_VERIFY_EQUAL_INT(list->isEmpty(), false);
        CUNIT_VERIFY_EQUAL_PTR(list->begin(), origItem3);
        CUNIT_VERIFY_EQUAL_PTR(list->end(), origItem3);
        CUNIT_VERIFY_EQUAL_PTR(list->first(), origItem3->getObject());
        CUNIT_VERIFY_EQUAL_PTR(list->last(), origItem3->getObject());
        CUNIT_VERIFY_EQUAL_INT(*(item->getObject()), 678);
        CUNIT_VERIFY_NULL(list->end()->getNext());
        
        item = list->shift();
        CUNIT_VERIFY_EQUAL_INT(list->length(), 0);
        CUNIT_VERIFY_EQUAL_INT(list->isEmpty(), true);
        CUNIT_VERIFY_EQUAL_PTR(list->begin(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->end(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->first(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->last(), NULL);
        CUNIT_VERIFY_EQUAL_INT(*(item->getObject()), 901);
        
        delete origItem1;
        delete origItem2;
        delete origItem3;
    }
}

CUNIT_TEST(MainSuite, EagleSynchronizedLinkedList, shift)
{
    EagleLinkedList<EagleDataTypeInteger*> *list = new EagleSynchronizedLinkedList<EagleDataTypeInteger*>();
    EagleLinkedList_testShift(list);
    delete list;
}

CUNIT_TEST(MainSuite, eagle_ArrayPageProvider, getDataByRecordIdForFloat)
{
    int totalRecords = 3, recordsPerPage = 2;
    EagleDataTypeFloat *d = new EagleDataTypeFloat[totalRecords];
    d[0] = 1.3;
    d[1] = -3.4;
    d[2] = 67.8;
    
    eagle::ArrayPageProvider *provider = new eagle::ArrayPageProvider(EagleData::Type::Float, d, totalRecords,
                                                                      recordsPerPage, "", true);
    
    CUNIT_VERIFY_EQUAL_DOUBLE(*(EagleDataTypeFloat*) provider->getDataByRecordId(1), -3.4);
    CUNIT_VERIFY_EQUAL_DOUBLE(*(EagleDataTypeFloat*) provider->getDataByRecordId(2), 67.8);
    CUNIT_VERIFY_EQUAL_DOUBLE(*(EagleDataTypeFloat*) provider->getDataByRecordId(0), 1.3);
    
    delete provider;
}

CUNIT_TEST(MainSuite, eagle_ArrayPageProvider, getDataByRecordIdForInteger)
{
    int totalRecords = 3, recordsPerPage = 2;
    EagleDataTypeInteger *d = new EagleDataTypeInteger[totalRecords];
    d[0] = 3;
    d[1] = -6;
    d[2] = 23;
    
    eagle::ArrayPageProvider *provider = new eagle::ArrayPageProvider(EagleData::Type::Integer, d, totalRecords,
                                                                      recordsPerPage, "", true);
    
    CUNIT_VERIFY_EQUAL_INT(*(EagleDataTypeInteger*) provider->getDataByRecordId(1), -6);
    CUNIT_VERIFY_EQUAL_INT(*(EagleDataTypeInteger*) provider->getDataByRecordId(2), 23);
    CUNIT_VERIFY_EQUAL_INT(*(EagleDataTypeInteger*) provider->getDataByRecordId(0), 3);
    
    delete provider;
}

CUNIT_TEST(MainSuite, eagle_ArrayPageProvider, getDataByRecordIdForVarchar)
{
    int totalRecords = 3, recordsPerPage = 2;
    EagleDataTypeVarchar *d = new EagleDataTypeVarchar[totalRecords];
    d[0] = "a";
    d[1] = "b";
    d[2] = "c";
    
    eagle::ArrayPageProvider *provider = new eagle::ArrayPageProvider(EagleData::Type::Varchar, d, totalRecords,
                                                                      recordsPerPage, "", true);
    
    CUNIT_VERIFY_EQUAL_STRING(*(EagleDataTypeVarchar*) provider->getDataByRecordId(1), "b");
    CUNIT_VERIFY_EQUAL_STRING(*(EagleDataTypeVarchar*) provider->getDataByRecordId(2), "c");
    CUNIT_VERIFY_EQUAL_STRING(*(EagleDataTypeVarchar*) provider->getDataByRecordId(0), "a");
    
    delete provider;
}

CUNIT_TEST(MainSuite, eagle_ArrayPageProvider, getDataByRecordIdOutOfBounds)
{
    eagle::ArrayPageProvider *provider = new eagle::ArrayPageProvider(EagleData::Type::Integer, NULL, 0, 1, "", false);
    
    CUNIT_VERIFY_NULL(provider->getDataByRecordId(10));
    CUNIT_VERIFY_NULL(provider->getDataByRecordId(-1));
    
    delete provider;
}

CUNIT_TEST(MainSuite, eagle_ArrayPageProvider, getDataByRecordIdForUnknown)
{
    eagle::ArrayPageProvider *provider = new eagle::ArrayPageProvider(EagleData::Type::Unknown, NULL, 1, 1, "", false);
    
    CUNIT_VERIFY_NULL(provider->getDataByRecordId(10));
    CUNIT_VERIFY_NULL(provider->getDataByRecordId(-1));
    CUNIT_VERIFY_NULL(provider->getDataByRecordId(0));
    
    delete provider;
}

CUNIT_TEST(MainSuite, eagle_page_Sort_CreateFromPage, ThatIsNull)
{
    CUNIT_VERIFY_NULL(eagle::page::Sort::CreateFromPage(NULL));
}

CUNIT_TEST(MainSuite, eagle_plan_job_SortPage, CompareInteger)
{
    eagle::plan::job::SortPage::DataWithId<EagleDataTypeInteger> a = {1, 1}, b = {1, 2};
    CUNIT_VERIFY_EQUAL_INT(eagle::plan::job::SortPage::Compare<EagleDataTypeInteger>(&a, &a), 0);
    CUNIT_VERIFY_EQUAL_INT(eagle::plan::job::SortPage::Compare<EagleDataTypeInteger>(&a, &b), -1);
    CUNIT_VERIFY_EQUAL_INT(eagle::plan::job::SortPage::Compare<EagleDataTypeInteger>(&b, &a), 1);
}

CUNIT_TEST(MainSuite, eagle_plan_job_SortPage, CompareFloat)
{
    eagle::plan::job::SortPage::DataWithId<EagleDataTypeFloat> a = {1, 1.1}, b = {1, 2.2};
    CUNIT_VERIFY_EQUAL_INT(eagle::plan::job::SortPage::Compare<EagleDataTypeFloat>(&a, &a), 0);
    CUNIT_VERIFY_EQUAL_INT(eagle::plan::job::SortPage::Compare<EagleDataTypeFloat>(&a, &b), -1);
    CUNIT_VERIFY_EQUAL_INT(eagle::plan::job::SortPage::Compare<EagleDataTypeFloat>(&b, &a), 1);
}

CUNIT_TEST(MainSuite, eagle_plan_job_SortPage, CompareVarchar)
{
    eagle::plan::job::SortPage::DataWithId<EagleDataTypeVarchar> a = {1, "a"}, b = {1, "b"};
    CUNIT_VERIFY_EQUAL_INT(eagle::plan::job::SortPage::CompareVarchar(&a, &a), 0);
    CUNIT_VERIFY_EQUAL_INT(eagle::plan::job::SortPage::CompareVarchar(&a, &b), -1);
    CUNIT_VERIFY_EQUAL_INT(eagle::plan::job::SortPage::CompareVarchar(&b, &a), 1);
}

CUNIT_TEST(MainSuite, eagle_plan_stage_EntireSort, toString)
{
    eagle::plan::stage::EntireSort es(NULL, 1);
    CUNIT_VERIFY_EQUAL_STRING(es.toString().c_str(), "EntireSort stage:\n  ");
}

CUNIT_TEST(MainSuite, EagleUnsynchronizedLinkedList, shift)
{
    EagleLinkedList<EagleDataTypeInteger*> *list = new EagleUnsynchronizedLinkedList<EagleDataTypeInteger*>();
    EagleLinkedList_testShift(list);
    delete list;
}

CUNIT_TEST(MainSuite, eagle_PageProvider, getDataByRecordId)
{
    eagle::PageProvider provider;
    CUNIT_VERIFY_NULL(provider.getDataByRecordId(-1));
    CUNIT_VERIFY_NULL(provider.getDataByRecordId(0));
    CUNIT_VERIFY_NULL(provider.getDataByRecordId(10000));
}

CUNIT_TEST(MainSuite, eagle_PageProvider, deleteWithUsageCountAboveOne)
{
    eagle::PageProvider *provider = new eagle::PageProvider();
    ++provider->usageCount;
    delete provider;
    delete provider;
}

CUNIT_TEST(MainSuite, eagle_Plan, toString)
{
    eagle::Plan plan;
    
    // add a stage
    eagle::plan::stage::SelectStage stage(&plan, 1, 1);
    plan.addStage(&stage, false);
    
    // render
    CUNIT_VERIFY_EQUAL_STRING(plan.toString().c_str(), "Select stage:\n");
}

CUNIT_TEST(MainSuite, eagle_Plan, getStartTime)
{
    eagle::Plan plan;
    
    // add two stages
    eagle::plan::stage::SelectStage stage1(&plan, 1, 1), stage2(&plan, 1, 1);
    stage1.startTime = 1234;
    stage2.startTime = 4567;
    plan.addStage(&stage1, false);
    plan.addStage(&stage2, false);
    
    // verify we get the start time of the first stage
    CUNIT_VERIFY_EQUAL_INT((int) plan.getStartTime(), 1234);
}

CUNIT_TEST(MainSuite, eagle_Plan, getStartTimeZeroStages)
{
    eagle::Plan plan;
    
    // verify we get the start time of the first stage
    CUNIT_VERIFY_EQUAL_INT((int) plan.getStartTime(), 0);
}

CUNIT_TEST(MainSuite, eagle_StreamPageProvider, getDataByRecordIdForVarchar)
{
    int totalRecords = 3, recordsPerPage = 2;
    EagleDataTypeVarchar *d = new EagleDataTypeVarchar[totalRecords];
    d[0] = "a";
    d[1] = "b";
    d[2] = "c";
    
    eagle::StreamPageProvider *provider = new eagle::StreamPageProvider(EagleData::Type::Varchar, recordsPerPage, "");
    for(int i = 0; i < totalRecords; ++i) {
        provider->add(strdup(d[i]));
    }
    
    CUNIT_VERIFY_EQUAL_STRING(*(EagleDataTypeVarchar*) provider->getDataByRecordId(1), "b");
    CUNIT_VERIFY_EQUAL_STRING(*(EagleDataTypeVarchar*) provider->getDataByRecordId(2), "c");
    CUNIT_VERIFY_EQUAL_STRING(*(EagleDataTypeVarchar*) provider->getDataByRecordId(0), "a");
    
    delete provider;
    delete d;
}

CUNIT_TEST(MainSuite, eagle_StreamPageProvider, getDataByRecordIdForUnknown)
{
    eagle::StreamPageProvider *provider = new eagle::StreamPageProvider(EagleData::Type::Unknown, 1, "");
    
    CUNIT_VERIFY_NULL(provider->getDataByRecordId(10));
    CUNIT_VERIFY_NULL(provider->getDataByRecordId(-1));
    CUNIT_VERIFY_NULL(provider->getDataByRecordId(0));
    
    delete provider;
}

CUNIT_TEST(MainSuite, eagle_SinglePageProvider, pagesRemaining)
{
    eagle::SinglePageProvider *provider = eagle::SinglePageProvider::NewInt(123, 1, "");
    
    // make sure that eagle::SinglePageProvider::pagesRemaining() always returns 1
    CUNIT_VERIFY_EQUAL_INT(provider->pagesRemaining(), 1);
    CUNIT_VERIFY_EQUAL_INT(provider->pagesRemaining(), 1);
    
    delete provider;
}

CUNIT_TEST(MainSuite, EagleLinkedList_Item, destructorWithDelete)
{
    EagleUnsynchronizedLinkedList<EagleDataTypeInteger*> list;
    list.addObject(new int, true);
    
    // the destructor will run now
}

CUNIT_TEST(MainSuite, eagle_plan_job_MergePages, runWithFloat)
{
    // test data, the individual lists have to already be sorted
    EagleDataTypeFloat *p1data = new EagleDataTypeFloat[3];
    p1data[0] = 1.23;
    p1data[1] = 7.67;
    p1data[2] = 8.95;
    EagleDataTypeFloat *p2data = new EagleDataTypeFloat[3];
    p2data[0] = -5;
    p2data[1] = 3.23;
    p2data[2] = 4.35;
    
    // pages
    eagle::page::Sort *p1 = new eagle::page::Sort(EagleData::Type::Float, p1data, 3, 3, 0, false, NULL);
    eagle::page::Sort *p2 = new eagle::page::Sort(EagleData::Type::Float, p2data, 3, 3, 0, false, NULL);
    eagle::page::Sort *p3 = new eagle::page::Sort(EagleData::Type::Float, NULL, 0, 0, 0, true, NULL);
    
    eagle::plan::job::MergePages *job = new eagle::plan::job::MergePages(NULL, p1, p2, p3);
    job->run();
    
    // verify
    EagleDataTypeFloat answers[6] = { -5, 1.23, 3.23, 4.35, 7.67, 8.95 };
    for(int i = 0; i < 6; ++i) {
        CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) p3->data)[i], answers[i]);
    }
    
    delete job->mergedPage;
    delete job;
}

CUNIT_TEST(MainSuite, eagle_plan_job_MergePages, runWithInteger)
{
    // test data, the individual lists have to already be sorted
    EagleDataTypeInteger *p1data = new EagleDataTypeInteger[3];
    p1data[0] = 1;
    p1data[1] = 7;
    p1data[2] = 8;
    EagleDataTypeInteger *p2data = new EagleDataTypeInteger[3];
    p2data[0] = -5;
    p2data[1] = 3;
    p2data[2] = 4;
    
    // pages
    eagle::page::Sort *p1 = new eagle::page::Sort(EagleData::Type::Integer, p1data, 3, 3, 0, false, NULL);
    eagle::page::Sort *p2 = new eagle::page::Sort(EagleData::Type::Integer, p2data, 3, 3, 0, false, NULL);
    eagle::page::Sort *p3 = new eagle::page::Sort(EagleData::Type::Integer, NULL, 0, 0, 0, true, NULL);
    
    eagle::plan::job::MergePages *job = new eagle::plan::job::MergePages(NULL, p1, p2, p3);
    job->run();
    
    // verify
    EagleDataTypeInteger answers[6] = { -5, 1, 3, 4, 7, 8 };
    for(int i = 0; i < 6; ++i) {
        CUNIT_VERIFY_EQUAL_INT(((EagleDataTypeInteger*) p3->data)[i], answers[i]);
    }
    
    delete job->mergedPage;
    delete job;
}

CUNIT_TEST(MainSuite, eagle_plan_job_MergePages, runWithVarchar)
{
    // test data, the individual lists have to already be sorted
    EagleDataTypeVarchar *p1data = new EagleDataTypeVarchar[3];
    p1data[0] = strdup("b");
    p1data[1] = strdup("e");
    p1data[2] = strdup("f");
    EagleDataTypeVarchar *p2data = new EagleDataTypeVarchar[3];
    p2data[0] = strdup("a");
    p2data[1] = strdup("c");
    p2data[2] = strdup("d");
    
    // pages
    eagle::page::Sort *p1 = new eagle::page::Sort(EagleData::Type::Varchar, p1data, 3, 3, 0, false, NULL);
    eagle::page::Sort *p2 = new eagle::page::Sort(EagleData::Type::Varchar, p2data, 3, 3, 0, false, NULL);
    eagle::page::Sort *p3 = new eagle::page::Sort(EagleData::Type::Varchar, NULL, 0, 0, 0, true, NULL);
    
    eagle::plan::job::MergePages *job = new eagle::plan::job::MergePages(NULL, p1, p2, p3);
    job->run();
    
    // verify
    EagleDataTypeVarchar answers[6] = { "a", "b", "c", "d", "e", "f" };
    for(int i = 0; i < 6; ++i) {
        CUNIT_VERIFY_EQUAL_STRING(((EagleDataTypeVarchar*) p3->data)[i], answers[i]);
    }
    
    delete job->mergedPage;
    delete job;
}

CUNIT_TEST(MainSuite, eagle_plan_job_MergePages, runWithUnknown)
{
    eagle::page::Sort *p1 = new eagle::page::Sort(EagleData::Type::Unknown, NULL, 0, 0, 0, false, NULL);
    eagle::page::Sort *p2 = new eagle::page::Sort(EagleData::Type::Unknown, NULL, 0, 0, 0, false, NULL);
    eagle::page::Sort *p3 = new eagle::page::Sort(EagleData::Type::Unknown, NULL, 0, 0, 0, false, NULL);
    eagle::plan::job::MergePages *job = new eagle::plan::job::MergePages(NULL, p1, p2, p3);
    job->run();
    delete job;
    delete p3;
}

CUNIT_TEST(MainSuite, eagle_plan_stage_EntireSort_getSortPageJob, morePages)
{
    eagle::plan::stage::EntireSortTestStub *stage = new eagle::plan::stage::EntireSortTestStub(NULL, 1);
    bool doReturn;
    
    stage->orderByProviderId = 0;
    stage->selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    stage->selectStage->resultFields = 1;
    stage->selectStage->result = new eagle::PageProvider*[1];
    stage->selectStage->result[0] = eagle::SinglePageProvider::NewInt(123, 1, "");
    
    eagle::plan::job::AbstractJob *job = stage->getSortPageJob_(&doReturn);
    CUNIT_VERIFY_NOT_NULL(job);
    CUNIT_VERIFY_TRUE(doReturn);
    delete ((eagle::plan::job::SortPage*) job)->sortPage;
    delete job;
    
    delete stage->selectStage;
    delete stage;
}

CUNIT_TEST(MainSuite, eagle_plan_stage_EntireSort_getSortPageJob, mergeStackNotFinished)
{
    eagle::plan::stage::EntireSortTestStub *stage = new eagle::plan::stage::EntireSortTestStub(NULL, 1);
    bool doReturn;
    
    stage->orderByProviderId = 0;
    stage->selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    stage->selectStage->resultFields = 1;
    stage->selectStage->result = new eagle::PageProvider*[1];
    stage->selectStage->result[0] = new eagle::ArrayPageProvider(EagleData::Type::Integer, NULL, 0, 1, "", false);
    stage->getSortPageJob_(&doReturn);
    stage->totalPages = 1;
    
    CUNIT_VERIFY_NULL(stage->getSortPageJob_(&doReturn));
    CUNIT_VERIFY_TRUE(doReturn);
    
    delete stage->selectStage;
    delete stage;
}

CUNIT_TEST(MainSuite, eagle_plan_stage_EntireSort_EntireSort, getMergeJobFromStack)
{
    eagle::plan::stage::EntireSortTestStub *stage = new eagle::plan::stage::EntireSortTestStub(NULL, 1);
    eagle::plan::job::AbstractJob *job = NULL;
    
    // mergeSortStack needs two items
    stage->mergeSortStack->addObject(new eagle::page::Sort(EagleData::Type::Integer, NULL, 0, 0, 0, false, NULL), true);
    stage->mergeSortStack->addObject(new eagle::page::Sort(EagleData::Type::Integer, NULL, 0, 0, 0, false, NULL), true);
    
    // one of them is working
    stage->mergeSortStack->get(0)->working = true;
    stage->mergeSortStack->get(1)->working = false;
    
    // test
    job = stage->getMergeJobFromStack_();
    CUNIT_VERIFY_NULL(job);
    
    // the other one is working
    stage->mergeSortStack->get(0)->working = false;
    stage->mergeSortStack->get(1)->working = true;
    
    // test
    job = stage->getMergeJobFromStack_();
    CUNIT_VERIFY_NULL(job);
    
    // both are working
    stage->mergeSortStack->get(0)->working = true;
    stage->mergeSortStack->get(1)->working = true;
    
    // test
    job = stage->getMergeJobFromStack_();
    CUNIT_VERIFY_NULL(job);
    
    delete stage;
}

template<typename T>
eagle::plan::stage::EntireSort* eagle_plan_stage_EntireSort_EntireSort_reorderAllDataTest(EagleData::Type type, T a, T b, T c)
{
    eagle::plan::stage::EntireSortTestStub *stage = new eagle::plan::stage::EntireSortTestStub(NULL, 1);
    
    // mergeSortStack needs one object to pop
    const int totalSize = 3;
    int *recordIds = new int[totalSize];
    recordIds[0] = 2;
    recordIds[1] = 0;
    recordIds[2] = 1;
    T *data = new T[totalSize];
    data[0] = a;
    data[1] = b;
    data[2] = c;
    stage->mergeSortStack->addObject(new eagle::page::Sort(type, data, totalSize, 0, 0, false,
                                                           recordIds), true);
    
    
    // where the data is coming from
    stage->selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    stage->selectStage->resultFields = 1;
    stage->selectStage->result = new eagle::PageProvider*[stage->selectStage->resultFields];
    stage->selectStage->result[0] = new eagle::ArrayPageProvider(type, data, totalSize, 3, "", false);
    
    // where the data is going to
    stage->resultFields = 1;
    stage->result = new eagle::PageProvider*[stage->resultFields];
    
    // run
    stage->reorderAllData_();
    
    delete stage->selectStage;
    return stage;
}

CUNIT_TEST(MainSuite, eagle_plan_stage_EntireSort_EntireSort, reorderAllDataWithFloat)
{
    eagle::plan::stage::EntireSort *stage = eagle_plan_stage_EntireSort_EntireSort_reorderAllDataTest<EagleDataTypeFloat>(EagleData::Type::Float, 1.23, 2.34, 3.45);
    eagle::PageProvider *result = stage->result[0];
    CUNIT_VERIFY_EQUAL_DOUBLE(*(EagleDataTypeFloat*) result->getDataByRecordId(0), 3.45);
    CUNIT_VERIFY_EQUAL_DOUBLE(*(EagleDataTypeFloat*) result->getDataByRecordId(1), 1.23);
    CUNIT_VERIFY_EQUAL_DOUBLE(*(EagleDataTypeFloat*) result->getDataByRecordId(2), 2.34);
    delete stage;
}

CUNIT_TEST(MainSuite, eagle_plan_stage_EntireSort_EntireSort, reorderAllDataWithInteger)
{
    eagle::plan::stage::EntireSort *stage = eagle_plan_stage_EntireSort_EntireSort_reorderAllDataTest<EagleDataTypeInteger>(EagleData::Type::Integer, 1, 2, 3);
    eagle::PageProvider *result = stage->result[0];
    CUNIT_VERIFY_EQUAL_INT(*(EagleDataTypeInteger*) result->getDataByRecordId(0), 3);
    CUNIT_VERIFY_EQUAL_INT(*(EagleDataTypeInteger*) result->getDataByRecordId(1), 1);
    CUNIT_VERIFY_EQUAL_INT(*(EagleDataTypeInteger*) result->getDataByRecordId(2), 2);
    delete stage;
}

CUNIT_TEST(MainSuite, eagle_plan_stage_EntireSort_EntireSort, reorderAllDataWithVarchar)
{
    eagle::plan::stage::EntireSort *stage = eagle_plan_stage_EntireSort_EntireSort_reorderAllDataTest<EagleDataTypeVarchar>(EagleData::Type::Varchar, "a", "b", "c");
    eagle::PageProvider *result = stage->result[0];
    CUNIT_VERIFY_EQUAL_STRING(*(EagleDataTypeVarchar*) result->getDataByRecordId(0), "c");
    CUNIT_VERIFY_EQUAL_STRING(*(EagleDataTypeVarchar*) result->getDataByRecordId(1), "a");
    CUNIT_VERIFY_EQUAL_STRING(*(EagleDataTypeVarchar*) result->getDataByRecordId(2), "b");
    delete stage;
}

template<typename T>
eagle::page::Sort* eagle_plan_job_SortPage_runTest(EagleData::Type type, T a, T b, T c)
{
    const int totalSize = 3;
    T *data = new T[totalSize];
    data[0] = a;
    data[1] = b;
    data[2] = c;
    
    EaglePage *page = new EaglePage(type, data, totalSize, totalSize, 0, false);
    eagle::plan::job::SortPage *job = new eagle::plan::job::SortPage(NULL, page);
    eagle::plan::stage::EntireSort *stage = new eagle::plan::stage::EntireSort(NULL, 1);
    job->stage = stage;
    job->run();
    
    eagle::page::Sort *r = stage->mergeSortStack->popObject();
    delete stage;
    delete job;
    delete[] data;
    
    return r;
}

CUNIT_TEST(MainSuite, eagle_plan_job_SortPage, runWithInteger)
{
    eagle::page::Sort *page = eagle_plan_job_SortPage_runTest<EagleDataTypeInteger>(EagleData::Type::Integer, 5, 9, 3);
    
    // verify
    EagleDataTypeInteger answers[3] = { 3, 5, 9 };
    for(int i = 0; i < 3; ++i) {
        CUNIT_VERIFY_EQUAL_INT(((EagleDataTypeInteger*) page->data)[i], answers[i]);
    }
    
    delete page;
}

CUNIT_TEST(MainSuite, eagle_plan_job_SortPage, runWithFloat)
{
    eagle::page::Sort *page = eagle_plan_job_SortPage_runTest<EagleDataTypeFloat>(EagleData::Type::Float, 5.1, 9.2, 3.3);
    
    // verify
    EagleDataTypeFloat answers[3] = { 3.3, 5.1, 9.2 };
    for(int i = 0; i < 3; ++i) {
        CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) page->data)[i], answers[i]);
    }
    
    delete page;
}

CUNIT_TEST(MainSuite, eagle_plan_job_SortPage, runWithVarchar)
{
    eagle::page::Sort *page = eagle_plan_job_SortPage_runTest<EagleDataTypeVarchar>(EagleData::Type::Varchar, "bda", "baa", "a");
    
    // verify
    EagleDataTypeVarchar answers[3] = { "a", "baa", "bda" };
    for(int i = 0; i < 3; ++i) {
        CUNIT_VERIFY_EQUAL_STRING(((EagleDataTypeVarchar*) page->data)[i], answers[i]);
    }
    
    delete page;
}

CUNIT_TEST(MainSuite, eagle_plan_job_SortPage, runWithUnknown)
{
    EaglePage *page = new EaglePage(EagleData::Type::Integer, NULL, 0, 0, 0, false);
    eagle::plan::job::SortPage *job = new eagle::plan::job::SortPage(NULL, page);
    eagle::plan::stage::EntireSort *stage = new eagle::plan::stage::EntireSort(NULL, 1);
    job->stage = stage;
    
    // change the type before we run, not sure how this would happen in practice...
    job->sortPage->type = EagleData::Type::Unknown;
    
    job->run();
    
    delete job->sortPage;
    delete job;
    delete stage;
}

CUNIT_TEST(MainSuite, eagle_plan_stage_EntireSort_EntireSort, reorderAllDataWithUnknown)
{
    eagle::plan::stage::EntireSort *stage = eagle_plan_stage_EntireSort_EntireSort_reorderAllDataTest<EagleDataTypeVarchar>(EagleData::Type::Unknown, "a", "b", "c");
    CUNIT_VERIFY_NULL(stage->result[0]);
    delete stage;
}

CUNIT_TEST(MainSuite, eagle_plan_stage_EntireSort, nextJobWithFinalPageNotMergedYet)
{
    eagle::plan::stage::EntireSort stage(NULL, 1);
    stage.selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    stage.step = 2;
    stage.mergeSortStack->addObject(new eagle::page::Sort(EagleData::Type::Integer, NULL, 0, 0, 0, false, NULL), true);
    CUNIT_VERIFY_NULL(stage.nextJob());
    delete stage.mergeSortStack->pop();
    delete stage.selectStage;
}

CUNIT_TEST(MainSuite, eagle_plan_stage_SelectStage, toStringUnderError)
{
    eagle::plan::stage::SelectStage stage(NULL, 1, 1);
    stage.setError(EaglePlanErrorNoSuchTable, "No such table");
    CUNIT_VERIFY_EQUAL_STRING(stage.toString().c_str(), "Select stage:\n  Error: No such table\n");
}

CUNIT_TEST(MainSuite, eagle_plan_stage_SelectStage, needsSynchronisationNo)
{
    eagle::plan::stage::SelectStage stage(NULL, 1, 1);
    EagleDbSqlValue *value = EagleDbSqlValue_NewWithInteger(123);
    stage.providers->addObject(new eagle::PlanBufferProvider(0, value), true);
    CUNIT_VERIFY_FALSE(stage.needsSynchronisation());
    EagleDbSqlValue_Delete(value);
}

class PlanIsStalled : public eagle::Plan
{
    
public:
    
    int isStalledCount;
    
    PlanIsStalled() :  eagle::Plan()
    {
        isStalledCount = 0;
    }
    
    bool isStalled()
    {
        if(isStalledCount++ < 1) {
            return true;
        }
        ++isStalledCount;
        return false;
    }
    
    eagle::plan::job::AbstractJob* nextJob()
    {
        return NULL;
    }
    
};

CUNIT_TEST(MainSuite, eagle_Instance, testPlanIsStalled)
{
    eagle::Instance instance(1);
    eagle::Plan *plan = new PlanIsStalled();
    instance.addPlan(plan);
    instance.nextJob();
    delete plan;
}

class StageReturnsNull : public eagle::plan::stage::SelectStage
{
    
public:
    
    StageReturnsNull(eagle::Plan *thePlan, int thePageSize, int theCores) : eagle::plan::stage::SelectStage(thePlan, thePageSize, theCores)
    {
    }
    
    eagle::plan::job::AbstractJob* nextJob()
    {
        return NULL;
    }
    
};

CUNIT_TEST(MainSuite, eagle_Plan, nextJobIsStalled)
{
    eagle::Plan plan;
    StageReturnsNull *stage = new StageReturnsNull(&plan, 1, 1);
    stage->complete = false;
    plan.addStage(stage, true);
    plan.nextJob();
    CUNIT_VERIFY_TRUE(plan.isStalled());
}

class eagle_ObjectStub : public eagle::Object
{
    
public:
    
    virtual std::string toString()
    {
        return "a";
    }
    
};

CUNIT_TEST(MainSuite, eagle_Object, Instantiation)
{
    eagle_ObjectStub o;
}

CUnitTests* MainSuite4_tests()
{
    CUnitTests *tests = CUnitTests_New(100);
    
    // method tests
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_Object, Instantiation));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_Plan, nextJobIsStalled));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_Instance, testPlanIsStalled));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_plan_stage_SelectStage, needsSynchronisationNo));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_plan_stage_SelectStage, toStringUnderError));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_plan_stage_EntireSort, nextJobWithFinalPageNotMergedYet));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_plan_stage_EntireSort_EntireSort, reorderAllDataWithUnknown));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_plan_job_SortPage, runWithUnknown));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_plan_job_SortPage, runWithVarchar));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_plan_job_SortPage, runWithFloat));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_plan_job_SortPage, runWithInteger));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_plan_stage_EntireSort_EntireSort, reorderAllDataWithVarchar));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_plan_stage_EntireSort_EntireSort, reorderAllDataWithInteger));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_plan_stage_EntireSort_EntireSort, reorderAllDataWithFloat));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_plan_stage_EntireSort_EntireSort, getMergeJobFromStack));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_plan_stage_EntireSort_getSortPageJob, morePages));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_plan_stage_EntireSort_getSortPageJob, mergeStackNotFinished));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_plan_job_MergePages, runWithUnknown));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_plan_job_MergePages, runWithVarchar));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_plan_job_MergePages, runWithInteger));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_plan_job_MergePages, runWithFloat));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLinkedList_Item, destructorWithDelete));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_SinglePageProvider, pagesRemaining));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_StreamPageProvider, getDataByRecordIdForUnknown));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_StreamPageProvider, getDataByRecordIdForVarchar));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_Plan, getStartTimeZeroStages));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_Plan, getStartTime));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_Plan, toString));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_PageProvider, deleteWithUsageCountAboveOne));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_PageProvider, getDataByRecordId));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleUnsynchronizedLinkedList, shift));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_plan_stage_EntireSort, toString));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_plan_job_SortPage, CompareVarchar));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_plan_job_SortPage, CompareFloat));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_plan_job_SortPage, CompareInteger));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_page_Sort_CreateFromPage, ThatIsNull));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_ArrayPageProvider, getDataByRecordIdForUnknown));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_ArrayPageProvider, getDataByRecordIdOutOfBounds));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_ArrayPageProvider, getDataByRecordIdForVarchar));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_ArrayPageProvider, getDataByRecordIdForInteger));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_ArrayPageProvider, getDataByRecordIdForFloat));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleSynchronizedLinkedList, shift));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_Comparable, equalsNull));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLogger, InvalidGetLogFile));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleReadWriteLock, unlock));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleReadWriteLock, holdWriteLock));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleReadWriteLock, holdReadLock));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagleRandom_nextIntInRange, DivideZero));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagleRandom_nextIntInRange, Equal));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLinkedList_toArray, Never));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLinkedList_isEmpty, Never));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLinkedList_end, Never));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLinkedList_begin, Never));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLinkedList_last, Never));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLinkedList_first, Never));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLinkedList, NoSync));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLinkedList, Sync));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleWorker, runJobFunction));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleData, NameToTypeNull));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLinkedList_toString, Custom));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLinkedList_toString, Simple));
    
    return tests;
}
