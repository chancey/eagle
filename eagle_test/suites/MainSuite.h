#ifndef eagle_MainSuite_h
#define eagle_MainSuite_h

#include "TestSuite.h"
#include <CUnit/Basic.h>
#include "eagle/EaglePage.h"
#include "eagle/Instance.h"
#include "eagle/EagleReadWriteLock.h"
#include "eagle/EagleSynchronizedLinkedList.h"
#include "eagle/Worker.h"
#include "eagle/plan/stage/SelectStage.h"
#include "eagle/plan/stage/EntireSort.h"
#include "eagle/plan/job/SelectJob.h"

int MainSuite_init();
CUnitTests* MainSuite1_tests();
CUnitTests* MainSuite2_tests();
CUnitTests* MainSuite3_tests();
CUnitTests* MainSuite4_tests();
int MainSuite_clean();

namespace eagle
{
    namespace plan
    {
        namespace stage
        {
            class EntireSortTestStub;
        }
    }
}

// for mocking
class EagleReadWriteLockMock : public EagleReadWriteLock
{
    
protected:
    
    virtual int aquireReadLock();
    virtual int aquireWriteLock();
    virtual int releaseLock();
    
};

class EagleLinkedListMock : public EagleSynchronizedLinkedList<EagleDataTypeInteger*>
{
    
public:
    
    void _setSize(int s)
    {
        size = s;
    }
    
    void _initIndex_()
    {
        initIndex_();
    }
    
    void _setIndexSize(int s)
    {
        indexSize = s;
    }
    
    void _resizeIndex()
    {
        resizeIndex();
    }
    
    void _setIndex(EagleLinkedList<EagleDataTypeInteger*>::Item **idx)
    {
        index = idx;
    }
    
    int _getIndexSize()
    {
        return indexSize;
    }
    
    EagleLinkedList<EagleDataTypeInteger*>::Item** _getIndex()
    {
        return index;
    }
    
};

class EagleSelectStageMock : public eagle::plan::stage::SelectStage
{

public:
    
    static void _NextJobSync(eagle::plan::job::AbstractJob **job)
    {
        NextJobSync(job);
    }
    
};

class EagleSelectJobMock : public eagle::plan::job::SelectJob
{
    
public:

    EagleSelectJobMock(eagle::plan::stage::SelectStage *job) : eagle::plan::job::SelectJob(job)
    {
    }
    
    void runJobFunction_(eagle::plan::Operation *epo)
    {
        runJobFunction(epo);
    }
    
};

class eagle::plan::stage::EntireSortTestStub : public eagle::plan::stage::EntireSort
{
    
public:
    
    EntireSortTestStub(eagle::Plan *thePlan, int theCores) : eagle::plan::stage::EntireSort(thePlan, theCores)
    {
    }
    
    eagle::plan::job::AbstractJob* getSortPageJob_(bool *doReturn)
    {
        return getSortPageJob(doReturn);
    }
    
    eagle::plan::job::AbstractJob* getMergeJobFromStack_()
    {
        return getMergeJobFromStack();
    }
    
    void reorderAllData_()
    {
        reorderAllData();
    }
    
};

class eagle_plan_OperationAccess : public eagle::plan::Operation
{
    
public:
    
    void setDestination(int theDestination)
    {
        destination = theDestination;
    }
    
    void setSource1(int theSource1)
    {
        source1 = theSource1;
    }
    
    void setObject(void *o)
    {
        obj = o;
    }
    
    void setArgumentCount(int x)
    {
        argCount = x;
    }
    
};

#endif
