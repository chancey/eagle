#include "DBSuite.h"
#include <stdio.h>
#include <stdlib.h>
#include "eagledb/EagleDbSqlSelect.h"
#include "eagledb/EagleDbSqlBinaryExpression.h"
#include "eagledb/EagleDbSqlValue.h"
#include "eagle/Instance.h"
#include "eagle/EagleData.h"
#include "eagle/db/Column.h"
#include "eagle/db/Table.h"
#include "eagle/db/TableData.h"
#include "eagle/db/Tuple.h"
#include "eagledb/EagleDbConsole.h"
#include "eagledb/EagleDbParser.h"
#include "eagle/EagleLogger.h"
#include "eagledb/EagleDbInstance.h"
#include "eagledb/EagleDbSqlUnaryExpression.h"
#include "eagle/StreamPageProvider.h"
#include "eagle/ArrayPageProvider.h"
#include "eagledb/EagleDbInformationSchema.h"
#include "eagledb/EagleDbSqlFunctionExpression.h"
#include "eagledb/EagleDbSqlCastExpression.h"
#include "eagledb/EagleDbSqlFunction.h"
#include "eagle/EagleSynchronizedLinkedList.h"
#include "eagle/plan/stage/SelectStage.h"

CUNIT_TEST(DBSuite, _INSERT_BadMatch)
{
    EagleDbInstance *db = EagleInstanceTest(10);
    
    EagleLogger::Event *error = NULL;
    bool success = EagleDbInstance_execute(db, "INSERT INTO mytable (col1, col2) VALUES (123);", &error, NULL);
    CUNIT_ASSERT_FALSE(success);
    CUNIT_ASSERT_LAST_ERROR("There are 2 columns and 1 values");
    
    EagleInstanceTest_Cleanup(db);
}

CUNIT_TEST(DBSuite, _INSERT_BadColumn1)
{
    EagleDbInstance *db = EagleInstanceTest(10);
    
    EagleLogger::Event *error = NULL;
    bool success = EagleDbInstance_execute(db, "INSERT INTO mytable (123) VALUES (123);", &error, NULL);
    CUNIT_ASSERT_FALSE(success);
    CUNIT_ASSERT_LAST_ERROR("You cannot use expressions for column names");
    
    EagleInstanceTest_Cleanup(db);
}

CUNIT_TEST(DBSuite, _INSERT_BadColumn2)
{
    EagleDbInstance *db = EagleInstanceTest(10);
    
    EagleLogger::Event *error = NULL;
    bool success = EagleDbInstance_execute(db, "INSERT INTO mytable (123 + 456) VALUES (123);", &error, NULL);
    CUNIT_ASSERT_FALSE(success);
    CUNIT_ASSERT_LAST_ERROR("You cannot use expressions for column names");
    
    EagleInstanceTest_Cleanup(db);
}

CUNIT_TEST(DBSuite, _INSERT_BadValue1)
{
    EagleDbInstance *db = EagleInstanceTest(10);
    
    EagleLogger::Event *error = NULL;
    bool success = EagleDbInstance_execute(db, "INSERT INTO mytable (col1) VALUES (123 + 456);", &error, NULL);
    CUNIT_ASSERT_FALSE(success);
    CUNIT_ASSERT_LAST_ERROR("Expressions in VALUES are not yet supported for column 'col1'");
    
    EagleInstanceTest_Cleanup(db);
}

CUNIT_TEST(DBSuite, _INSERT_BadValue2)
{
    EagleDbInstance *db = EagleInstanceTest(10);
    
    EagleLogger::Event *error = NULL;
    bool success = EagleDbInstance_execute(db, "INSERT INTO mytable (col1) VALUES (col1);", &error, NULL);
    CUNIT_ASSERT_FALSE(success);
    CUNIT_ASSERT_LAST_ERROR("Type for column col1 is INTEGER, but Identifier given.");
    
    EagleInstanceTest_Cleanup(db);
}

CUNIT_TEST(DBSuite, EagleDbSqlSelect_toString)
{
    EagleDbSqlSelect *select = EagleDbSqlSelect_New();
    select->selectExpressions = new EagleSynchronizedLinkedList<EagleDbSqlExpression*>();
    select->selectExpressions->addObject((EagleDbSqlExpression*) EagleDbSqlValue_NewWithInteger(123), true, (void(*)(EagleDbSqlExpression*))EagleDbSqlExpression_DeleteRecursive);
    select->selectExpressions->addObject((EagleDbSqlExpression*) EagleDbSqlValue_NewWithInteger(456), true, (void(*)(EagleDbSqlExpression*))EagleDbSqlExpression_DeleteRecursive);
    select->whereExpression = (EagleDbSqlExpression*) EagleDbSqlValue_NewWithInteger(789);
    select->tableName = strdup("mytable");
    
    char *desc = EagleDbSqlSelect_toString(select);
    CUNIT_VERIFY_EQUAL_STRING(desc, "SELECT 123, 456 FROM mytable WHERE 789");
    free(desc);
    
    EagleDbSqlSelect_DeleteRecursive(select);
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_2)
{
    EagleDbSqlExpression *op = (EagleDbSqlExpression*) EagleDbSqlUnaryExpression_New(EagleDbSqlUnaryExpressionOperatorNot, NULL);
    EagleDbSqlExpression *expr = (EagleDbSqlExpression*) EagleDbSqlUnaryExpression_New(EagleDbSqlUnaryExpressionOperatorNot, op);
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    
    selectStage->prepareBuffers(10);
    int destinationBuffer = 0;
    int result = EagleDbSqlExpression_CompilePlanIntoBuffer_(expr, &destinationBuffer, selectStage, true);
    CUNIT_VERIFY_EQUAL_INT(result, EagleDbSqlExpression_ERROR);
    
    EagleDbSqlExpression_DeleteRecursive(expr);
    delete selectStage;
}

CUNIT_TEST(DBSuite, EagleDbSqlUnaryExpression_Delete)
{
    EagleDbSqlUnaryExpression_Delete(NULL);
}

CUNIT_TEST(DBSuite, EagleDbSqlUnaryExpression_DeleteRecursive)
{
    EagleDbSqlUnaryExpression_DeleteRecursive(NULL);
}

CUNIT_TEST(DBSuite, EagleDbParser_lastError)
{
    CUNIT_VERIFY_NULL(EagleDbParser_lastError(NULL));
    
    EagleDbParser *parser = EagleDbParser_New();
    CUNIT_VERIFY_NULL(EagleDbParser_lastError(parser));
    EagleDbParser_Delete(parser);
}

CUNIT_TEST(DBSuite, EagleDbParser_Delete)
{
    EagleDbParser_Delete(NULL);
}

CUNIT_TEST(DBSuite, EagleDbParser_IsReservedKeyword)
{
    CUNIT_VERIFY_TRUE(EagleDbParser_IsReservedKeyword("CREATE"));
    CUNIT_VERIFY_TRUE(EagleDbParser_IsReservedKeyword("table"));
    CUNIT_VERIFY_FALSE(EagleDbParser_IsReservedKeyword("notakeyword"));
}

CUNIT_TEST(DBSuite, _BadEntityName)
{
    EagleDbInstance *db = EagleInstanceTest(10);
    bool success;
    EagleLogger::Event *error = NULL;
    
    success = EagleDbInstance_execute(db, "CREATE TABLE insert (col1 int);", &error, NULL);
    CUNIT_ASSERT_FALSE(success);
    CUNIT_ASSERT_LAST_ERROR("Error: You cannot use the keyword 'INSERT' for an table name.");
    
    success = EagleDbInstance_execute(db, "CREATE TABLE mytable (TABLE int);", &error, NULL);
    CUNIT_ASSERT_FALSE(success);
    CUNIT_ASSERT_LAST_ERROR("Error: You cannot use the keyword 'TABLE' for a column name.");
    
    EagleInstanceTest_Cleanup(db);
}

CUNIT_TEST(DBSuite, EagleDbInformationSchema_tables)
{
    int cores = 1;
    EagleDbInstance *db = EagleDbInstance_New(10, cores);
    
    /* parse sql */
    EagleDbParser *p = EagleDbParser_ParseWithString("select table_schema, table_name from information_schema_tables;");
    if(true == EagleDbParser_hasError(p)) {
        CUNIT_FAIL("%s", EagleDbParser_lastError(p));
    }
    CUNIT_ASSERT_FALSE(EagleDbParser_hasError(p));
    
    eagle::Plan *plan = EagleDbSqlSelect_parse((EagleDbSqlSelect*) p->yyparse_ast, db);
    eagle::plan::stage::SelectStage *selectStage = (eagle::plan::stage::SelectStage*) plan->stages->get(0);
    if(selectStage->isError()) {
        CUNIT_FAIL("%s", selectStage->errorMessage.c_str());
    }
    CUNIT_ASSERT_FALSE(selectStage->isError());
    plan->addStage(selectStage, false);
    
    /* execute */
    eagle::Instance *eagle = new eagle::Instance(cores);
    eagle->addPlan(plan);
    eagle->run();
    
    /* check results */
    EaglePage *page1 = selectStage->result[0]->nextPage();
    EaglePage *page2 = selectStage->result[1]->nextPage();
    CUNIT_ASSERT_NOT_NULL(page1);
    CUNIT_ASSERT_NOT_NULL(page2);
    
    delete page1;
    delete page2;
    
    delete plan;
    EagleDbSqlExpression_DeleteRecursive((EagleDbSqlExpression*) p->yyparse_ast);
    delete eagle;
    EagleDbParser_Delete(p);
    EagleDbInstance_Delete(db);
}

CUNIT_TEST(DBSuite, EagleDbInformationSchema_Delete)
{
    EagleDbInformationSchema_Delete(NULL);
}

CUNIT_TEST(DBSuite, _comment_multi)
{
    EagleDbInstance *db = EagleInstanceTest(10);
    bool success;
    EagleLogger::Event *error = NULL;
    
    success = EagleDbInstance_execute(db, "CREATE TABLE /* abc */ mytable2 (col1 int);", &error, NULL);
    if(false == success) {
        CUNIT_FAIL("%s", error->getMessage().c_str());
    }
    CUNIT_ASSERT_TRUE(success);
    
    success = EagleDbInstance_execute(db, "CREATE TABLE /* abc", &error, NULL);
    CUNIT_ASSERT_FALSE(success);
    CUNIT_ASSERT_LAST_ERROR("Error: syntax error, unexpected end of file");
    
    EagleInstanceTest_Cleanup(db);
}

CUNIT_TEST(DBSuite, _comment_single)
{
    EagleDbInstance *db = EagleInstanceTest(10);
    bool success;
    EagleLogger::Event *error = NULL;
    
    success = EagleDbInstance_execute(db, "-- create a table\nCREATE TABLE mytable2 (col1 int);", &error, NULL);
    CUNIT_ASSERT_TRUE(success);
    
    success = EagleDbInstance_execute(db, "CREATE TABLE mytable3 -- create a table\n (col1 int);", &error, NULL);
    CUNIT_ASSERT_TRUE(success);
    
    success = EagleDbInstance_execute(db, "CREATE TABLE mytable4 -- create a table (col1 int);", &error, NULL);
    CUNIT_ASSERT_FALSE(success);
    CUNIT_ASSERT_LAST_ERROR("Error: syntax error, unexpected end of file, expecting (");
    
    EagleInstanceTest_Cleanup(db);
}

CUNIT_TEST(DBSuite, EagleDbParser_IsNonreservedKeyword)
{
    CUNIT_VERIFY_FALSE(EagleDbParser_IsNonreservedKeyword("CREATE"));
    CUNIT_VERIFY_FALSE(EagleDbParser_IsNonreservedKeyword("table"));
    CUNIT_VERIFY_TRUE(EagleDbParser_IsNonreservedKeyword("A"));
}

CUNIT_TEST(DBSuite, _CREATE)
{
    EagleDbInstance *db = EagleInstanceTest(10);
    bool success;
    EagleLogger::Event *error = NULL;
    
    success = EagleDbInstance_execute(db, "CREATE TABLE mytable2 (col1 int, col2 integer, col3 varchar, col4 text);", &error, NULL);
    if(false == success) {
        CUNIT_FAIL("%s", error->getMessage().c_str());
    }
    CUNIT_ASSERT_TRUE(success);
    
    success = EagleDbInstance_execute(db, "CREATE TABLE mytable2 (col1 int, col2 badtype, col4 text);", &error, NULL);
    CUNIT_ASSERT_FALSE(success);
    CUNIT_ASSERT_LAST_ERROR("Error: syntax error, unexpected identifier");
    
    EagleInstanceTest_Cleanup(db);
}

CUNIT_TEST(DBSuite, _string_literal)
{
    EagleDbInstance *db = EagleInstanceTest(10);
    bool success;
    EagleLogger::Event *error = NULL;
    
    success = EagleDbInstance_execute(db, "CREATE TABLE mytable2 (col1 int, col2 text);", &error, NULL);
    if(false == success) {
        CUNIT_FAIL("%s", error->getMessage().c_str());
    }
    CUNIT_ASSERT_TRUE(success);
    
    success = EagleDbInstance_execute(db, "INSERT INTO mytable2 (col1, col2) VALUES (123, 'some '' cool \' text');", &error, NULL);
    if(false == success) {
        CUNIT_FAIL("%s", error->getMessage().c_str());
    }
    CUNIT_ASSERT_TRUE(success);
    
    // SELECT data back
    EagleDbParser *p = EagleDbParser_ParseWithString("SELECT col1, col2, 'some string' FROM mytable2;");
    CUNIT_ASSERT_FALSE(EagleDbParser_hasError(p));
    
    eagle::Plan *plan = EagleDbSqlSelect_parse((EagleDbSqlSelect*) p->yyparse_ast, db);
    eagle::plan::stage::SelectStage *selectStage = (eagle::plan::stage::SelectStage*) plan->stages->get(0);
    
    // catch compilation error
    CUNIT_ASSERT_FALSE(selectStage->isError());
    
    // execute
    eagle::Instance *eagle = new eagle::Instance(1);
    eagle->addPlan(plan);
    eagle->run();
    
    // validate data
    EaglePage *page1 = selectStage->result[0]->nextPage();
    EaglePage *page2 = selectStage->result[1]->nextPage();
    EaglePage *page3 = selectStage->result[2]->nextPage();
    CUNIT_ASSERT_NOT_NULL(page1);
    CUNIT_ASSERT_NOT_NULL(page2);
    CUNIT_ASSERT_NOT_NULL(page3);
    
    CUNIT_ASSERT_EQUAL_INT(page1->type, EagleData::Type::Integer);
    CUNIT_ASSERT_EQUAL_INT(page2->type, EagleData::Type::Varchar);
    CUNIT_ASSERT_EQUAL_INT(page3->type, EagleData::Type::Varchar);
    CUNIT_ASSERT_EQUAL_INT(1, page1->count);
    CUNIT_ASSERT_EQUAL_INT(1, page2->count);
    CUNIT_ASSERT_EQUAL_INT(10, page3->count);
    CUNIT_ASSERT_EQUAL_INT(((int*) page1->data)[0], 123);
    CUNIT_ASSERT_EQUAL_STRING(((char**) page2->data)[0], "some ' cool ' text");
    CUNIT_ASSERT_EQUAL_STRING(((char**) page3->data)[0], "some string");
    
    delete page1;
    delete page2;
    delete page3;
    
    delete eagle;
    delete plan;
    
    EagleDbSqlSelect_DeleteRecursive((EagleDbSqlSelect*) p->yyparse_ast);
    EagleDbParser_Delete(p);
    
    EagleInstanceTest_Cleanup(db);
}

CUNIT_TEST(DBSuite, EagleDbSqlValue_toString_2)
{
    EagleDbSqlValue *v = EagleDbSqlValue_NewWithString("some string", false);
    char *desc = EagleDbSqlValue_toString(v);
    CUNIT_VERIFY_EQUAL_STRING(desc, "'some string'");
    free(desc);
    EagleDbSqlValue_Delete(v);
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Function_, sqrt)
{
    EagleDbSqlExpression *expr0 = (EagleDbSqlExpression*) EagleDbSqlValue_NewWithFloat(123.456);
    EagleDbSqlFunctionExpression *expr = EagleDbSqlFunctionExpression_NewWithSingleArgument("sqrt", expr0, false);
    
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    selectStage->prepareBuffers(10);
    
    int destinationBuffer = 0;
    EagleDbSqlExpression_CompilePlanIntoBuffer_((EagleDbSqlExpression*) expr, &destinationBuffer, selectStage, false);
    CUNIT_VERIFY_TRUE(destinationBuffer >= 0);
    CUNIT_VERIFY_TRUE(selectStage->errorMessage.empty());
    
    EagleDbSqlExpression_Delete((EagleDbSqlExpression*) expr);
    EagleDbSqlExpression_Delete((EagleDbSqlExpression*) expr0);
    delete selectStage;
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Function_, NoSuchFunction)
{
    EagleDbSqlExpression *expr0 = (EagleDbSqlExpression*) EagleDbSqlValue_NewWithFloat(123.456);
    EagleDbSqlExpression *expr1 = (EagleDbSqlExpression*) EagleDbSqlValue_NewWithInteger(789);
    EagleDbSqlFunctionExpression *expr = EagleDbSqlFunctionExpression_NewWithTwoArguments("doesNotExist", expr0, false, expr1, false);
    
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    selectStage->prepareBuffers(10);
    
    int destinationBuffer = 0;
    int r = EagleDbSqlExpression_CompilePlanIntoBuffer_Function_((EagleDbSqlExpression*) expr, &destinationBuffer, selectStage);
    CUNIT_VERIFY_EQUAL_INT(r, EagleDbSqlExpression_ERROR);
    CUNIT_VERIFY_EQUAL_STRING(selectStage->errorMessage.c_str(), "Function doesNotExist(FLOAT, INTEGER) does not exist.");
    
    EagleDbSqlExpression_Delete((EagleDbSqlExpression*) expr);
    EagleDbSqlExpression_Delete((EagleDbSqlExpression*) expr0);
    EagleDbSqlExpression_Delete((EagleDbSqlExpression*) expr1);
    delete selectStage;
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Function_, Error)
{
    EagleDbSqlFunctionExpression *expr = EagleDbSqlFunctionExpression_New("doesNotExist", NULL);
    
    int destinationBuffer = 0;
    int r = EagleDbSqlExpression_CompilePlanIntoBuffer_Function_((EagleDbSqlExpression*) expr, &destinationBuffer, NULL);
    CUNIT_VERIFY_EQUAL_INT(r, EagleDbSqlExpression_ERROR);
    
    EagleDbSqlExpression_Delete((EagleDbSqlExpression*) expr);
}

CUNIT_TEST(DBSuite, EagleDbSqlFunctionExpression_Delete)
{
    EagleDbSqlFunctionExpression_Delete(NULL);
    
    EagleDbSqlFunctionExpression *expr = EagleDbSqlFunctionExpression_New("sqrt", NULL);
    EagleDbSqlFunctionExpression_Delete(expr);
}

CUNIT_TEST(DBSuite, EagleDbSqlFunctionExpression_DeleteRecursive)
{
    EagleDbSqlFunctionExpression_DeleteRecursive(NULL);
}

CUNIT_TEST(DBSuite, EagleDbSqlBinaryExpression_GetPageOperation)
{
    EagleDbSqlBinaryOperator op;
    CUNIT_ASSERT_FALSE(EagleDbSqlBinaryExpression_GetPageOperation(EagleData::Type::Unknown, EagleDbSqlBinaryExpressionOperatorAnd, EagleData::Type::Unknown, &op));
}

CUNIT_TEST(DBSuite, EagleDbSqlValue_toString, Float1)
{
    EagleDbSqlValue *v = EagleDbSqlValue_NewWithFloat(123.0);
    char *desc = EagleDbSqlValue_toString(v);
    CUNIT_VERIFY_EQUAL_STRING(desc, "123");
    free(desc);
    EagleDbSqlValue_Delete(v);
}

CUNIT_TEST(DBSuite, EagleDbSqlValue_toString, Float2)
{
    EagleDbSqlValue *v = EagleDbSqlValue_NewWithFloat(123.456);
    char *desc = EagleDbSqlValue_toString(v);
    CUNIT_VERIFY_EQUAL_STRING(desc, "123.456");
    free(desc);
    EagleDbSqlValue_Delete(v);
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Binary_)
{
    EagleDbSqlBinaryExpression *expr = EagleDbSqlBinaryExpression_New((EagleDbSqlExpression*) EagleDbSqlValue_NewWithFloat(123.0),
                                                                      EagleDbSqlBinaryExpressionOperatorPlus,
                                                                      (EagleDbSqlExpression*) EagleDbSqlValue_NewWithInteger(456));
    int destinationBuffer = 0;
    
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    selectStage->prepareBuffers(10);
    
    EagleDbSqlExpression_CompilePlanIntoBuffer_Binary_((EagleDbSqlExpression*) expr, &destinationBuffer, selectStage);
    CUNIT_ASSERT_EQUAL_STRING(selectStage->errorMessage.c_str(), "No such binary operator FLOAT + INTEGER");
    
    EagleDbSqlExpression_DeleteRecursive((EagleDbSqlExpression*) expr);
    delete selectStage;
}

CUNIT_TEST(DBSuite, EagleDbSqlUnaryExpression_GetOperation, NotFound)
{
    EagleDbSqlUnaryOperator op;
    CUNIT_ASSERT_FALSE(EagleDbSqlUnaryExpression_GetOperation(EagleDbSqlUnaryExpressionOperatorNegate, EagleData::Type::Unknown, &op));
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Unary_)
{
    EagleDbSqlUnaryExpression *expr = EagleDbSqlUnaryExpression_New(EagleDbSqlUnaryExpressionOperatorNot,
                                                                    (EagleDbSqlExpression*) EagleDbSqlValue_NewWithFloat(123.456));
    int destinationBuffer = 0;
    
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    selectStage->prepareBuffers(10);
    
    EagleDbSqlExpression_CompilePlanIntoBuffer_Unary_((EagleDbSqlExpression*) expr, &destinationBuffer, selectStage);
    CUNIT_ASSERT_EQUAL_STRING(selectStage->errorMessage.c_str(), "No such unary operator NOT FLOAT");
    
    EagleDbSqlExpression_DeleteRecursive((EagleDbSqlExpression*) expr);
    delete selectStage;
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Cast_)
{
    EagleDbSqlCastExpression *expr = EagleDbSqlCastExpression_New(NULL,
                                                                  EagleData::Type::Float);
    int destinationBuffer = 0;
    
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    selectStage->prepareBuffers(10);
    
    CUNIT_VERIFY_EQUAL_INT(EagleDbSqlExpression_CompilePlanIntoBuffer_Cast_((EagleDbSqlExpression*) expr, &destinationBuffer, selectStage), EagleDbSqlExpression_ERROR);
    
    EagleDbSqlExpression_DeleteRecursive((EagleDbSqlExpression*) expr);
    delete selectStage;
}

CUNIT_TEST(DBSuite, EagleDbSqlCastExpression_Delete)
{
    EagleDbSqlCastExpression_Delete(NULL);
}

CUNIT_TEST(DBSuite, EagleDbSqlCastExpression_DeleteRecursive)
{
    EagleDbSqlCastExpression_DeleteRecursive(NULL);
}

CUNIT_TEST(DBSuite, EagleDbSqlValue_getInteger)
{
    EagleDbSqlValue *value = EagleDbSqlValue_NewWithFloat(123.456);
    bool success;
    CUNIT_VERIFY_EQUAL_INT(EagleDbSqlValue_getInteger(value, &success), 123);
    CUNIT_VERIFY_EQUAL_INT(success, true);
    EagleDbSqlValue_Delete(value);
}

CUNIT_TEST(DBSuite, EagleDbSqlValue_getFloat_1)
{
    EagleDbSqlValue *value = EagleDbSqlValue_NewWithAsterisk();
    bool success;
    CUNIT_VERIFY_EQUAL_DOUBLE(EagleDbSqlValue_getFloat(value, &success), 0.0);
    CUNIT_VERIFY_EQUAL_INT(success, false);
    EagleDbSqlValue_Delete(value);
}

CUNIT_TEST(DBSuite, EagleDbSqlValue_getFloat_2)
{
    EagleDbSqlValue *value = EagleDbSqlValue_NewWithInteger(456);
    bool success;
    CUNIT_VERIFY_EQUAL_DOUBLE(EagleDbSqlValue_getFloat(value, &success), 456.0);
    CUNIT_VERIFY_EQUAL_INT(success, true);
    EagleDbSqlValue_Delete(value);
}

CUNIT_TEST(DBSuite, EagleDbSqlValue_getVarchar)
{
    EagleDbSqlValue *value = EagleDbSqlValue_NewWithFloat(123.456);
    bool success;
    CUNIT_VERIFY_NULL(EagleDbSqlValue_getVarchar(value, &success));
    CUNIT_VERIFY_EQUAL_INT(success, false);
    EagleDbSqlValue_Delete(value);
}

CUNIT_TEST(DBSuite, EagleDbSqlValue_castable)
{
    EagleDbSqlValue *value = EagleDbSqlValue_NewWithFloat(123.456);
    bool success = EagleDbSqlValue_castable(value, EagleData::Type::Unknown);
    CUNIT_VERIFY_EQUAL_INT(success, false);
    EagleDbSqlValue_Delete(value);
}

CUNIT_TEST(DBSuite, EagleDbSqlValueType_toString, Asterisk)
{
    char *s = EagleDbSqlValueType_toString(EagleDbSqlValueTypeAsterisk);
    CUNIT_VERIFY_EQUAL_STRING(s, "Asterisk");
    free(s);
}

CUNIT_TEST(DBSuite, EagleDbSqlValueType_toString, Float)
{
    char *s = EagleDbSqlValueType_toString(EagleDbSqlValueTypeFloat);
    CUNIT_VERIFY_EQUAL_STRING(s, "FLOAT");
    free(s);
}

CUNIT_TEST(DBSuite, EagleDbSqlValueType_toString, Identifier)
{
    char *s = EagleDbSqlValueType_toString(EagleDbSqlValueTypeIdentifier);
    CUNIT_VERIFY_EQUAL_STRING(s, "Identifier");
    free(s);
}

CUNIT_TEST(DBSuite, EagleDbSqlValueType_toString, Integer)
{
    char *s = EagleDbSqlValueType_toString(EagleDbSqlValueTypeInteger);
    CUNIT_VERIFY_EQUAL_STRING(s, "INTEGER");
    free(s);
}

CUNIT_TEST(DBSuite, EagleDbSqlValueType_toString, Varchar)
{
    char *s = EagleDbSqlValueType_toString(EagleDbSqlValueTypeString);
    CUNIT_VERIFY_EQUAL_STRING(s, "VARCHAR");
    free(s);
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Value_)
{
    EagleDbSqlValue *value = EagleDbSqlValue_NewWithString("abc", false);
    int destinationBuffer = 1;
    
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 10, 1);
    selectStage->prepareBuffers(3);
    
    EagleDbSqlExpression_CompilePlanIntoBuffer_Value_((EagleDbSqlExpression*) value, &destinationBuffer, selectStage, false);
    CUNIT_VERIFY_EQUAL_INT(selectStage->bufferTypes[destinationBuffer], EagleData::Type::Varchar);
    
    delete selectStage;
    EagleDbSqlValue_Delete(value);
}

CUNIT_TEST(DBSuite, EagleDbSqlBinaryExpression_GetRightOperation)
{
    EagleDbSqlBinaryOperator match;
    CUNIT_VERIFY_FALSE(EagleDbSqlBinaryExpression_GetRightOperation(EagleData::Type::Unknown, EagleDbSqlBinaryExpressionOperatorAnd, &match));
}

CUNIT_TEST(DBSuite, EagleDbSqlBinaryExpression_GetLeftOperation)
{
    EagleDbSqlBinaryOperator match;
    CUNIT_VERIFY_FALSE(EagleDbSqlBinaryExpression_GetLeftOperation(EagleData::Type::Unknown, EagleDbSqlBinaryExpressionOperatorAnd, &match));
}

CUNIT_TEST(DBSuite, EagleDbInstance_executeParser)
{
    EagleDbParser *p = EagleDbParser_New();
    CUNIT_VERIFY_TRUE(EagleDbInstance_executeParser(NULL, p, NULL, NULL));
    EagleDbParser_Delete(p);
}

CUNIT_TEST(DBSuite, EagleDbSqlBinaryExpressionOperator_toString, OR)
{
    char *s = EagleDbSqlBinaryExpressionOperator_toString(EagleDbSqlBinaryExpressionOperatorOr);
    CUNIT_ASSERT_EQUAL_STRING(s, "OR");
    free(s);
}

CUNIT_TEST(DBSuite, EagleDbSqlBinaryExpressionOperator_toString, AND)
{
    char *s = EagleDbSqlBinaryExpressionOperator_toString(EagleDbSqlBinaryExpressionOperatorAnd);
    CUNIT_ASSERT_EQUAL_STRING(s, "AND");
    free(s);
}

CUNIT_TEST(DBSuite, EagleDbSqlUnaryExpression_toString)
{
    EagleDbSqlUnaryExpression *expr = EagleDbSqlUnaryExpression_New(EagleDbSqlUnaryExpressionOperatorNot, (EagleDbSqlExpression*) EagleDbSqlValue_NewWithIdentifier("abc"));
    char *desc = EagleDbSqlExpression_toString((EagleDbSqlExpression*) expr);
    CUNIT_VERIFY_EQUAL_STRING(desc, "NOT abc");
    free(desc);
    EagleDbSqlExpression_DeleteRecursive((EagleDbSqlExpression*) expr);
}

CUNIT_TEST(DBSuite, EagleDbSqlUnaryExpression_GetOperation, Found)
{
    EagleDbSqlUnaryOperator op;
    CUNIT_VERIFY_TRUE(EagleDbSqlUnaryExpression_GetOperation(EagleDbSqlUnaryExpressionOperatorNot, EagleData::Type::Integer, &op));
}

CUNIT_TEST(DBSuite, EagleDbSqlUnaryExpressionOperator_toString, EagleDbSqlUnaryExpressionOperatorNegate)
{
    char *before, *after;
    EagleDbSqlUnaryExpressionOperator_toString(EagleDbSqlUnaryExpressionOperatorNegate, &before, &after);
    
    CUNIT_VERIFY_EQUAL_STRING(before, "-");
    CUNIT_VERIFY_EQUAL_STRING(after, "");
    
    free(before);
    free(after);
}

CUNIT_TEST(DBSuite, EagleDbSqlUnaryExpressionOperator_toString, EagleDbSqlUnaryExpressionOperatorGrouping)
{
    char *before, *after;
    EagleDbSqlUnaryExpressionOperator_toString(EagleDbSqlUnaryExpressionOperatorGrouping, &before, &after);
    
    CUNIT_VERIFY_EQUAL_STRING(before, "(");
    CUNIT_VERIFY_EQUAL_STRING(after, ")");
    
    free(before);
    free(after);
}

CUNIT_TEST(DBSuite, EagleDbSqlUnaryExpressionOperator_toString, EagleDbSqlUnaryExpressionOperatorNot)
{
    char *before, *after;
    EagleDbSqlUnaryExpressionOperator_toString(EagleDbSqlUnaryExpressionOperatorNot, &before, &after);
    
    CUNIT_VERIFY_EQUAL_STRING(before, "NOT ");
    CUNIT_VERIFY_EQUAL_STRING(after, "");
    
    free(before);
    free(after);
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Unary_, Grouping)
{
    EagleDbSqlUnaryExpression *expr = EagleDbSqlUnaryExpression_New(EagleDbSqlUnaryExpressionOperatorGrouping,
                                                                    (EagleDbSqlExpression*) EagleDbSqlValue_NewWithFloat(123.456));
    int destinationBuffer = 0;
    
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    selectStage->prepareBuffers(10);
    
    int r = EagleDbSqlExpression_CompilePlanIntoBuffer_Unary_((EagleDbSqlExpression*) expr, &destinationBuffer, selectStage);
    CUNIT_ASSERT_EQUAL_INT(r, destinationBuffer);
    
    EagleDbSqlExpression_DeleteRecursive((EagleDbSqlExpression*) expr);
    delete selectStage;
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Cast_, NoMatch)
{
    EagleDbSqlExpression *expr0 = (EagleDbSqlExpression*) EagleDbSqlValue_NewWithFloat(123.456);
    EagleDbSqlCastExpression *expr = EagleDbSqlCastExpression_New(expr0, EagleData::Type::Integer);
    
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    selectStage->prepareBuffers(10);
    
    int destinationBuffer = 0;
    int r = EagleDbSqlExpression_CompilePlanIntoBuffer_Cast_((EagleDbSqlExpression*) expr, &destinationBuffer, selectStage);
    CUNIT_VERIFY_EQUAL_INT(r, EagleDbSqlExpression_ERROR);
    
    EagleDbSqlExpression_Delete((EagleDbSqlExpression*) expr);
    EagleDbSqlExpression_Delete((EagleDbSqlExpression*) expr0);
    delete selectStage;
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Cast_, Match)
{
    EagleDbSqlExpression *expr0 = (EagleDbSqlExpression*) EagleDbSqlValue_NewWithInteger(123);
    EagleDbSqlCastExpression *expr = EagleDbSqlCastExpression_New(expr0, EagleData::Type::Float);
    
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    selectStage->prepareBuffers(10);
    
    int destinationBuffer = 0;
    int r = EagleDbSqlExpression_CompilePlanIntoBuffer_((EagleDbSqlExpression*) expr, &destinationBuffer, selectStage, false);
    CUNIT_VERIFY_TRUE(r >= 0);
    
    EagleDbSqlExpression_Delete((EagleDbSqlExpression*) expr);
    EagleDbSqlExpression_Delete((EagleDbSqlExpression*) expr0);
    delete selectStage;
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Binary_, LeftFail)
{
    EagleDbSqlBinaryExpression *expr = EagleDbSqlBinaryExpression_New(NULL,
                                                                      EagleDbSqlBinaryExpressionOperatorPlus,
                                                                      (EagleDbSqlExpression*) EagleDbSqlValue_NewWithInteger(456));
    int destinationBuffer = 0;
    
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    selectStage->prepareBuffers(10);
    
    int r = EagleDbSqlExpression_CompilePlanIntoBuffer_Binary_((EagleDbSqlExpression*) expr, &destinationBuffer, selectStage);
    CUNIT_ASSERT_EQUAL_INT(r, EagleDbSqlExpression_ERROR);
    
    EagleDbSqlExpression_DeleteRecursive((EagleDbSqlExpression*) expr);
    delete selectStage;
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Binary_, RightFail)
{
    EagleDbSqlBinaryExpression *expr = EagleDbSqlBinaryExpression_New((EagleDbSqlExpression*) EagleDbSqlValue_NewWithInteger(456),
                                                                      EagleDbSqlBinaryExpressionOperatorPlus,
                                                                      NULL);
    int destinationBuffer = 0;
    
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    selectStage->prepareBuffers(10);
    
    int r = EagleDbSqlExpression_CompilePlanIntoBuffer_Binary_((EagleDbSqlExpression*) expr, &destinationBuffer, selectStage);
    CUNIT_ASSERT_EQUAL_INT(r, EagleDbSqlExpression_ERROR);
    
    EagleDbSqlExpression_DeleteRecursive((EagleDbSqlExpression*) expr);
    delete selectStage;
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Value_, IdentifierError)
{
    EagleDbSqlValue *value = EagleDbSqlValue_NewWithIdentifier("dummy");
    int destinationBuffer = 1;
    
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 10, 1);
    selectStage->prepareBuffers(3);
    
    EagleDbSqlExpression_CompilePlanIntoBuffer_Value_((EagleDbSqlExpression*) value, &destinationBuffer, selectStage, false);
    CUNIT_VERIFY_EQUAL_STRING(selectStage->errorMessage.c_str(), "Unknown column 'dummy'");
    
    delete selectStage;
    EagleDbSqlValue_Delete(value);
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Value_, Asterisk)
{
    EagleDbSqlValue *value = EagleDbSqlValue_NewWithAsterisk();
    int destinationBuffer = 1;
    
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 10, 1);
    selectStage->prepareBuffers(3);
    
    EagleDbSqlExpression_CompilePlanIntoBuffer_Value_((EagleDbSqlExpression*) value, &destinationBuffer, selectStage, false);
    CUNIT_VERIFY_EQUAL_STRING(selectStage->errorMessage.c_str(), "You can not use the star operator like this.");
    
    delete selectStage;
    EagleDbSqlValue_Delete(value);
}

CUNIT_TEST(DBSuite, EagleDbSqlFunctionExpression_toString)
{
    EagleDbSqlFunctionExpression *expr = EagleDbSqlFunctionExpression_New("sqrt", NULL);
    char *desc = EagleDbSqlExpression_toString((EagleDbSqlExpression*) expr);
    CUNIT_VERIFY_EQUAL_STRING(desc, "sqrt()");
    free(desc);
    EagleDbSqlExpression_DeleteRecursive((EagleDbSqlExpression*) expr);
}

CUNIT_TEST(DBSuite, EagleDbSqlCastExpression_toString)
{
    EagleDbSqlCastExpression *expr = EagleDbSqlCastExpression_New(NULL, EagleData::Type::Integer);
    char *desc = EagleDbSqlExpression_toString((EagleDbSqlExpression*) expr);
    CUNIT_VERIFY_EQUAL_STRING(desc, "CAST( AS INTEGER)");
    free(desc);
    EagleDbSqlExpression_DeleteRecursive((EagleDbSqlExpression*) expr);
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Unary_, Match)
{
    EagleDbSqlUnaryExpression *expr = EagleDbSqlUnaryExpression_New(EagleDbSqlUnaryExpressionOperatorNegate,
                                                                    (EagleDbSqlExpression*) EagleDbSqlValue_NewWithFloat(123.456));
    int destinationBuffer = 0;
    
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    selectStage->prepareBuffers(10);
    
    int r = EagleDbSqlExpression_CompilePlanIntoBuffer_Unary_((EagleDbSqlExpression*) expr, &destinationBuffer, selectStage);
    CUNIT_ASSERT_TRUE(r >= 0);
    CUNIT_ASSERT_TRUE(destinationBuffer >= 0);
    
    EagleDbSqlExpression_DeleteRecursive((EagleDbSqlExpression*) expr);
    delete selectStage;
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_CompilePlan, Fail)
{
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    EagleDbSqlExpression **exprs = (EagleDbSqlExpression**) calloc(1, sizeof(EagleDbSqlExpression*));
    exprs[0] = NULL;
    
    EagleDbSqlExpression_CompilePlan(exprs, 1, -1, selectStage);
    selectStage->result[0] = NULL;
    
    delete exprs;
    delete selectStage;
}

CUNIT_TEST(DBSuite, EagleDbSqlFunction_Delete)
{
    EagleDbSqlFunction_Delete(NULL);
    
    EagleDbSqlFunction *f = EagleDbSqlFunction_New(NULL, "some name", EagleData::Type::Unknown, 0, NULL);
    CUNIT_ASSERT_NOT_NULL(f);
    EagleDbSqlFunction_Delete(f);
}

CUNIT_TEST(DBSuite, EagleDbSqlFunction_Find, Found)
{
    EagleData::Type *types = (EagleData::Type*) calloc(1, sizeof(EagleData::Type));
    types[0] = EagleData::Type::Float;
    
    EagleDbSqlFunction *f = EagleDbSqlFunction_Find("sqrt", types, 1);
    CUNIT_VERIFY_NOT_NULL(f);
    
    // clean up
    free(types);
}

CUNIT_TEST(DBSuite, EagleDbSqlFunction_Find, NotFound)
{
    EagleData::Type *types = (EagleData::Type*) calloc(1, sizeof(EagleData::Type));
    EagleDbSqlFunction *f = EagleDbSqlFunction_Find("doesnotexist", types, 1);
    
    CUNIT_VERIFY_NULL(f);
    
    // clean up
    free(types);
}

CUNIT_TEST(DBSuite, EagleDbSqlFunction_Find, FoundWrongType)
{
    EagleData::Type *types = (EagleData::Type*) calloc(1, sizeof(EagleData::Type));
    types[0] = EagleData::Type::Integer;
    
    EagleDbSqlFunction *f = EagleDbSqlFunction_Find("sqrt", types, 1);
    CUNIT_VERIFY_NULL(f);
    
    // clean up
    free(types);
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Function_)
{
    EagleLinkedList<EagleDbSqlExpression*> *args = new EagleSynchronizedLinkedList<EagleDbSqlExpression*>();
    args->addObject((EagleDbSqlExpression*) EagleDbSqlValue_NewWithAsterisk(), true, (void(*)(EagleDbSqlExpression*)) EagleDbSqlValue_Delete);
    
    EagleDbSqlFunctionExpression *expr = EagleDbSqlFunctionExpression_New("sqrt", args);
    int destinationBuffer = 0;
    
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    selectStage->prepareBuffers(10);
    
    EagleDbSqlExpression_CompilePlanIntoBuffer_Function_((EagleDbSqlExpression*) expr, &destinationBuffer, selectStage);
    CUNIT_ASSERT_EQUAL_STRING(selectStage->errorMessage.c_str(), "You can not use the star operator like this.");
    
    EagleDbSqlExpression_DeleteRecursive((EagleDbSqlExpression*) expr);
    delete selectStage;
}

CUnitTests* DBSuite2_tests()
{
    CUnitTests *tests = CUnitTests_New(100);
    
    // method tests
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Function_));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlFunction_Find, FoundWrongType));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlFunction_Find, NotFound));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlFunction_Find, Found));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlFunction_Delete));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_CompilePlan, Fail));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Unary_, Match));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlCastExpression_toString));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlFunctionExpression_toString));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Value_, Asterisk));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Value_, IdentifierError));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Binary_, RightFail));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Binary_, LeftFail));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Cast_, NoMatch));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Cast_, Match));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Unary_, Grouping));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Function_, Error));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Function_, NoSuchFunction));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlUnaryExpressionOperator_toString, EagleDbSqlUnaryExpressionOperatorGrouping));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlUnaryExpressionOperator_toString, EagleDbSqlUnaryExpressionOperatorNot));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlUnaryExpressionOperator_toString, EagleDbSqlUnaryExpressionOperatorNegate));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlUnaryExpression_GetOperation, Found));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlUnaryExpression_toString));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlBinaryExpressionOperator_toString, AND));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlBinaryExpressionOperator_toString, OR));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbInstance_executeParser));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlBinaryExpression_GetLeftOperation));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlBinaryExpression_GetRightOperation));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Value_));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlValueType_toString, Asterisk));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlValueType_toString, Float));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlValueType_toString, Identifier));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlValueType_toString, Integer));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlValueType_toString, Varchar));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlValue_castable));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlValue_getVarchar));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlValue_getFloat_2));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlValue_getFloat_1));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlValue_getInteger));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlCastExpression_Delete));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlCastExpression_DeleteRecursive));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Cast_));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Unary_));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlUnaryExpression_GetOperation, NotFound));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Binary_));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlValue_toString, Float1));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlValue_toString, Float2));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlBinaryExpression_GetPageOperation));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlFunctionExpression_DeleteRecursive));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlFunctionExpression_Delete));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Function_, sqrt));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlValue_toString_2));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, _string_literal));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, _CREATE));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbParser_IsNonreservedKeyword));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, _comment_single));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, _comment_multi));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbInformationSchema_Delete));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbInformationSchema_tables));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, _BadEntityName));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbParser_IsReservedKeyword));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbParser_Delete));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbParser_lastError));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlUnaryExpression_DeleteRecursive));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlUnaryExpression_Delete));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_2));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlSelect_toString));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, _INSERT_BadValue2));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, _INSERT_BadValue1));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, _INSERT_BadColumn2));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, _INSERT_BadColumn1));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, _INSERT_BadMatch));
    
    return tests;
}
