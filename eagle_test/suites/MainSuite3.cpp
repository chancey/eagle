#include "MainSuite.h"
#include <stdio.h>
#include <stdlib.h>
#include "eagle/PageProvider.h"
#include "eagle/StreamPageProvider.h"
#include "eagle/EaglePageOperations.h"
#include "eagle/Random.h"
#include "eagle/plan/Operation.h"
#include "eagle/Worker.h"
#include "eagle/EagleLogger.h"

CUNIT_TEST(MainSuite, EagleData, NameToTypeInteger)
{
    CUNIT_VERIFY_EQUAL_INT(EagleData::NameToType("int"), EagleData::Type::Integer);
    CUNIT_VERIFY_EQUAL_INT(EagleData::NameToType("integer"), EagleData::Type::Integer);
}

CUNIT_TEST(MainSuite, EagleData, NameToTypeVarchar)
{
    CUNIT_VERIFY_EQUAL_INT(EagleData::NameToType("varchar"), EagleData::Type::Varchar);
    CUNIT_VERIFY_EQUAL_INT(EagleData::NameToType("text"), EagleData::Type::Varchar);
}

CUNIT_TEST(MainSuite, EagleData, NameToTypeFloat)
{
    CUNIT_VERIFY_EQUAL_INT(EagleData::NameToType("float"), EagleData::Type::Float);
    CUNIT_VERIFY_EQUAL_INT(EagleData::NameToType("double"), EagleData::Type::Float);
    CUNIT_VERIFY_EQUAL_INT(EagleData::NameToType("double precision"), EagleData::Type::Float);
    CUNIT_VERIFY_EQUAL_INT(EagleData::NameToType("real"), EagleData::Type::Float);
}

CUNIT_TEST(MainSuite, EaglePageOperations_SendPageToProviderVarchar_, All)
{
    int recordsPerPage = 1;
    EaglePage *source2 = EaglePage::AllocVarchar(recordsPerPage);
    eagle::PageProvider *provider = (eagle::PageProvider*) new eagle::StreamPageProvider(EagleData::Type::Varchar,
                                                                                   recordsPerPage, "dummy");
    
    EagleDataTypeVarchar *source2data = (EagleDataTypeVarchar*) source2->data;
    source2data[0] = strdup("whatever");
    
    CUNIT_VERIFY_EQUAL_INT(provider->pagesRemaining(), 0);
    EaglePageOperations_SendPageToProviderVarchar_(provider, NULL, source2);
    CUNIT_VERIFY_EQUAL_INT(provider->pagesRemaining(), 1);
    
    delete source2;
    delete provider;
}

CUNIT_TEST(MainSuite, EaglePageOperations_SendPageToProviderFloat_, All)
{
    int recordsPerPage = 1;
    EaglePage *source2 = EaglePage::AllocFloat(recordsPerPage);
    eagle::PageProvider *provider = (eagle::PageProvider*) new eagle::StreamPageProvider(EagleData::Type::Float,
                                                                                   recordsPerPage, "dummy");
    
    CUNIT_VERIFY_EQUAL_INT(provider->pagesRemaining(), 0);
    EaglePageOperations_SendPageToProvider(NULL, NULL, source2, provider);
    CUNIT_VERIFY_EQUAL_INT(provider->pagesRemaining(), 1);
    
    delete source2;
    delete provider;
}

CUNIT_TEST(MainSuite, EaglePageOperations_SendPageToProviderVarchar_, Some)
{
    int recordsPerPage = 1;
    EaglePage *source1 = EaglePage::AllocInt(recordsPerPage);
    EaglePage *source2 = EaglePage::AllocVarchar(recordsPerPage);
    eagle::PageProvider *provider = (eagle::PageProvider*) new eagle::StreamPageProvider(EagleData::Type::Varchar,
                                                                                   recordsPerPage, "dummy");
    
    EagleDataTypeInteger *source1data = (EagleDataTypeInteger*) source1->data;
    source1data[0] = 1;
    EagleDataTypeVarchar *source2data = (EagleDataTypeVarchar*) source2->data;
    source2data[0] = strdup("whatever");
    
    CUNIT_VERIFY_EQUAL_INT(provider->pagesRemaining(), 0);
    EaglePageOperations_SendPageToProviderVarchar_(provider, source1, source2);
    CUNIT_VERIFY_EQUAL_INT(provider->pagesRemaining(), 1);
    
    delete source1;
    delete source2;
    delete provider;
}

CUNIT_TEST(MainSuite, eagleRandom_nextInt, SeededSingle)
{
    eagle::Random *random = new eagle::Random(123);
    
    int a = random->nextInt() % 1000000;
    int b = random->nextInt() % 1000000;
    int c = random->nextInt() % 1000000;
    int d = random->nextInt() % 1000000;
    int e = random->nextInt() % 1000000;
    
    CUNIT_VERIFY_EQUAL_INT(a, -499963);
    CUNIT_VERIFY_EQUAL_INT(b, -882850);
    CUNIT_VERIFY_EQUAL_INT(c, -886095);
    CUNIT_VERIFY_EQUAL_INT(d, 736922);
    CUNIT_VERIFY_EQUAL_INT(e, 978365);
    
    delete random;
}

CUNIT_TEST(MainSuite, eagleRandom_nextInt, Unseeded)
{
    const int size = 5;
    int a[size], b[size];
    
    eagle::Random *random1 = new eagle::Random();
    for(int i = 0; i < size; ++i) {
        a[i] = random1->nextInt() % 1000000;
    }
    
    eagle::Random *random2 = new eagle::Random();
    for(int i = 0; i < size; ++i) {
        b[i] = random2->nextInt() % 1000000;
    }
    
    // verify
    for(int i = 0; i < size; ++i) {
        for(int j = 0; j < size; ++j) {
            if(i == j) {
                continue;
            }
            
            if(a[i] == b[j]) {
                CUNIT_FAIL("%d found in both random sets", a[i]);
            }
        }
    }
    
    delete random1;
    delete random2;
}

CUNIT_TEST(MainSuite, eagleRandom, nextPositiveIntDistribution)
{
    eagle::Random *random = new eagle::Random(0);
    
    // the number of buckets to distribute the random numbers into
    int buckets = 64;
    
    // the minimum amount of random numbers that will give an acceptable distribution
    int period = buckets * buckets;
    
    // the number of random numbers to generate
    int setSize = period * buckets;
    
    // the maximum percentage the distribution buckets are allowed to differ from ideal (0.1 = 10%)
    double allowedVariance = 0.1;
    
    int min = (int) ((double) period * (1.0 - allowedVariance));
    int max = (int) ((double) period * (1.0 + allowedVariance));
    
    // prepare buckets
    int distribution[64];
    for(int i = 0; i < buckets; ++i) {
        distribution[i] = 0;
    }
    
    // generate random numbers
    for(int i = 0; i < setSize; ++i) {
        unsigned int r = random->nextPositiveInt();
        ++distribution[r / (eagle::Random::Max / 64)];
    }
    
    // validate the distribution
    for(int i = 0; i < buckets; ++i) {
        if(distribution[i] < min) {
            CUNIT_FAIL("%d is less than acceptable %d", distribution[i], min);
        }
        if(distribution[i] > max) {
            CUNIT_FAIL("%d is greater than acceptable %d", distribution[i], max);
        }
    }
    
    delete random;
}

CUNIT_TEST(MainSuite, eagleRandom_nextInt, SeededDouble)
{
    const int size = 5;
    int a[size], b[size];
    
    eagle::Random *random1 = new eagle::Random(123);
    a[0] = random1->nextPositiveInt() % 1000000;
    a[1] = random1->nextPositiveInt() % 1000000;
    eagle::Random *random2 = new eagle::Random(123);
    b[0] = random2->nextPositiveInt() % 1000000;
    a[2] = random1->nextPositiveInt() % 1000000;
    b[1] = random2->nextPositiveInt() % 1000000;
    b[2] = random2->nextPositiveInt() % 1000000;
    a[3] = random1->nextPositiveInt() % 1000000;
    b[3] = random2->nextPositiveInt() % 1000000;
    b[4] = random2->nextPositiveInt() % 1000000;
    a[4] = random1->nextPositiveInt() % 1000000;
    
    for(int i = 0; i < size; ++i) {
        CUNIT_ASSERT_EQUAL_INT(a[i], b[i]);
    }
    
    delete random1;
    delete random2;
}

CUNIT_TEST(MainSuite, eagleRandom, GetDefault)
{
    eagle::Random *random1 = eagle::Random::GetDefault();
    CUNIT_ASSERT_NOT_NULL(random1);
    
    eagle::Random *random2 = eagle::Random::GetDefault();
    CUNIT_ASSERT_NOT_NULL(random2);
    CUNIT_ASSERT_EQUAL_PTR(random1, random2);
}

CUNIT_TEST(MainSuite, eagleRandom_setSeed, Default)
{
    eagle::Random *random = eagle::Random::GetDefault();
    
    // try to modify the seed
    CUNIT_ASSERT_FALSE(random->setSeed(0));
}

CUNIT_TEST(MainSuite, eagleRandom_setSeed)
{
    unsigned int originalSeed = 123;
    eagle::Random *random = new eagle::Random(originalSeed);
    
    // generate some numbers
    int n1 = random->nextInt() % 1000000;
    int n2 = random->nextInt() % 1000000;
    
    // modify seed
    random->setSeed(originalSeed);
    int n3 = random->nextInt() % 1000000;
    int n4 = random->nextInt() % 1000000;
    
    // next numbers must be equal
    CUNIT_VERIFY_EQUAL_INT(n3, n1);
    CUNIT_VERIFY_EQUAL_INT(n4, n2);
    
    delete random;
}

CUNIT_TEST(MainSuite, eagleRandom_nextIntInRange, Positive)
{
    eagle::Random *random = eagle::Random::GetDefault();
    const int size = 100, min = 1000, max = 10000;
    
    for(int i = 0; i < size; ++i) {
        int next = random->nextIntInRange(min, max);
        if(next < min) {
            CUNIT_FAIL("%d is less than min %d", next, min);
        }
        if(next > max) {
            CUNIT_FAIL("%d is greater than max %d", next, max);
        }
    }
}

CUNIT_TEST(MainSuite, EagleWorker_runJobFunction, NotEnoughBuffers)
{
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    selectStage->prepareBuffers(1);
    
    EagleSelectJobMock *job = new EagleSelectJobMock(selectStage);
    job->buffers[0] = NULL;
    
    eagle::plan::Operation *epo = eagle::plan::Operation::NewWithLiteral(NULL, 20, -1, NULL, false, "dummy");
    job->runJobFunction_(epo);
    
    CUNIT_ASSERT_NOT_NULL(EagleLogger::LastEvent());
    CUNIT_VERIFY_EQUAL_STRING(EagleLogger::LastEvent()->getMessage().c_str(),
                              "destination 20 is greater than allowed 1 buffers!");
    
    delete job;
    delete selectStage;
    delete epo;
}

CUNIT_TEST(MainSuite, EagleWorker_runJobFunction, NotEnoughBuffers2)
{
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    selectStage->prepareBuffers(1);
    
    EagleSelectJobMock *job = new EagleSelectJobMock(selectStage);
    job->buffers[0] = NULL;
    
    eagle_plan_OperationAccess *epo = static_cast<eagle_plan_OperationAccess*>(eagle::plan::Operation::NewWithLiteral(NULL, 0, -1, NULL, false, "dummy"));
    epo->setArgumentCount(1);
    epo->setObject(calloc(sizeof(int), 1));
    ((int*) epo->getObject())[0] = 10;
    job->runJobFunction_(epo);
    
    CUNIT_ASSERT_NOT_NULL(EagleLogger::LastEvent());
    CUNIT_VERIFY_EQUAL_STRING(EagleLogger::LastEvent()->getMessage().c_str(),
                              "argument destination 10 for argument 1 is greater than allowed 1 buffers!");
    
    free(epo->getObject());
    delete job;
    delete selectStage;
    delete epo;
}

CUnitTests* MainSuite3_tests()
{
    CUnitTests *tests = CUnitTests_New(100);
    
    // method tests
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleWorker_runJobFunction, NotEnoughBuffers2));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleWorker_runJobFunction, NotEnoughBuffers));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagleRandom_nextIntInRange, Positive));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagleRandom_setSeed));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagleRandom_setSeed, Default));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagleRandom, GetDefault));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagleRandom_nextInt, SeededDouble));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagleRandom_nextInt, Unseeded));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagleRandom_nextInt, SeededSingle));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagleRandom, nextPositiveIntDistribution));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EaglePageOperations_SendPageToProviderVarchar_, Some));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EaglePageOperations_SendPageToProviderFloat_, All));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EaglePageOperations_SendPageToProviderVarchar_, All));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleData, NameToTypeFloat));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleData, NameToTypeVarchar));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleData, NameToTypeInteger));
    
    return tests;
}
