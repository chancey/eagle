#include "MainSuite.h"
#include <stdio.h>
#include <stdlib.h>
#include "eagle/EagleData.h"
#include "eagle/EaglePageOperations.h"
#include "eagle/Instance.h"
#include "eagle/EagleLinkedList.h"
#include "eagle/EagleLogger.h"
#include "eagle/StreamPageProvider.h"
#include "eagle/ArrayPageProvider.h"
#include "eagle/SinglePageProvider.h"
#include "eagle/Comparable.h"
#include "eagle/Random.h"
#include "eagle/EagleSynchronizedLinkedList.h"
#include "eagle/plan/Operation.h"
#include "eagle/Plan.h"

CUNIT_TEST(MainSuite, eagle_ArrayPageProvider, NewInt)
{
    int testDataSize = 3, recordsPerPage = 2;
    int *testData = (int*) calloc(testDataSize, sizeof(int));
    testData[0] = 123;
    testData[1] = 456;
    testData[2] = 789;
    
    eagle::ArrayPageProvider *provider = new eagle::ArrayPageProvider(EagleData::Type::Integer, testData, testDataSize,
                                                                      recordsPerPage, "", false);
    CUNIT_VERIFY_EQUAL_INT(provider->totalRecords, testDataSize);
    CUNIT_VERIFY_EQUAL_INT(provider->recordsPerPage, recordsPerPage);
    
    // read first page
    CUNIT_VERIFY_EQUAL_INT(provider->pagesRemaining(), 2);
    EaglePage *page1 = provider->nextPage();
    CUNIT_VERIFY_EQUAL_INT(page1->count, recordsPerPage);
    CUNIT_VERIFY_EQUAL_INT(((int*) page1->data)[0], testData[0]);
    CUNIT_VERIFY_EQUAL_INT(((int*) page1->data)[1], testData[1]);
    CUNIT_VERIFY_EQUAL_INT(page1->recordOffset, 0);
    delete page1;
    
    // reset
    provider->reset();
    
    // reread the first page and beyond
    CUNIT_VERIFY_EQUAL_INT(provider->pagesRemaining(), 2);
    page1 = provider->nextPage();
    CUNIT_VERIFY_EQUAL_INT(page1->count, recordsPerPage);
    CUNIT_VERIFY_EQUAL_INT(((int*) page1->data)[0], testData[0]);
    CUNIT_VERIFY_EQUAL_INT(((int*) page1->data)[1], testData[1]);
    CUNIT_VERIFY_EQUAL_INT(page1->recordOffset, 0);
    delete page1;
    
    CUNIT_VERIFY_EQUAL_INT(provider->pagesRemaining(), 1);
    EaglePage *page2 = provider->nextPage();
    CUNIT_VERIFY_EQUAL_INT(page2->count, 1);
    CUNIT_VERIFY_EQUAL_INT(((int*) page2->data)[0], testData[2]);
    CUNIT_VERIFY_EQUAL_INT(page2->recordOffset, recordsPerPage);
    delete page2;
    
    CUNIT_VERIFY_EQUAL_INT(provider->pagesRemaining(), 0);
    
    // clean up
    free(testData);
    delete provider;
}

CUNIT_TEST(MainSuite, eagle_plan_Operation, toString)
{
    eagle::plan::Operation *op = eagle::plan::Operation::NewWithPage(NULL, -1, -1, -1, NULL, false, "some description");
    CUNIT_ASSERT_EQUAL_STRING(op->toString().c_str(), "some description");
    delete op;
}

CUNIT_TEST(MainSuite, eagle_plan_stage_SelectStage, toString)
{
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 0, 1);
    CUNIT_ASSERT_EQUAL_STRING(selectStage->toString().c_str(), "Select stage:\n");
    
    // add some buffer providers
    eagle::ArrayPageProvider *provider = new eagle::ArrayPageProvider(EagleData::Type::Integer, NULL, 0, 10, "", false);
    eagle::PlanBufferProvider *bp = new eagle::PlanBufferProvider(123, (eagle::PageProvider*) provider, true);
    selectStage->addBufferProvider(bp, true);
    selectStage->prepareBuffers(1);
    
    // output providers
    selectStage->resultFields = 1;
    selectStage->result = new eagle::PageProvider*[selectStage->resultFields];
    selectStage->result[0] = eagle::SinglePageProvider::NewInt(123, 1, "name");
    
    // add some steps
    eagle::plan::Operation *op1, *op2, *op3;
    selectStage->addOperation(op1 = eagle::plan::Operation::NewWithPage(EaglePageOperations_GreaterThanInt, 2, 1, -1, NULL, false, "Step 1"), false);
    selectStage->addOperation(op2 = eagle::plan::Operation::NewWithPage(EaglePageOperations_LessThanInt,    3, 1, -1, NULL, false, "Step 2"), false);
    selectStage->addOperation(op3 = eagle::plan::Operation::NewWithPage(EaglePageOperations_AndPageInt,     0, 2,  3, NULL, false, "Step 3"), false);
    
    CUNIT_ASSERT_EQUAL_STRING(selectStage->toString().c_str(), "Select stage:\n  Input Providers:\n    destination = 123, name = , type = INTEGER\n  Output Providers:\n    destination = 0, name = name, type = INTEGER\n  Operations:\n    Step 1\n    Step 2\n    Step 3\n  Buffers:\n    0 type=UNKNOWN\n");
    delete selectStage;
    delete op1;
    delete op2;
    delete op3;
}

CUNIT_TEST(MainSuite, eagle_PlanBufferProvider_toString1)
{
    eagle::ArrayPageProvider *provider = new eagle::ArrayPageProvider(EagleData::Type::Integer, NULL, 0, 10, "something", false);
    eagle::PlanBufferProvider *bp = new eagle::PlanBufferProvider(123, (eagle::PageProvider*) provider, true);
    char *description = bp->toString();
    CUNIT_ASSERT_EQUAL_STRING(description, "destination = 123, name = something, type = INTEGER");
    delete description;
    delete bp;
}

CUNIT_TEST(MainSuite, EaglePageProvider_add)
{
    eagle::ArrayPageProvider *provider = new eagle::ArrayPageProvider(EagleData::Type::Integer, NULL, 0, 1, "",
                                                                      false);
    CUNIT_ASSERT_EQUAL_INT(provider->add(NULL), false);
    delete provider;
}

CUNIT_TEST(MainSuite, EagleLinkedList_New)
{
    EagleLinkedList<EagleDataTypeInteger*> *list = new EagleSynchronizedLinkedList<EagleDataTypeInteger*>();
    CUNIT_ASSERT_EQUAL_INT(list->length(), 0);
    
    // try to add a NULL item
    list->add(NULL);
    CUNIT_ASSERT_EQUAL_INT(list->length(), 0);
    
    // add some items
    for(int i = 0; i < 10; ++i) {
        EagleLinkedList<EagleDataTypeInteger*>::Item *item;
        item = new EagleLinkedList<EagleDataTypeInteger*>::Item(EagleData::CreateInt(i * 2), true, NULL);
        list->add(item);
    }
    CUNIT_ASSERT_EQUAL_INT(list->length(), 10);
    
    // validate list (must be FIFO)
    int count = 0;
    for(EagleLinkedList<EagleDataTypeInteger*>::Item *i = list->begin(); NULL != i; i = i->getNext()) {
        if(*(i->getObject()) != count * 2) {
            CUNIT_FAIL("Linked list in bad order (count = %d, i->obj = %d).", count, *(i->getObject()));
        }
        ++count;
    }
    CUNIT_ASSERT_EQUAL_INT(count, 10);
    
    delete list;
}

CUNIT_TEST(MainSuite, eagle_StreamPageProvider, eagle_StreamPageProvider)
{
    int testDataSize = 5, recordsPerPage = 2;
    int *testData = (EagleDataTypeInteger*) calloc(testDataSize, sizeof(EagleDataTypeInteger));
    testData[0] = 123;
    testData[1] = 456;
    testData[2] = 789;
    testData[3] = 234;
    testData[4] = 567;
    
    eagle::StreamPageProvider *provider = new eagle::StreamPageProvider(EagleData::Type::Integer, recordsPerPage, "");
    CUNIT_VERIFY_EQUAL_INT(provider->totalRecords, 0);
    CUNIT_VERIFY_EQUAL_INT(provider->recordsPerPage, recordsPerPage);
    
    // add data
    for(int i = 0; i < testDataSize; ++i) {
        EagleDataTypeInteger *ptr = EagleData::CreateInt(testData[i]);
        provider->add(ptr);
        delete ptr;
    }
    
    // get first page
    CUNIT_VERIFY_EQUAL_INT(provider->pagesRemaining(), 3);
    EaglePage *page1 = provider->nextPage();
    CUNIT_ASSERT_NOT_NULL(page1);
    CUNIT_VERIFY_EQUAL_INT(page1->count, recordsPerPage);
    CUNIT_VERIFY_EQUAL_INT(((int*) page1->data)[0], testData[0]);
    CUNIT_VERIFY_EQUAL_INT(((int*) page1->data)[1], testData[1]);
    CUNIT_VERIFY_EQUAL_INT(page1->recordOffset, 0);
    delete page1;
    
    // reset and read from the first page again
    provider->reset();
    
    CUNIT_VERIFY_EQUAL_INT(provider->pagesRemaining(), 3);
    page1 = provider->nextPage();
    CUNIT_ASSERT_NOT_NULL(page1);
    CUNIT_VERIFY_EQUAL_INT(page1->count, recordsPerPage);
    CUNIT_VERIFY_EQUAL_INT(((int*) page1->data)[0], testData[0]);
    CUNIT_VERIFY_EQUAL_INT(((int*) page1->data)[1], testData[1]);
    CUNIT_VERIFY_EQUAL_INT(page1->recordOffset, 0);
    delete page1;
    
    CUNIT_VERIFY_EQUAL_INT(provider->pagesRemaining(), 2);
    EaglePage *page2 = provider->nextPage();
    CUNIT_ASSERT_NOT_NULL(page1);
    CUNIT_VERIFY_EQUAL_INT(page2->count, recordsPerPage);
    CUNIT_VERIFY_EQUAL_INT(((int*) page2->data)[0], testData[2]);
    CUNIT_VERIFY_EQUAL_INT(((int*) page2->data)[1], testData[3]);
    delete page2;
    
    CUNIT_VERIFY_EQUAL_INT(provider->pagesRemaining(), 1);
    EaglePage *page3 = provider->nextPage();
    CUNIT_ASSERT_NOT_NULL(page3);
    CUNIT_VERIFY_EQUAL_INT(page3->count, 1);
    CUNIT_VERIFY_EQUAL_INT(((int*) page3->data)[0], testData[4]);
    delete page3;
    
    CUNIT_VERIFY_EQUAL_INT(provider->pagesRemaining(), 0);
    CUNIT_VERIFY_NULL(provider->nextPage());
    
    // clean up
    delete testData;
    delete provider;
}

CUNIT_TEST(MainSuite, eagle_plan_stage_SelectStage, getBufferProviderByName)
{
    eagle::plan::stage::SelectStage selectStage(NULL, 1, 1);
    CUNIT_ASSERT_NULL(selectStage.getBufferProviderByName("abc"));
}

CUNIT_TEST(MainSuite, EagleData, NameToType)
{
    CUNIT_VERIFY_EQUAL_INT(EagleData::NameToType("bla"), EagleData::Type::Unknown);
}

CUNIT_TEST(MainSuite, EagleLogger, SeverityToString)
{
    CUNIT_ASSERT_EQUAL_STRING(EagleLogger::SeverityToString(EagleLogger::Severity::Debug), "DEBUG");
    CUNIT_ASSERT_EQUAL_STRING(EagleLogger::SeverityToString(EagleLogger::Severity::Info), "INFO");
    CUNIT_ASSERT_EQUAL_STRING(EagleLogger::SeverityToString(EagleLogger::Severity::UserError), "USERERROR");
    CUNIT_ASSERT_EQUAL_STRING(EagleLogger::SeverityToString(EagleLogger::Severity::Error), "ERROR");
    CUNIT_ASSERT_EQUAL_STRING(EagleLogger::SeverityToString(EagleLogger::Severity::Fatal), "FATAL");
}

EagleLogger* GetLogger()
{
    EagleLogger *logger = new EagleLogger(fopen("log.txt", "w"));
    CUNIT_ASSERT_NOT_NULL(logger);
    if(NULL != logger) {
        CUNIT_ASSERT_NOT_NULL(logger->getOutHandle());
    }
    return logger;
}

CUNIT_TEST(MainSuite, EagleLogger_log)
{
    EagleLogger *logger = GetLogger();
    logger->log(EagleLogger::Severity::Debug, "some message");
    if(NULL != logger) {
        CUNIT_ASSERT_EQUAL_INT(logger->getTotalMessages(), 1);
    }
    delete logger;
}

CUNIT_TEST(MainSuite, EagleLogger_logEvent)
{
    EagleLogger *logger = GetLogger();
    logger->logEvent(NULL);
    delete logger;
}

CUNIT_TEST(MainSuite, EagleLogger_Get)
{
    EagleLogger *logger = EagleLogger::Get();
    CUNIT_ASSERT_NOT_NULL(logger);
}

CUNIT_TEST(MainSuite, EagleLogger_Log)
{
    EagleLogger::Get()->disableOutHandle();
    EagleLogger::Log(EagleLogger::Severity::Debug, "some message");
}

CUNIT_TEST(MainSuite, EagleLogger_LogEvent)
{
    EagleLogger::LogEvent(NULL);
    EagleLogger::Event *event = new EagleLogger::Event(EagleLogger::Severity::Debug, "bla bla");
    EagleLogger::LogEvent(event);
}

CUNIT_TEST(MainSuite, EaglePage_toString)
{
    EaglePage *page = new EaglePage(EagleData::Type::Integer, NULL, 123, 456, 789, false);
    CUNIT_ASSERT_NOT_NULL(page);
    char *desc = page->toString();
    CUNIT_ASSERT_EQUAL_STRING(desc, "EaglePage { type = INTEGER, size = 123, count = 456, offset = 789 }");
    free(desc);
    delete page;
}

CUNIT_TEST(MainSuite, EaglePage, Copy)
{
    EaglePage *page = new EaglePage(EagleData::Type::Unknown, NULL, 123, 456, 789, false);
    CUNIT_ASSERT_NOT_NULL(page);
    
    EaglePage *page2 = page->copy();
    CUNIT_ASSERT_NOT_NULL(page2);
    
    delete page2;
    delete page;
}

CUNIT_TEST(MainSuite, EaglePageOperations_SendPageToProvider)
{
    eagle::StreamPageProvider *provider = new eagle::StreamPageProvider(EagleData::Type::Integer, 1, "dummy");
    provider->type = EagleData::Type::Unknown;
    EaglePage *source2 = EaglePage::Alloc(EagleData::Type::Integer, 1);
    EaglePageOperations_SendPageToProvider(NULL, NULL, source2, provider);
    
    delete provider;
    delete source2;
}

CUNIT_TEST(MainSuite, EaglePageProvider_addStream_)
{
    eagle::PageProvider *provider = (eagle::PageProvider*) new eagle::StreamPageProvider(EagleData::Type::Integer, 10, "dummy");
    
    // before we change to Unknown we need to add one record so that the first page is initialised
    int *data = EagleData::CreateInt(123);
    provider->add(data);
    free(data);
    CUNIT_ASSERT_EQUAL_INT(provider->pagesRemaining(), 1);
    
    // now skrew up the type
    provider->type = EagleData::Type::Unknown;
    provider->add(NULL);
    
    delete provider;
}

CUNIT_TEST(MainSuite, EaglePlan_getExecutionSeconds)
{
    int cores = 1;
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 1, cores);
    
    double time = selectStage->getExecutionSeconds();
    CUNIT_VERIFY_EQUAL_DOUBLE(0.0, time);
    
    selectStage->executionTime[0] = 100000;
    time = selectStage->getExecutionSeconds();
    CUNIT_VERIFY_EQUAL_DOUBLE(0.0001, time);
    
    delete selectStage;
}

CUNIT_TEST(MainSuite, eagle_Instance_nextJob, 1)
{
    int cores = 1;
    eagle::StreamPageProvider *provider = new eagle::StreamPageProvider(EagleData::Type::Integer, 1, "dummy");
    int *ptr = EagleData::CreateInt(123);
    provider->add(ptr);
    free(ptr);
    
    eagle::Plan *plan = new eagle::Plan();
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(plan, 1, cores);
    eagle::PlanBufferProvider *bp = new eagle::PlanBufferProvider(0, (eagle::PageProvider*) provider, false);
    selectStage->addBufferProvider(bp, false);
    plan->addStage(selectStage, true);
    
    eagle::Instance *instance = new eagle::Instance(cores);
    instance->addPlan(plan);
    eagle::plan::job::AbstractJob *job = instance->nextJob();
    CUNIT_VERIFY_NULL(job);
    
    delete instance;
    delete provider;
    delete bp;
    delete job;
    delete plan;
}

CUNIT_TEST(MainSuite, EagleWorker, runJobPage)
{
    // redirect the errors to nowhere
    EagleLogger::Get()->disableOutHandle();
    
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    
    eagle_plan_OperationAccess *op = static_cast<eagle_plan_OperationAccess*>(eagle::plan::Operation::NewWithPage(EaglePageOperations_SendPageToProvider, 1, 1, 1, NULL, false, "1"));
    selectStage->addOperation(op, false);
    selectStage->prepareBuffers(1);
    
    eagle::plan::job::SelectJob *job = new eagle::plan::job::SelectJob(selectStage);
    
    job->runJob();
    CUNIT_ASSERT_NOT_NULL(EagleLogger::LastEvent());
    CUNIT_VERIFY_EQUAL_STRING(EagleLogger::LastEvent()->getMessage().c_str(), "destination 1 is greater than allowed 1 buffers!");
    
    op->setDestination(0);
    job->runJob();
    CUNIT_ASSERT_NOT_NULL(EagleLogger::LastEvent());
    CUNIT_VERIFY_EQUAL_STRING(EagleLogger::LastEvent()->getMessage().c_str(), "source1 1 is greater than allowed 1 buffers!");
    
    op->setSource1(0);
    job->runJob();
    CUNIT_ASSERT_NOT_NULL(EagleLogger::LastEvent());
    CUNIT_VERIFY_EQUAL_STRING(EagleLogger::LastEvent()->getMessage().c_str(), "source2 1 is greater than allowed 1 buffers!");
    
    delete op;
    delete selectStage;
    delete job;
}

CUNIT_TEST(MainSuite, EagleLogger_LastEvent)
{
    // redirect the errors to nowhere
    EagleLogger::Get()->disableOutHandle();
    
    EagleLogger::Log(EagleLogger::Severity::Debug, "some message 123");
    
    EagleLogger::Event *event = EagleLogger::LastEvent();
    CUNIT_ASSERT_NOT_NULL(event);
    if(NULL != event) {
        CUNIT_VERIFY_EQUAL_STRING(event->getMessage().c_str(), "some message 123");
    }
}

CUNIT_TEST(MainSuite, EagleLogger_lastEvent)
{
    // redirect the errors to nowhere
    EagleLogger::Get()->disableOutHandle();
    
    EagleLogger *logger = GetLogger();
    logger->log(EagleLogger::Severity::Debug, "some message 456");
    
    EagleLogger::Event *event = logger->lastEvent();
    CUNIT_ASSERT_NOT_NULL(event);
    if(NULL != event) {
        CUNIT_VERIFY_EQUAL_STRING(event->getMessage().c_str(), "some message 456");
    }
    
    delete logger;
}

CUNIT_TEST(MainSuite, EagleLinkedList_pop)
{
    EagleLinkedList<EagleDataTypeInteger*> *list = new EagleSynchronizedLinkedList<EagleDataTypeInteger*>();
    
    // empty list
    {
        CUNIT_VERIFY_EQUAL_INT(list->length(), 0);
        CUNIT_VERIFY_EQUAL_INT(list->isEmpty(), true);
        CUNIT_VERIFY_EQUAL_PTR(list->begin(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->end(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->first(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->last(), NULL);
        
        EagleLinkedList<EagleDataTypeInteger*>::Item *item = list->pop();
        CUNIT_ASSERT_NULL(item);
        CUNIT_VERIFY_EQUAL_INT(list->length(), 0);
        CUNIT_VERIFY_EQUAL_INT(list->isEmpty(), true);
        CUNIT_VERIFY_EQUAL_PTR(list->begin(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->end(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->first(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->last(), NULL);
    }
    
    // one item
    {
        EagleLinkedList<EagleDataTypeInteger*>::Item *origItem;
        origItem = new EagleLinkedList<EagleDataTypeInteger*>::Item(EagleData::CreateInt(123), true, NULL);
        list->add(origItem);
        CUNIT_VERIFY_EQUAL_INT(list->length(), 1);
        CUNIT_VERIFY_EQUAL_INT(list->isEmpty(), false);
        CUNIT_VERIFY_EQUAL_PTR(list->begin(), origItem);
        CUNIT_VERIFY_EQUAL_PTR(list->end(), origItem);
        CUNIT_VERIFY_EQUAL_PTR(list->first(), origItem->getObject());
        CUNIT_VERIFY_EQUAL_PTR(list->last(), origItem->getObject());
        
        EagleLinkedList<EagleDataTypeInteger*>::Item *item = list->pop();
        
        CUNIT_VERIFY_EQUAL_INT(list->length(), 0);
        CUNIT_VERIFY_EQUAL_INT(list->isEmpty(), true);
        CUNIT_VERIFY_EQUAL_PTR(list->begin(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->end(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->first(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->last(), NULL);
        CUNIT_VERIFY_EQUAL_INT(*(item->getObject()), 123);
        
        delete origItem;
    }
    
    // two items
    {
        EagleLinkedList<EagleDataTypeInteger*>::Item *item;
        EagleLinkedList<EagleDataTypeInteger*>::Item *origItem1;
        origItem1 = new EagleLinkedList<EagleDataTypeInteger*>::Item(EagleData::CreateInt(234), true, NULL);
        EagleLinkedList<EagleDataTypeInteger*>::Item *origItem2;
        origItem2 = new EagleLinkedList<EagleDataTypeInteger*>::Item(EagleData::CreateInt(567), true, NULL);
        
        list->add(origItem1);
        list->add(origItem2);
        CUNIT_VERIFY_EQUAL_INT(list->length(), 2);
        CUNIT_VERIFY_EQUAL_INT(list->isEmpty(), false);
        CUNIT_VERIFY_EQUAL_PTR(list->begin(), origItem1);
        CUNIT_VERIFY_EQUAL_PTR(list->end(), origItem2);
        CUNIT_VERIFY_EQUAL_PTR(list->first(), origItem1->getObject());
        CUNIT_VERIFY_EQUAL_PTR(list->last(), origItem2->getObject());
        
        item = list->pop();
        CUNIT_VERIFY_EQUAL_INT(list->length(), 1);
        CUNIT_VERIFY_EQUAL_INT(list->isEmpty(), false);
        CUNIT_VERIFY_EQUAL_INT(*(item->getObject()), 567);
        CUNIT_VERIFY_EQUAL_PTR(list->begin(), origItem1);
        CUNIT_VERIFY_EQUAL_PTR(list->end(), origItem1);
        CUNIT_VERIFY_EQUAL_PTR(list->first(), origItem1->getObject());
        CUNIT_VERIFY_EQUAL_PTR(list->last(), origItem1->getObject());
        
        item = list->pop();
        CUNIT_VERIFY_EQUAL_INT(list->length(), 0);
        CUNIT_VERIFY_EQUAL_INT(list->isEmpty(), true);
        CUNIT_VERIFY_EQUAL_PTR(list->begin(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->end(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->first(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->last(), NULL);
        CUNIT_VERIFY_EQUAL_INT(*(item->getObject()), 234);
        
        delete origItem1;
        delete origItem2;
    }
    
    // several items
    {
        EagleLinkedList<EagleDataTypeInteger*>::Item *item;
        EagleLinkedList<EagleDataTypeInteger*>::Item *origItem1;
        origItem1 = new EagleLinkedList<EagleDataTypeInteger*>::Item(EagleData::CreateInt(345), true, NULL);
        EagleLinkedList<EagleDataTypeInteger*>::Item *origItem2;
        origItem2 = new EagleLinkedList<EagleDataTypeInteger*>::Item(EagleData::CreateInt(678), true, NULL);
        EagleLinkedList<EagleDataTypeInteger*>::Item *origItem3;
        origItem3 = new EagleLinkedList<EagleDataTypeInteger*>::Item(EagleData::CreateInt(901), true, NULL);
        
        list->add(origItem1);
        list->add(origItem2);
        list->add(origItem3);
        CUNIT_VERIFY_EQUAL_INT(list->length(), 3);
        CUNIT_VERIFY_EQUAL_INT(list->isEmpty(), false);
        CUNIT_VERIFY_EQUAL_PTR(list->begin(), origItem1);
        CUNIT_VERIFY_EQUAL_PTR(list->end(), origItem3);
        CUNIT_VERIFY_EQUAL_PTR(list->first(), origItem1->getObject());
        CUNIT_VERIFY_EQUAL_PTR(list->last(), origItem3->getObject());
        CUNIT_VERIFY_NULL(list->end()->getNext());
        
        item = list->pop();
        CUNIT_VERIFY_EQUAL_INT(list->length(), 2);
        CUNIT_VERIFY_EQUAL_INT(list->isEmpty(), false);
        CUNIT_VERIFY_EQUAL_PTR(list->begin(), origItem1);
        CUNIT_VERIFY_EQUAL_PTR(list->end(), origItem2);
        CUNIT_VERIFY_EQUAL_PTR(list->first(), origItem1->getObject());
        CUNIT_VERIFY_EQUAL_PTR(list->last(), origItem2->getObject());
        CUNIT_VERIFY_EQUAL_INT(*(item->getObject()), 901);
        CUNIT_VERIFY_NULL(list->end()->getNext());
        
        item = list->pop();
        CUNIT_VERIFY_EQUAL_INT(list->length(), 1);
        CUNIT_VERIFY_EQUAL_INT(list->isEmpty(), false);
        CUNIT_VERIFY_EQUAL_PTR(list->begin(), origItem1);
        CUNIT_VERIFY_EQUAL_PTR(list->end(), origItem1);
        CUNIT_VERIFY_EQUAL_PTR(list->first(), origItem1->getObject());
        CUNIT_VERIFY_EQUAL_PTR(list->last(), origItem1->getObject());
        CUNIT_VERIFY_EQUAL_INT(*(item->getObject()), 678);
        CUNIT_VERIFY_NULL(list->end()->getNext());
        
        item = list->pop();
        CUNIT_VERIFY_EQUAL_INT(list->length(), 0);
        CUNIT_VERIFY_EQUAL_INT(list->isEmpty(), true);
        CUNIT_VERIFY_EQUAL_PTR(list->begin(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->end(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->first(), NULL);
        CUNIT_VERIFY_EQUAL_PTR(list->last(), NULL);
        CUNIT_VERIFY_EQUAL_INT(*(item->getObject()), 345);
        
        delete origItem1;
        delete origItem2;
        delete origItem3;
    }
    
    delete list;
}

CUNIT_TEST(MainSuite, EagleLinkedList_toArray)
{
    int size;
    
    EagleLinkedList<EagleDataTypeInteger*> *list = new EagleSynchronizedLinkedList<EagleDataTypeInteger*>();
    list->add(new EagleLinkedList<EagleDataTypeInteger*>::Item(EagleData::CreateInt(345), true, NULL));
    list->add(new EagleLinkedList<EagleDataTypeInteger*>::Item(EagleData::CreateInt(678), true, NULL));
    list->add(new EagleLinkedList<EagleDataTypeInteger*>::Item(EagleData::CreateInt(901), true, NULL));
    
    int **data = (int**) list->toArray(&size);
    CUNIT_ASSERT_EQUAL_INT(size, 3);
    CUNIT_VERIFY_EQUAL_INT(*data[0], 345);
    CUNIT_VERIFY_EQUAL_INT(*data[1], 678);
    CUNIT_VERIFY_EQUAL_INT(*data[2], 901);
    free(data);
    
    delete list;
}

CUNIT_TEST(MainSuite, EagleLinkedList_get)
{
    EagleLinkedList<EagleDataTypeInteger*> *list = new EagleSynchronizedLinkedList<EagleDataTypeInteger*>();
    list->add(new EagleLinkedList<EagleDataTypeInteger*>::Item(EagleData::CreateInt(345), true, NULL));
    
    CUNIT_VERIFY_NULL(list->get(-1));
    CUNIT_VERIFY_NULL(list->get(1));
    
    delete list;
}

CUNIT_TEST(MainSuite, eagle_PageProvider, pagesRemaining)
{
    eagle::PageProvider *provider = new eagle::PageProvider();
    CUNIT_VERIFY_EQUAL_INT(provider->pagesRemaining(), 0);
    delete provider;
}

CUNIT_TEST(MainSuite, eagle_PageProvider, nextPage)
{
    eagle::PageProvider *provider = new eagle::PageProvider();
    CUNIT_VERIFY_NULL(provider->nextPage());
    delete provider;
}

CUNIT_TEST(MainSuite, eagle_PageProvider, getPage)
{
    eagle::PageProvider *provider = new eagle::PageProvider();
    CUNIT_VERIFY_NULL(provider->getPage(0));
    delete provider;
}

CUnitTests* MainSuite1_tests()
{
    CUnitTests *tests = CUnitTests_New(100);
    
    // method tests
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_PageProvider, getPage));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_PageProvider, nextPage));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_PageProvider, pagesRemaining));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLinkedList_get));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLinkedList_toArray));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLinkedList_pop));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLogger_LastEvent));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLogger_lastEvent));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleWorker, runJobPage));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_Instance_nextJob, 1));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EaglePlan_getExecutionSeconds));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleData, NameToType));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLinkedList_New));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_ArrayPageProvider, NewInt));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_StreamPageProvider, eagle_StreamPageProvider));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EaglePageProvider_add));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_plan_stage_SelectStage, toString));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_plan_stage_SelectStage, getBufferProviderByName));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_PlanBufferProvider_toString1));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_plan_Operation, toString));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLogger, SeverityToString));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLogger_log));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLogger_logEvent));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLogger_Get));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLogger_Log));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLogger_LogEvent));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EaglePage_toString));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EaglePage, Copy));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EaglePageOperations_SendPageToProvider));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EaglePageProvider_addStream_));
    
    return tests;
}

/**
 * The suite init function.
 */
int MainSuite_init()
{
    return 0;
}

/**
 * The suite cleanup function.
 */
int MainSuite_clean()
{
    return 0;
}
