#include "DBSuite.h"
#include <stdio.h>
#include <stdlib.h>
#include "eagledb/EagleDbSqlFunctionExpression.h"
#include "eagle/EagleUnsynchronizedLinkedList.h"
#include "eagle/plan/stage/SelectStage.h"
#include "eagle/Plan.h"
#include "eagledb/EagleDbSqlBinaryExpression.h"
#include "eagledb/EagleDbSqlBinaryExpressionOperator.h"
#include "eagle/ArrayPageProvider.h"
#include "eagle/Instance.h"
#include "eagledb/EagleDbConsole.h"

CUNIT_TEST(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Function_, atan2WrongArgs)
{
    EagleDbSqlExpression *expr0 = (EagleDbSqlExpression*) EagleDbSqlValue_NewWithFloat(123.456);
    EagleDbSqlExpression *expr1 = (EagleDbSqlExpression*) EagleDbSqlValue_NewWithInteger(789);
    EagleDbSqlFunctionExpression *expr = EagleDbSqlFunctionExpression_NewWithTwoArguments("atan2", expr0, false, expr1, false);
    
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    selectStage->prepareBuffers(10);
    
    int destinationBuffer = 0;
    EagleDbSqlExpression_CompilePlanIntoBuffer_((EagleDbSqlExpression*) expr, &destinationBuffer, selectStage, false);
    CUNIT_VERIFY_TRUE(destinationBuffer >= 0);
    CUNIT_VERIFY_EQUAL_STRING(selectStage->errorMessage.c_str(), "Function atan2(FLOAT, INTEGER) does not exist.");
    
    EagleDbSqlExpression_Delete((EagleDbSqlExpression*) expr);
    EagleDbSqlExpression_Delete((EagleDbSqlExpression*) expr0);
    EagleDbSqlExpression_Delete((EagleDbSqlExpression*) expr1);
    delete selectStage;
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Function_, atan2)
{
    EagleDbSqlExpression *expr0 = (EagleDbSqlExpression*) EagleDbSqlValue_NewWithFloat(123.456);
    EagleDbSqlExpression *expr1 = (EagleDbSqlExpression*) EagleDbSqlValue_NewWithFloat(789.123);
    EagleDbSqlFunctionExpression *expr = EagleDbSqlFunctionExpression_NewWithTwoArguments("atan2", expr0, false, expr1, false);
    
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    selectStage->prepareBuffers(10);
    
    int destinationBuffer = 0;
    EagleDbSqlExpression_CompilePlanIntoBuffer_((EagleDbSqlExpression*) expr, &destinationBuffer, selectStage, false);
    CUNIT_VERIFY_TRUE(destinationBuffer >= 0);
    CUNIT_VERIFY_TRUE(selectStage->errorMessage.empty());
    
    EagleDbSqlExpression_Delete((EagleDbSqlExpression*) expr);
    EagleDbSqlExpression_Delete((EagleDbSqlExpression*) expr0);
    EagleDbSqlExpression_Delete((EagleDbSqlExpression*) expr1);
    delete selectStage;
}

CUNIT_TEST(DBSuite, EagleDbSqlFunctionExpression_toString, position)
{
    EagleDbSqlExpression *expr1 = (EagleDbSqlExpression*) EagleDbSqlValue_NewWithString("b", false);
    EagleDbSqlExpression *expr2 = (EagleDbSqlExpression*) EagleDbSqlValue_NewWithString("abc", false);
    EagleDbSqlFunctionExpression *expr = EagleDbSqlFunctionExpression_NewWithTwoArguments("position", expr1, true, expr2, true);
    
    char *desc = EagleDbSqlExpression_toString((EagleDbSqlExpression*) expr);
    CUNIT_VERIFY_EQUAL_STRING(desc, "position('b' IN 'abc')");
    free(desc);
    EagleDbSqlExpression_DeleteRecursive((EagleDbSqlExpression*) expr);
}

CUNIT_TEST(DBSuite, EagleDbSqlFunctionExpression_toString, 2)
{
    EagleLinkedList<EagleDbSqlExpression*> *exprs = new EagleUnsynchronizedLinkedList<EagleDbSqlExpression*>();
    exprs->addObject((EagleDbSqlExpression*) EagleDbSqlValue_NewWithFloat(1.23), true,
                     (void(*)(EagleDbSqlExpression*)) EagleDbSqlValue_Delete);
    EagleDbSqlFunctionExpression *expr = EagleDbSqlFunctionExpression_New("myfunc", exprs);
    
    char *desc = EagleDbSqlExpression_toString((EagleDbSqlExpression*) expr);
    CUNIT_VERIFY_EQUAL_STRING(desc, "myfunc(1.23)");
    
    free(desc);
    EagleDbSqlExpression_DeleteRecursive((EagleDbSqlExpression*) expr);
}

CUNIT_TEST(DBSuite, _SELECT_ORDER_BY)
{
    int pageSize = 10;
    EagleLogger::Event *error = NULL;
    bool success;
    eagle::Plan *plan = NULL;
    
    EagleDbInstance *db = EagleDbInstance_New(pageSize, 1);
    
    success = EagleDbInstance_execute(db, "CREATE TABLE mytable (col1 INT, col2 FLOAT);", &error, NULL);
    CUNIT_ASSERT_TRUE(success);
    
    // insert some data
    for(int i = 0; i < 100; ++i) {
        char sql[1024];
        sprintf(sql, "INSERT INTO mytable (col1, col2) VALUES (%d, %g);", rand() % 10000, 0.1 * (double) (rand() % 10000));
        success = EagleDbInstance_execute(db, sql, &error, NULL);
        CUNIT_ASSERT_TRUE(success);
    }
    
    // run
    success = EagleDbInstance_execute(db, "SELECT col2, col1 FROM mytable ORDER BY col1;", &error, &plan);
    CUNIT_ASSERT_TRUE(success);
    
    eagle::plan::stage::SelectStage *selectStage = (eagle::plan::stage::SelectStage*) plan->stages->get(0);
    
    // validate data
    EaglePage *page = NULL;
    int pageCount = 0;
    EagleDataTypeInteger lastValue = 0;
    while(true) {
        // col1
        page = selectStage->result[1]->nextPage();
        if(NULL == page) {
            break;
        }
        
        for(int i = 0; i < page->count; ++i) {
            EagleDataTypeInteger d = ((EagleDataTypeInteger*) page->data)[i];
            if(d < lastValue) {
                CUNIT_FAIL("%s", "ORDER BY incorrect");
            }
        }
        ++pageCount;
    }
    
    delete plan;
    EagleInstanceTest_Cleanup(db);
}

CUNIT_TEST(DBSuite, EagleDbSqlBinaryExpression_GetRightOperation, withMatch)
{
    EagleDbSqlBinaryOperator match;
    CUNIT_VERIFY_TRUE(EagleDbSqlBinaryExpression_GetRightOperation(EagleData::Type::Integer,
                                                                   EagleDbSqlBinaryExpressionOperatorLessThan, &match));
    
    CUNIT_VERIFY_EQUAL_INT(match.left, EagleData::Type::Integer);
    CUNIT_VERIFY_EQUAL_INT(match.op, EagleDbSqlBinaryExpressionOperatorLessThan);
    CUNIT_VERIFY_EQUAL_INT(match.right, EagleData::Type::Integer);
    CUNIT_VERIFY_EQUAL_PTR(match.func, EaglePageOperations_GreaterThanInt);
    CUNIT_VERIFY_EQUAL_INT(match.returnType, EagleData::Type::Integer);
}

CUNIT_TEST(DBSuite, EagleDbSqlBinaryExpression_GetPageOperation, withMatch)
{
    EagleDbSqlBinaryOperator match;
    CUNIT_VERIFY_TRUE(EagleDbSqlBinaryExpression_GetPageOperation(EagleData::Type::Integer,
                      EagleDbSqlBinaryExpressionOperatorLessThan, EagleData::Type::Integer, &match));
    
    CUNIT_VERIFY_EQUAL_INT(match.left, EagleData::Type::Integer);
    CUNIT_VERIFY_EQUAL_INT(match.op, EagleDbSqlBinaryExpressionOperatorLessThan);
    CUNIT_VERIFY_EQUAL_INT(match.right, EagleData::Type::Integer);
    CUNIT_VERIFY_EQUAL_PTR(match.func, EaglePageOperations_LessThanPageInt);
    CUNIT_VERIFY_EQUAL_INT(match.returnType, EagleData::Type::Integer);
}

CUNIT_TEST(DBSuite, EagleDbSqlBinaryExpressionOperator_toString, Multiply)
{
    char *str = EagleDbSqlBinaryExpressionOperator_toString(EagleDbSqlBinaryExpressionOperatorMultiply);
    CUNIT_VERIFY_EQUAL_STRING(str, "*");
    free(str);
}

CUNIT_TEST(DBSuite, EagleDbSqlBinaryExpressionOperator_toString, NotEquals)
{
    char *str = EagleDbSqlBinaryExpressionOperator_toString(EagleDbSqlBinaryExpressionOperatorNotEquals);
    CUNIT_VERIFY_EQUAL_STRING(str, "!=");
    free(str);
}

CUNIT_TEST(DBSuite, EagleDbSqlBinaryExpressionOperator_toString, GreaterThan)
{
    char *str = EagleDbSqlBinaryExpressionOperator_toString(EagleDbSqlBinaryExpressionOperatorGreaterThan);
    CUNIT_VERIFY_EQUAL_STRING(str, ">");
    free(str);
}

CUNIT_TEST(DBSuite, EagleDbSqlBinaryExpressionOperator_toString, LessThan)
{
    char *str = EagleDbSqlBinaryExpressionOperator_toString(EagleDbSqlBinaryExpressionOperatorLessThan);
    CUNIT_VERIFY_EQUAL_STRING(str, "<");
    free(str);
}

CUNIT_TEST(DBSuite, EagleDbSqlBinaryExpressionOperator_toString, GreaterThanEqual)
{
    char *str = EagleDbSqlBinaryExpressionOperator_toString(EagleDbSqlBinaryExpressionOperatorGreaterThanEqual);
    CUNIT_VERIFY_EQUAL_STRING(str, ">=");
    free(str);
}

CUNIT_TEST(DBSuite, EagleDbSqlBinaryExpressionOperator_toString, LessThanEqual)
{
    char *str = EagleDbSqlBinaryExpressionOperator_toString(EagleDbSqlBinaryExpressionOperatorLessThanEqual);
    CUNIT_VERIFY_EQUAL_STRING(str, "<=");
    free(str);
}

CUNIT_TEST(DBSuite, EagleDbSqlBinaryExpressionOperator_toString, Minus)
{
    char *str = EagleDbSqlBinaryExpressionOperator_toString(EagleDbSqlBinaryExpressionOperatorMinus);
    CUNIT_VERIFY_EQUAL_STRING(str, "-");
    free(str);
}

CUNIT_TEST(DBSuite, EagleDbSqlBinaryExpressionOperator_toString, Divide)
{
    char *str = EagleDbSqlBinaryExpressionOperator_toString(EagleDbSqlBinaryExpressionOperatorDivide);
    CUNIT_VERIFY_EQUAL_STRING(str, "/");
    free(str);
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_CompilePlan, Right)
{
    int pageSize = 10;
    int exprs = 3;
    int totalResults = 1;
    int cores = 1;
    EagleDbSqlExpression **expr = (EagleDbSqlExpression**) calloc(exprs, sizeof(EagleDbSqlExpression*));
    
    // SELECT col1, col2 + 8 FROM mytable WHERE 2 + col1 = 3
    expr[0] = (EagleDbSqlExpression*) EagleDbSqlValue_NewWithIdentifier("col1");
    expr[1] = (EagleDbSqlExpression*) EagleDbSqlBinaryExpression_New(
         (EagleDbSqlExpression*) EagleDbSqlValue_NewWithIdentifier("col2"),
         EagleDbSqlBinaryExpressionOperatorPlus,
         (EagleDbSqlExpression*) EagleDbSqlValue_NewWithInteger(8)
         );
    expr[2] = (EagleDbSqlExpression*) EagleDbSqlBinaryExpression_New(
             (EagleDbSqlExpression*) EagleDbSqlBinaryExpression_New(
                    (EagleDbSqlExpression*) EagleDbSqlValue_NewWithInteger(2),
                    EagleDbSqlBinaryExpressionOperatorPlus,
            (EagleDbSqlExpression*) EagleDbSqlValue_NewWithIdentifier("col1")
            ),
         EagleDbSqlBinaryExpressionOperatorEquals,
         (EagleDbSqlExpression*) EagleDbSqlValue_NewWithInteger(3)
         );
    
    // create the plan skeleton
    eagle::Plan *plan = new eagle::Plan();
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(plan, pageSize, cores);
    plan->addStage(selectStage, true);
    
    // create a virtual table that consists of 2 columns; col1 and col2
    int *col1Data = (int*) calloc((size_t) pageSize, sizeof(int));
    int *col2Data = (int*) calloc((size_t) pageSize, sizeof(int));
    for(int i = 0; i < pageSize; ++i) {
        col1Data[i] = i;
        col2Data[i] = i * 2;
    }
    eagle::ArrayPageProvider *col1 = new eagle::ArrayPageProvider(EagleData::Type::Integer, col1Data, pageSize,
                                                                  pageSize, "col1", false);
    eagle::ArrayPageProvider *col2 = new eagle::ArrayPageProvider(EagleData::Type::Integer, col2Data, pageSize,
                                                                  pageSize, "col2", false);
    selectStage->addBufferProvider(new eagle::PlanBufferProvider(1, (eagle::PageProvider*) col1, true), true);
    selectStage->addBufferProvider(new eagle::PlanBufferProvider(2, (eagle::PageProvider*) col2, true), true);
    
    // compile plan
    EagleDbSqlExpression_CompilePlan(expr, exprs, 2, selectStage);
    
    CUNIT_ASSERT_EQUAL_INT(selectStage->providers->length(), 2);
    CUNIT_ASSERT_EQUAL_INT(selectStage->operations->length(), 5);
    
    // execute
    eagle::Instance *eagle = new eagle::Instance(cores);
    eagle->addPlan(plan);
    eagle->run();
    
    // validate result
    CREATE_EXPRESSION_ARRAY(answers_0, pageSize, col1Data[i]);          // col1
    CREATE_EXPRESSION_ARRAY(answers_1, pageSize, col2Data[i] + 8);      // col2 + 8
    
    int **answers = (int**) calloc(exprs, sizeof(int*));
    answers[0] = (int*) calloc(totalResults, sizeof(int));
    answers[1] = (int*) calloc(totalResults, sizeof(int));
    
    for(int i = 0, j = 0; i < pageSize; ++i) {
        if(2 + col1Data[i] == 3) {
            answers[0][j] = answers_0[i];
            answers[1][j] = answers_1[i];
            ++j;
        }
    }
    
    // subtract 1 because the WHERE clause does not get emitted
    for(int i = 0; i < selectStage->resultFields - 1; ++i) {
        EaglePage *page = selectStage->result[i]->nextPage();
        CUNIT_ASSERT_NOT_NULL(page);
        CUNIT_VERIFY_EQUAL_INT(page->count, totalResults);
        
        int valid = 1;
        for(int j = 0; j < page->count; ++j) {
            if(((int*) page->data)[j] != answers[i][j]) {
                valid = 0;
                break;
            }
        }
        CUNIT_VERIFY_EQUAL_INT(valid, 1);
        
        delete page;
    }
    
    free(col1Data);
    free(col2Data);
    for(int i = 0; i < exprs; ++i) {
        EagleDbSqlExpression_DeleteRecursive(expr[i]);
    }
    for(int i = 0; i < selectStage->resultFields; ++i) {
        delete answers[i];
    }
    delete answers_0;
    delete answers_1;
    delete answers;
    delete expr;
    delete eagle;
    delete plan;
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_CompilePlan, Both)
{
    int pageSize = 10;
    int exprs = 3;
    int totalResults = 1;
    int cores = 1;
    EagleDbSqlExpression **expr = (EagleDbSqlExpression**) calloc(exprs, sizeof(EagleDbSqlExpression*));
    
    // SELECT col1, col2 + 8 FROM mytable WHERE col1 + col1 = 4
    expr[0] = (EagleDbSqlExpression*) EagleDbSqlValue_NewWithIdentifier("col1");
    expr[1] = (EagleDbSqlExpression*) EagleDbSqlBinaryExpression_New(
                 (EagleDbSqlExpression*) EagleDbSqlValue_NewWithIdentifier("col2"),
                 EagleDbSqlBinaryExpressionOperatorPlus,
                 (EagleDbSqlExpression*) EagleDbSqlValue_NewWithInteger(8)
                 );
    expr[2] = (EagleDbSqlExpression*) EagleDbSqlBinaryExpression_New(
             (EagleDbSqlExpression*) EagleDbSqlBinaryExpression_New(
                (EagleDbSqlExpression*) EagleDbSqlValue_NewWithIdentifier("col1"),
                EagleDbSqlBinaryExpressionOperatorPlus,
                (EagleDbSqlExpression*) EagleDbSqlValue_NewWithIdentifier("col1")
                ),
             EagleDbSqlBinaryExpressionOperatorEquals,
             (EagleDbSqlExpression*) EagleDbSqlValue_NewWithInteger(4)
             );
    
    // create the plan skeleton
    eagle::Plan *plan = new eagle::Plan();
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(plan, pageSize, cores);
    plan->addStage(selectStage, true);
    
    // create a virtual table that consists of 2 columns; col1 and col2
    int *col1Data = (int*) calloc((size_t) pageSize, sizeof(int));
    int *col2Data = (int*) calloc((size_t) pageSize, sizeof(int));
    for(int i = 0; i < pageSize; ++i) {
        col1Data[i] = i;
        col2Data[i] = i * 2;
    }
    eagle::ArrayPageProvider *col1 = new eagle::ArrayPageProvider(EagleData::Type::Integer, col1Data, pageSize,
                                                                  pageSize, "col1", false);
    eagle::ArrayPageProvider *col2 = new eagle::ArrayPageProvider(EagleData::Type::Integer, col2Data, pageSize,
                                                                  pageSize, "col2", false);
    selectStage->addBufferProvider(new eagle::PlanBufferProvider(1, (eagle::PageProvider*) col1, true), true);
    selectStage->addBufferProvider(new eagle::PlanBufferProvider(2, (eagle::PageProvider*) col2, true), true);
    
    // compile plan
    EagleDbSqlExpression_CompilePlan(expr, exprs, 2, selectStage);
    
    CUNIT_ASSERT_EQUAL_INT(selectStage->providers->length(), 2);
    CUNIT_ASSERT_EQUAL_INT(selectStage->operations->length(), 5);
    
    // execute
    eagle::Instance *eagle = new eagle::Instance(cores);
    eagle->addPlan(plan);
    eagle->run();
    
    // validate result
    CREATE_EXPRESSION_ARRAY(answers_0, pageSize, col1Data[i]);          // col1
    CREATE_EXPRESSION_ARRAY(answers_1, pageSize, col2Data[i] + 8);      // col2 + 8
    
    int **answers = (int**) calloc(exprs, sizeof(int*));
    answers[0] = (int*) calloc(totalResults, sizeof(int));
    answers[1] = (int*) calloc(totalResults, sizeof(int));
    
    for(int i = 0, j = 0; i < pageSize; ++i) {
        if(col1Data[i] + col1Data[i] == 4) {
            answers[0][j] = answers_0[i];
            answers[1][j] = answers_1[i];
            ++j;
        }
    }
    
    // subtract 1 because the WHERE clause does not get emitted
    for(int i = 0; i < selectStage->resultFields - 1; ++i) {
        EaglePage *page = selectStage->result[i]->nextPage();
        CUNIT_ASSERT_NOT_NULL(page);
        CUNIT_VERIFY_EQUAL_INT(page->count, totalResults);
        
        int valid = 1;
        for(int j = 0; j < page->count; ++j) {
            if(((int*) page->data)[j] != answers[i][j]) {
                valid = 0;
                break;
            }
        }
        CUNIT_VERIFY_EQUAL_INT(valid, 1);
        
        delete page;
    }
    
    free(col1Data);
    free(col2Data);
    for(int i = 0; i < exprs; ++i) {
        EagleDbSqlExpression_DeleteRecursive(expr[i]);
    }
    for(int i = 0; i < selectStage->resultFields; ++i) {
        delete answers[i];
    }
    delete answers_0;
    delete answers_1;
    delete answers;
    delete expr;
    delete eagle;
    delete plan;
}

CUNIT_TEST(DBSuite, EagleDbSqlSelect_toString, withOrderBy)
{
    EagleDbSqlSelect *select = EagleDbSqlSelect_New();
    select->selectExpressions = new EagleSynchronizedLinkedList<EagleDbSqlExpression*>();
    select->selectExpressions->addObject((EagleDbSqlExpression*) EagleDbSqlValue_NewWithInteger(123), true, (void(*)(EagleDbSqlExpression*))EagleDbSqlExpression_DeleteRecursive);
    select->selectExpressions->addObject((EagleDbSqlExpression*) EagleDbSqlValue_NewWithInteger(456), true, (void(*)(EagleDbSqlExpression*))EagleDbSqlExpression_DeleteRecursive);
    select->whereExpression = (EagleDbSqlExpression*) EagleDbSqlValue_NewWithInteger(789);
    select->orderByExpression = EagleDbSqlValue_NewWithIdentifier("abc");
    
    char *desc = EagleDbSqlSelect_toString(select);
    CUNIT_VERIFY_EQUAL_STRING(desc, "SELECT 123, 456 WHERE 789 ORDER BY abc");
    free(desc);
    
    EagleDbSqlSelect_DeleteRecursive(select);
}

CUNIT_TEST(DBSuite, EagleDbConsole_run)
{
    EagleDbConsole_run();
}

CUnitTests* DBSuite3_tests()
{
    CUnitTests *tests = CUnitTests_New(100);
    
    // method tests
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbConsole_run));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlSelect_toString, withOrderBy));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_CompilePlan, Both));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_CompilePlan, Right));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlBinaryExpressionOperator_toString, Divide));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlBinaryExpressionOperator_toString, Minus));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlBinaryExpressionOperator_toString, LessThanEqual));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlBinaryExpressionOperator_toString, GreaterThanEqual));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlBinaryExpressionOperator_toString, LessThan));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlBinaryExpressionOperator_toString, GreaterThan));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlBinaryExpressionOperator_toString, NotEquals));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlBinaryExpressionOperator_toString, Multiply));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlBinaryExpression_GetPageOperation, withMatch));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlBinaryExpression_GetRightOperation, withMatch));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, _SELECT_ORDER_BY));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlFunctionExpression_toString, 2));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlFunctionExpression_toString, position));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Function_, atan2WrongArgs));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer_Function_, atan2));
    
    return tests;
}
