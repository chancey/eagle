#include "DBSuite.h"
#include <stdio.h>
#include <stdlib.h>
#include "eagledb/EagleDbSqlSelect.h"
#include "eagledb/EagleDbSqlBinaryExpression.h"
#include "eagledb/EagleDbSqlValue.h"
#include "eagle/Instance.h"
#include "eagle/EagleData.h"
#include "eagle/db/Column.h"
#include "eagle/db/Table.h"
#include "eagle/db/TableData.h"
#include "eagle/db/Tuple.h"
#include "eagledb/EagleDbConsole.h"
#include "eagledb/EagleDbParser.h"
#include "eagle/EagleLogger.h"
#include "eagledb/EagleDbInstance.h"
#include "eagledb/EagleDbSqlUnaryExpression.h"
#include "eagle/StreamPageProvider.h"
#include "eagle/ArrayPageProvider.h"
#include "eagledb/EagleDbInformationSchema.h"
#include "eagledb/EagleDbSqlCastExpression.h"
#include "eagle/EagleSynchronizedLinkedList.h"
#include "eagle/Plan.h"
#include "eagle/plan/stage/SelectStage.h"

EagleDbParser* _testSqlSelect(const char *sql)
{
    // add ;
    char *newsql = (char*) malloc(strlen(sql) + 2);
    sprintf(newsql, "%s;", sql);
    
    EagleDbParser *parser = EagleDbParser_ParseWithString(newsql);
    delete newsql;
    return parser;
}

CUNIT_TEST(DBSuite, EagleDbSqlSelect_New)
{
    EagleDbSqlSelect *select = EagleDbSqlSelect_New();
    
    CUNIT_ASSERT_NOT_NULL(select);
    if(select != NULL) {
        CUNIT_ASSERT_EQUAL_INT(select->expressionType, EagleDbSqlExpressionTypeSelect);
        
        CUNIT_ASSERT_TRUE(select->tableName.empty());
        CUNIT_ASSERT_NULL(select->whereExpression);
    }
    
    EagleDbSqlSelect_DeleteRecursive(select);
}

CUNIT_TEST(DBSuite, EagleDbSqlBinaryExpression_New)
{
    EagleDbSqlExpression *left = (EagleDbSqlExpression*) EagleDbSqlValue_NewWithInteger(123);
    EagleDbSqlExpression *right = (EagleDbSqlExpression*) EagleDbSqlValue_NewWithInteger(456);
    
    EagleDbSqlBinaryExpression *binary = EagleDbSqlBinaryExpression_New(left, EagleDbSqlBinaryExpressionOperatorPlus, right);
    CUNIT_ASSERT_NOT_NULL(binary);
    if(NULL != binary) {
        CUNIT_ASSERT_EQUAL_INT(binary->expressionType, EagleDbSqlExpressionTypeBinaryExpression);
        
        CUNIT_VERIFY_EQUAL_PTR(binary->left, left);
        CUNIT_VERIFY_EQUAL_INT(binary->op, EagleDbSqlBinaryExpressionOperatorPlus);
        CUNIT_VERIFY_EQUAL_PTR(binary->right, right);
    }
    
    EagleDbSqlBinaryExpression_DeleteRecursive(binary);
}

CUNIT_TEST(DBSuite, EagleDbSqlValue_NewWithInteger)
{
    EagleDbSqlValue *value = EagleDbSqlValue_NewWithInteger(123);
    
    CUNIT_ASSERT_NOT_NULL(value);
    if(NULL != value) {
        CUNIT_ASSERT_EQUAL_INT(value->expressionType, EagleDbSqlExpressionTypeValue);
        
        CUNIT_ASSERT_EQUAL_INT(value->type, EagleDbSqlValueTypeInteger);
        CUNIT_ASSERT_EQUAL_INT(value->value.intValue, 123);
    }
    
    EagleDbSqlValue_Delete(value);
}

EagleDbSqlExpression* _getExpression(const char *sql)
{
    EagleDbParser *p = _testSqlSelect(sql);
    if(EagleDbParser_hasError(p)) {
        CUNIT_FAIL(EagleDbParser_lastError(p), NULL);
    }
    
    EagleDbSqlSelect *select = (EagleDbSqlSelect*) p->yyparse_ast;
    CUNIT_ASSERT_NOT_NULL(select->selectExpressions->get(0));
    
    return (EagleDbSqlExpression*) select->selectExpressions->get(0);
}

void _testExpression(EagleDbSqlExpression *where, int usedProviders, int usedOperations, int *answers)
{
    // compile plan
    int pageSize = 10, cores = 1;
    eagle::StreamPageProvider *receiver = new eagle::StreamPageProvider(EagleData::Type::Integer, pageSize, NULL);
    eagle::Plan *plan = new eagle::Plan();
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(plan, pageSize, cores);
    
    // setup the table
    int *col1Data = (int*) calloc((size_t) pageSize, sizeof(int));
    for(int i = 0; i < pageSize; ++i) {
        col1Data[i] = i;
    }
    eagle::PageProvider *col1 = (eagle::PageProvider*) new eagle::ArrayPageProvider(EagleData::Type::Integer, col1Data,
                                                                                    pageSize, pageSize, "col1", false);
    selectStage->addBufferProvider(new eagle::PlanBufferProvider(1, col1, true), true);
    CUNIT_ASSERT_EQUAL_INT(selectStage->providers->length(), 1);
    
    EagleDbSqlExpression_CompilePlan((EagleDbSqlExpression**) where, 1, -1, selectStage);
    //printf("\n%s\n", EaglePlan_toString(plan));
    
    CUNIT_ASSERT_EQUAL_INT(selectStage->providers->length(), usedProviders);
    CUNIT_ASSERT_EQUAL_INT(selectStage->operations->length(), usedOperations);
    
    // execute
    eagle::Instance *eagle = new eagle::Instance(cores);
    eagle->addPlan(plan);
    eagle->run();
    
    // validate result
    EaglePage *page = receiver->nextPage();
    CUNIT_ASSERT_EQUAL_INT(receiver->totalRecords, pageSize);
    int valid = 1;
    for(int i = 0; i < pageSize; ++i) {
        //printf("\n%d != %d", page->data[i], answers[i]);
        if(((int*) page->data)[i] != answers[i]) {
            valid = 0;
            break;
        }
    }
    CUNIT_ASSERT_EQUAL_INT(valid, 1);
    
    delete receiver;
    delete eagle;
    delete plan;
}

CUNIT_TEST(DBSuite, EagleDbSqlSelect_Delete)
{
    EagleDbSqlSelect_DeleteRecursive(NULL);
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_CompilePlan, Left)
{
    int pageSize = 10;
    int exprs = 3;
    int totalResults = 5;
    int cores = 1;
    EagleDbSqlExpression **expr = (EagleDbSqlExpression**) calloc(exprs, sizeof(EagleDbSqlExpression*));
    
    // SELECT col1, col2 + 8 FROM mytable WHERE col1 % 2 = 1
    expr[0] = (EagleDbSqlExpression*) EagleDbSqlValue_NewWithIdentifier("col1");
    expr[1] = (EagleDbSqlExpression*) EagleDbSqlBinaryExpression_New(
        (EagleDbSqlExpression*) EagleDbSqlValue_NewWithIdentifier("col2"),
        EagleDbSqlBinaryExpressionOperatorPlus,
        (EagleDbSqlExpression*) EagleDbSqlValue_NewWithInteger(8)
    );
    expr[2] = (EagleDbSqlExpression*) EagleDbSqlBinaryExpression_New(
        (EagleDbSqlExpression*) EagleDbSqlBinaryExpression_New(
            (EagleDbSqlExpression*) EagleDbSqlValue_NewWithIdentifier("col1"),
            EagleDbSqlBinaryExpressionOperatorModulus,
            (EagleDbSqlExpression*) EagleDbSqlValue_NewWithInteger(2)
        ),
        EagleDbSqlBinaryExpressionOperatorEquals,
        (EagleDbSqlExpression*) EagleDbSqlValue_NewWithInteger(1)
    );
    
    // create the plan skeleton
    eagle::Plan *plan = new eagle::Plan();
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(plan, pageSize, cores);
    plan->addStage(selectStage, true);
    
    // create a virtual table that consists of 2 columns; col1 and col2
    int *col1Data = (int*) calloc((size_t) pageSize, sizeof(int));
    int *col2Data = (int*) calloc((size_t) pageSize, sizeof(int));
    for(int i = 0; i < pageSize; ++i) {
        col1Data[i] = i;
        col2Data[i] = i * 2;
    }
    eagle::ArrayPageProvider *col1 = new eagle::ArrayPageProvider(EagleData::Type::Integer, col1Data, pageSize,
                                                                  pageSize, "col1", false);
    eagle::ArrayPageProvider *col2 = new eagle::ArrayPageProvider(EagleData::Type::Integer, col2Data, pageSize,
                                                                  pageSize, "col2", false);
    selectStage->addBufferProvider(new eagle::PlanBufferProvider(1, (eagle::PageProvider*) col1, true), true);
    selectStage->addBufferProvider(new eagle::PlanBufferProvider(2, (eagle::PageProvider*) col2, true), true);
    
    // compile plan
    EagleDbSqlExpression_CompilePlan(expr, exprs, 2, selectStage);
    //printf("\n%s\n", EaglePlan_toString(plan));
    
    CUNIT_ASSERT_EQUAL_INT(selectStage->providers->length(), 2);
    CUNIT_ASSERT_EQUAL_INT(selectStage->operations->length(), 5);
    
    // execute
    eagle::Instance *eagle = new eagle::Instance(cores);
    eagle->addPlan(plan);
    eagle->run();
    
    // validate result
    CREATE_EXPRESSION_ARRAY(answers_0, pageSize, col1Data[i]);          // col1
    CREATE_EXPRESSION_ARRAY(answers_1, pageSize, col2Data[i] + 8);      // col2 + 8
    
    int **answers = (int**) calloc(exprs, sizeof(int*));
    answers[0] = (int*) calloc(totalResults, sizeof(int));
    answers[1] = (int*) calloc(totalResults, sizeof(int));
    
    for(int i = 0, j = 0; i < pageSize; ++i) {
        if(col1Data[i] % 2 == 1) {
            answers[0][j] = answers_0[i];
            answers[1][j] = answers_1[i];
            ++j;
        }
    }
    
    // subtract 1 because the WHERE clause does not get emitted
    for(int i = 0; i < selectStage->resultFields - 1; ++i) {
        EaglePage *page = selectStage->result[i]->nextPage();
        CUNIT_ASSERT_NOT_NULL(page);
        
        // there should only be 5 records in each page because of the modulus
        CUNIT_VERIFY_EQUAL_INT(page->count, totalResults);
        
        int valid = 1;
        for(int j = 0; j < page->count; ++j) {
            if(((int*) page->data)[j] != answers[i][j]) {
                valid = 0;
                break;
            }
        }
        CUNIT_VERIFY_EQUAL_INT(valid, 1);
        
        delete page;
    }
    
    free(col1Data);
    free(col2Data);
    for(int i = 0; i < exprs; ++i) {
        EagleDbSqlExpression_DeleteRecursive(expr[i]);
    }
    for(int i = 0; i < selectStage->resultFields; ++i) {
        delete answers[i];
    }
    delete answers_0;
    delete answers_1;
    delete answers;
    delete expr;
    delete eagle;
    delete plan;
}

CUNIT_TEST(DBSuite, EagleDbConsole_New)
{
    EagleDbConsole *console = EagleDbConsole_New();
    EagleDbConsole_Delete(console);
}

CUNIT_TEST(DBSuite, EagleDbInstance_New)
{
    EagleDbInstance *instance = EagleDbInstance_New(1000, 1);
    EagleDbInstance_Delete(instance);
}

CUNIT_TEST(DBSuite, EagleDbSqlValue_toString, 1)
{
    EagleDbSqlValue *v = EagleDbSqlValue_NewWithAsterisk();
    char *desc = EagleDbSqlValue_toString(v);
    CUNIT_VERIFY_EQUAL_STRING(desc, "*");
    free(desc);
    EagleDbSqlValue_Delete(v);
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer, 1)
{
    EagleDbSqlSelect *select = EagleDbSqlSelect_New();
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    
    CUNIT_VERIFY_EQUAL_INT(EagleDbSqlExpression_CompilePlanIntoBuffer_((EagleDbSqlExpression*) select, NULL, NULL, true), EagleDbSqlExpression_ERROR);
    
    int result = EagleDbSqlExpression_CompilePlanIntoBuffer_((EagleDbSqlExpression*) select, NULL, selectStage, true);
    CUNIT_VERIFY_EQUAL_INT(result, 0);
    
    EagleDbSqlSelect_DeleteRecursive(select);
    delete selectStage;
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_toString)
{
    char *desc = EagleDbSqlExpression_toString(NULL);
    CUNIT_VERIFY_EQUAL_STRING(desc, "");
    free(desc);
    
    EagleDbSqlSelect *select = EagleDbSqlSelect_New();
    desc = EagleDbSqlExpression_toString((EagleDbSqlExpression*) select);
    CUNIT_VERIFY_EQUAL_STRING(desc, "SELECT ");
    free(desc);
    EagleDbSqlExpression_DeleteRecursive((EagleDbSqlExpression*) select);
}

CUNIT_TEST(DBSuite, EagleDbSqlSelect_parse, 1)
{
    EagleDbSqlSelect *select = EagleDbSqlSelect_New();
    CUNIT_VERIFY_NULL(EagleDbSqlSelect_parse(NULL, NULL));
    CUNIT_VERIFY_NULL(EagleDbSqlSelect_parse(select, NULL));
    EagleDbSqlSelect_DeleteRecursive(select);
}

CUNIT_TEST(DBSuite, EagleDbSqlSelect_parse, 2)
{
    int pageSize = 1;
    char *tableName = "mytable";
    EagleDbInstance *instance = EagleDbInstance_New(pageSize, 1);
    
    eagle::db::Table *table = new eagle::db::Table(tableName);
    table->addColumn(new eagle::db::Column("a", EagleData::Type::Integer));
    
    EagleDbSqlSelect *select = EagleDbSqlSelect_New();
    select->tableName = strdup(tableName);
    select->selectExpressions = new EagleSynchronizedLinkedList<EagleDbSqlExpression*>();
    select->selectExpressions->add(new EagleLinkedList<EagleDbSqlExpression*>::Item((EagleDbSqlExpression*) EagleDbSqlValue_NewWithInteger(123), true, (void(*)(EagleDbSqlExpression*))EagleDbSqlValue_Delete));
    select->whereExpression = (EagleDbSqlExpression*) EagleDbSqlValue_NewWithInteger(456);
    CUNIT_ASSERT_EQUAL_INT(EagleDbSqlSelect_getFieldCount(select), 1);
    
    EagleDbSchema *schema = EagleDbInstance_getSchema(instance, EagleDbSchema::DefaultSchemaName);
    eagle::db::TableData *td = new eagle::db::TableData(table, pageSize);
    schema->addTable(td);
    
    CUNIT_ASSERT_NOT_NULL(EagleDbInstance_getTable(instance, tableName));
    
    eagle::Plan *plan = EagleDbSqlSelect_parse(select, instance);
    CUNIT_VERIFY_NOT_NULL(plan);
    
    EagleDbSqlExpression_DeleteRecursive((EagleDbSqlExpression*) select);
    eagle_db_Table_DeleteWithColumns(table);
    delete td;
    EagleDbInstance_Delete(instance);
    delete plan;
}

CUNIT_TEST(DBSuite, EagleDbSqlSelect_parse, 3)
{
    int pageSize = 1;
    char *tableName = "mytable";
    EagleDbInstance *instance = EagleDbInstance_New(pageSize, 1);
    
    EagleDbSqlSelect *select = EagleDbSqlSelect_New();
    select->tableName = strdup(tableName);
    select->selectExpressions = new EagleSynchronizedLinkedList<EagleDbSqlExpression*>();
    select->selectExpressions->addObject((EagleDbSqlExpression*) EagleDbSqlValue_NewWithInteger(123), true, (void(*)(EagleDbSqlExpression*)) EagleDbSqlValue_Delete);
    CUNIT_ASSERT_EQUAL_INT(EagleDbSqlSelect_getFieldCount(select), 1);
    
    CUNIT_ASSERT_NULL(EagleDbInstance_getTable(instance, tableName));
    
    eagle::Plan *plan = EagleDbSqlSelect_parse(select, instance);
    eagle::plan::stage::SelectStage *selectStage = (eagle::plan::stage::SelectStage*) plan->stages->get(0);
    CUNIT_VERIFY_NOT_NULL(selectStage);
    if(NULL != selectStage) {
        CUNIT_VERIFY_EQUAL_INT(selectStage->errorCode, EaglePlanErrorNoSuchTable);
        CUNIT_VERIFY_EQUAL_STRING(selectStage->errorMessage.c_str(), tableName);
    }
    
    EagleDbSqlSelect_DeleteRecursive(select);
    EagleDbInstance_Delete(instance);
    delete plan;
}

CUNIT_TEST(DBSuite, EagleDbInstance_PrintResults)
{
    EagleDbInstance_PrintResults(NULL);
    
    int pageSize = 1;
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, pageSize, 1);
    
    selectStage->resultFields = 1;
    selectStage->result = new eagle::PageProvider*[selectStage->resultFields];
    selectStage->result[0] = new eagle::StreamPageProvider(EagleData::Type::Integer, pageSize, "name");
    
    // some data
    int *data = EagleData::CreateInt(123);
    selectStage->result[0]->add(data);
    free(data);
    
    EagleDbInstance_PrintResults(selectStage);
    delete selectStage;
}

CUNIT_TEST(DBSuite, EagleDbInstance_executeSelect, 1)
{
    EagleDbInstance *db = EagleDbInstance_New(1, 1);
    EagleDbSqlSelect *select = EagleDbSqlSelect_New();
    
    EagleLogger::Event *error = NULL;
    eagle::Plan *p = NULL;
    EagleDbInstance_executeSelect(db, select, &error, &p);
    delete p;
    
    EagleDbInstance_Delete(db);
    EagleDbSqlSelect_DeleteRecursive(select);
}

CUNIT_TEST(DBSuite, EagleDbInstance_executeSelect, 2)
{
    const char *tableName = "mytable";
    int pageSize = 1;
    
    EagleDbInstance *db = EagleDbInstance_New(1, 1);
    EagleDbSqlSelect *select = EagleDbSqlSelect_New();
    select->tableName = strdup(tableName);
    select->selectExpressions = new EagleSynchronizedLinkedList<EagleDbSqlExpression*>();
    select->selectExpressions->addObject((EagleDbSqlExpression*) EagleDbSqlValue_NewWithInteger(123), true, (void(*)(EagleDbSqlExpression*)) EagleDbSqlValue_Delete);
    CUNIT_ASSERT_EQUAL_INT(EagleDbSqlSelect_getFieldCount(select), 1);
    
    eagle::db::Table *table = new eagle::db::Table((char*) tableName);
    table->addColumn(new eagle::db::Column("a", EagleData::Type::Integer));
    
    EagleDbSchema *schema = EagleDbInstance_getSchema(db, EagleDbSchema::DefaultSchemaName);
    
    eagle::db::TableData *td = new eagle::db::TableData(table, pageSize);
    schema->addTable(td);
    
    EagleLogger::Event *error = NULL;
    eagle::Plan *p = NULL;
    EagleDbInstance_executeSelect(db, select, &error, &p);
    
    EagleDbInstance_Delete(db);
    EagleDbSqlSelect_DeleteRecursive(select);
    eagle_db_Table_DeleteWithColumns(table);
    delete td;
    delete p;
}

CUNIT_TEST(DBSuite, EagleDbInstance_executeCreateTable)
{
    EagleDbInstance *db = EagleDbInstance_New(1, 1);
    
    eagle::db::Table *table = new eagle::db::Table("mytable");
    table->addColumn(new eagle::db::Column("a", EagleData::Type::Integer));
    
    EagleLogger::Event *error = NULL;
    CUNIT_VERIFY_TRUE(EagleDbInstance_executeCreateTable(db, table, &error));
    CUNIT_ASSERT_LAST_ERROR("Table \"default.mytable\" created.");
    
    EagleDbInstance_DeleteAll(db);
}

CUNIT_TEST(DBSuite, _DuplicateTable)
{
    EagleDbInstance *db = EagleDbInstance_New(1, 1);
    
    EagleLogger::Event *error = NULL;
    CUNIT_VERIFY_TRUE(EagleDbInstance_execute(db, "CREATE TABLE sometable (id INT);", &error, NULL));
    CUNIT_VERIFY_EQUAL_INT(error->getSeverity(), EagleLogger::Severity::Info);
    CUNIT_ASSERT_LAST_ERROR("Table \"default.sometable\" created.");
    
    CUNIT_VERIFY_FALSE(EagleDbInstance_execute(db, "CREATE TABLE sometable (id INT);", &error, NULL));
    CUNIT_VERIFY_EQUAL_INT(error->getSeverity(), EagleLogger::Severity::UserError);
    CUNIT_ASSERT_LAST_ERROR("Table \"default.sometable\" already exists.");
    
    EagleDbInstance_DeleteAll(db);
}

CUNIT_TEST(DBSuite, _DuplicateSchema)
{
    EagleDbInstance *db = EagleDbInstance_New(1, 1);
    EagleDbSchema *schema = new EagleDbSchema((char*) EagleDbSchema::DefaultSchemaName);
    
    CUNIT_VERIFY_FALSE(EagleDbInstance_addSchema(db, schema));
    CUNIT_ASSERT_LAST_ERROR("Error: Schema \"default\" already exists.");
    
    delete schema;
    EagleDbInstance_DeleteAll(db);
}

CUNIT_TEST(DBSuite, EagleDbInstance_execute, 1)
{
    EagleDbInstance *db = EagleDbInstance_New(1, 1);
    
    EagleLogger::Event *error = NULL;
    CUNIT_VERIFY_TRUE(EagleDbInstance_execute(db, "CREATE TABLE sometable (id INT);", &error, NULL));
    CUNIT_ASSERT_LAST_ERROR("Table \"default.sometable\" created.");
    
    EagleDbInstance_DeleteAll(db);
}

CUNIT_TEST(DBSuite, EagleDbInstance_execute, 2)
{
    EagleDbInstance *db = EagleDbInstance_New(1, 1);
    
    EagleLogger::Event *error = NULL;
    CUNIT_VERIFY_FALSE(EagleDbInstance_execute(db, "CREATE TABL sometable (id INT);", &error, NULL));
    CUNIT_ASSERT_LAST_ERROR("Error: syntax error, unexpected identifier, expecting TABLE");
    
    EagleDbInstance_Delete(db);
}

CUNIT_TEST(DBSuite, EagleDbInstance_execute, 3)
{
    EagleDbInstance *db = EagleDbInstance_New(1, 1);
    
    EagleLogger::Event *error = NULL;
    eagle::Plan *p = NULL;
    CUNIT_VERIFY_FALSE(EagleDbInstance_execute(db, "SELECT * FROM mytable;", &error, &p));
    CUNIT_ASSERT_LAST_ERROR("mytable");
    
    EagleDbInstance_Delete(db);
    delete p;
}

CUNIT_TEST(DBSuite, EagleDbInstance_getTable, 1)
{
    EagleDbInstance *db = EagleDbInstance_New(1, 1);
    
    eagle::db::TableData *td = EagleDbInstance_getTable(db, "no_such_table");
    CUNIT_VERIFY_NULL(td);
    
    delete td;
    EagleDbInstance_Delete(db);
}

CUNIT_TEST(DBSuite, EagleDbInstance_getTable, 2)
{
    int pageSize = 1;
    EagleDbInstance *db = EagleDbInstance_New(pageSize, 1);
    
    EagleDbSchema *schema = EagleDbInstance_getSchema(db, EagleDbSchema::DefaultSchemaName);
    
    eagle::db::Table *table1 = new eagle::db::Table("table1");
    eagle::db::TableData *td1 = new eagle::db::TableData(table1, pageSize);
    schema->addTable(td1);
    
    eagle::db::Table *table2 = new eagle::db::Table("table2");
    eagle::db::TableData *td2 = new eagle::db::TableData(table2, pageSize);
    schema->addTable(td2);
    
    eagle::db::TableData *td = EagleDbInstance_getTable(db, "table2");
    CUNIT_ASSERT_NOT_NULL(td);
    if(NULL != td) {
        CUNIT_VERIFY_EQUAL_STRING(td->table->name, "table2");
    }
    
    EagleDbInstance_Delete(db);
    delete td1;
    delete td2;
    delete table1;
    delete table2;
}

CUNIT_TEST(DBSuite, EagleDbInstance_getSchema)
{
    EagleDbInstance *db = EagleDbInstance_New(1, 1);
    
    EagleDbSchema *schema1 = new EagleDbSchema("schema1");
    EagleDbInstance_addSchema(db, schema1);
    
    EagleDbSchema *schema2 = new EagleDbSchema("schema2");
    EagleDbInstance_addSchema(db, schema2);
    
    EagleDbSchema *schema = EagleDbInstance_getSchema(db, "schema2");
    CUNIT_ASSERT_NOT_NULL(schema);
    if(NULL != schema) {
        CUNIT_VERIFY_EQUAL_STRING(schema->name, "schema2");
    }
    
    schema = EagleDbInstance_getSchema(db, "schema3");
    CUNIT_VERIFY_NULL(schema);
    
    EagleDbInstance_Delete(db);
    delete schema1;
    delete schema2;
}

CUNIT_TEST(DBSuite, EagleDbConsole_GetLine)
{
    CUNIT_ASSERT_NULL(EagleDbConsole_GetLine());
}

CUNIT_TEST(DBSuite, EagleDbSqlSelect_Delete, 2)
{
    EagleDbSqlSelect_Delete(NULL);
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_Delete, null)
{
    EagleDbSqlExpression_Delete(NULL);
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_Delete, Not)
{
    EagleDbSqlExpression *expr = (EagleDbSqlExpression*) EagleDbSqlUnaryExpression_New(EagleDbSqlUnaryExpressionOperatorNot, NULL);
    CUNIT_VERIFY_NOT_NULL(expr);
    EagleDbSqlExpression_Delete(expr);
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_Delete, Equals)
{
    EagleDbSqlExpression *expr = (EagleDbSqlExpression*) EagleDbSqlBinaryExpression_New(NULL, EagleDbSqlBinaryExpressionOperatorEquals, NULL);
    CUNIT_VERIFY_NOT_NULL(expr);
    EagleDbSqlExpression_Delete(expr);
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_Delete, Select)
{
    EagleDbSqlExpression *expr = (EagleDbSqlExpression*) EagleDbSqlSelect_New();
    CUNIT_VERIFY_NOT_NULL(expr);
    EagleDbSqlExpression_Delete(expr);
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_Delete, NewWithInteger)
{
    EagleDbSqlExpression *expr = (EagleDbSqlExpression*) EagleDbSqlValue_NewWithInteger(123);
    CUNIT_VERIFY_NOT_NULL(expr);
    EagleDbSqlExpression_Delete(expr);
}

CUNIT_TEST(DBSuite, EagleDbSqlExpression_Delete, Cast)
{
    EagleDbSqlExpression *expr = (EagleDbSqlExpression*) EagleDbSqlCastExpression_New(NULL, EagleData::Type::Integer);
    CUNIT_VERIFY_NOT_NULL(expr);
    EagleDbSqlExpression_Delete(expr);
}

CUNIT_TEST(DBSuite, EagleDbInstance_Delete)
{
    EagleDbInstance_Delete(NULL);
}

CUNIT_TEST(DBSuite, EagleDbInstance_DeleteAll)
{
    EagleDbInstance_DeleteAll(NULL);
}

CUNIT_TEST(DBSuite, EagleDbSqlValue_Delete)
{
    EagleDbSqlValue_Delete(NULL);
}

CUNIT_TEST(DBSuite, EagleDbConsole_Delete)
{
    EagleDbConsole_Delete(NULL);
}

CUNIT_TEST(DBSuite, EagleDbSqlBinaryExpression_Delete)
{
    EagleDbSqlBinaryExpression_Delete(NULL);
}

CUNIT_TEST(DBSuite, EagleDbSqlBinaryExpression_DeleteRecursive)
{
    EagleDbSqlBinaryExpression_DeleteRecursive(NULL);
}

EagleDbInstance* EagleInstanceTest(int pageSize)
{
    EagleDbInstance *db = EagleDbInstance_New(pageSize, 1);
    
    EagleDbSchema *schema =  EagleDbInstance_getSchema(db, EagleDbSchema::DefaultSchemaName);
    
    eagle::db::Table *table = new eagle::db::Table("mytable");
    table->addColumn(new eagle::db::Column("col1", EagleData::Type::Integer));
    table->addColumn(new eagle::db::Column("col2", EagleData::Type::Float));
    
    eagle::db::TableData *td = new eagle::db::TableData(table, pageSize);
    schema->addTable(td);
    
    return db;
}

void EagleInstanceTest_Cleanup(EagleDbInstance* db)
{
    EagleDbSchema *schema = EagleDbInstance_getSchema(db, EagleDbSchema::DefaultSchemaName);
    
    EagleLinkedList_Foreach(schema->tables, eagle::db::TableData*, td)
    {
        // FIXME: This must be removed with Issue #64
        if(!strcmp(td->table->name, "information_schema_tables")) {
            continue;
        }
        
        eagle_db_Table_DeleteWithColumns(td->table);
        delete td;
    }
    EagleLinkedList_ForeachEnd
    
    EagleDbInstance_Delete(db);
}

CUNIT_TEST(DBSuite, _INSERT_BadTableName)
{
    EagleLogger::Get()->disableOutHandle();
    EagleDbInstance *db = EagleInstanceTest(10);
    
    EagleLogger::Event *error = NULL;
    bool success = EagleDbInstance_execute(db, "INSERT INTO mytable2 (col1) VALUES (123);", &error, NULL);
    CUNIT_ASSERT_FALSE(success);
    CUNIT_ASSERT_LAST_ERROR("No such table 'mytable2'");
    
    EagleInstanceTest_Cleanup(db);
}

CUNIT_TEST(DBSuite, _INSERT_BadColumnName)
{
    EagleDbInstance *db = EagleInstanceTest(10);
    
    EagleLogger::Event *error = NULL;
    bool success = EagleDbInstance_execute(db, "INSERT INTO mytable (col1000) VALUES (123);", &error, NULL);
    CUNIT_ASSERT_FALSE(success);
    CUNIT_ASSERT_LAST_ERROR("No such column 'col1000' in table 'mytable'");
    
    EagleInstanceTest_Cleanup(db);
}

CUNIT_TEST(DBSuite, _INSERT_Good)
{
    EagleDbInstance *db = EagleInstanceTest(10);
    
    // make sure there are no rows in the table
    eagle::db::TableData *td = EagleDbInstance_getTable(db, "mytable");
    EaglePage *p = td->providers[0]->nextPage();
    CUNIT_ASSERT_NULL(p);
    
    EagleLogger::Event *error = NULL;
    bool success = EagleDbInstance_execute(db, "INSERT INTO mytable (col1, col2) VALUES (123, 123.123);", &error, NULL);
    if(false == success) {
        CUNIT_FAIL("%s", EagleLogger::LastEvent()->getMessage().c_str());
    }
    
    p = td->providers[0]->nextPage();
    CUNIT_ASSERT_NOT_NULL(p);
    if(NULL != p) {
        CUNIT_VERIFY_EQUAL_INT(p->count, 1);
    }
    
    delete p;
    EagleInstanceTest_Cleanup(db);
}

CUnitTests* DBSuite1_tests()
{
    CUnitTests *tests = CUnitTests_New(100);
    
    // method tests
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, _DuplicateSchema));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, _DuplicateTable));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, _INSERT_Good));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, _INSERT_BadColumnName));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, _INSERT_BadTableName));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlBinaryExpression_DeleteRecursive));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlBinaryExpression_Delete));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbConsole_Delete));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlValue_Delete));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbInstance_Delete));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbInstance_DeleteAll));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_Delete, Not));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_Delete, null));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_Delete, Equals));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_Delete, Select));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_Delete, NewWithInteger));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_Delete, Cast));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlSelect_Delete, 2));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbConsole_GetLine));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbInstance_getSchema));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbInstance_getTable, 2));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbInstance_getTable, 1));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbInstance_execute, 3));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbInstance_execute, 2));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbInstance_execute, 1));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbInstance_executeCreateTable));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbInstance_executeSelect, 2));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbInstance_executeSelect, 1));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbInstance_PrintResults));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlSelect_parse, 3));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlSelect_parse, 2));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlSelect_parse, 1));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_toString));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_CompilePlanIntoBuffer, 1));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlValue_toString, 1));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbConsole_New));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbInstance_New));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlBinaryExpression_New));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlExpression_CompilePlan, Left));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlSelect_New));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlSelect_Delete));
    CUnitTests_addTest(tests, CUNIT_NEW(DBSuite, EagleDbSqlValue_NewWithInteger));
    
    return tests;
}

/**
 * The suite init function.
 */
int DBSuite_init()
{
    return 0;
}

/**
 * The suite cleanup function.
 */
int DBSuite_clean()
{
    return 0;
}
