#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "MainSuite.h"
#include "eagle/EagleData.h"
#include "eagle/EaglePageOperations.h"
#include "eagle/Instance.h"
#include "eagle/EagleLinkedList.h"
#include "eagle/EagleLogger.h"
#include "eagle/StreamPageProvider.h"
#include "eagle/ArrayPageProvider.h"
#include "eagle/SinglePageProvider.h"
#include "eagle/EagleSynchronizedLinkedList.h"
#include "eagle/EagleUnsynchronizedLinkedList.h"
#include "eagle/plan/Operation.h"
#include "eagle/PlanBufferProvider.h"
#include "eagle/Plan.h"
#include "eagle/Calendar.h"

CUNIT_TEST(MainSuite, EaglePageProvider_reset)
{
    eagle::SinglePageProvider *epp = eagle::SinglePageProvider::NewInt(123, 1, "bla");
    epp->reset();
    delete epp;
}

CUNIT_TEST(MainSuite, eagle_ArrayPageProvider_nextPage, Int)
{
    eagle::ArrayPageProvider *epp = new eagle::ArrayPageProvider(EagleData::Type::Integer, NULL, 0, 1, "name", false);
    CUNIT_VERIFY_NULL(epp->nextPage());
    delete epp;
}

CUNIT_TEST(MainSuite, eagle_SinglePageProvider_nextPage, 1)
{
    eagle::SinglePageProvider *epp = eagle::SinglePageProvider::NewInt(123, 1, "name");
    epp->type = EagleData::Type::Unknown;
    CUNIT_VERIFY_NULL(epp->nextPage());
    delete epp;
}

CUNIT_TEST(MainSuite, eagle_SinglePageProvider_nextPage, 2)
{
    int recordsPerPage = 5;
    eagle::SinglePageProvider *epp = eagle::SinglePageProvider::NewVarchar("some value", recordsPerPage, "name");
    
    EaglePage *page = epp->nextPage();
    CUNIT_ASSERT_NOT_NULL(page);
    if(NULL != page) {
        CUNIT_VERIFY_EQUAL_INT(page->count, recordsPerPage);
    }
    delete page;
    
    delete epp;
}

CUNIT_TEST(MainSuite, EagleLinkedList_deleteObject, 1)
{
    int *obj1 = EagleData::CreateInt(123);
    int *obj2 = EagleData::CreateInt(456);
    EagleLinkedList<EagleDataTypeInteger*> *list = new EagleSynchronizedLinkedList<EagleDataTypeInteger*>();
    
    // NULL object
    CUNIT_ASSERT_FALSE(list->deleteObject(NULL));
    CUNIT_ASSERT_EQUAL_INT(0, list->length());
    
    // empty list
    CUNIT_ASSERT_FALSE(list->deleteObject(obj1));
    CUNIT_ASSERT_EQUAL_INT(0, list->length());
    
    // one object
    list->addObject(obj1, false, NULL);
    CUNIT_ASSERT_EQUAL_INT(1, list->length());
    
    CUNIT_ASSERT_FALSE(list->deleteObject(obj2));
    CUNIT_ASSERT_EQUAL_INT(1, list->length());
    CUNIT_ASSERT_NOT_NULL(list->begin());
    CUNIT_ASSERT_NOT_NULL(list->end());
    
    CUNIT_ASSERT_TRUE(list->deleteObject(obj1));
    CUNIT_ASSERT_EQUAL_INT(0, list->length());
    CUNIT_ASSERT_NULL(list->begin());
    CUNIT_ASSERT_NULL(list->end());
    
    delete list;
    delete obj1;
    delete obj2;
}

CUNIT_TEST(MainSuite, EagleLinkedList_deleteObject, 2)
{
    int *obj1 = EagleData::CreateInt(123);
    int *obj2 = EagleData::CreateInt(456);
    int *obj3 = EagleData::CreateInt(789);
    int *obj4 = EagleData::CreateInt(789);
    EagleLinkedList<EagleDataTypeInteger*> *list = new EagleSynchronizedLinkedList<EagleDataTypeInteger*>();
    
    // add objects
    list->addObject(obj1, true, NULL);
    list->addObject(obj2, true, NULL);
    list->addObject(obj3, true, NULL);
    CUNIT_ASSERT_EQUAL_INT(3, list->length());
    CUNIT_ASSERT_EQUAL_PTR(list->first(), obj1);
    CUNIT_ASSERT_EQUAL_PTR(list->last(), obj3);
    
    // object that doesn't exist
    CUNIT_ASSERT_FALSE(list->deleteObject(obj4));
    CUNIT_ASSERT_EQUAL_INT(3, list->length());
    CUNIT_ASSERT_NOT_NULL(list->first());
    CUNIT_ASSERT_NOT_NULL(list->last());
    
    // first object
    CUNIT_ASSERT_TRUE(list->deleteObject(obj1));
    CUNIT_ASSERT_EQUAL_INT(2, list->length());
    CUNIT_ASSERT_NOT_NULL(list->first());
    CUNIT_ASSERT_NOT_NULL(list->last());
    if(NULL != list->begin()) {
        CUNIT_ASSERT_EQUAL_PTR(list->first(), obj2);
    }
    if(NULL != list->end()) {
        CUNIT_ASSERT_EQUAL_PTR(list->last(), obj3);
    }
    
    delete list;
    delete obj4;
}

CUNIT_TEST(MainSuite, EagleLinkedList_deleteObject, 3)
{
    int *obj1 = EagleData::CreateInt(123);
    int *obj2 = EagleData::CreateInt(456);
    int *obj3 = EagleData::CreateInt(789);
    EagleLinkedList<EagleDataTypeInteger*> *list = new EagleSynchronizedLinkedList<EagleDataTypeInteger*>();
    
    // add objects
    list->addObject(obj1, true, NULL);
    list->addObject(obj2, true, NULL);
    list->addObject(obj3, true, NULL);
    CUNIT_ASSERT_EQUAL_INT(3, list->length());
    CUNIT_ASSERT_EQUAL_PTR(list->first(), obj1);
    CUNIT_ASSERT_EQUAL_PTR(list->last(), obj3);
    
    // middle object
    CUNIT_ASSERT_TRUE(list->deleteObject(obj2));
    CUNIT_ASSERT_EQUAL_INT(2, list->length());
    CUNIT_ASSERT_NOT_NULL(list->begin());
    CUNIT_ASSERT_NOT_NULL(list->end());
    if(NULL != list->begin()) {
        CUNIT_ASSERT_EQUAL_PTR(list->first(), obj1);
    }
    if(NULL != list->end()) {
        CUNIT_ASSERT_EQUAL_PTR(list->last(), obj3);
    }
    
    delete list;
}

CUNIT_TEST(MainSuite, EagleLinkedList_deleteObject, 4)
{
    int *obj1 = EagleData::CreateInt(123);
    int *obj2 = EagleData::CreateInt(456);
    int *obj3 = EagleData::CreateInt(789);
    EagleLinkedList<EagleDataTypeInteger*> *list = new EagleSynchronizedLinkedList<EagleDataTypeInteger*>();
    
    // add objects
    list->addObject(obj1, true, NULL);
    list->addObject(obj2, true, NULL);
    list->addObject(obj3, true, NULL);
    CUNIT_ASSERT_EQUAL_INT(3, list->length());
    CUNIT_ASSERT_EQUAL_PTR(list->first(), obj1);
    CUNIT_ASSERT_EQUAL_PTR(list->last(), obj3);
    
    // last object
    CUNIT_ASSERT_TRUE(list->deleteObject(obj3));
    CUNIT_ASSERT_EQUAL_INT(2, list->length());
    CUNIT_ASSERT_NOT_NULL(list->begin());
    CUNIT_ASSERT_NOT_NULL(list->end());
    if(NULL != list->begin()) {
        CUNIT_ASSERT_EQUAL_PTR(list->first(), obj1);
    }
    if(NULL != list->end()) {
        CUNIT_ASSERT_EQUAL_PTR(list->last(), obj2);
    }
    
    delete list;
}

CUNIT_TEST(MainSuite, eagle_SinglePageProvider_nextPage, 3)
{
    int recordsPerPage = 5;
    eagle::SinglePageProvider *epp = eagle::SinglePageProvider::NewFloat(123.0, recordsPerPage, "name");
    
    EaglePage *page = epp->nextPage();
    CUNIT_ASSERT_NOT_NULL(page);
    if(NULL != page) {
        CUNIT_VERIFY_EQUAL_INT(page->count, recordsPerPage);
    }
    delete page;
    
    delete epp;
}

CUNIT_TEST(MainSuite, EagleWorker, runJobLiteral1)
{
    // redirect the errors to nowhere
    EagleLogger::Get()->disableOutHandle();
    
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    
    eagle_plan_OperationAccess *op = static_cast<eagle_plan_OperationAccess*>(eagle::plan::Operation::NewWithLiteral(EaglePageOperations_AdditionInt, 10, 10, NULL, false, "1"));
    selectStage->addOperation(op, false);
    selectStage->prepareBuffers(1);
    
    eagle::plan::job::SelectJob *job = new eagle::plan::job::SelectJob(selectStage);
    
    job->runJob();
    CUNIT_ASSERT_NOT_NULL(EagleLogger::LastEvent);
    CUNIT_VERIFY_EQUAL_STRING(EagleLogger::LastEvent()->getMessage().c_str(), "destination 10 is greater than allowed 1 buffers!");
    
    op->setDestination(0);
    job->runJob();
    CUNIT_ASSERT_NOT_NULL(EagleLogger::LastEvent);
    CUNIT_VERIFY_EQUAL_STRING(EagleLogger::LastEvent()->getMessage().c_str(), "source1 10 is greater than allowed 1 buffers!");
    
    delete op;
    delete job;
    delete selectStage;
}

CUNIT_TEST(MainSuite, EagleWorker, runJobLiteralInteger)
{
    // redirect the errors to nowhere
    EagleLogger::Get()->disableOutHandle();
    
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    
    eagle_plan_OperationAccess *op = static_cast<eagle_plan_OperationAccess*>(eagle::plan::Operation::NewWithLiteral(EaglePageOperations_AdditionInt, 0, 1, NULL, false, "1"));
    selectStage->addOperation(op, false);
    selectStage->prepareBuffers(2);
    
    selectStage->bufferTypes[0] = EagleData::Type::Integer;
    selectStage->bufferTypes[1] = EagleData::Type::Integer;
    
    eagle::plan::job::SelectJob *job = new eagle::plan::job::SelectJob(selectStage);
    
    op->setObject(EagleDbSqlValue_NewWithInteger(123));
    job->runJob();
    EagleDbSqlValue_Delete((EagleDbSqlValue*) op->getObject());
        
    delete job;
    delete op;
    delete selectStage;
}

CUNIT_TEST(MainSuite, EagleWorker, runJobLiteralFloat)
{
    // redirect the errors to nowhere
    EagleLogger::Get()->disableOutHandle();
    
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    
    eagle_plan_OperationAccess *op = static_cast<eagle_plan_OperationAccess*>(eagle::plan::Operation::NewWithLiteral(EaglePageOperations_AdditionInt, 0, 1, NULL, false, "1"));
    selectStage->addOperation(op, false);
    selectStage->prepareBuffers(2);
    
    selectStage->bufferTypes[0] = EagleData::Type::Integer;
    selectStage->bufferTypes[1] = EagleData::Type::Integer;
    
    eagle::plan::job::SelectJob *job = new eagle::plan::job::SelectJob(selectStage);
    
    op->setObject(EagleDbSqlValue_NewWithFloat(123.456));
    job->runJob();
    EagleDbSqlValue_Delete((EagleDbSqlValue*) op->getObject());
    
    delete job;
    delete op;
    delete selectStage;
}

CUNIT_TEST(MainSuite, EagleWorker, runJobLiteralLiteral1)
{
    // redirect the errors to nowhere
    EagleLogger::Get()->disableOutHandle();
    
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    
    eagle_plan_OperationAccess *op = static_cast<eagle_plan_OperationAccess*>(eagle::plan::Operation::NewWithLiteral(EaglePageOperations_AdditionInt, 0, 1, NULL, false, "1"));
    selectStage->addOperation(op, false);
    selectStage->prepareBuffers(2);
    
    selectStage->bufferTypes[0] = EagleData::Type::Integer;
    selectStage->bufferTypes[1] = EagleData::Type::Integer;
    
    eagle::plan::job::SelectJob *job = new eagle::plan::job::SelectJob(selectStage);
    
    op->setObject(EagleDbSqlValue_NewWithString("abc", false));
    job->runJob();
    EagleDbSqlValue_Delete((EagleDbSqlValue*) op->getObject());
    
    delete job;
    delete op;
    delete selectStage;
}

CUNIT_TEST(MainSuite, EagleWorker, runJobLiteralLiteral2)
{
    // redirect the errors to nowhere
    EagleLogger::Get()->disableOutHandle();
    
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    
    eagle_plan_OperationAccess *op = static_cast<eagle_plan_OperationAccess*>(eagle::plan::Operation::NewWithLiteral(EaglePageOperations_AdditionInt, 0, 1, NULL, false, "1"));
    selectStage->addOperation(op, false);
    selectStage->prepareBuffers(2);
    
    selectStage->bufferTypes[0] = EagleData::Type::Integer;
    selectStage->bufferTypes[1] = EagleData::Type::Integer;
    
    eagle::plan::job::SelectJob *job = new eagle::plan::job::SelectJob(selectStage);
    
    op->setObject(EagleDbSqlValue_NewWithAsterisk());
    job->runJob();
    EagleDbSqlValue_Delete((EagleDbSqlValue*) op->getObject());
    
    delete job;
    delete op;
    delete selectStage;
}

CUNIT_TEST(MainSuite, eagle_PlanBufferProvider, New)
{
    EagleDbSqlValue *value = EagleDbSqlValue_NewWithFloat(123.456);
    eagle::PlanBufferProvider *provider = new eagle::PlanBufferProvider(1, value);
    
    CUNIT_ASSERT_NOT_NULL(provider);
    if(NULL != provider) {
        CUNIT_VERIFY_EQUAL_INT(provider->type, eagle::PlanBufferProvider::Type::ValueType);
        CUNIT_VERIFY_EQUAL_INT(provider->destinationBuffer, 1);
    }
    
    EagleDbSqlValue_Delete(value);
    delete provider;
}

CUNIT_TEST(MainSuite, eagle_PlanBufferProvider_toString, 2)
{
    EagleDbSqlValue *value = EagleDbSqlValue_NewWithFloat(123.456);
    eagle::PlanBufferProvider *bp = new eagle::PlanBufferProvider(789, value);
    
    char *description = bp->toString();
    CUNIT_ASSERT_EQUAL_STRING(description, "destination = 789, type = FLOAT");
    
    EagleDbSqlValue_Delete(value);
    delete description;
    delete bp;
}

CUNIT_TEST(MainSuite, EaglePageOperations_SendPageToProviderFloat_, Some)
{
    int recordsPerPage = 1;
    EaglePage *source1 = EaglePage::AllocInt(recordsPerPage);
    EaglePage *source2 = EaglePage::AllocFloat(recordsPerPage);
    eagle::PageProvider *provider = (eagle::PageProvider*) new eagle::StreamPageProvider(EagleData::Type::Float, recordsPerPage, "dummy");
    
    EagleDataTypeInteger *source1data = (EagleDataTypeInteger*) source1->data;
    source1data[0] = 1;
    CUNIT_VERIFY_EQUAL_INT(provider->pagesRemaining(), 0);
    EaglePageOperations_SendPageToProviderFloat_(provider, source1, source2);
    CUNIT_VERIFY_EQUAL_INT(provider->pagesRemaining(), 1);
    
    delete source1;
    delete source2;
    delete provider;
}

CUNIT_TEST(MainSuite, eagle_SinglePageProvider_getPage)
{
    int recordsPerPage = 5;
    eagle::SinglePageProvider *epp = eagle::SinglePageProvider::NewVarchar("some value", recordsPerPage, "name");
    
    EaglePage *page1 = epp->nextPage();
    EaglePage *page2 = epp->getPage(0);
    CUNIT_ASSERT_NOT_NULL(page1);
    CUNIT_ASSERT_NOT_NULL(page2);
    
    if(NULL != page1) {
        CUNIT_VERIFY_EQUAL_INT(page1->count, recordsPerPage);
    }
    if(NULL != page2) {
        CUNIT_VERIFY_EQUAL_INT(page2->count, recordsPerPage);
    }
    
    delete page1;
    delete page2;
    
    delete epp;
}

CUNIT_TEST(MainSuite, EaglePageProvider_getPage, Array)
{
    eagle::PageProvider *provider = (eagle::PageProvider*) new eagle::ArrayPageProvider(EagleData::Type::Integer, NULL,
                                                                                        0, 1, "name", false);
    EaglePage *page = provider->getPage(0);
    CUNIT_VERIFY_NULL(page);
    delete provider;
}

CUNIT_TEST(MainSuite, EaglePageProvider_getPage, Single)
{
    eagle::PageProvider *provider = (eagle::PageProvider*) eagle::SinglePageProvider::NewInt(0, 1, "name");
    EaglePage *page = provider->getPage(0);
    CUNIT_VERIFY_NOT_NULL(page);
    delete page;
    delete provider;
}

CUNIT_TEST(MainSuite, EaglePageProvider_getPage, Stream)
{
    eagle::PageProvider *provider = (eagle::PageProvider*) new eagle::StreamPageProvider(EagleData::Type::Integer, 1, "name");
    EaglePage *page = provider->getPage(0);
    CUNIT_VERIFY_NULL(page);
    delete provider;
}

CUNIT_TEST(MainSuite, eagle_Instance_nextJob, 2)
{
    int cores = 1;
    EagleDbSqlValue *value = EagleDbSqlValue_NewWithInteger(123);
    eagle::Plan *plan = new eagle::Plan();
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(plan, 1, cores);
    eagle::PlanBufferProvider *bp = new eagle::PlanBufferProvider(0, value);
    selectStage->addBufferProvider(bp, false);
    
    eagle::Instance *instance = new eagle::Instance(cores);
    instance->addPlan(plan);
    eagle::plan::job::AbstractJob *job = instance->nextJob();
    CUNIT_VERIFY_NULL(job);
    
    delete instance;
    delete plan;
    delete bp;
    delete job;
    delete selectStage;
    EagleDbSqlValue_Delete(value);
}

CUNIT_TEST(MainSuite, eagle_ArrayPageProvider_nextPage, Float)
{
    EagleDataTypeFloat *data = EagleData::CreateFloat(123.456);
    eagle::ArrayPageProvider *epp = new eagle::ArrayPageProvider(EagleData::Type::Float, data, 1, 1, "name", false);
    EaglePage *page = epp->nextPage();
    CUNIT_VERIFY_NOT_NULL(page);
    delete page;
    delete epp;
    delete data;
}

CUNIT_TEST(MainSuite, eagle_ArrayPageProvider_nextPage, Varchar)
{
    EagleDataTypeVarchar *data = (EagleDataTypeVarchar*) malloc(sizeof(EagleDataTypeVarchar));
    data[0] = "whatever";
    eagle::ArrayPageProvider *epp = new eagle::ArrayPageProvider(EagleData::Type::Varchar, (void*) data, 1, 1, "name",
                                                                 false);
    EaglePage *page = epp->nextPage();
    CUNIT_VERIFY_NOT_NULL(page);
    delete page;
    delete epp;
    delete data;
}

CUNIT_TEST(MainSuite, eagle_ArrayPageProvider_nextPage, Unknown)
{
    eagle::ArrayPageProvider *epp = new eagle::ArrayPageProvider(EagleData::Type::Unknown, NULL, 1, 1, "name", false);
    CUNIT_VERIFY_NULL(epp->nextPage());
    delete epp;
}

CUNIT_TEST(MainSuite, eagle_ArrayPageProvider_getPage, Integer)
{
    EagleDataTypeInteger *data = EagleData::CreateInt(123);
    eagle::ArrayPageProvider *epp = new eagle::ArrayPageProvider(EagleData::Type::Integer, data, 1, 1, "name", false);
    EaglePage *page = epp->getPage(0);
    CUNIT_VERIFY_NOT_NULL(page);
    delete page;
    delete epp;
    delete data;
}

CUNIT_TEST(MainSuite, eagle_ArrayPageProvider_getPage, Float)
{
    EagleDataTypeFloat *data = EagleData::CreateFloat(123.456);
    eagle::ArrayPageProvider *epp = new eagle::ArrayPageProvider(EagleData::Type::Float, data, 1, 1, "name", false);
    EaglePage *page = epp->getPage(0);
    CUNIT_VERIFY_NOT_NULL(page);
    delete page;
    delete epp;
    delete data;
}

CUNIT_TEST(MainSuite, eagle_ArrayPageProvider_getPage, Varchar)
{
    EagleDataTypeVarchar *data = (EagleDataTypeVarchar*) malloc(sizeof(EagleDataTypeVarchar));
    data[0] = "whatever";
    eagle::ArrayPageProvider *epp = new eagle::ArrayPageProvider(EagleData::Type::Varchar, data, 1, 1, "name", false);
    EaglePage *page = epp->getPage(0);
    CUNIT_VERIFY_NOT_NULL(page);
    delete page;
    delete epp;
    delete data;
}

CUNIT_TEST(MainSuite, eagle_ArrayPageProvider_getPage, Unknown)
{
    eagle::ArrayPageProvider *epp = new eagle::ArrayPageProvider(EagleData::Type::Unknown, NULL, 1, 1, "name", false);
    CUNIT_VERIFY_NULL(epp->getPage(0));
    delete epp;
}

class eagle_TimeMock : public eagle::Calendar
{
    
public:
    
    unsigned long mockAbsoluteTime;
    
    virtual unsigned long getAbsoluteTime()
    {
        if(mockAbsoluteTime > 0) {
            return mockAbsoluteTime;
        }
        return eagle::Calendar::getAbsoluteTime();
    }
    
    static void init()
    {
        reset();
        instance = new eagle_TimeMock();
    }
    
    static void reset()
    {
        if(NULL != instance) {
            delete instance;
            instance = NULL;
        }
    }
    
};

CUNIT_TEST(MainSuite, EaglePlan_getRealExecutionSeconds)
{
    int cores = 1;
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 1, cores);
    
    double time = selectStage->getRealExecutionSeconds();
    CUNIT_VERIFY_EQUAL_DOUBLE(0.0, time);
    
    // setup a mock Time instance
    eagle_TimeMock::init();
    
    selectStage->startTime = 1000000;
    ((eagle_TimeMock*) eagle::Calendar::getInstance())->mockAbsoluteTime = 2000000;
    time = selectStage->getRealExecutionSeconds();
    CUNIT_VERIFY_EQUAL_DOUBLE(0.001, time);
    
    // undo
    eagle_TimeMock::reset();
    
    delete selectStage;
}

CUNIT_TEST(MainSuite, eagle_SinglePageProvider_isRandomAccess)
{
    eagle::PageProvider *provider = eagle::SinglePageProvider::NewInt(123, 1, "dummy");
    CUNIT_VERIFY_TRUE(provider->isRandomAccess());
    delete provider;
}

CUNIT_TEST(MainSuite, eagle_StreamPageProvider_isRandomAccess)
{
    eagle::PageProvider *provider = new eagle::StreamPageProvider(EagleData::Type::Integer, 1, "dummy");
    CUNIT_VERIFY_TRUE(provider->isRandomAccess());
    delete provider;
}

CUNIT_TEST(MainSuite, eagle_ArrayPageProvider_isRandomAccess)
{
    eagle::PageProvider *provider = new eagle::ArrayPageProvider(EagleData::Type::Integer, NULL, 0, 1, "dummy", false);
    CUNIT_VERIFY_TRUE(provider->isRandomAccess());
    delete provider;
}

CUNIT_TEST(MainSuite, EaglePage, RealCopyInt)
{
    EagleDataTypeInteger *data = (EagleDataTypeInteger*) calloc(sizeof(EagleDataTypeInteger), 3);
    data[0] = 123;
    data[1] = 456;
    data[2] = 789;
    
    EaglePage *page = new EaglePage(EagleData::Type::Integer, data, 3, 3, 0, true);
    
    EaglePage *page2 = EaglePage::RealCopy(page);
    CUNIT_ASSERT_TRUE(page->equals(page2));
    delete page2;
    
    delete page;
}

CUNIT_TEST(MainSuite, EaglePage, RealCopyFloat)
{
    EagleDataTypeFloat *data = (EagleDataTypeFloat*) calloc(sizeof(EagleDataTypeFloat), 3);
    data[0] = 123.0;
    data[1] = 456.0;
    data[2] = 789.0;
    
    EaglePage *page = new EaglePage(EagleData::Type::Float, data, 3, 3, 0, true);
    
    EaglePage *page2 = EaglePage::RealCopy(page);
    CUNIT_ASSERT_TRUE(page->equals(page2));
    delete page2;
    
    delete page;
}

CUNIT_TEST(MainSuite, EaglePage, RealCopyVarchar)
{
    EagleDataTypeVarchar *data = (EagleDataTypeVarchar*) calloc(sizeof(EagleDataTypeVarchar), 3);
    data[0] = strdup("123");
    data[1] = strdup("456");
    data[2] = strdup("789");
    
    EaglePage *page = new EaglePage(EagleData::Type::Varchar, data, 3, 3, 0, true);
    
    EaglePage *page2 = EaglePage::RealCopy(page);
    CUNIT_ASSERT_TRUE(page->equals(page2));
    delete page2;
    
    delete page;
}

CUNIT_TEST(MainSuite, EaglePage_equals, pointersEqual)
{
    EaglePage *page1 = new EaglePage(EagleData::Type::Integer, NULL, 0, 0, 0, false);
    CUNIT_ASSERT_TRUE(page1->equals(page1));
    delete page1;
}

CUNIT_TEST(MainSuite, EaglePage_equals, count)
{
    EaglePage *page1 = new EaglePage(EagleData::Type::Integer, NULL, 0, 1, 0, false);
    EaglePage *page2 = new EaglePage(EagleData::Type::Integer, NULL, 0, 2, 0, false);
    CUNIT_ASSERT_FALSE(page1->equals(page2));
    delete page1;
    delete page2;
}

CUNIT_TEST(MainSuite, EaglePage_equals, totalSize)
{
    EaglePage *page1 = new EaglePage(EagleData::Type::Integer, NULL, 1, 0, 0, false);
    EaglePage *page2 = new EaglePage(EagleData::Type::Integer, NULL, 2, 0, 0, false);
    CUNIT_ASSERT_FALSE(page1->equals(page2));
    delete page1;
    delete page2;
}

CUNIT_TEST(MainSuite, EaglePage_equals, type)
{
    EaglePage *page1 = new EaglePage(EagleData::Type::Float, NULL, 0, 0, 0, false);
    EaglePage *page2 = new EaglePage(EagleData::Type::Integer, NULL, 0, 0, 0, false);
    CUNIT_ASSERT_FALSE(page1->equals(page2));
    delete page1;
    delete page2;
}

CUNIT_TEST(MainSuite, EaglePage_equals, typeUnknown)
{
    int d1 = 123, d2 = 456;
    EaglePage *page1 = new EaglePage(EagleData::Type::Unknown, &d1, 0, 0, 0, false);
    EaglePage *page2 = new EaglePage(EagleData::Type::Unknown, &d2, 0, 0, 0, false);
    CUNIT_ASSERT_FALSE(page1->equals(page2));
    delete page1;
    delete page2;
}

CUNIT_TEST(MainSuite, EaglePage_equals, typeInteger)
{
    EagleDataTypeInteger d1 = 123, d2 = 456;
    EaglePage *page1 = new EaglePage(EagleData::Type::Integer, &d1, 1, 1, 0, false);
    EaglePage *page2 = new EaglePage(EagleData::Type::Integer, &d2, 1, 1, 0, false);
    CUNIT_ASSERT_FALSE(page1->equals(page2));
    delete page1;
    delete page2;
}

CUNIT_TEST(MainSuite, EaglePage_equals, typeFloat)
{
    EagleDataTypeFloat d1 = 123.0, d2 = 456.0;
    EaglePage *page1 = new EaglePage(EagleData::Type::Float, &d1, 1, 1, 0, false);
    EaglePage *page2 = new EaglePage(EagleData::Type::Float, &d2, 1, 1, 0, false);
    CUNIT_ASSERT_FALSE(page1->equals(page2));
    delete page1;
    delete page2;
}

CUNIT_TEST(MainSuite, EaglePage_equals, typeVarchar)
{
    EagleDataTypeVarchar d1 = "abc", d2 = "123";
    EaglePage *page1 = new EaglePage(EagleData::Type::Varchar, &d1, 1, 1, 0, false);
    EaglePage *page2 = new EaglePage(EagleData::Type::Varchar, &d2, 1, 1, 0, false);
    CUNIT_ASSERT_FALSE(page1->equals(page2));
    delete page1;
    delete page2;
}

CUNIT_TEST(MainSuite, EaglePage, RealCopyUnknown)
{
    EaglePage *page = new EaglePage(EagleData::Type::Unknown, NULL, 0, 0, 0, false);
    CUNIT_VERIFY_NULL(EaglePage::RealCopy(page));
    delete page;
}

CUNIT_TEST(MainSuite, EaglePage, RealCopyNull)
{
    CUNIT_VERIFY_NULL(EaglePage::RealCopy(NULL));
}

CUNIT_TEST(MainSuite, EagleLinkedList_add, ReadWrite)
{
    EagleLinkedList<EagleDataTypeInteger*> *list = new EagleSynchronizedLinkedList<EagleDataTypeInteger*>();
    
    list->add(new EagleLinkedList<EagleDataTypeInteger*>::Item(NULL, false, NULL));
    CUNIT_VERIFY_EQUAL_INT(list->length(), 1);
    
    delete list;
}

CUNIT_TEST(MainSuite, EagleLinkedList_add, Never)
{
    EagleLinkedList<EagleDataTypeInteger*> *list = new EagleUnsynchronizedLinkedList<EagleDataTypeInteger*>();
    
    list->add(new EagleLinkedList<EagleDataTypeInteger*>::Item(NULL, false, NULL));
    CUNIT_VERIFY_EQUAL_INT(list->length(), 1);
    
    delete list;
}

CUNIT_TEST(MainSuite, EagleLinkedList_length, ReadWrite)
{
    EagleLinkedList<EagleDataTypeInteger*> *list = new EagleSynchronizedLinkedList<EagleDataTypeInteger*>();
    
    list->add(new EagleLinkedList<EagleDataTypeInteger*>::Item(NULL, false, NULL));
    CUNIT_VERIFY_EQUAL_INT(list->length(), 1);
    
    delete list;
}

CUNIT_TEST(MainSuite, EagleLinkedList_begin, ReadWrite)
{
    EagleLinkedList<EagleDataTypeInteger*> *list = new EagleSynchronizedLinkedList<EagleDataTypeInteger*>();
    EagleLinkedList<EagleDataTypeInteger*>::Item *item = new EagleLinkedList<EagleDataTypeInteger*>::Item(NULL, false, NULL);
    
    list->add(item);
    CUNIT_VERIFY_EQUAL_PTR(list->begin(), item);
    
    delete list;
}

CUNIT_TEST(MainSuite, EagleLinkedList_end, ReadWrite)
{
    EagleLinkedList<EagleDataTypeInteger*> *list = new EagleSynchronizedLinkedList<EagleDataTypeInteger*>();
    EagleLinkedList<EagleDataTypeInteger*>::Item *item = new EagleLinkedList<EagleDataTypeInteger*>::Item(NULL, false, NULL);
    
    list->add(item);
    CUNIT_VERIFY_EQUAL_PTR(list->end(), item);
    
    delete list;
}

CUNIT_TEST(MainSuite, EagleLinkedList_pop, Never)
{
    EagleLinkedList<EagleDataTypeInteger*> *list = new EagleUnsynchronizedLinkedList<EagleDataTypeInteger*>();
    EagleLinkedList<EagleDataTypeInteger*>::Item *item = new EagleLinkedList<EagleDataTypeInteger*>::Item(NULL, false, NULL);
    
    list->add(item);
    CUNIT_VERIFY_EQUAL_PTR(list->pop(), item);
    
    delete item;
    delete list;
}

CUNIT_TEST(MainSuite, EagleLinkedList_pop, ReadWrite)
{
    EagleLinkedList<EagleDataTypeInteger*> *list = new EagleSynchronizedLinkedList<EagleDataTypeInteger*>();
    EagleLinkedList<EagleDataTypeInteger*>::Item *item = new EagleLinkedList<EagleDataTypeInteger*>::Item(NULL, false, NULL);
    
    list->add(item);
    CUNIT_VERIFY_EQUAL_PTR(list->pop(), item);
    
    delete item;
    delete list;
}

CUNIT_TEST(MainSuite, EagleLinkedList_isEmpty, ReadWrite)
{
    EagleLinkedList<EagleDataTypeInteger*> *list = new EagleSynchronizedLinkedList<EagleDataTypeInteger*>();
    EagleLinkedList<EagleDataTypeInteger*>::Item *item = new EagleLinkedList<EagleDataTypeInteger*>::Item(NULL, false, NULL);
    
    list->add(item);
    CUNIT_VERIFY_FALSE(list->isEmpty());
    
    delete list;
}

CUNIT_TEST(MainSuite, EagleLinkedList_calculateIndexSize_)
{
    int ms = EagleLinkedList<void*>::MinimumSizeForIndex;
    CUNIT_VERIFY_EQUAL_INT(EagleLinkedList<void*>::CalculateIndexSize(0), ms);
    CUNIT_VERIFY_EQUAL_INT(EagleLinkedList<void*>::CalculateIndexSize(1), ms);
    CUNIT_VERIFY_EQUAL_INT(EagleLinkedList<void*>::CalculateIndexSize(ms + 1), ms * 2);
    CUNIT_VERIFY_EQUAL_INT(EagleLinkedList<void*>::CalculateIndexSize(ms * 2), ms * 4);
}

CUNIT_TEST(MainSuite, EagleLinkedList_add, index)
{
    EagleLinkedListMock list;
    EagleDataTypeInteger **d = (EagleDataTypeInteger**) calloc(sizeof(EagleDataTypeInteger*),
                                                               EagleLinkedList<void*>::MinimumSizeForIndex * 3);
    int i;
    
    // some items
    for(i = 0; i < EagleLinkedList<void*>::MinimumSizeForIndex - 1; ++i) {
        d[i] = EagleData::CreateInt(i);
        list.addObject(d[i], true, NULL);
        list[i];
    }
    
    // make sure no index yet
    CUNIT_VERIFY_EQUAL_INT(list.length(), EagleLinkedList<void*>::MinimumSizeForIndex - 1);
    CUNIT_VERIFY_EQUAL_INT(list._getIndexSize(), 0);
    
    // add one more item to activate the index
    d[i] = EagleData::CreateInt(i);
    list.addObject(d[i], true, NULL);
    list[EagleLinkedList<void*>::MinimumSizeForIndex - 1];
    ++i;
    
    CUNIT_VERIFY_EQUAL_INT(list.length(), EagleLinkedList<void*>::MinimumSizeForIndex);
    CUNIT_VERIFY_EQUAL_INT(list._getIndexSize(), EagleLinkedList<void*>::MinimumSizeForIndex * 2);
    
    // verify index
    for(int j = 0; j < EagleLinkedList<void*>::MinimumSizeForIndex; ++j) {
        EagleDataTypeInteger g = *((EagleDataTypeInteger*) list[j]);
        if(j != g) {
            CUNIT_ASSERT_EQUAL_INT(g, j);
        }
        
        if(list._getIndex()[j]->getObject() != d[j]) {
            CUNIT_ASSERT_EQUAL_PTR(list._getIndex()[j]->getObject(), d[j]);
        }
    }
    
    // add more items to see if the index grows
    for(; i < EagleLinkedList<void*>::MinimumSizeForIndex * 2; ++i) {
        d[i] = EagleData::CreateInt(i);
        list.addObject(d[i], true, NULL);
        list[i];
    }
    
    CUNIT_VERIFY_EQUAL_INT(list.length(), EagleLinkedList<void*>::MinimumSizeForIndex * 2);
    CUNIT_VERIFY_EQUAL_INT(list._getIndexSize(), EagleLinkedList<void*>::MinimumSizeForIndex * 4);
    
    delete d;
}

CUNIT_TEST(MainSuite, eagle_Plan, NextJobSync)
{
    eagle::plan::stage::SelectStage *selectStage = new eagle::plan::stage::SelectStage(NULL, 1, 1);
    eagle::plan::job::AbstractJob *job = new eagle::plan::job::SelectJob(selectStage);
    eagle::PlanBufferProvider *bp = new eagle::PlanBufferProvider(1000, NULL);
    
    selectStage->addBufferProvider(bp, false);
    EagleSelectStageMock::_NextJobSync(&job);
    
    delete bp;
    delete job;
    delete selectStage;
}

CUNIT_TEST(MainSuite, eagle_Time, getAbsoluteTime)
{
    // make sure that eagle::Calendar::getInstance()->getAbsoluteTime returns something close to what we expect
    long start = eagle::Calendar::getInstance()->getAbsoluteTime();
    sleep(1);
    long elapsed = eagle::Calendar::getInstance()->getAbsoluteTime() - start;
    
    CUNIT_ASSERT_BETWEEN_LONG(elapsed, eagle::Calendar::TicksPerSecond, eagle::Calendar::TicksPerSecond * 2);
    CUNIT_ASSERT_BETWEEN_LONG(elapsed, 1000000000L, 2000000000L);
}

CUnitTests* MainSuite2_tests()
{
    CUnitTests *tests = CUnitTests_New(100);
    
    // method tests
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_Time, getAbsoluteTime));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_Plan, NextJobSync));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLinkedList_add, index));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLinkedList_calculateIndexSize_));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLinkedList_isEmpty, ReadWrite));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLinkedList_pop, ReadWrite));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLinkedList_pop, Never));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLinkedList_end, ReadWrite));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLinkedList_begin, ReadWrite));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLinkedList_length, ReadWrite));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLinkedList_add, Never));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLinkedList_add, ReadWrite));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EaglePage, RealCopyNull));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EaglePage, RealCopyUnknown));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EaglePage_equals, typeVarchar));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EaglePage_equals, typeFloat));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EaglePage_equals, typeInteger));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EaglePage_equals, typeUnknown));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EaglePage_equals, type));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EaglePage_equals, count));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EaglePage_equals, totalSize));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EaglePage_equals, pointersEqual));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EaglePage, RealCopyVarchar));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EaglePage, RealCopyFloat));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EaglePage, RealCopyInt));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_ArrayPageProvider_isRandomAccess));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_StreamPageProvider_isRandomAccess));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_SinglePageProvider_isRandomAccess));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EaglePlan_getRealExecutionSeconds));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_ArrayPageProvider_getPage, Unknown));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_ArrayPageProvider_getPage, Varchar));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_ArrayPageProvider_getPage, Float));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_ArrayPageProvider_getPage, Integer));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_ArrayPageProvider_nextPage, Unknown));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_ArrayPageProvider_nextPage, Varchar));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_ArrayPageProvider_nextPage, Float));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EaglePageProvider_getPage, Stream));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EaglePageProvider_getPage, Single));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EaglePageProvider_getPage, Array));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_SinglePageProvider_getPage));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EaglePageOperations_SendPageToProviderFloat_, Some));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_PlanBufferProvider_toString, 2));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_PlanBufferProvider, New));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleWorker, runJobLiteralInteger));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleWorker, runJobLiteralFloat));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleWorker, runJobLiteralLiteral1));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleWorker, runJobLiteralLiteral2));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleWorker, runJobLiteral1));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_SinglePageProvider_nextPage, 3));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_SinglePageProvider_nextPage, 2));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_SinglePageProvider_nextPage, 1));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_ArrayPageProvider_nextPage, Int));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EaglePageProvider_reset));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLinkedList_deleteObject, 4));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLinkedList_deleteObject, 3));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLinkedList_deleteObject, 2));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, EagleLinkedList_deleteObject, 1));
    CUnitTests_addTest(tests, CUNIT_NEW(MainSuite, eagle_Instance_nextJob, 2));
    
    return tests;
}
