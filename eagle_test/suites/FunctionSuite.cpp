#include "FunctionSuite.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "eagle/EaglePageOperations.h"
#include "eagle/algorithm.h"

CUNIT_TEST(FunctionSuite, EaglePageOperations_CastPageIntFloat)
{
    int pageSize = 1000;
    EaglePage *page = OperatorSuite_GeneratePageInt(pageSize, 100);
    EaglePage *out = EaglePage::AllocFloat(pageSize);
    
    EaglePageOperations_CastPageIntFloat(out, page, NULL, NULL);
    
    int valid = 1;
    for(int i = 0; i < pageSize; ++i) {
        if((((EagleDataTypeInteger*) page->data)[i] != ((EagleDataTypeFloat*) out->data)[i])) {
            valid = 0;
            break;
        }
    }
    CUNIT_ASSERT_EQUAL_INT(valid, 1);
    
    delete page;
    delete out;
}

CUNIT_TEST(FunctionSuite, EaglePageOperations_LowerPageVarchar)
{
    int pageSize = 1000;
    EaglePage **args = (EaglePage**) calloc(1, sizeof(EaglePage*));
    args[0] = OperatorSuite_GeneratePageVarchar(pageSize, 0, 32);
    EaglePage *out = EaglePage::AllocVarchar(pageSize);
    
    EaglePageOperations_LowerPageVarchar(out, NULL, NULL, args);
    
    for(int i = 0; i < pageSize; ++i) {
        char *left = eagle::algorithm::toLowerCase(((EagleDataTypeVarchar*) args[0]->data)[i]);
        
        if(0 != strcmp(left, ((EagleDataTypeVarchar*) out->data)[i])) {
            CUNIT_FAIL("'%s' != '%s'", ((EagleDataTypeVarchar*) args[0]->data)[i], ((EagleDataTypeVarchar*) out->data)[i]);
        }
        free(left);
    }
    
    delete args[0];
    free(args);
    delete out;
}

CUNIT_TEST(FunctionSuite, EaglePageOperations_UpperPageVarchar)
{
    int pageSize = 1000;
    EaglePage **args = (EaglePage**) calloc(1, sizeof(EaglePage*));
    args[0] = OperatorSuite_GeneratePageVarchar(pageSize, 0, 32);
    EaglePage *out = EaglePage::AllocVarchar(pageSize);
    
    EaglePageOperations_UpperPageVarchar(out, NULL, NULL, args);
    
    for(int i = 0; i < pageSize; ++i) {
        char *left = eagle::algorithm::toUpperCase(((EagleDataTypeVarchar*) args[0]->data)[i]);
        if(0 != strcmp(left, ((EagleDataTypeVarchar*) out->data)[i])) {
            CUNIT_FAIL("'%s' != '%s'", ((EagleDataTypeVarchar*) args[0]->data)[i], ((EagleDataTypeVarchar*) out->data)[i]);
        }
        free(left);
    }
    
    delete args[0];
    free(args);
    delete out;
}

CUNIT_TEST(FunctionSuite, EaglePageOperations_LnPageFloat)
{
    int pageSize = 1000;
    EaglePage **args = (EaglePage**) calloc(1, sizeof(EaglePage*));
    args[0] = OperatorSuite_GeneratePageFloat(pageSize);
    EaglePage *out = EaglePage::AllocFloat(pageSize);
    
    EaglePageOperations_LnPageFloat(out, NULL, NULL, args);
    
    for(int i = 0; i < pageSize; ++i) {
        if(log((((EagleDataTypeFloat*) args[0]->data)[i])) != ((EagleDataTypeFloat*) out->data)[i]) {
            CUNIT_VERIFY_EQUAL_DOUBLE(log(((EagleDataTypeFloat*) args[0]->data)[i]), ((EagleDataTypeFloat*) out->data)[i]);
        }
    }
    
    delete args[0];
    free(args);
    delete out;
}

CUNIT_TEST(FunctionSuite, EaglePageOperations_AbsPageFloat)
{
    int pageSize = 6;
    EaglePage **args = (EaglePage**) calloc(1, sizeof(EaglePage*));
    args[0] = EaglePage::AllocFloat(pageSize);
    
    // test data
    ((EagleDataTypeFloat*) args[0]->data)[0] = 0;
    ((EagleDataTypeFloat*) args[0]->data)[1] = 1.34;
    ((EagleDataTypeFloat*) args[0]->data)[2] = -1.34;
    ((EagleDataTypeFloat*) args[0]->data)[3] = INFINITY;
    ((EagleDataTypeFloat*) args[0]->data)[4] = -INFINITY;
    ((EagleDataTypeFloat*) args[0]->data)[5] = NAN;
    
    // process
    EaglePage *out = EaglePage::AllocFloat(pageSize);
    EaglePageOperations_AbsPageFloat(out, NULL, NULL, args);
    
    // result
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[0], 0);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[1], 1.34);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[2], 1.34);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[3], INFINITY);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[4], INFINITY);
    CUNIT_VERIFY_EQUAL_INT(isnan(((EagleDataTypeFloat*) out->data)[5]), 1);
    
    delete args[0];
    free(args);
    delete out;
}

CUNIT_TEST(FunctionSuite, EaglePageOperations_Atan2PageFloatPageFloat)
{
    int pageSize = 6;
    EaglePage **args = (EaglePage**) calloc(2, sizeof(EaglePage*));
    args[0] = EaglePage::AllocFloat(pageSize);
    args[1] = EaglePage::AllocFloat(pageSize);
    
    // test data
    ((EagleDataTypeFloat*) args[0]->data)[0] = 0;
    ((EagleDataTypeFloat*) args[0]->data)[1] = 2.34;
    ((EagleDataTypeFloat*) args[0]->data)[2] = -1.34;
    ((EagleDataTypeFloat*) args[0]->data)[3] = INFINITY;
    ((EagleDataTypeFloat*) args[0]->data)[4] = -INFINITY;
    ((EagleDataTypeFloat*) args[0]->data)[5] = NAN;
    ((EagleDataTypeFloat*) args[1]->data)[0] = 0;
    ((EagleDataTypeFloat*) args[1]->data)[1] = 3.34;
    ((EagleDataTypeFloat*) args[1]->data)[2] = -2.34;
    ((EagleDataTypeFloat*) args[1]->data)[3] = INFINITY;
    ((EagleDataTypeFloat*) args[1]->data)[4] = -INFINITY;
    ((EagleDataTypeFloat*) args[1]->data)[5] = NAN;
    
    // process
    EaglePage *out = EaglePage::AllocFloat(pageSize);
    EaglePageOperations_Atan2PageFloatPageFloat(out, NULL, NULL, args);
    
    // result
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[0], 0);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[1], 0.611127732145815394382282192964);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[2], -2.621526576401329577947763027623);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[3], 0.785398163397448278999490867136);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[4], -2.356194490192344836998472601408);
    CUNIT_VERIFY_EQUAL_INT(isnan(((EagleDataTypeFloat*) out->data)[5]), 1);
    
    delete args[0];
    delete args[1];
    free(args);
    delete out;
}

CUNIT_TEST(FunctionSuite, EaglePageOperations_SqrtPageFloat)
{
    int pageSize = 6;
    EaglePage **args = (EaglePage**) calloc(1, sizeof(EaglePage*));
    args[0] = EaglePage::AllocFloat(pageSize);
    
    // test data
    ((EagleDataTypeFloat*) args[0]->data)[0] = 0;
    ((EagleDataTypeFloat*) args[0]->data)[1] = 1.34;
    ((EagleDataTypeFloat*) args[0]->data)[2] = -1.34;
    ((EagleDataTypeFloat*) args[0]->data)[3] = INFINITY;
    ((EagleDataTypeFloat*) args[0]->data)[4] = -INFINITY;
    ((EagleDataTypeFloat*) args[0]->data)[5] = NAN;
    
    // process
    EaglePage *out = EaglePage::AllocFloat(pageSize);
    EaglePageOperations_SqrtPageFloat(out, NULL, NULL, args);
    
    // result
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[0], 0);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[1], 1.157583690279022636815398072940297);
    CUNIT_VERIFY_EQUAL_INT(isnan(((EagleDataTypeFloat*) out->data)[2]), 1);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[3], INFINITY);
    CUNIT_VERIFY_EQUAL_INT(isnan(((EagleDataTypeFloat*) out->data)[4]), 1);
    CUNIT_VERIFY_EQUAL_INT(isnan(((EagleDataTypeFloat*) out->data)[5]), 1);
    
    delete args[0];
    free(args);
    delete out;
}

CUNIT_TEST(FunctionSuite, EaglePageOperations_FloorPageFloat)
{
    int pageSize = 6;
    EaglePage **args = (EaglePage**) calloc(1, sizeof(EaglePage*));
    args[0] = EaglePage::AllocFloat(pageSize);
    
    // test data
    ((EagleDataTypeFloat*) args[0]->data)[0] = 0;
    ((EagleDataTypeFloat*) args[0]->data)[1] = 1.34;
    ((EagleDataTypeFloat*) args[0]->data)[2] = -1.34;
    ((EagleDataTypeFloat*) args[0]->data)[3] = INFINITY;
    ((EagleDataTypeFloat*) args[0]->data)[4] = -INFINITY;
    ((EagleDataTypeFloat*) args[0]->data)[5] = NAN;
    
    // process
    EaglePage *out = EaglePage::AllocFloat(pageSize);
    EaglePageOperations_FloorPageFloat(out, NULL, NULL, args);
    
    // result
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[0], 0);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[1], 1);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[2], -2);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[3], INFINITY);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[4], -INFINITY);
    CUNIT_VERIFY_EQUAL_INT(isnan(((EagleDataTypeFloat*) out->data)[5]), 1);
    
    delete args[0];
    free(args);
    delete out;
}

CUNIT_TEST(FunctionSuite, EaglePageOperations_CeilPageFloat)
{
    int pageSize = 6;
    EaglePage **args = (EaglePage**) calloc(1, sizeof(EaglePage*));
    args[0] = EaglePage::AllocFloat(pageSize);
    
    // test data
    ((EagleDataTypeFloat*) args[0]->data)[0] = 0;
    ((EagleDataTypeFloat*) args[0]->data)[1] = 1.34;
    ((EagleDataTypeFloat*) args[0]->data)[2] = -1.34;
    ((EagleDataTypeFloat*) args[0]->data)[3] = INFINITY;
    ((EagleDataTypeFloat*) args[0]->data)[4] = -INFINITY;
    ((EagleDataTypeFloat*) args[0]->data)[5] = NAN;
    
    // process
    EaglePage *out = EaglePage::AllocFloat(pageSize);
    EaglePageOperations_CeilPageFloat(out, NULL, NULL, args);
    
    // result
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[0], 0);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[1], 2);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[2], -1);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[3], INFINITY);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[4], -INFINITY);
    CUNIT_VERIFY_EQUAL_INT(isnan(((EagleDataTypeFloat*) out->data)[5]), 1);
    
    delete args[0];
    free(args);
    delete out;
}

CUNIT_TEST(FunctionSuite, EaglePageOperations_ExpPageFloat)
{
    int pageSize = 6;
    EaglePage **args = (EaglePage**) calloc(1, sizeof(EaglePage*));
    args[0] = EaglePage::AllocFloat(pageSize);
    
    // test data
    ((EagleDataTypeFloat*) args[0]->data)[0] = 0;
    ((EagleDataTypeFloat*) args[0]->data)[1] = 1.34;
    ((EagleDataTypeFloat*) args[0]->data)[2] = -1.34;
    ((EagleDataTypeFloat*) args[0]->data)[3] = INFINITY;
    ((EagleDataTypeFloat*) args[0]->data)[4] = -INFINITY;
    ((EagleDataTypeFloat*) args[0]->data)[5] = NAN;
    
    // process
    EaglePage *out = EaglePage::AllocFloat(pageSize);
    EaglePageOperations_ExpPageFloat(out, NULL, NULL, args);
    
    // result
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[0], 1);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[1], 3.819043505366336077599953569006175);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[2], 0.2618456685803259853528857092896942);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[3], INFINITY);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[4], 0);
    CUNIT_VERIFY_EQUAL_INT(isnan(((EagleDataTypeFloat*) out->data)[5]), 1);
    
    delete args[0];
    free(args);
    delete out;
}

CUNIT_TEST(FunctionSuite, EaglePageOperations_PowerPageFloatPageFloat)
{
    int pageSize = 6;
    EaglePage **args = (EaglePage**) calloc(2, sizeof(EaglePage*));
    args[0] = EaglePage::AllocFloat(pageSize);
    args[1] = EaglePage::AllocFloat(pageSize);
    
    // test data
    ((EagleDataTypeFloat*) args[0]->data)[0] = 0;
    ((EagleDataTypeFloat*) args[0]->data)[1] = 2.34;
    ((EagleDataTypeFloat*) args[0]->data)[2] = -1.34;
    ((EagleDataTypeFloat*) args[0]->data)[3] = INFINITY;
    ((EagleDataTypeFloat*) args[0]->data)[4] = -INFINITY;
    ((EagleDataTypeFloat*) args[0]->data)[5] = NAN;
    ((EagleDataTypeFloat*) args[1]->data)[0] = 0;
    ((EagleDataTypeFloat*) args[1]->data)[1] = 3.34;
    ((EagleDataTypeFloat*) args[1]->data)[2] = -2.34;
    ((EagleDataTypeFloat*) args[1]->data)[3] = INFINITY;
    ((EagleDataTypeFloat*) args[1]->data)[4] = -INFINITY;
    ((EagleDataTypeFloat*) args[1]->data)[5] = NAN;
    
    // process
    EaglePage *out = EaglePage::AllocFloat(pageSize);
    EaglePageOperations_PowerPageFloatPageFloat(out, NULL, NULL, args);
    
    // result
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[0], 1);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[1], 17.10728000324111519603320630267262);
    CUNIT_VERIFY_EQUAL_INT(isnan(((EagleDataTypeFloat*) out->data)[2]), 1);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[3], INFINITY);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[4], 0);
    CUNIT_VERIFY_EQUAL_INT(isnan(((EagleDataTypeFloat*) out->data)[5]), 1);
    
    delete args[0];
    delete args[1];
    free(args);
    delete out;
}

CUNIT_TEST(FunctionSuite, EaglePageOperations_PositionPageVarcharPageVarchar)
{
    int pageSize = 4;
    EaglePage **args = (EaglePage**) calloc(2, sizeof(EaglePage*));
    args[0] = EaglePage::AllocVarchar(pageSize);
    args[1] = EaglePage::AllocVarchar(pageSize);
    
    // test data
    ((EagleDataTypeVarchar*) args[0]->data)[0] = strdup("ll");
    ((EagleDataTypeVarchar*) args[1]->data)[0] = strdup("hello");
    ((EagleDataTypeVarchar*) args[0]->data)[1] = strdup("ll");
    ((EagleDataTypeVarchar*) args[1]->data)[1] = strdup("hell");
    ((EagleDataTypeVarchar*) args[0]->data)[2] = strdup("he");
    ((EagleDataTypeVarchar*) args[1]->data)[2] = strdup("hello");
    ((EagleDataTypeVarchar*) args[0]->data)[3] = strdup("he");
    ((EagleDataTypeVarchar*) args[1]->data)[3] = strdup("hallo");
    
    // process
    EaglePage *out = EaglePage::AllocInt(pageSize);
    EaglePageOperations_PositionPageVarcharPageVarchar(out, NULL, NULL, args);
    
    // result
    CUNIT_VERIFY_EQUAL_INT(((EagleDataTypeInteger*) out->data)[0], 2);
    CUNIT_VERIFY_EQUAL_INT(((EagleDataTypeInteger*) out->data)[1], 2);
    CUNIT_VERIFY_EQUAL_INT(((EagleDataTypeInteger*) out->data)[2], 0);
    CUNIT_VERIFY_EQUAL_INT(((EagleDataTypeInteger*) out->data)[3], -1);
    
    delete args[0];
    delete args[1];
    free(args);
    delete out;
}

CUNIT_TEST(FunctionSuite, EaglePageOperations_ModPageFloatPageFloat)
{
    int pageSize = 6;
    EaglePage **args = (EaglePage**) calloc(2, sizeof(EaglePage*));
    args[0] = EaglePage::AllocFloat(pageSize);
    args[1] = EaglePage::AllocFloat(pageSize);
    
    // test data
    ((EagleDataTypeFloat*) args[0]->data)[0] = 0;
    ((EagleDataTypeFloat*) args[0]->data)[1] = 2.34;
    ((EagleDataTypeFloat*) args[0]->data)[2] = -1.34;
    ((EagleDataTypeFloat*) args[0]->data)[3] = INFINITY;
    ((EagleDataTypeFloat*) args[0]->data)[4] = -INFINITY;
    ((EagleDataTypeFloat*) args[0]->data)[5] = NAN;
    ((EagleDataTypeFloat*) args[1]->data)[0] = 0;
    ((EagleDataTypeFloat*) args[1]->data)[1] = 3.34;
    ((EagleDataTypeFloat*) args[1]->data)[2] = -2.34;
    ((EagleDataTypeFloat*) args[1]->data)[3] = INFINITY;
    ((EagleDataTypeFloat*) args[1]->data)[4] = -INFINITY;
    ((EagleDataTypeFloat*) args[1]->data)[5] = NAN;
    
    // process
    EaglePage *out = EaglePage::AllocFloat(pageSize);
    EaglePageOperations_ModPageFloatPageFloat(out, NULL, NULL, args);
    
    // result
    CUNIT_VERIFY_EQUAL_INT(isnan(((EagleDataTypeFloat*) out->data)[0]), 1);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeFloat*) out->data)[1], 2.34);
    CUNIT_VERIFY_EQUAL_INT(isnan(((EagleDataTypeFloat*) out->data)[2]), 0);
    CUNIT_VERIFY_EQUAL_INT(isnan(((EagleDataTypeFloat*) out->data)[3]), 1);
    CUNIT_VERIFY_EQUAL_INT(isnan(((EagleDataTypeFloat*) out->data)[4]), 1);
    CUNIT_VERIFY_EQUAL_INT(isnan(((EagleDataTypeFloat*) out->data)[5]), 1);
    
    delete args[0];
    delete args[1];
    free(args);
    delete out;
}

CUNIT_TEST(FunctionSuite, EaglePageOperations_WidthBucketPageFloatPageFloatPageFloatPageFloat)
{
    int pageSize = 7;
    EaglePage **args = (EaglePage**) calloc(4, sizeof(EaglePage*));
    args[0] = EaglePage::AllocFloat(pageSize);
    args[1] = EaglePage::AllocFloat(pageSize);
    args[2] = EaglePage::AllocFloat(pageSize);
    args[3] = EaglePage::AllocInt(pageSize);
    
    // zero buckets
    ((EagleDataTypeFloat*) args[0]->data)[0] = 0;
    ((EagleDataTypeFloat*) args[1]->data)[0] = 0;
    ((EagleDataTypeFloat*) args[2]->data)[0] = 0;
    ((EagleDataTypeInteger*) args[3]->data)[0] = 0;
    
    // within range
    ((EagleDataTypeFloat*) args[0]->data)[1] = 5.7;
    ((EagleDataTypeFloat*) args[1]->data)[1] = 0;
    ((EagleDataTypeFloat*) args[2]->data)[1] = 10;
    ((EagleDataTypeInteger*) args[3]->data)[1] = 10;
    
    // below minimum
    ((EagleDataTypeFloat*) args[0]->data)[2] = -1;
    ((EagleDataTypeFloat*) args[1]->data)[2] = 0;
    ((EagleDataTypeFloat*) args[2]->data)[2] = 10;
    ((EagleDataTypeInteger*) args[3]->data)[2] = 10;
    
    // above maximum
    ((EagleDataTypeFloat*) args[0]->data)[3] = 20;
    ((EagleDataTypeFloat*) args[1]->data)[3] = 0;
    ((EagleDataTypeFloat*) args[2]->data)[3] = 10;
    ((EagleDataTypeInteger*) args[3]->data)[3] = 10;
    
    // zero range
    ((EagleDataTypeFloat*) args[0]->data)[4] = 5;
    ((EagleDataTypeFloat*) args[1]->data)[4] = 5;
    ((EagleDataTypeFloat*) args[2]->data)[4] = 5;
    ((EagleDataTypeInteger*) args[3]->data)[4] = 1;
    
    // reverse range
    ((EagleDataTypeFloat*) args[0]->data)[5] = 6.7;
    ((EagleDataTypeFloat*) args[1]->data)[5] = 10;
    ((EagleDataTypeFloat*) args[2]->data)[5] = 5;
    ((EagleDataTypeInteger*) args[3]->data)[5] = 5;
    
    // reverse out of range
    ((EagleDataTypeFloat*) args[0]->data)[6] = 2;
    ((EagleDataTypeFloat*) args[1]->data)[6] = 10;
    ((EagleDataTypeFloat*) args[2]->data)[6] = 5;
    ((EagleDataTypeInteger*) args[3]->data)[6] = 5;
    
    // process
    EaglePage *out = EaglePage::AllocInt(pageSize);
    EaglePageOperations_WidthBucketPageFloatPageFloatPageFloatPageInteger(out, NULL, NULL, args);
    
    // result
    CUNIT_VERIFY_EQUAL_INT(((EagleDataTypeInteger*) out->data)[0], 0);
    CUNIT_VERIFY_EQUAL_INT(((EagleDataTypeInteger*) out->data)[1], 6);
    CUNIT_VERIFY_EQUAL_INT(((EagleDataTypeInteger*) out->data)[2], 0);
    CUNIT_VERIFY_EQUAL_INT(((EagleDataTypeInteger*) out->data)[3], 0);
    CUNIT_VERIFY_EQUAL_INT(((EagleDataTypeInteger*) out->data)[4], 0);
    CUNIT_VERIFY_EQUAL_INT(((EagleDataTypeInteger*) out->data)[5], 4);
    CUNIT_VERIFY_EQUAL_INT(((EagleDataTypeInteger*) out->data)[6], 0);
    
    delete args[0];
    delete args[1];
    delete args[2];
    delete args[3];
    free(args);
    delete out;
}

CUNIT_TEST(FunctionSuite, EaglePageOperations_BitLengthPageVarchar)
{
    int pageSize = 5;
    EaglePage **args = (EaglePage**) calloc(1, sizeof(EaglePage*));
    args[0] = EaglePage::AllocVarchar(pageSize);
    
    // test data
    ((EagleDataTypeVarchar*) args[0]->data)[0] = strdup("hello");
    ((EagleDataTypeVarchar*) args[0]->data)[1] = strdup("  hello");
    ((EagleDataTypeVarchar*) args[0]->data)[2] = strdup("hello ");
    ((EagleDataTypeVarchar*) args[0]->data)[3] = strdup(" hello  ");
    ((EagleDataTypeVarchar*) args[0]->data)[4] = strdup("123");
    
    // process
    EaglePage *out = EaglePage::AllocInt(pageSize);
    EaglePageOperations_BitLengthPageVarchar(out, NULL, NULL, args);
    
    // result
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeInteger*) out->data)[0], 5 * 8);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeInteger*) out->data)[1], 7 * 8);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeInteger*) out->data)[2], 6 * 8);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeInteger*) out->data)[3], 8 * 8);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeInteger*) out->data)[4], 3 * 8);
    
    delete args[0];
    free(args);
    delete out;
}

CUNIT_TEST(FunctionSuite, EaglePageOperations_CharLengthPageVarchar)
{
    int pageSize = 5;
    EaglePage **args = (EaglePage**) calloc(1, sizeof(EaglePage*));
    args[0] = EaglePage::AllocVarchar(pageSize);
    
    // test data
    ((EagleDataTypeVarchar*) args[0]->data)[0] = strdup("hello");
    ((EagleDataTypeVarchar*) args[0]->data)[1] = strdup("  hello");
    ((EagleDataTypeVarchar*) args[0]->data)[2] = strdup("hello ");
    ((EagleDataTypeVarchar*) args[0]->data)[3] = strdup(" hello  ");
    ((EagleDataTypeVarchar*) args[0]->data)[4] = strdup("123");
    
    // process
    EaglePage *out = EaglePage::AllocInt(pageSize);
    EaglePageOperations_CharLengthPageVarchar(out, NULL, NULL, args);
    
    // result
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeInteger*) out->data)[0], 5);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeInteger*) out->data)[1], 7);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeInteger*) out->data)[2], 6);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeInteger*) out->data)[3], 8);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeInteger*) out->data)[4], 3);
    
    delete args[0];
    free(args);
    delete out;
}

CUNIT_TEST(FunctionSuite, EaglePageOperations_OctetLengthPageVarchar)
{
    int pageSize = 5;
    EaglePage **args = (EaglePage**) calloc(1, sizeof(EaglePage*));
    args[0] = EaglePage::AllocVarchar(pageSize);
    
    // test data
    ((EagleDataTypeVarchar*) args[0]->data)[0] = strdup("hello");
    ((EagleDataTypeVarchar*) args[0]->data)[1] = strdup("  hello");
    ((EagleDataTypeVarchar*) args[0]->data)[2] = strdup("hello ");
    ((EagleDataTypeVarchar*) args[0]->data)[3] = strdup(" hello  ");
    ((EagleDataTypeVarchar*) args[0]->data)[4] = strdup("123");
    
    // process
    EaglePage *out = EaglePage::AllocInt(pageSize);
    EaglePageOperations_OctetLengthPageVarchar(out, NULL, NULL, args);
    
    // result
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeInteger*) out->data)[0], 5);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeInteger*) out->data)[1], 7);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeInteger*) out->data)[2], 6);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeInteger*) out->data)[3], 8);
    CUNIT_VERIFY_EQUAL_DOUBLE(((EagleDataTypeInteger*) out->data)[4], 3);
    
    delete args[0];
    free(args);
    delete out;
}

CUnitTests* FunctionSuite_tests()
{
    CUnitTests *tests = CUnitTests_New(100);
    
    // method tests
    CUnitTests_addTest(tests, CUNIT_NEW_NAME(FunctionSuite, EaglePageOperations_OctetLengthPageVarchar, "OCTET_LENGTH(varchar)"));
    CUnitTests_addTest(tests, CUNIT_NEW_NAME(FunctionSuite, EaglePageOperations_CharLengthPageVarchar, "CHAR_LENGTH(varchar)"));
    CUnitTests_addTest(tests, CUNIT_NEW_NAME(FunctionSuite, EaglePageOperations_BitLengthPageVarchar, "BIT_LENGTH(varchar)"));
    CUnitTests_addTest(tests, CUNIT_NEW_NAME(FunctionSuite, EaglePageOperations_AbsPageFloat, "ABS(float)"));
    CUnitTests_addTest(tests, CUNIT_NEW_NAME(FunctionSuite, EaglePageOperations_Atan2PageFloatPageFloat, "ATAN2(float, float)"));
    CUnitTests_addTest(tests, CUNIT_NEW_NAME(FunctionSuite, EaglePageOperations_CastPageIntFloat, "CAST(page(int) TO float)"));
    CUnitTests_addTest(tests, CUNIT_NEW_NAME(FunctionSuite, EaglePageOperations_CeilPageFloat, "CEIL(float)"));
    CUnitTests_addTest(tests, CUNIT_NEW_NAME(FunctionSuite, EaglePageOperations_ExpPageFloat, "EXP(float)"));
    CUnitTests_addTest(tests, CUNIT_NEW_NAME(FunctionSuite, EaglePageOperations_FloorPageFloat, "FLOOR(float)"));
    CUnitTests_addTest(tests, CUNIT_NEW_NAME(FunctionSuite, EaglePageOperations_LnPageFloat, "LN(page(float))"));
    CUnitTests_addTest(tests, CUNIT_NEW_NAME(FunctionSuite, EaglePageOperations_LowerPageVarchar, "LOWER(varchar)"));
    CUnitTests_addTest(tests, CUNIT_NEW_NAME(FunctionSuite, EaglePageOperations_ModPageFloatPageFloat, "MOD(float, float)"));
    CUnitTests_addTest(tests, CUNIT_NEW_NAME(FunctionSuite, EaglePageOperations_PositionPageVarcharPageVarchar, "POSITION(varchar IN varchar)"));
    CUnitTests_addTest(tests, CUNIT_NEW_NAME(FunctionSuite, EaglePageOperations_PowerPageFloatPageFloat, "POWER(float, float)"));
    CUnitTests_addTest(tests, CUNIT_NEW_NAME(FunctionSuite, EaglePageOperations_SqrtPageFloat, "SQRT(float)"));
    CUnitTests_addTest(tests, CUNIT_NEW_NAME(FunctionSuite, EaglePageOperations_UpperPageVarchar, "UPPER(varchar)"));
    CUnitTests_addTest(tests, CUNIT_NEW_NAME(FunctionSuite, EaglePageOperations_WidthBucketPageFloatPageFloatPageFloatPageFloat, "WIDTH_BUCKET(float, float, float, int)"));
    
    return tests;
}

/**
 * The suite init function.
 */
int FunctionSuite_init()
{
    return 0;
}

/**
 * The suite cleanup function.
 */
int FunctionSuite_clean()
{
    return 0;
}
