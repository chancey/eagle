#include "SQLSuite.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "eagle/db/Table.h"
#include "eagle/db/Tuple.h"
#include "eagle/db/TableData.h"
#include "eagle/Instance.h"
#include "eagle/EaglePage.h"
#include "eagle/PageProvider.h"
#include "eagledb/EagleDbSqlSelect.h"
#include "eagledb/EagleDbInstance.h"
#include "eagledb/EagleDbParser.h"
#include "eagle/plan/stage/SelectStage.h"
#include "eagle/Maths.h"

EagleDbParser* _testSqlSelect(const char *sql);

eagle::db::TableData **tables;
int allocatedTables = 0;
int usedTables = 0;

typedef struct {
    CUnitTest *test;
    char *sql;
    char *errorMessage;
    eagle::db::Tuple **answers;
    int allocatedAnswers;
    int usedAnswers;
} SQLTest;

SQLTest *sqlTests;
int allocatedTests;
int usedTests;
int currentTest;

/**
 * The suite init function.
 */
int SQLSuite_init()
{
    return 0;
}

/**
 * The suite cleanup function.
 */
int SQLSuite_clean()
{
    for(int i = 0; i < usedTables; ++i) {
        eagle_db_Table_DeleteWithColumns(tables[i]->table);
    }
    
    return 0;
}

void die(char *msg)
{
    perror(msg);
    exit(1);
}

int countColumns(char *line, char ch)
{
    int columnCount = 1;
    for(int i = 0; i < strlen(line); ++i) {
        if(line[i] == ch) {
            ++columnCount;
        }
    }
    return columnCount;
}

char **split(char *line, int *count, char *chs)
{
    *count = countColumns(line, chs[0]);
    
    char **r = (char**) calloc((size_t) *count, sizeof(char*));
    r[0] = strdup(strtok(line, chs));
    for(int i = 1; i < *count; ++i) {
        r[i] = strdup(strtok(NULL, chs));
    }
    
    return r;
}

char* SQLSuite_describeNumber(double a)
{
    if(isnan(a)) {
        return "nan";
    }
    else if(isinf(a) > 0) {
        return "+inf";
    }
    else if(isinf(a) < 0) {
        return "-inf";
    }
    return "unknown";
}

void SQLSuiteTest()
{
    int pageSize = 5;
    int cores = 1;
    
    SQLTest test = sqlTests[currentTest++];
    
    EagleDbInstance *db = EagleDbInstance_New(pageSize, cores);
    EagleDbSchema *defaultSchema = EagleDbInstance_getSchema(db, EagleDbSchema::DefaultSchemaName);
    for(int i = 0; i < usedTables; ++i) {
        defaultSchema->addTable(tables[i]);
    }
    
    EagleLogger::Event *error = NULL;
    eagle::Plan *plan = NULL;
    bool executeSucceeded = EagleDbInstance_execute(db, test.sql, &error, &plan);
    
    // check for parser errors
    if(!executeSucceeded || NULL != error) {
        // expected error
        if(NULL != test.errorMessage) {
            if(error->getMessage() != test.errorMessage) {
                delete plan;
                EagleDbInstance_Delete(db);
                return;
            }
            CUNIT_ASSERT_EQUAL_STRING(error->getMessage().c_str(), test.errorMessage);
        }
        // unexpected error
        else {
            CUNIT_FAIL("%s", error->getMessage().c_str());
        }
        return;
    }
    
    //printf("%s\n", plan->toString());
    //exit(0);
    
    // a big naughty, but this is only for testing
    eagle::plan::stage::AbstractStage *finalStage = plan->stages->last();
    
    // validate column names
    for(int j = 0; j < test.answers[0]->table->countColumns(); ++j) {
        CUNIT_ASSERT_EQUAL_STRING(test.answers[0]->table->getColumn(j).name.c_str(), finalStage->result[j]->name.c_str());
        CUNIT_ASSERT_EQUAL_INT(test.answers[0]->table->getColumn(j).type, finalStage->result[j]->type);
    }
    
    // validate results
    int valid = 1;
    for(int j = 0; j < test.answers[0]->table->countColumns(); ++j) {
        EaglePage *page = finalStage->result[j]->nextPage();
        
        CUNIT_ASSERT_NOT_NULL(page);
        if(NULL != page) {
            CUNIT_ASSERT_EQUAL_INT(page->count, test.usedAnswers);
            CUNIT_ASSERT_EQUAL_INT(page->type, finalStage->result[j]->type);
            
            for(int i = 0; i < test.usedAnswers; ++i) {
                switch(page->type) {
                        
                    case EagleData::Type::Unknown:
                        CUNIT_FAIL("%s", "UNKNOWN type");
                        valid = 0;
                        break;
                        
                    case EagleData::Type::Integer:
                        if(*((EagleDataTypeInteger*) test.answers[i]->data[j]) != ((EagleDataTypeInteger*) page->data)[i]) {
                            CUNIT_FAIL("%d != %d for result %d\n", *((EagleDataTypeInteger*) test.answers[i]->data[j]), ((EagleDataTypeInteger*) page->data)[i], i);
                            valid = 0;
                        }
                        break;
                        
                    case EagleData::Type::Varchar:
                        if(strcmp(((EagleDataTypeVarchar*) test.answers[i]->data)[j], ((EagleDataTypeVarchar*) page->data)[i])) {
                            CUNIT_FAIL("'%s' != '%s'\n", ((EagleDataTypeVarchar*) test.answers[i]->data)[j], ((EagleDataTypeVarchar*) page->data)[i]);
                            valid = 0;
                        }
                        break;
                        
                    case EagleData::Type::Float:
                    {
                        EagleDataTypeFloat a = *((EagleDataTypeFloat*) test.answers[i]->data[j]);
                        EagleDataTypeFloat b = ((EagleDataTypeFloat*) page->data)[i];
                        
                        // numeric comparison
                        if(isfinite(a) && isfinite(b)) {
                            EagleDataTypeFloat epsilon = fabs(eagle::Maths::Min(a, b)) / 1000.0;
                            if(fabs(a - b) > epsilon) {
                                CUNIT_FAIL("%g != %g (difference = %g)\n", a, b, fabs(a - b));
                                valid = 0;
                            }
                        }
                        else {
                            char *left = SQLSuite_describeNumber(a);
                            char *right = SQLSuite_describeNumber(b);
                            if(strcmp(left, right)) {
                                CUNIT_VERIFY_EQUAL_STRING(left, right);
                            }
                        }
                        
                        break;
                    }
                        
                }
                
                if(valid == 0) {
                    break;
                }
            }
        }
        
        delete page;
    }
    CUNIT_ASSERT_EQUAL_INT(valid, 1);
    
    // clean up
    delete plan;
    EagleDbInstance_Delete(db);
}

void controlTest(FILE *file, int *lineNumber)
{
    SQLTest test;
    test.sql = (char*) malloc(1024);
    test.errorMessage = NULL;
    test.test = CUnitTest_New(test.sql, SQLSuiteTest);
    test.usedAnswers = 0;
    test.allocatedAnswers = 5;
    test.answers = (eagle::db::Tuple**) calloc(test.allocatedAnswers, sizeof(eagle::db::Tuple*));
    
    // get the SQL
    fgets(test.sql, 1024, file);
    test.sql[strlen(test.sql) - 1] = 0;
    
    // get the definition for the answers
    char line[1024];
    eagle::db::Table *table = NULL;
    fgets(line, sizeof(line), file);
    
    int columns = 0;
    char **data = split(strtok(line, "\n"), &columns, "|\n");
    
    table = new eagle::db::Table("result");
    for(int j = 0; j < columns; ++j) {
        int count;
        char **parts = split(data[j], &count, ":");
        table->addColumn(new eagle::db::Column(parts[0], EagleData::NameToType(parts[1])));
        delete data[j];
        
        for(int k = 0; k < count; ++k) {
            delete parts[k];
        }
        delete parts;
    }
    delete data;
    
    // get the answers
    while(fgets(line, sizeof(line), file) != NULL) {
        if(!strcmp("\n", line)) {
            break;
        }
        
        int columns = 0;
        char **data = split(strtok(line, "\n"), &columns, "|\n");
        
        test.answers[test.usedAnswers] = new eagle::db::Tuple(table);
        for(int j = 0; j < columns; ++j) {
            switch(table->getColumn(j).type) {
                    
                case EagleData::Type::Unknown:
                    fprintf(stderr, "Unknown type: %s", table->getColumn(j).name.c_str());
                    exit(1);
                    break;
                    
                case EagleData::Type::Integer:
                    test.answers[test.usedAnswers]->setInteger(j, atoi(data[j]));
                    break;
                    
                case EagleData::Type::Varchar:
                    test.answers[test.usedAnswers]->setVarchar(j, data[j]);
                    break;
                    
                case EagleData::Type::Float:
                    if(0 == strcmp("nan", data[j])) {
                        test.answers[test.usedAnswers]->setFloat(j, NAN);
                    }
                    else if(0 == strcmp("inf", data[j])) {
                        test.answers[test.usedAnswers]->setFloat(j, INFINITY);
                    }
                    else if(0 == strcmp("+inf", data[j])) {
                        test.answers[test.usedAnswers]->setFloat(j, INFINITY);
                    }
                    else if(0 == strcmp("-inf", data[j])) {
                        test.answers[test.usedAnswers]->setFloat(j, -INFINITY);
                    }
                    else {
                        test.answers[test.usedAnswers]->setFloat(j, atof(data[j]));
                    }
                    break;
                    
            }
            delete data[j];
        }
        delete data;
        ++test.usedAnswers;
    }
    
    sqlTests[usedTests++] = test;
}

void controlTestError(FILE *file, int *lineNumber)
{
    SQLTest test;
    test.sql = (char*) malloc(1024);
    test.errorMessage = (char*) malloc(1024);
    test.test = CUnitTest_New(test.sql, SQLSuiteTest);
    test.usedAnswers = 0;
    test.allocatedAnswers = 0;
    test.answers = NULL;
    
    // get the SQL
    fgets(test.sql, 1024, file);
    test.sql[strlen(test.sql) - 1] = 0;
    
    // get the expected error message
    fgets(test.errorMessage, 1024, file);
    test.errorMessage[strlen(test.errorMessage) - 1] = 0;
    
    sqlTests[usedTests++] = test;
}

void controlTable(FILE *file, char *firstLine, int *lineNumber)
{
    // get the table name
    char line[1024];
    char *tableName = (char*) malloc(64);
    sscanf(firstLine, "%% table %s", tableName);
    
    // read the column names
    fgets(line, sizeof(line), file);
    ++*lineNumber;
    int columnCount;
    char **columnNames = split(line, &columnCount, "|\n");
    
    // construct the virtual table
    eagle::db::Table *table = new eagle::db::Table(tableName);
    for(int i = 0; i < columnCount; ++i) {
        int partsCount;
        char **parts = split(columnNames[i], &partsCount, " ");
        if(partsCount != 2) {
            printf("Wrong number of partsCount for '%s' (got %d)\n", columnNames[i], partsCount);
            exit(1);
        }
        
        eagle::db::Column *column = new eagle::db::Column(parts[0], EagleData::NameToType(parts[1]));
        table->addColumn(column);
        
        for(int j = 0; j < partsCount; ++j) {
            delete parts[j];
        }
        delete parts;
    }
    eagle::db::TableData *td = new eagle::db::TableData(table, 1000);
    tables[usedTables] = td;
    ++usedTables;
    
    // read the data
    while(fgets(line, sizeof(line), file) != NULL) {
        ++*lineNumber;
        
        if(!strcmp("\n", line)) {
            break;
        }
        
        // check we have the right amount of columns
        if(countColumns(line, '|') != columnCount) {
            char msg[1024];
            sprintf(msg, "Expected %d columns on line %d, but found %d columns.", columnCount, *lineNumber, countColumns(line, '|'));
            die(msg);
        }
        
        // create the tuple
        int count;
        char **data = split(line, &count, "|\n");
        eagle::db::Tuple *tuple = new eagle::db::Tuple(table);
        for(int i = 0; i < count; ++i) {
            switch(table->getColumn(i).type) {
                    
                case EagleData::Type::Unknown:
                    fprintf(stderr, "Unknown type: %s\n", table->getColumn(i).name.c_str());
                    exit(1);
                    
                case EagleData::Type::Integer:
                    tuple->setInteger(i, atoi(data[i]));
                    break;
                    
                case EagleData::Type::Varchar:
                    tuple->setVarchar(i, data[i]);
                    break;
                    
                case EagleData::Type::Float:
                    tuple->setFloat(i, atof(data[i]));
                    break;
                    
            }
        }
        
        // insert the data
        td->insert(tuple);
        delete tuple;
        
        // clean
        for(int i = 0; i < count; ++i) {
            delete data[i];
        }
        delete data;
    }
    
    // clean
    for(int i = 0; i < columnCount; ++i) {
        delete columnNames[i];
    }
    delete columnNames;
    delete tableName;
}

CUnitTests* SQLSuite_tests()
{
    // init
    allocatedTables = 10;
    usedTables = 0;
    tables = (eagle::db::TableData**) calloc(allocatedTables, sizeof(eagle::db::TableData*));
    
    allocatedTests = 100;
    usedTests = 0;
    sqlTests = (SQLTest*) calloc(allocatedTests, sizeof(SQLTest));
    
    CUnitTests *tests = CUnitTests_New(allocatedTests);
    
    // open test file
    const char *fileName = "SQLSuite.txt";
    FILE *file = fopen(fileName, "r");
    if(NULL == file) {
        char cwd[1024], msg[1024];
        getcwd(cwd, sizeof(cwd));
        sprintf(msg, "Could not open %s/%s", cwd, fileName);
        die(msg);
    }
    
    // read test file
    char line[1024];
    int lineNumber = 1;
    while(fgets(line, sizeof(line), file) != NULL) {
        /* skip blank lines */
        if(strlen(line) == 0) {
            continue;
        }
        
        /* control line */
        if(line[0] == '%') {
            if(!strncasecmp(line, "% table", strlen("% table"))) {
                controlTable(file, line, &lineNumber);
            }
            else if(!strncasecmp(line, "% test_error", strlen("% test_error"))) {
                controlTestError(file, &lineNumber);
            }
            else if(!strncasecmp(line, "% test", strlen("% test"))) {
                controlTest(file, &lineNumber);
            }
            else {
                char msg[1024];
                sprintf(msg, "Can not understand control line:\n%sError", line);
                die(msg);
            }
        }
    }
    
    for(int i = 0; i < usedTests; ++i) {
        CUnitTests_addTest(tests, sqlTests[i].test);
    }
    
    // clean
    fclose(file);
    
    return tests;
}
