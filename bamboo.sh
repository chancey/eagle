#!/bin/bash

export OS=bamboo.`uname | tr '[A-Z]' '[a-z]'`
make clean bamboo-$1
make bamboo-$1-post
